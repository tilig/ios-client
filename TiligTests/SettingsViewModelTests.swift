//
//  SettingsViewModelTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 14/03/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Firebase
import Combine

@testable import Tilig

class SettingsViewModelTests: TiligTestCase {
    var viewModel: SettingsViewModel!
    var userProfile: UserProfile!
    var authManager: AuthManager!

    override func setUpWithError() throws {
        userProfile = UserProfile.random()
        authManager = AuthManager()
        viewModel = SettingsViewModel(
            authManager: authManager,
            userProfile: userProfile
        )
    }

//    func testSignoutPublishesTrigger() throws {
//        waitForFirstValue(from: viewModel.signOutTriggered, matching: true) {
//            self.viewModel.signOut()
//        }
//    }

//    func testDeleteUserTriggered() throws {
//        waitForFirstValue(from: viewModel.deleteUserTriggered, matching: true) {
//            self.viewModel.deleteProfile()
//        }
//    }
}
