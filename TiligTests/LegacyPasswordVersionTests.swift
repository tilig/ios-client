//
//  PasswordVersion.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 22/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest

// swiftlint:disable line_length
class LegacyPasswordVersionTests: TiligTestCase {

    var passwordVersion: LegacyPasswordVersion!

    override func setUpWithError() throws {
        passwordVersion = LegacyPasswordVersion(
            id: "4f062feb-403f-457a-be6a-ac598680d8bf",
            password: "VEQsXb1wh6RwoW/m1TmX2DosbTDm9Ems30o/6vJ1EKx8myGVVDioMPYtyLg5fGDn1PnB29xnh7Iua6QK32aae06BXWb7ZOrAEtiKTpExZKav7BzvYX2FkkX+cuVkvt8MRnDAgVM4DlA0J1emGyfWePbBK8il9wUnEs4S/+niEmziBTghsr8QDLIxu51qnJDRStLwzoHt4EAxtz8QorQfNkTLgy26rZvMj8YhZsMvgcSNgnTzWsRImghg0R0n6zC7YAM01rhS8Oex/DElHoQ68eU2XywvutI+mSiwWQse4uJaF/M2MiMw7oh8QlK+slaqXG2LEnLJjC9zQob4AMH/6A==",
            createdAt: "2022-07-31T23:28:37.423Z"
        )
    }

    func testGetFormattedDate() {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        let date = formatter.date(from: passwordVersion.getFormattedDate())

        let newFormatter = DateFormatter()
        newFormatter.dateStyle = .medium
        newFormatter.timeStyle = .short
        newFormatter.timeZone = TimeZone(identifier: "UTC")
        let newDate = newFormatter.date(from: "Jul 31, 2022 at 11:28 PM")

        XCTAssertNotNil(date)
        // TODO: this doesn't work when running test in a different timezone
        // XCTAssertEqual(date, newDate)
    }
}
