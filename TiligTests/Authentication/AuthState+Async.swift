//
//  AuthState+Async.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 27/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

 import XCTest

 class AuthStateAsync: TiligTestCase {
    func testGetAccessToken() async throws {
        // If
        let authState = AuthState.random()
        try AuthState.cache(authState: authState)

        // When
        let accessToken = try await AuthState.cached?.getAccessToken()

        // Then
        XCTAssertEqual(accessToken, authState.tiligAuthTokens.accessToken)
    }
 }
