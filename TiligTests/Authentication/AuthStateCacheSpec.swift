//
//  AuthStateCacheSpec.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 14/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation

import XCTest
import Nimble
import Quick
import Valet

@testable import Tilig

class AuthStateCacheSpec: QuickSpec {
    override func spec() {
        describe("when nothing is cached in Valet") {
            beforeEach {
                expect {
                    try Valet.auth.removeObject(forKey: ValetKeys.authState.rawValue)
                }.toNot(throwError())
            }

            it("cached returns nil") {
                expect(AuthState.cached).to(beNil())
            }

            it("is able to cache a new authState") {
                var authState = AuthState.random()
                expect {
                    try AuthState.cache(authState: authState)
                }.toNot(throwError())
                expect(AuthState.cached).to(equal(authState))
            }
        }

        describe("when an authState is cached") {
            let authState = AuthState.random()

            beforeEach {
                do {
                    try AuthState.cache(authState: authState)
                } catch {
                    XCTFail("Caching state failed")
                }
            }

            it("returns the cached state") {
                expect(AuthState.cached).to(equal(authState))
            }

            it("is can overwrite with a new authState") {
                let authState2 = AuthState.random()
                expect(authState).toNot(equal(authState2))
                expect(AuthState.cached).to(equal(authState))
                expect {
                    try AuthState.cache(authState: authState2)
                }.toNot(throwError())
                expect(AuthState.cached).to(equal(authState2))
            }
        }
    }
}
