//
//  LegacyKeyPairGenerationTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 17/05/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import XCTest
import Nimble
import Quick
import Foundation

@testable import Tilig

class LegacyKeyPairGenerationSpec: QuickSpec {
    override func spec() {
        describe("generateRSAKeyPair()") {
            var keyPair: LegacyKeyPair!

            beforeEach {
                do {
                    keyPair = try LegacyKeyPair.generateRSAKeyPair()
                } catch {
                    return XCTFail("KeyPair generation raised an unexpected error: \(error)")
                }
            }

            it("makes the public key and private key not identical") {
                expect(keyPair.privateKey.rawValue).toNot(equal(keyPair.publicKey.rawValue))
            }

            // not relevant anymore because I removed the data() method on PrivateKey
            describe("data()") {
                it("can create a key from the data") {
                    guard let data = try? keyPair.privateKey.secKey().data() else {
                        return XCTFail("data() did not return any data")
                    }
                    var privateKeyCopyError: Unmanaged<CFError>?
                    let copyPrivateKeyAttrs: [String: Any] = [
                        kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
                        kSecAttrKeyClass as String: kSecAttrKeyClassPrivate,
                        kSecAttrKeySizeInBits as String: 2048
                    ]
                    guard let privateKeyCopy = SecKeyCreateWithData(
                        data as CFData,
                        copyPrivateKeyAttrs as CFDictionary,
                        &privateKeyCopyError
                    ) else {
                        throw privateKeyCopyError!.takeRetainedValue() as Error
                    }
                    let x509: String
                    do {
                       x509 = try privateKeyCopy.x509Format()
                    } catch {
                        return XCTFail("x509 fails: \(error.localizedDescription)")
                    }
                    expect(x509).to(equal(keyPair.privateKey.rawValue))
                }
            }

            describe("secKey()") {
                it("should return the encoded string (rawValue) as a SecKey") {
                    guard let secKey = try? keyPair.privateKey.secKey() else {
                        return XCTFail("secKey should not be nil")
                    }
                    let x509Out = try? secKey.x509Format()
                    expect(x509Out).to(equal(keyPair.privateKey.rawValue))
                }

                it("outputs the key in X.509 format") {
                    guard let secKey = try? keyPair.privateKey.secKey() else {
                        return XCTFail("secKey should not be nil")
                    }
                    let x509Out = try? secKey.x509Format()
                    let expectedBase64Body = try secKey.data().base64EncodedString(options: .lineLength64Characters)
                    expect(x509Out).to(equal("""
-----BEGIN RSA PRIVATE KEY-----
\(expectedBase64Body)
-----END RSA PRIVATE KEY-----
"""))
                }
            }

            it("can encrypt a password with the publicKey key") {
                guard let publicKey = try? keyPair.publicKey.secKey() else {
                    return XCTFail("Could not convert the public key to SecKey format")
                }
                let encrypted = self.encrypt(string: "this-is-an-example", publicKey: publicKey)
                expect(encrypted).toNot(beNil())
            }

            it("can decrypt an encrypted password with the public key") {
                guard let publicKey = try? keyPair.publicKey.secKey() else {
                    return XCTFail("Could not convert the public key to SecKey format")
                }
                guard let privateKey = try? keyPair.privateKey.secKey() else {
                    return XCTFail("Could not convert the private key to SecKey format")
                }

                let plainText = "this-is-an-example"
                let cipherText = self.encrypt(string: plainText, publicKey: publicKey)!

                guard let decrypted = try self.decrypt(string: cipherText, privateKey: privateKey) else {
                    return XCTFail("Could not decrypt cipherText using private key")
                }
                expect(decrypted).to(equal(plainText))
            }
        }
    }

    func encrypt(string plainText: String, publicKey: SecKey) -> String? {
        let buffer = [UInt8](plainText.utf8)
        var keySize = SecKeyGetBlockSize(publicKey)
        var keyBuffer = [UInt8](repeating: 0, count: keySize)

        let status = SecKeyEncrypt(
            publicKey, // public key
            SecPadding.PKCS1, // padding
            buffer, // plain text
            buffer.count, // plain text length
            &keyBuffer, // On return, the encrypted text (cipherText)
            &keySize // cipherText length
        )
        guard status == errSecSuccess else {
            let errorDescription = SecCopyErrorMessageString(status, nil)
            print("❌ An error occured encrypting text: \(String(describing: errorDescription)) (status \(status))")
            return nil
        }
        let data = Data(bytes: keyBuffer, count: keySize)
        return data.base64EncodedString()
    }

    func decrypt(string base64Encoded: String, privateKey: SecKey) throws -> String? {
        let decodedData = Data(base64Encoded: base64Encoded)!
        // validation check
        guard decodedData.count == SecKeyGetBlockSize(privateKey) else {
            XCTFail("decryption failed: block size doesn't match")
            return nil
        }

        let cipherTextBytes = [UInt8](decodedData)
        let cipherTextSize = cipherTextBytes.count

        // TODO: Use private or public key block size here?
        // var publicKeyBlockSize = SecKeyGetBlockSize(publicKey)
        // var plainTextBuffer = [UInt8](repeating: 0, count: publicKeyBlockSize)
        // var plainTextSize: Int = publicKeyBlockSize
        let privateKeyBlockSize = SecKeyGetBlockSize(privateKey)
        var plainTextBuffer = [UInt8](repeating: 0, count: privateKeyBlockSize)
        var plainTextSize: Int = privateKeyBlockSize

        let decryptStatus = SecKeyDecrypt(
            // private key with which to decrypt the data.
            privateKey,
            // The type of padding used.
            SecPadding.PKCS1,
            // The data to decrypt.
            cipherTextBytes,
            // Length in bytes of the data in the cipherText buffer. This must be less than or equal to
            // the value returned by the SecKeyGetBlockSize(_:) function.
            cipherTextSize,
            // On return, the decrypted text.
            &plainTextBuffer,
            // On entry, the size of the buffer provided in the plainText parameter.
            // On return, the amount of data actually placed in the buffer.
            &plainTextSize
        )

        guard decryptStatus == errSecSuccess else {
            let cfErrorDescription = SecCopyErrorMessageString(decryptStatus, nil)
            let errorString = "\(String(describing: cfErrorDescription)) (status \(decryptStatus))"
            XCTFail("❌ An Error occured encrypting text: \(errorString)")
            return nil
        }

        let outputData = Data(bytes: plainTextBuffer, count: plainTextSize)

        return String(data: outputData, encoding: .utf8)
    }

    /*
    describe("KeySec.createFromData()") {
        let tagPublic = "com.tilig.encryption.public"
        let tagPrivate = "com.tilig.encryption.private"
        let storeInKeyChain = false
        
        let publicKeyAttributes: [String: Any] = [
            kSecAttrIsPermanent as String: storeInKeyChain,
            kSecAttrApplicationTag as String: tagPublic,
        ]
        
        let privateKeyAttributes: [String: Any] = [
            kSecAttrIsPermanent as String: storeInKeyChain,
            kSecAttrApplicationTag as String: tagPrivate,
        ]
        
        let attributes: [String: Any] = [
            kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
            kSecAttrKeySizeInBits as String: 2048,
            kSecPublicKeyAttrs as String: publicKeyAttributes,
            kSecPrivateKeyAttrs as String: privateKeyAttributes
        ]
        
        it("creates a private key from data") {
            // Generate private key
            var error: Unmanaged<CFError>?
            guard let privateKey = SecKeyCreateRandomKey(attributes as CFDictionary, &error) else {
                let error = error!.takeRetainedValue() as Error
                print("Error occurred while creating private key: \(error)")
                throw error
            }
            // Convert to data
            var privateKeyExportError: Unmanaged<CFError>?
            guard let cfData = SecKeyCopyExternalRepresentation(privateKey, &privateKeyExportError) else {
                throw privateKeyExportError!.takeRetainedValue() as Error
            }
            // Create from data
            let privateKeyCopy = try! SecKey.create(fromData: cfData as Data, type: .privateKey)
            expect(privateKeyCopy).to(equal(privateKey))
        }
        
        it("creates a public key from data") {
            // First generate th private key
            var error: Unmanaged<CFError>?
            guard let privateKey = SecKeyCreateRandomKey(attributes as CFDictionary, &error) else {
                let error = error!.takeRetainedValue() as Error
                print("Error occurred while creating private key: \(error)")
                throw error
            }
            // Then generate the public key
            guard let publicKey = SecKeyCopyPublicKey(privateKey) else {
                return XCTFail("Error occured: could not create public key")
            }
            // Convert to data
            var publicKeyExportError: Unmanaged<CFError>?
            guard let cfData = SecKeyCopyExternalRepresentation(publicKey, &publicKeyExportError) else {
                throw publicKeyExportError!.takeRetainedValue() as Error
            }
            // Create from data
            let publicKeyCopy = try! SecKey.create(fromData: cfData as Data, type: .publicKey)
            expect(publicKeyCopy).to(equal(publicKey))
        }
    }
    */
}
