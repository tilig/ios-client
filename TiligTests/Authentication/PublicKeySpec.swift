//
//  PublicKeySpec.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 12/05/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import XCTest
import Nimble
import Quick
import Mocker

import Combine
import Firebase

@testable import Tilig

class PublicKeySpec: QuickSpec {
    override func spec() {
        beforeEach {
            // Mocker setup
            let configuration = URLSessionConfiguration.default
            configuration.protocolClasses = [MockingURLProtocol.self]
            URLSession.current = URLSession(configuration: configuration)
        }

        describe("public key publisher") {
            describe("when the server returns a valid response") {
                beforeEach {
                    expect {
                        Mock(url: Endpoint.refresh.url,
                             dataType: .json,
                             statusCode: 200,
                             data: [.post: try Data(contentsOf: MockedRequestData.refresh)]
                        ).register()

                        // TODO: mock authorized response, or use token that is not expired...
                        Mock(url: Endpoint.profile.url,
                             dataType: .json,
                             statusCode: 200,
                             data: [.get: try Data(contentsOf: MockedRequestData.profileSuccess)]
                        ).register()
                    }.toNot(throwError())
                }

                it("publishes the public key") {
                    var publicKey: LegacyPublicKey?
                    let cancellable = LegacyPublicKey
                        .publisher(forAccessToken: Token.random())
                        .sink { _ in
                            return
                        } receiveValue: { key in
                            publicKey = key
                        }
                    expect(publicKey).toEventually(equal("this-is-the-publick-key"))
                    cancellable.cancel()
                }
            }

            describe("when the server returns 401") {
                beforeEach {
                    expect {
                        Mock(url: Endpoint.profile.url,
                             dataType: .json,
                             statusCode: 401,
                             data: [.get: try Data(contentsOf: MockedRequestData.empty)]
                        ).register()
                    }.toNot(throwError())
                }

                it("does not publish the public key") {
                    var publicKey: LegacyPublicKey?
                    let cancellable = LegacyPublicKey
                        .publisher(forAccessToken: Token(rawValue: "fake"))
                        .sink { _ in
                            //
                        } receiveValue: { key in
                            publicKey = key
                        }
                    expect(publicKey).toEventually(beNil())
                    cancellable.cancel()
                }
            }
        }
    }
}
