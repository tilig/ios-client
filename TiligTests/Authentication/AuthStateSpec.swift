//
//  AuthStateSpec.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 04/05/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//
import XCTest
import Nimble
import Quick
import Mocker
import Valet
import FirebaseAuth
import Sodium

import Combine
import Firebase

@testable import Tilig

/// Mock all steps in the authentication pipeline
func mockFullPipeline(keyPair: Box.KeyPair,
                      legacyPrivateKey: LegacyPrivateKey?,
                      legacyPublicKey: LegacyPublicKey,
                      userProfile: UserProfile) {
    // 1) Mock the Key Service JWT response
    KeyServiceTokenManager.mockedResponse = .success(
        ["token": "the-key-service-jwt-token"]
    )

    // 2) Mock the Tilig server's authenticate response
    Mock(url: Endpoint.authenticate.url,
         dataType: .json,
         statusCode: 200,
         // swiftlint:disable force_try
         data: [.post: try! Data(contentsOf: MockedRequestData.authenticateSuccess)]
    ).register()

    // 3) Mock the KeyPair publisher
    Box.KeyPair.mockedPublisher = Just(keyPair)
        .setFailureType(to: Error.self)
        .eraseToAnyPublisher()

    // 4) Mock the Firestore respone (legacy private key)
    LegacyPrivateKey.mockedPublisher = Just(legacyPrivateKey)
        .setFailureType(to: Error.self)
        .eraseToAnyPublisher()

    // 5) Mock the legacy public key publisher
    LegacyPublicKey.mockedPublisher = Just(legacyPublicKey)
        .setFailureType(to: Error.self)
        .eraseToAnyPublisher()

    // An alternative would be to mock 5 and 6 at once:
    // LegacyKeyPair.mockedKeyPair = LegacyKeyPair(privateKey: "priv", publicKey: "pub")

    // 6) Mock the UserProfile
    UserProfile.mockedPublisher = Just(userProfile)
        .setFailureType(to: Error.self)
        .eraseToAnyPublisher()
}

class AuthStateSpec: QuickSpec {
    // swiftlint:disable function_body_length
    override func spec() {
        beforeEach {
            // Start with a clean slate
            do {
                try AuthState.clearCache()
            } catch {
                XCTFail("Clearing the cache should not fail.")
            }
        }

        beforeEach {
            // Mocker setup
            let configuration = URLSessionConfiguration.default
            configuration.protocolClasses = [MockingURLProtocol.self]
            TiligServiceAuthentication.urlSession = URLSession(configuration: configuration)
        }

        afterEach {
            // Reset mocks
            KeyServiceTokenManager.mockedResponse = nil
            Box.KeyPair.mockedPublisher = nil
            LegacyKeyPair.mockedKeyPair = nil
            LegacyPublicKey.mockedPublisher = nil
            LegacyPrivateKey.mockedPublisher = nil
            TiligServiceAuthentication.mockedRefreshTokenPublisher = nil
            // Switch back to default URL session
            TiligServiceAuthentication.urlSession = URLSession.shared
        }

        describe("authStatePublisher") {
            let keyPair = Box.KeyPair.random()
            let firebaseUser = MockedUser(
                email: "testhenk@gmail.com",
                displayName: "Test Henk"
            )
            let firebaseUserProfile = FirebaseUserProfile(
                isNewUser: false,
                providerID: "google",
                username: "testhenk_2000",
                profile: ["given_name": NSString("Test"), "family_name": NSString("Henk")]
            )
            let userProfile = UserProfile.random()

            describe("when authentication endpoints return valid data") {
                beforeEach {
                    let privateKey: LegacyPrivateKey? = "mocked-private-key"
                    let publicKey: LegacyPublicKey = "mocked-public-key"
                    mockFullPipeline(
                        keyPair: keyPair,
                        legacyPrivateKey: privateKey,
                        legacyPublicKey: publicKey,
                        userProfile: userProfile
                    )
                }

                it("publishes the expected state") {
                    var authState: AuthState?
                    var cancellable: AnyCancellable?
                    var error: Error?

                    // 5 seconds because 3 requests including retries can
                    // take a long time
                    waitUntil(timeout: .seconds(5)) { done in
                        cancellable = AuthState
                            .authenticate(firebaseUser: firebaseUser,
                                          firebaseUserProfile: firebaseUserProfile)
                            .sink { completion in
                                if case .failure(let err) = completion {
                                    error = err
                                }
                                done()
                            } receiveValue: { state in
                                authState = state
                            }
                    }

                    expect(error).to(beNil())
                    expect(authState).toNot(beNil())
                    expect(authState?.firebaseUser.uid).to(equal("mocked-uid"))
                    expect(authState?.firebaseUser.email).to(equal("testhenk@gmail.com"))
                    expect(authState?.firebaseUser.displayName).to(equal("Test Henk"))
                    expect(authState?.legacyKeyPair.privateKey).to(equal("mocked-private-key"))
                    expect(authState?.legacyKeyPair.publicKey).to(equal("mocked-public-key"))
                    expect(authState?.tiligAuthTokens).toNot(beNil())
                    expect(authState?.userProfile).to(equal(userProfile))

                    cancellable!.cancel()
                }
            }

            describe("Cloud function JWT failure") {
                describe("when the JWT cloud function is unreachable") {
                    // This could be any error that Firebase generates
                    let wrappedError = URLError(.badServerResponse)
                    beforeEach {
                        // Mock the failure response in KeyServiceTokenManager
                        KeyServiceTokenManager.mockedResponse = .failure(.responseError(wrappedError))
                    }
                    it("returns an error that reflects that") {
                        var error: Error?
                        let cancellable = AuthState
                            .authenticate(firebaseUser: firebaseUser,
                                          firebaseUserProfile: firebaseUserProfile)
                            .sink { completion in
                                if case .failure(let err) = completion {
                                    error = err
                                }
                            } receiveValue: { _ in }

                        let authError = AuthError.keyServiceError(.responseError(wrappedError))
                        expect(error).toEventually(matchError(authError))
                        cancellable.cancel()
                    }
                }
            }

            describe("API authentication failure") {
                describe("when the Tilig Service is unreachable") {
                    beforeEach {
                        // Make the step before API authentication succeed: Mock the Key Service JWT response
                        KeyServiceTokenManager.mockedResponse = .success(
                            ["token": "the-key-service-jwt-token"]
                        )
                        // Make authentication fail
                        Mock(url: Endpoint.authenticate.url,
                             dataType: .json,
                             statusCode: 500,
                             data: [.post: Data()],
                             requestError: URLError(.networkConnectionLost)
                        ).register()
                    }

                    it("returns an error that reflects that") {
                        var error: Error?
                        var cancellable: AnyCancellable?

                        waitUntil(timeout: .seconds(5)) { done in
                            cancellable = AuthState
                                .authenticate(firebaseUser: firebaseUser,
                                              firebaseUserProfile: firebaseUserProfile)
                                .sink { completion in
                                    if case .failure(let err) = completion {
                                        error = err
                                    }
                                    done()
                                } receiveValue: { _ in }
                        }
                        let wrappedError: Error = URLError(.networkConnectionLost)
                        let authError = AuthError.tiligServiceConnectionFailed(wrappedError)
                        expect(error).to(matchError(authError))
                        cancellable?.cancel()
                    }
                }
            }
        }

        describe("caching") {
            let firebaseUser = MockedUser(
                email: "testhenk@gmail.com",
                displayName: "Test Henk"
            )
            let keyPair = Box.KeyPair.random()

            let privateKey: LegacyPrivateKey = "mocked-private-key"
            let publicKey: LegacyPublicKey = "mocked-public-key"

            // token values are hardcoded in authenticate.json
            let tokens = TiligAuthTokens(
                accessToken: Token(rawValue: "access-token-abcde"),
                refreshToken: Token(rawValue: "refresh-token-fghij")
            )
            let userProfile = UserProfile.random()
            let firebaseUserProfile = FirebaseUserProfile.random()

            let expectedAuthState = AuthState(
                firebaseUser: firebaseUser.toCodable(),
                tiligAuthTokens: tokens,
                keyPair: keyPair,
                legacyKeyPair: LegacyKeyPair(privateKey: privateKey, publicKey: publicKey),
                userProfile: userProfile
            )

            beforeEach {
                mockFullPipeline(
                    keyPair: keyPair,
                    legacyPrivateKey: privateKey,
                    legacyPublicKey: publicKey,
                    userProfile: userProfile
                )
            }

            it("caches after completing the pipeline") {
                expect(AuthState.cached).to(beNil())

                var cancellable: AnyCancellable?
                waitUntil(timeout: .seconds(5)) { done in
                    cancellable = AuthState
                        .authenticate(firebaseUser: firebaseUser,
                                      firebaseUserProfile: firebaseUserProfile)
                        .sink { _ in
                            done()
                        } receiveValue: { _ in
                        }
                }
                expect(AuthState.cached).to(equal(expectedAuthState))
                cancellable?.cancel()
            }

            it("stores the cached values in keychain with Valet") {
                let dataBefore = try? Valet.auth.object(forKey: ValetKeys.authState.rawValue)
                expect(dataBefore).to(beNil())
                do {
                    try AuthState.cache(authState: expectedAuthState)
                } catch {
                    XCTFail("Caching authState failed")
                }
                let dataAfter = try? Valet.auth.object(forKey: ValetKeys.authState.rawValue)
                expect(dataAfter).toNot(beNil())

                // also test the data integrity decoding
                do {
                    let decoder = JSONDecoder()
                    let authStateDecoded = try decoder.decode(AuthState.self, from: dataAfter!)
                    expect(authStateDecoded).to(equal(expectedAuthState))
                } catch {
                    XCTFail("Caching authState failed")
                }
            }
        }

        describe("legacy auth state caching") {
            let legacyAuthState = LegacyAuthState(
                firebaseUser: .random(),
                tiligAuthTokens: .random(),
                keyPair: .random(),
                userProfile: .random()
            )

            beforeEach {
                // Cache an AuthState in legacy format
                do {
                    // Cache it (this is how the state was cached in the legacy app)
                    let encoder = JSONEncoder()
                    let data = try encoder.encode(legacyAuthState)
                    try Valet.auth.setObject(data, forKey: ValetKeys.authState.rawValue)
                    // Set a flag that the cache should be persisted
                    UserDefaults.standard.set(true, forKey: "persistCache")
                } catch {
                    XCTFail("Failed to cache legacy state")
                }

                // Mock the refresh endpoints
                Mock(url: Endpoint.refresh.url,
                     dataType: .json,
                     statusCode: 200,
                     // swiftlint:disable force_try
                     data: [.post: try! Data(contentsOf: MockedRequestData.refresh)]
                ).register()

                // Mock the profile response
                Mock(url: Endpoint.profile.url,
                     dataType: .json,
                     statusCode: 200,
                     // swiftlint:disable force_try
                     data: [.get: try! Data(contentsOf: MockedRequestData.profileSuccess)]
                ).register()

                // Mock the `encryptKeypair` Firebase cloud function
                Box.KeyPair.mockedFirebaseEncryptResponse = KeyPairJWT(rawValue: "mocked-JWT")

                // Mock the store keypair request
                Mock(url: Endpoint.keypairs.url,
                     dataType: .json,
                     statusCode: 200,
                     // swiftlint:disable force_try
                     data: [.post: try! Data(contentsOf: MockedRequestData.postKeyPairSuccess)]
                ).register()
            }

            afterEach {
                // swiftlint:disable force_try
                try! AuthState.clearCache()
            }

            it("has a non-nil cached legacy state") {
                expect(AuthState.cachedLegacy).toNot(beNil())
            }

            it("has a nil cached (non-legacy) state") {
                expect(AuthState.cached).to(beNil())
            }

            it("migrates to a non-legacy AuthState object") {
                let migratedStatePublisher = AuthState.authenticateFromCache()

                // Wait until the pipeline finishes
                var cancellable: AnyCancellable?
                waitUntil(timeout: .seconds(5)) { done in
                    cancellable = migratedStatePublisher
                        .sink(receiveCompletion: { completion in
                            switch completion {
                            case .failure(let error):
                                print("--- COMPLETED WITH ERROR ---")
                                print(error)
                            case .finished:
                                done()
                            }
                        }, receiveValue: { authState in
                            print("Completed with auth state: \(authState)")
                        })
                }

                // Then test against the cached values. Not ideal, but this is only temporary.
                expect(AuthState.cached?.keyPair).toNot(beNil())
                expect(AuthState.cached?.legacyKeyPair).to(equal(legacyAuthState.keyPair))
                cancellable?.cancel()
            }
        }
    }
}
