//
//  KeyServiceTokenManagerSpec.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 29/04/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import XCTest
import Nimble
import Quick
import Mocker

import Combine
import Firebase

@testable import Tilig

class KeyServiceTokenManagerSpec: QuickSpec {
    override func spec() {
        describe("fetchJWT") {
            describe("when not signed in to Firebase") {
                beforeEach {
                    do {
                        try Auth.auth().signOut()
                    } catch {
                        XCTFail("Could not sign out user")
                    }
                }

                it("publishes an error") {
                    var didFail: Bool?
                    var error: Error?
                    var cancellable: AnyCancellable?
                    waitUntil(timeout: .seconds(5)) { done in
                        cancellable = KeyServiceTokenManager
                            .fetchJWT()
                            .sink(receiveCompletion: { completion in
                                if case .failure(let err) = completion {
                                    didFail = true
                                    error = err
                                }
                                done()
                            }, receiveValue: { _ in
                                done()
                            })
                    }
                    expect(didFail).toEventually(be(true))
                    expect(error).toEventually(matchError(NSError.self))
                    expect(error?.localizedDescription).toEventuallyNot(beNil())
                    cancellable?.cancel()
                }
            }

            describe("when signed in to Firebase") {
                beforeEach {
                    KeyServiceTokenManager.mockedResponse = .success(
                        ["token": "le-token-oui-oui"]
                    )
                }
                afterEach {
                    KeyServiceTokenManager.mockedResponse = nil
                }
                it("mocks as expected") {
                    var didComplete = false
                    var token: String?
                    let cancellable: AnyCancellable? = KeyServiceTokenManager
                        .fetchJWT()
                        .sink(receiveCompletion: { completion in
                            switch completion {
                            case .finished:
                                didComplete = true
                            case .failure:
                                XCTFail("Failed to fetch token")
                            }
                        }, receiveValue: { receivedToken in
                            token = receivedToken
                        })
                    expect(token).toEventually(equal("le-token-oui-oui"))
                    expect(didComplete).toEventually(be(true))
                    cancellable?.cancel()
                }
            }

            describe("when the JWT cloud function request fails") {
                beforeEach {
                    KeyServiceTokenManager.mockedResponse = .failure(.responseError(URLError(.networkConnectionLost)))
                }
                afterEach {
                    KeyServiceTokenManager.mockedResponse = nil
                }
                it("completes the pipeline with an error") {
                    var error: Error?
                    let cancellable = KeyServiceTokenManager
                        .fetchJWT()
                        .sink { completion in
                            if case .failure(let err) = completion {
                                error = err
                            }
                        } receiveValue: { _ in }
                    expect(error).toEventually(
                        matchError(
                            AuthError.keyServiceError(.responseError(URLError(.networkConnectionLost)))
                        )
                    )
                    cancellable.cancel()
                }
            }
        }
    }
}
