//
//  AuthManagerSpec.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 03/11/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import XCTest

@testable import Tilig

class AuthManagerTests: TiligTestCase {
    var authManager: AuthManager!

    override func setUp() {
        super.setUp()
        authManager = AuthManager()
    }

    func testIsAuthenticatingAfterInit() throws {
        XCTAssertTrue(authManager.isAuthenticating)
    }

    func testTogglesIsAuthenticating() throws {
        let isAuthenticatingPublisher = authManager
            .$isAuthenticating
            .collect(2)
            .first()

        authManager!.authenticateFromCache()

        let values = try awaitPublisher(isAuthenticatingPublisher, timeout: 5)

        XCTAssertEqual(values, [true, false])
    }

    func testSignoutClearsAuthState() throws {
        authManager.authState = .random()
        XCTAssertNotNil(authManager.authState)
        try authManager.signOut()
        XCTAssertNil(authManager.authState)
    }

    func testSignoutClearsCache() throws {
        let authState = AuthState.random()
        try AuthState.cache(authState: authState)
        XCTAssertNotNil(AuthState.cached)
        try authManager.signOut()
        XCTAssertNil(AuthState.cached)
    }
}
