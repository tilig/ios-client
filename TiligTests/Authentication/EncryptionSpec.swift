//
//  EncryptionSpec.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 09/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import XCTest
import Nimble
import Quick
import Mocker
import Foundation

@testable import Tilig

class EncryptionSpec: QuickSpec {
    // swiftlint:disable function_body_length
    override func spec() {
        var keyPair: LegacyKeyPair!

        beforeEach {
            do {
                keyPair = try LegacyKeyPair.generateRSAKeyPair()
            } catch {
                XCTFail("Could not generate key pair")
            }
        }

        it("encrypts a string") {
            let plainText = "Hey hey hey"
            let ciphterText = try keyPair.encrypt(plainText: plainText)
            expect(ciphterText).toNot(equal(plainText))
        }

        it("can decrypt a cipherText to the original plainText") {
            let plainText = "Je suis le plaintext"
            let ciphterText = try keyPair.encrypt(plainText: plainText)
            let decrypted = try keyPair.decrypt(cipherText: ciphterText)
            expect(decrypted).to(equal(plainText))
        }

        it("makes the cipherText longer than the plaintext") {
            let plainText = "Hey hey hey"
            let ciphterText = try keyPair.encrypt(plainText: plainText)
            expect(ciphterText.count).to(beGreaterThan(plainText.count))
        }

        describe("when decrypting with a different keyPair") {
            var otherKeyPair: LegacyKeyPair!

            beforeEach {
                do {
                    otherKeyPair = try LegacyKeyPair.generateRSAKeyPair()
                } catch {
                    XCTFail("Could not generate key pair")
                }
            }

            it("cannot decrypt the cipherText") {
                let plainText = "Je suis le plaintext"
                let ciphterText = try keyPair.encrypt(plainText: plainText)
                expect { try otherKeyPair.decrypt(cipherText: ciphterText) }.to(
                    throwError(errorType: DecryptionError.self)
                )
            }
        }

        describe("when the ciphterText is not base64 encoded") {
            it("raises an error") {
                let ciphterText = "A@#($*($)(DDD)()#)(!@#@("
                expect { try keyPair.decrypt(cipherText: ciphterText) }.to(
                    throwError(errorType: DecryptionError.self)
                )
            }
        }

        describe("when the ciphterText is an empty string") {
            it("fails silently and just returns an empty string") {
                let ciphterText = ""
                let plainText = try keyPair.decrypt(cipherText: ciphterText)
                expect(plainText).to(equal(""))
            }
        }

        describe("android data") {
            it("can decrypt values with newlines") {
                // swiftlint:disable line_length
                let publicKey = "-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAn1w2C3oL677/d0OKYQ1WPz6O/RpGFvPN5DB6xk98r64m00rA7qeX\r\nkryPr8rYU+WdTgdQJJIBUdF2NLHDMQuL+j9gtg7x00eUGwe9mRn1Pr9vg/Va+Mya\r\nbPsmtruEf+MLPbSG48MPIBpETD2sLUy8jMT8igA7GfWm9WGNak7LYXlyiVMYhWsI\r\n1q8YOPel7Bise6ig0oLPyOWIMs00S+C7BBgT6HEL7b/8OuIB17xkvVdSyHS2cofo\r\nUUTyfgbTLJ4nHld0ldFBoeJ8BzVm7KkJcFgwuzkbF/A5eximegXB5/lMiMbEbHm5\r\nIID2YmB6O1WmpioVe4fnVXMDoT9DQcLEWwIDAQAB\n-----END RSA PUBLIC KEY-----"
                let privateKey = "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAn1w2C3oL677/d0OKYQ1WPz6O/RpGFvPN5DB6xk98r64m00rA\r\n7qeXkryPr8rYU+WdTgdQJJIBUdF2NLHDMQuL+j9gtg7x00eUGwe9mRn1Pr9vg/Va\r\n+MyabPsmtruEf+MLPbSG48MPIBpETD2sLUy8jMT8igA7GfWm9WGNak7LYXlyiVMY\r\nhWsI1q8YOPel7Bise6ig0oLPyOWIMs00S+C7BBgT6HEL7b/8OuIB17xkvVdSyHS2\r\ncofoUUTyfgbTLJ4nHld0ldFBoeJ8BzVm7KkJcFgwuzkbF/A5eximegXB5/lMiMbE\r\nbHm5IID2YmB6O1WmpioVe4fnVXMDoT9DQcLEWwIDAQABAoIBABEx8nTvkNy5qbgV\r\nNhe9yCof1m7yCFEOso8mOgEu733HVVIkNUgrFjxTk099HDWFR6F1QY0IohLRfqc1\r\n7SFcJRj2ALn0R9yr0wvNnIQqHF/OaTp8XXoOHbWXrfg2qJfiCEL6uTlXQUkN/IOb\r\nffoPVyL7WGKNntLwwjik84zefqyAMiSGAkFIb+MagAblTFZcwagKKqDxZmWl3Isl\r\n77bg4FNkEvtlm0WE8hUkrNPgeAoGcgBMrgMZqW8Eo3KVNVjvbqXbjRuEt46Hd8LE\r\nbvX86rzFrNCMxt9F0Qyox/EDCg/F2/oiUKwrqRBzlVoPofwGpKhe6b+FwzQg2ZAt\r\ndlDCVY0CgYEAz3KiVxrfAj2QGCM9B0v4vxACc81nbuoYVIk7q8JMiWvDV83c/e4p\r\nQJZbqvxjCmV64dj0PR6su60zVAcHRrPQjOO4CzUFwao7RxLf1EAN44Nxt7WlL+4/\r\n0g2sXMbD+hcP+/UPgXz5NuXgiCLGjFlCNMx1hwWqh7FuwLowbRDQp90CgYEAxKhg\r\n5xdwanV8Ee+iEQRT1wThCJ1/LI6kSXk4zt2naM+dUt3dbej9uRZVXAgjSe0MdjGs\r\nA2P6SdlfhrVi2jWxeIPHbw8qopXafn1VGBSG/meJW+OwkzWJzSerJf6qazNv4qaj\r\nhT1Ji8zgQuRIffEc9YiOhbhUobsTPeyBYQQ7NZcCgYEAyNxWE8EQ7TpuqvBzxvzo\r\nfRcjSL05yUshA7lc7ZHUSbruCbZqo/RWPRNY7tYxj39y2/sVgZiR56ZsbI/hm7O/\r\n2qsjKLbOPBZ4DvlXgEwFJqW/FgHrT92ALgLPQYWzfBOYhn1h5h77rwwlu6rwVDSY\r\nxkayBWqXE4vZkQK2oOQHJIECgYEAv+VF2Xnnuho2HRJmZJCCdxhNtihpRkNSUFe3\r\ndTSY/Q9KBuhz/6IQoyEF3i4asTJcrKC/pvdR7ens6vfoI+Gs8Vo1q0tzYP81G6SU\r\n+lBdgbXqNP4y+RKPCt+ghAzCoqSoktPkUEmDkKU0WFGF40754SWFXTqHauGvXwd/\r\nH/oPBLUCgYA14AZfwV9Pxr+/EIH/cDQ7yGWumVp93Ya2TUE2t6Eh3+1mUk648hgQ\r\nAG7j0wjy/7NYq3LDKsWYR5IdnBLQMFya24GWZjsg/ilG+KsUPLu5hTqbge+nyX8s\r\nD9LHen5dUU7e7M9EdS92Ae39bV2cFwJQ8Lj1HYitk+0GAr/if/mUcw==\n-----END RSA PRIVATE KEY-----"
                let keyPair = LegacyKeyPair(
                    privateKey: LegacyPrivateKey(rawValue: privateKey),
                    publicKey: LegacyPublicKey(rawValue: publicKey)
                )
                // note the "\n" at index 4
                let usernameWithNewLine = "aGtp\nnBGegUVe0/6MARUXp1UnqkbFYL2tVdwtYEzzbL4mtirZeiJjzomj+TTqQBVQVdrUGGkvg4FRiPH1easOFDNWdks0DYwKjKZgO/f9HLG+KE7FWHRoMfcPuh2j/q91nFCXtdp4VM0HfwOCMkjNOy3JAZ4sy9PJVPozar12W4oLFs+0B2eSW2linBkw/4DwoXAr/vng+3ZtBaNwcB0jSDe7krBr6BqH58bS+zOPrfg+GJCJXGrlVnBFW4fzXf5Oy/Bq6Qn6ZGr4tC6mepv8eBubsJZNSulw1xEvsCLlsMYAcf9hPnSa40iy9u6Z+8RlkpDk6FOt8wldcwxYP6wa/w=="
                let username = try keyPair.decrypt(cipherText: usernameWithNewLine)
                XCTAssertEqual(username, "gurt")
            }
        }

        describe("decrypting payloads with a block size of 255 (edge case)") {
            it("decrypts the payload") {
                let privateKey = "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEA7pOSCrTJTEccpCDtvqPZTabuBp+xzUIfMfu9nU1o0aytBhKP0BBBmarbD2pwoXJJsMXy/lVNRWu6gN3xQC2tw3yvZcMOiyfiAuZQNRLiIdgyuBZxwSjX2GewJp26nayNPig65OPqQu2o4FHomh+FxFB8u9gdpT3vOjk/wVvm5jEJUYL58JnkhOGVt5Xem+1aP2kMqybOC9zRBkI3dlDpNWXYpJROerXnUAWwU9ypwEiR5AsuUFcf2qtT3DnApHxgCJ1kDcie73vNqPrlJ+97jv1lpgNQ45g/aFy5truOgIRsj+jmMu6PheuXAIvD1lB1uzXDnEsQTeq6h3dyppf2TwIDAQABAoIBAB8a1VkY/f+4a8cIVgbbdOr3XyQ08OkKfGe3Yx7dp1Ni+IIIrIBbhjoDOSFW5s1dybbG8CnxtCvfYkcH6LrN2dBZcsLJPh2tev2WypFI33u7JM20e2tMKGwaBLDcMqb5Bsc6VodeIb5NHw30D+f8QSP5vHF/OUpAe26zZRkQItgS/yQYDXhpV6dAqhpZP+fiuRRrW7gL/7H7bi1X8+BHSDmJXQQXLLr+7nNW5hBeuIPWDsM1EMBoelP/WW3QlacXpCjE6eh79OMdsq6mEIDOXYYuSuECtn6OlTxHotNw3lOXNL5qUyOv01J4N8BqeGz+UdC+5oS3a3HzIx4tTGych7ECgYEA/o5UYCv3n6+YOmDCyLBWdS2D93QsUUYlzPNZgcyEKBsbai0ywvxNSW2nyvMSEVpsKif9PZG97blxrp7BCmWjqcNx0GKlvYc0EmgxRBFWWfzfgrOcycrC66v8BqwvHFQEf3jnKK02XmNdhXIw3hK3S6Jo5nwTr0fWlqq2q4ffbVECgYEA7+4I/3iZVq/bIqcnYz66Dgl6TGfHL/A9kKgcIXhjxYcAN5CHBCfghXzwZGdfzbJJ7IciRSFAn+go1vSEP25ALI7fIGW8cso4MBb8I3tFN05SKpL1J+sgNJDVZLyCiTMuFAUxa9lPqJCZiXSFr6+XlQ4RXHh/ZmArFokLiryZwZ8CgYAk9i2yOTAfc7O65kniMhibsxwyViAJPRJIDB23Byc5W5ZDeISiY1gljVKL2XeKJYMHK/9NbVgSqJalME//XnrBL6vbAjfmqTcaZR6iL5JGRr19kO73LgXXioVxwpakml580+VctqnqUqtFsSr/U+BhjxeEQAfrr5atEeSsS3JZwQKBgQDiOR1wy+RA0SUcgmDn+b/x9kvC+OLdt3ZFC80iXeIs38J7oaE8cOEI3CnJcZLqXt3G0A+nANhvLbY8H2cGa8zEdp25bVOHJFZgOXvZdKRppkZg8BmJXvAJwDoEdp3aAVDD/bWAi/DI7jZmXWCjqzuQhigmeVMeUzStVeB1Gqka3wKBgQDA1dBi06e7MyjZBHFUZnq+lunBbwAv5Hxt3cP2ECK5PhHNLpa1dW5ZZrCOFJ8oLoxFfCnn02evbhmAgaKrwQ3yoUEcaWv+g7YBYEH6YNM19VoyYayAcwAh4W7Nmu1t5uevVvtsry+QHYrqNriDhyhJFlz3HUQQTuB500ycTmE6TQ==\n-----END RSA PRIVATE KEY-----"

                let payload = "KudeNow47uyC2XphMoyRooNTKJWJEmLi0mMmWn5k4W+421Ob/+wvSSjOIn/dBi4R0fXKaNlIMPZdz9xLA6oWq5LAv/ADpZog3dTJpVqrTpDaDOcVaEHQld7F20Tvjx2UKbuj7vUMT/trX2z9UJsNDhNcOLH0hCMdUbm3yfQ6SLcWvbkaOj/iStUo+436pwg0BcCADTga7LaL34+qqREj2aWjlGp8acTc6ixjJJQUqS8lMifTjFF69nU3kig/M1hVvjjYQR+bdTyGyUElJNzyE+5bm3a7lafo1/S+O+nU2Dcut13ELSX596lMnrvnHFwcyjH2Z2zw9BAx2MbZv5Ul"

                let keyPair = LegacyKeyPair(
                    privateKey: LegacyPrivateKey(rawValue: privateKey),
                    publicKey: LegacyPublicKey(rawValue: "random")
                )

                let username = try keyPair.decrypt(cipherText: payload)
                XCTAssertEqual(username, "sebastian@gmail.com")
            }
        }
    }
}
