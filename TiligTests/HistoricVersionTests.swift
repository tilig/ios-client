//
//  PasswordVersionTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 20/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest

class HistoricVersionTests: TiligTestCase {

    var historicVersion: HistoricVersion!

    override func setUp() {
        super.setUp()
    }

    func testDecoding() throws {
        // If
        let json = "{\"kind\":\"password\",\"value\":\"previous_password\",\"replaced_at\":\"1970-01-01T00:00:00Z\"}"

        // When
        let data = try XCTUnwrap(json.data(using: .utf8))
        let jsonDecoder = JSONDecoder.snakeCaseConverting
        let version = try jsonDecoder.decode(HistoricVersion.self, from: data)

        // Then
        XCTAssertEqual(version.value, "previous_password")
        XCTAssertEqual(version.replacedAt.ISO8601Format(), "1970-01-01T00:00:00Z")
    }

    func testEncoding() throws {
        // if
        let version = HistoricVersion(
            kind: .password,
            value: "history_repeats",
            replacedAt: Date(timeIntervalSince1970: 0)
        )

        // When
        let encoded = try JSONEncoder.snakeCaseConverting.encode(version)
        let encodedString = String(data: encoded, encoding: .utf8)

        // Then
        XCTAssertEqual(
            encodedString,
            "{\"kind\":\"password\",\"value\":\"history_repeats\",\"replaced_at\":\"1970-01-01T00:00:00Z\"}"
        )
    }

    func testEqualilty() throws {
        let version = HistoricVersion(
            kind: .password,
            value: "history_repeats",
            replacedAt: Date(timeIntervalSince1970: 0)
        )
        let encoded = try JSONEncoder.snakeCaseConverting.encode(version)
        let decoded = try JSONDecoder.snakeCaseConverting.decode(HistoricVersion.self, from: encoded)
        XCTAssertEqual(version, decoded)
    }

    func testDecodesDatesWithFractionalSeconds() throws {
        // If
        let json = "{\"kind\":\"password\",\"value\":\"p\",\"replaced_at\":\"2022-09-23T05:58:59.522Z\"}"

        // When
        let data = try XCTUnwrap(json.data(using: .utf8))
        let jsonDecoder = JSONDecoder.snakeCaseConverting
        let version = try jsonDecoder.decode(HistoricVersion.self, from: data)

        // Then
        XCTAssertEqual(version.replacedAt.ISO8601Format(), "2022-09-23T05:58:59Z")
    }
}
