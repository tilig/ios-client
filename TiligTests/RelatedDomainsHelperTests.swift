//
//  RelatedDomainsHelperTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 24/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import XCTest

class RelatedDomainsHelperTests: TiligTestCase {
    var relatedDomainsHelper: RelatedDomainsHelper!

    let amazonDomains = [
        "amazon.com",
        "amazon.ae",
        "amazon.com.au",
        "amazon.com.br",
        "amazon.ca",
        "amazon.fr",
        "amazon.de",
        "amazon.in",
        "amazon.it",
        "amazon.com.mx",
        "amazon.nl",
        "amazon.es",
        "amazon.com.tr",
        "amazon.co.uk",
        "amazon.sa",
        "amazon.sg",
        "amazon.se",
        "amazon.pl",
        "ring.com"
    ]

    override func setUp() {
        super.setUp()
        relatedDomainsHelper = RelatedDomainsHelper()
    }

    func testInit() {
        XCTAssertEqual(relatedDomainsHelper.domains.isEmpty, false)
    }

    func testFindRelatedDomainsForAmazonDotCom() {
        let domains = relatedDomainsHelper.findRelatedDomains("amazon.com")

        XCTAssertEqual(domains, amazonDomains)
    }

    func testFindRelatedDomainsForAmazonGermany() {
        let domains = relatedDomainsHelper.findRelatedDomains("amazon.de")

        XCTAssertEqual(domains, amazonDomains)
    }

    func testDoesNotMatchURLs() {
        let domains = relatedDomainsHelper.findRelatedDomains("www.amazon.nl")

        XCTAssertEqual(domains, ["www.amazon.nl"])
    }

    func testDoesNotMatchURLsWithProtocols() {
        let domains = relatedDomainsHelper.findRelatedDomains("https://www.amazon.nl")

        XCTAssertEqual(domains, ["https://www.amazon.nl"])
    }

    func testReturnsSameDomainWhenNoRelatedDomains() {
        let domains = relatedDomainsHelper.findRelatedDomains("br.com")
        XCTAssertEqual(domains, ["br.com"])
    }
}
