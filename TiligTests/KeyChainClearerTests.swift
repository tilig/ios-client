//
//  KeyChainClearerTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 19/01/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import XCTest

final class KeyChainClearerTests: TiligTestCase {

    func testSetsPersistenceFlag() throws {
        // If
        UserDefaults.standard.removeObject(forKey: "persistCache")

        // When
        KeyChainClearer.setPersistenceFlag()

        // Then
        XCTAssertTrue(UserDefaults.standard.bool(forKey: "persistCache"))
    }

    func testIsReinstalledWhenPersistFlagIsBlank() throws {
        // If
        UserDefaults.standard.removeObject(forKey: "persistCache")

        // Then
        XCTAssertTrue(KeyChainClearer.isReinstalled)
    }

    func testIsReinstalledWhenPersistFlagIsPresent() throws {
        // If
        UserDefaults.standard.set(true, forKey: "persistCache")

        // Then
        XCTAssertFalse(KeyChainClearer.isReinstalled)
    }

    func testClearsAuthCache() throws {
        // If
        try AuthState.cache(authState: .random())
        UserDefaults.standard.removeObject(forKey: "persistCache")
        XCTAssertNotNil(AuthState.cached)

        // When
        XCTAssertTrue(KeyChainClearer.isReinstalled)
        try KeyChainClearer.clearIfReinstalled()

        // Then
        XCTAssertNil(AuthState.cached)
    }

    func testClearsCachedItems() throws {
        // If
        try ItemsCache().cacheAll(items: [Item.random()])
        let cachedItemsBefore = try ItemsCache().getCachedItems()
        UserDefaults.standard.removeObject(forKey: "persistCache")
        XCTAssertNotEqual(cachedItemsBefore, [])

        // When
        XCTAssertTrue(KeyChainClearer.isReinstalled)
        try KeyChainClearer.clearIfReinstalled()

        // Then
        let cachedItemsAfter = try ItemsCache().getCachedItems()
        XCTAssertEqual(cachedItemsAfter, [])
    }

}
