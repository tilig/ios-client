//
//  GlobalFactoryTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 14/11/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Combine

final class GlobalFactoryTests: TiligTestCase {
    var factory: GlobalFactory!

    override func setUp() {
        super.setUp()
    }

    func testCreatesAuthenticatedFactoryAfterAuthentication() {
        // If
        factory = GlobalFactory()

        // When
        factory.authManager.authState = .random()

        // Wait for non-nil value to publish
        waitForFirstValue(from: factory.$authenticatedModelFactory)
        XCTAssertNotNil(factory.authenticatedModelFactory)
    }

    func testClearsAuthenticatedFactoryAfterSignOut() {
        // If
        factory = GlobalFactory()
        factory.authManager.authState = .random()
        waitForFirstValue(from: factory.$authenticatedModelFactory)
        XCTAssertNotNil(factory.authenticatedModelFactory)

        // When
        factory.authManager.authState = nil

        // Wait for nil value to publish
        waitForFirstValue(from: factory.$authenticatedModelFactory)
        XCTAssertNil(factory.authenticatedModelFactory)
    }
}
