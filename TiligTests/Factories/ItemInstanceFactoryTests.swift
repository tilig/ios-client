//
//  ItemFactoryTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 14/11/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

 import XCTest
 import Mocker

final class ItemInstanceFactoryTests: TiligTestCase {
    var factory: ItemInstanceFactory!

    var item: Item!
    var crypto: Crypto!
    var legacyKeyPair: LegacyKeyPair! { crypto.legacyKeyPair }

    override func setUpWithError() throws {
        super.setUp()
        crypto = Crypto.random()
        item = Item.random(encryptedWithKeyPair: crypto.legacyKeyPair)
    }

    func initFactory(withItem item: Item) throws {
        factory = try ItemInstanceFactory(
            encryptedItem: item,
            crypto: crypto,
            tokenProvider: .mocked(),
            userProfile: .random()
        )
    }

    func testInitializerFullyDecrypts() throws {
        let item = Item.random(
            encryptedWithKeyPair: legacyKeyPair,
            name: "Factory Init Name",
            plainUsername: "Factory Init Username",
            plainPassword: "Factory Init Password"
        )

        // When
        try initFactory(withItem: item)

        // Then
        XCTAssertEqual(factory.itemViewModel.itemWrapper.name, "Factory Init Name")
        XCTAssertEqual(factory.itemViewModel.itemWrapper.username, "Factory Init Username")
        XCTAssertEqual(factory.itemViewModel.itemWrapper.password, "Factory Init Password")
    }

//    // TODO: fix this
//    func mockUpdateItemAPI(forItem item: Item) throws {
//        let configuration = URLSessionConfiguration.default
//        configuration.protocolClasses = [MockingURLProtocol.self]
//        Mock(
//            url: Endpoint.secret(try XCTUnwrap(item.id)).url,
//            dataType: .json,
//            statusCode: 200,
//            data: [.put: try Data(contentsOf: MockedRequestData.editItemSuccess)]
//        )
//        .register()
//    }

//    func testItemUpdateChangeDataFlows() throws {
//        // If
//        let item = Item.random(name: "Non-amazon")
//        try mockUpdateItemAPI(forItem: item)
//        try factory.setInstance(forItem: item)
//        let editViewModel = try XCTUnwrap(factory.editItemViewModel)
//
//        // When
//        // API is mocked with Amazon
//        editViewModel.name = "Amazon"
//        editViewModel.update()
//        waitForNonNilValue(from: editViewModel.itemUpdated)
//
//        // Then
//        // the item view model reflects the update
//        XCTAssertEqual(factory.itemViewModel?.item.name, "Amazon")
//        // and the item list as well
//        // TODO: needs item to be added to the itemlist model first
//        // XCTAssertEqual(factory.itemListViewModel.items.first { $0.id == item.id }?.name, "Amazon")
//    }

}
