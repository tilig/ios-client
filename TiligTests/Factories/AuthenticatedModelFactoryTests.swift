//
//  ViewModelFactoryTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 04/06/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

 import XCTest
 import Mocker

 class AuthenticatedModelFactoryTests: TiligTestCase {
    var factory: AuthenticatedModelFactory!

    // Dependencies
    var authManager: AuthManager!
    var crypto: Crypto!
    var userProfile: UserProfile!

    override func setUp() {
        super.setUp()

        authManager = AuthManager()
        crypto = Crypto.random()

        factory = AuthenticatedModelFactory(
            authManager: authManager,
            tokenProvider: .mocked(),
            userProfile: .random(),
            crypto: crypto
        )
    }

    func testDoesNotClearCachedModelsForSameIDs() throws {
        // If
        let encryptedItem = Item.random(
            encryptedWithKeyPair: crypto.legacyKeyPair,
            name: "Henk"
        )
        try factory.setInstance(forItem: encryptedItem)
        let model = try XCTUnwrap(factory.itemViewModel)

        // When
        try factory.setInstance(forItem: encryptedItem)
        model.update(with: ItemWrapper.random(encryptedWithKeyPair: crypto.legacyKeyPair, name: "Updated name"))

        // Then
        XCTAssertEqual(factory.itemViewModel?.itemWrapper.name, "Updated name")
    }

    // MARK: Item instance factory

     func testSetInstanceModels() throws {
         // If
         // TODO: add helper that also encrypts overview + details
         let encryptedItem = Item.random(
            encryptedWithKeyPair: crypto.legacyKeyPair,
            name: "Henk",
            plainUsername: "Henk3000",
            plainPassword: "Password",
            notes: "Henk has a password"
         )

         // When
         try factory.setInstance(forItem: encryptedItem)

         // Then
         XCTAssertEqual(factory.itemViewModel?.itemWrapper.name, "Henk")
         XCTAssertEqual(factory.itemViewModel?.itemWrapper.username, "Henk3000")
         XCTAssertEqual(factory.itemViewModel?.itemWrapper.password, "Password")
         XCTAssertEqual(factory.itemViewModel?.itemWrapper.notes, "Henk has a password")
     }

     func testInstanceModelCaching() throws {
         let item = Item.random(encryptedWithKeyPair: crypto.legacyKeyPair)
         try factory.setInstance(forItem: item)

         XCTAssertEqual(factory.itemViewModel?.itemWrapper.name, item.name)
         XCTAssertEqual(factory.editItemViewModel?.id, item.id)
         // delete viewmodel has no accessible identifier, so a bit harder to check
         // XCTAssertEqual(factory.deleteItemViewModel?.itemDeleted, item)
         XCTAssertEqual(factory.twoFactorViewModel?.itemName, item.name)

         // Set a different instance
         let otherItem = Item.random(encryptedWithKeyPair: crypto.legacyKeyPair, name: "Other name")
         XCTAssertNotEqual(item, otherItem)
         try factory.setInstance(forItem: otherItem)

         XCTAssertEqual(factory.itemViewModel?.itemWrapper.name, "Other name")
         XCTAssertEqual(factory.editItemViewModel?.id, otherItem.id)
         XCTAssertEqual(factory.twoFactorViewModel?.itemName, "Other name")
     }

     func testDeactivateClearsTheIntanceFactory() throws {
         // If
         let item = Item.random(encryptedWithKeyPair: crypto.legacyKeyPair)
         try factory.setInstance(forItem: item)
         XCTAssertNotNil(factory.instanceFactory)

         // When
         factory.deactivate()

         // Then
         XCTAssertNil(factory.instanceFactory)
     }

     func testClearInstanceFactory() throws {
         // If
         let item = Item.random(encryptedWithKeyPair: crypto.legacyKeyPair)
         try factory.setInstance(forItem: item)
         XCTAssertNotNil(factory.instanceFactory)

         // When
         factory.clearInstance()

         // Then
         XCTAssertNil(factory.instanceFactory)
     }

     func testDeactivateClearsCredLoader() async throws {
         // If
         let credLoader = try mockCredLoader()
         // Add an item
         let item = Item.random(encryptedWithKeyPair: crypto.legacyKeyPair, domain: "booking.com")
         await factory.itemListViewModel.add(encryptedItem: item)
         waitForFirstValue(from: credLoader.$identities.map { $0.count }, matching: 1)
         XCTAssertEqual(factory.credLoader?.identities.count, 1)

         // When
         factory.deactivate()

         // Then
         XCTAssertEqual(factory.itemListViewModel.items.count, 0)
         XCTAssertEqual(credLoader.identities.count, 0)
         XCTAssertEqual(factory.credLoader?.identities.count, 0)
     }

     // Mock the credential identities loader and make it enabled
     func mockCredLoader() throws -> CredentialIdentitiesLoader {
         // Make sure the credential identities loader is created
         _ = factory.itemListViewModel
         let credLoader = try XCTUnwrap(factory.credLoader)
         let stateProvider = MockedCredentialIdentityStateProvider()
         credLoader.credentialIdentityStateProvider = stateProvider
         stateProvider.triggerStateChange(isEnabled: true)
         return credLoader
     }
}
