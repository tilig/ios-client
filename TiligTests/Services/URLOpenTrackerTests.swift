//
//  URLOpenTrackerTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 11/01/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import XCTest

@testable import Tilig

class URLOpenTrackerTests: TiligTestCase {

    let tracker = URLOpenTracker()

    override func setUp() {
        super.setUp()
    }

    func testReturnsCorrectURL() throws {
        XCTAssertEqual(tracker.getAndTrackUrl(), URL(string: "App-prefs:PASSWORDS")!)
    }
}
