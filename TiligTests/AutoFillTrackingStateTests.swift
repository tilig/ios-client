//
//  AutoFillTrackingStateTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 31/01/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import XCTest

final class AutoFillTrackingStateTests: TiligTestCase {

    override func setUpWithError() throws {
    }

    func testDefaultStates() {
        XCTAssertEqual(AutoFillTrackingState.firstAutoFillState, .notCompleted)
        XCTAssertEqual(UserDefaults.standard.integer(forKey: UserDefaults.FirstAutoFill), 0)

        XCTAssertEqual(AutoFillTrackingState.autoFillCount, 0)
        XCTAssertEqual(UserDefaults.standard.integer(forKey: UserDefaults.AutoFillCount), 0)

        XCTAssertEqual(AutoFillTrackingState.thirdAutoFillState, .notCompleted)
        XCTAssertEqual(UserDefaults.standard.integer(forKey: UserDefaults.ThirdAutoFill), 0)
    }

    func testHandleUncompletedFirstAutoFillEvent() {
        // If
        AutoFillTrackingState.firstAutoFillState = .notCompleted
        XCTAssertEqual(UserDefaults.standard.integer(forKey: UserDefaults.FirstAutoFill), 0)

        // When
        AutoFillTrackingState.handleUntrackedEvents()

        // Then
        XCTAssertEqual(AutoFillTrackingState.firstAutoFillState, .notCompleted)
        XCTAssertEqual(UserDefaults.standard.integer(forKey: UserDefaults.FirstAutoFill), 0)
    }

    func testHandleUncompletedThirdAutoFillEvent() {
        // If
        AutoFillTrackingState.thirdAutoFillState = .notCompleted
        XCTAssertEqual(UserDefaults.standard.integer(forKey: UserDefaults.ThirdAutoFill), 0)

        // When
        AutoFillTrackingState.handleUntrackedEvents()

        // Then
        XCTAssertEqual(AutoFillTrackingState.thirdAutoFillState, .notCompleted)
        XCTAssertEqual(UserDefaults.standard.integer(forKey: UserDefaults.ThirdAutoFill), 0)
    }

    func testHandleCompletedAndUntrackedFirstAutoFillEvents() {
        // If
        AutoFillTrackingState.firstAutoFillState = .completedWithoutTracking
        XCTAssertEqual(UserDefaults.standard.integer(forKey: UserDefaults.FirstAutoFill), 1)

        // When
        AutoFillTrackingState.handleUntrackedEvents()

        // Then
        XCTAssertEqual(AutoFillTrackingState.firstAutoFillState, .completedAndTracked)
        XCTAssertEqual(UserDefaults.standard.integer(forKey: UserDefaults.FirstAutoFill), 2)
    }

    func testHandleCompletedAndUntrackedThirdAutoFillEvents() {
        // If
        AutoFillTrackingState.thirdAutoFillState = .completedWithoutTracking
        XCTAssertEqual(UserDefaults.standard.integer(forKey: UserDefaults.ThirdAutoFill), 1)

        // When
        AutoFillTrackingState.handleUntrackedEvents()

        // Then
        XCTAssertEqual(AutoFillTrackingState.thirdAutoFillState, .completedAndTracked)
        XCTAssertEqual(UserDefaults.standard.integer(forKey: UserDefaults.ThirdAutoFill), 2)
    }

    func testIncrementAutofillCount() {
        // If
        AutoFillTrackingState.autoFillCount = 0
        XCTAssertEqual(UserDefaults.standard.integer(forKey: UserDefaults.AutoFillCount), 0)

        // When
        AutoFillTrackingState.incrementAutoFillCount()

        // Then
        XCTAssertEqual(AutoFillTrackingState.autoFillCount, 1)
        XCTAssertEqual(UserDefaults.standard.integer(forKey: UserDefaults.AutoFillCount), 1)
    }

    func testIncrementSetsThirdAutoFillState() {
        // If
        AutoFillTrackingState.autoFillCount = 2
        XCTAssertEqual(UserDefaults.standard.integer(forKey: UserDefaults.AutoFillCount), 2)

        // When
        AutoFillTrackingState.incrementAutoFillCount()

        // Then
        XCTAssertEqual(AutoFillTrackingState.autoFillCount, 3)
        XCTAssertEqual(UserDefaults.standard.integer(forKey: UserDefaults.AutoFillCount), 3)
        XCTAssertEqual(AutoFillTrackingState.thirdAutoFillState, .completedWithoutTracking)
    }

    override func tearDown() async throws {
        AutoFillTrackingState.firstAutoFillState = .notCompleted
        AutoFillTrackingState.thirdAutoFillState = .notCompleted
        AutoFillTrackingState.autoFillCount = 0
    }

}
