//
//  TrackingEventSpec.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 25/08/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import Nimble
import Quick
import Mixpanel

@testable import Tilig

// class TrackingeventSpec: QuickSpec {
//    override func spec() {
//        describe("send()") {
//            let event = TrackingEvent.itemListViewed
//            it("is doesn't crash") {
//                // Not much to test yet since it doesn't return anything
//                // Just test if it doens't crash for now.
//                // We could watch network traffic or something
//                // like that, but that's too much work for now
//                event.send(withProperties: ["some": "properties"])
//            }
//        }
//    }
// }
