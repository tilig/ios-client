//
//  CredentialIdentitiesLoaderTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 06/06/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import AuthenticationServices

@testable import Tilig

class CredentialIdentitiesLoaderTests: TiligTestCase {
    @Published var crypto: Crypto!
    @Published var items: [Item] = []

    var loader: CredentialIdentitiesLoader!

    let mockCredentialIdentityStateProvider = MockedCredentialIdentityStateProvider()

    override func setUp() {
        crypto = .random()
        items = []

        // Set the store to enabled (didn't write test for disabled state yet)
        mockCredentialIdentityStateProvider.triggerStateChange(isEnabled: true)

        loader = CredentialIdentitiesLoader(
            crypto: crypto,
            items: items,
            itemsPublisher: $items.eraseToAnyPublisher(),
            credentialIdentityStateProvider: mockCredentialIdentityStateProvider
        )
    }

    // Helper method
    func prepareNonEmptyState() throws {
        let item = Item.random(
            encryptedWithKeyPair: crypto.legacyKeyPair,
            domain: "www.google.com"
        )
        items = [item]
        waitForFirstValue(from: loader.$identities.map { $0.count }, matching: 1)
        XCTAssertNotNil(loader.passwordCredentialIdentities(forItem: item))
    }

    func testCreatesIdentityWhenItemsPublished() throws {
        let item = Item.random(
            encryptedWithKeyPair: crypto.legacyKeyPair,
            domain: "www.google.com"
        )
        items = [item]
        waitForFirstValue(from: loader.$identities.map { $0.count }, matching: 1)
        XCTAssertNotNil(loader.passwordCredentialIdentities(forItem: item))
    }

    func testStoresDecryptedUsernames() throws {
        // If
        let item = Item.random(
            encryptedWithKeyPair: crypto.legacyKeyPair,
            plainUsername: "FredDurst2000",
            url: URL(string: "https://private.domain.com")!,
            domain: "domain.com"
        )
        items = [item]
        waitForFirstValue(from: loader.$identities.map { $0.count }, matching: 1)
        let identity = loader.passwordCredentialIdentities(forItem: item).first
        XCTAssertEqual(identity?.user, "FredDurst2000")
        XCTAssertEqual(identity?.serviceIdentifier.identifier, "domain.com")
        XCTAssertEqual(identity?.serviceIdentifier.type, ASCredentialServiceIdentifier.IdentifierType.domain)
    }

    func testDoesNotStoreItemWithoutUsername() throws {
        let item = Item(
            name: "Test",
            website: CodableOptionalURL(string: "https://www.test.com"),
            domain: "test.com",
            username: "",
            password: crypto.legacyKeyPair.randomEncrypedPassword()
        )
        items = [item]
        waitForFirstValue(from: loader.$identities)
        XCTAssertEqual(loader.passwordCredentialIdentities(forItem: item), [])
    }
}
