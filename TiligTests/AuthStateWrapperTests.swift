//
//  AuthStateWrapperTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 02/06/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Combine

@testable import Tilig

class AuthStateWrapperTests: TiligTestCase {
    @Published var authStatePub: AuthState?

    var authState: AuthState!
    var wrapper: AuthStateWrapper!

    override func setUp() {
        super.setUp()

        authState = AuthState.random()

        wrapper = AuthStateWrapper(
            authState: authState,
            authStatePublisher: $authStatePub.eraseToAnyPublisher()
        )
    }

    func testSetsInitialAuthState() throws {
        XCTAssertEqual(wrapper.wrappedValue, authState)
        XCTAssertEqual(wrapper.keyPair, authState.keyPair)
        XCTAssertEqual(wrapper.firebaseUser, authState.firebaseUser)
    }

    func testUpdatesPublishedValue() throws {
        let newState = AuthState.random()
        XCTAssertNotEqual(newState, authState)
        authStatePub = newState
        XCTAssertEqual(wrapper.wrappedValue, newState)
    }
}
