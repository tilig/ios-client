//
//  ItemsCacheTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 20/11/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Valet

final class ItemsCacheTests: TiligTestCase {

    var crypto: Crypto!
    var cache: ItemsCache!

    override func setUp() {
        super.setUp()

        crypto = Crypto.random()

        cache = ItemsCache()
    }

    override func tearDownWithError() throws {
        try cache.clear()
    }

    /// The valet instance that's being used as the underlying storage
    var valet: Valet {
        Valet.valet(
            with: Identifier(nonEmpty: ValetKeys.itemsCache.rawValue)!,
            accessibility: .whenUnlocked
        )
    }

    func testRoundTrip() throws {
        // If
        let items = [
            Item.random(encryptedWithKeyPair: crypto.legacyKeyPair),
            Item.random(encryptedWithKeyPair: crypto.legacyKeyPair)
        ]

        // When
        try cache.cacheAll(items: items)
        let items2 = try cache.getCachedItems()

        // Then
        XCTAssertEqual(items.count, items2.count)

        XCTAssertEqual(items[0].id, items2[0].id)
        XCTAssertEqual(items[0].name, items2[0].name)

        XCTAssertEqual(items[1].id, items2[1].id)
        XCTAssertEqual(items[1].name, items2[1].name)
    }

    func testStoringLargeItems() throws {
        // If
        // Workaround to disable legacy encryption
        // This is not necessary anymore as soon as MR #325 has been merged
        var itemWrapper = ItemWrapper(
            item: Item(legacyEncryptionDisabled: true),
            overview: DecryptedOverview(),
            details: DecryptedDetails()
        )
        itemWrapper.name = "Long title"
        // 400_000 chars seems to break, but it could be that that's just the
        // Xcode console having difficulty printing such large text blobs.
        itemWrapper.notes = (0...350_000).map { _ in return "a" }.joined()
        let item = try crypto.encrypt(itemWrapper: itemWrapper)

        // When
        try cache.cacheAll(items: [item])

        // Then
        let cachedItem = try cache.getCachedItems()[0]
        XCTAssertEqual(cachedItem, item)

        // Check the decrypted notes
        let cachedItemWrapper = try crypto.decrypt(fullItem: cachedItem)
        XCTAssertEqual(cachedItemWrapper.notes, itemWrapper.notes)
        XCTAssertEqual(cachedItemWrapper.notes?.first, "a")
    }

    func testCacheAllReplacesPreviousItems() throws {
        // If
        let item1 = try crypto.encrypt(itemWrapper: ItemWrapper(template: .loginV1))
        let item2 = try crypto.encrypt(itemWrapper: ItemWrapper(template: .loginV1))
        try cache.cacheAll(items: [item1, item2])

        // When
        try cache.cacheAll(items: [item2])

        // Then
        XCTAssertEqual(try cache.getCachedItems().count, 1)
        XCTAssertNoThrow(try valet.object(forKey: "item0"))
        XCTAssertThrowsError(try valet.object(forKey: "item1"))
    }
}
