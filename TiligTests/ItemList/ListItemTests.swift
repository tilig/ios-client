//
//  ListItemTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 29/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest

@testable import Tilig

final class ListItemTests: TiligTestCase {

    override func setUpWithError() throws {
        try super.setUpWithError()
    }

    func testListItemName() throws {
        let itemA = ListItem(
            encryptedItem: try .random(name: "a")
        )
        XCTAssertEqual(itemA.name, "a")
    }

    func testListItemSorting() throws {
        let itemA = ListItem(
            encryptedItem: try .random(name: "a")
        )
        let itemB = ListItem(
            encryptedItem: try .random(name: "b")
        )
        XCTAssert(itemA < itemB)
    }

    func testListItemEquality() throws {
        let itemA = ListItem(
            encryptedItem: try .random(name: "a")
        )
        let itemB = ListItem(
            encryptedItem: try .random(name: "b")
        )
        XCTAssertNotEqual(itemA, itemB)
        XCTAssertEqual(itemB, ListItem(encryptedItem: itemB.encryptedItem))
    }

    // TODO: add tests for:
    // - errors
}
