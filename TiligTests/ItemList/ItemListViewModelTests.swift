//
//  ItemListViewModelTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 26/07/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Mocker

@testable import Tilig

// swiftlint:disable type_body_length
class ItemListViewModelTests: TiligTestCase {
    var viewModel: ItemListViewModel!
    var cache: ItemsCache!
    var crypto: Crypto!

    @MainActor override func setUpWithError() throws {
        try super.setUpWithError()
        crypto = Crypto.random()

        cache = ItemsCache()
        try cache.clear()

        viewModel = ItemListViewModel(
            crypto: crypto,
            tokenProvider: .mocked(),
            itemsCache: cache
        )
    }

    override func tearDownWithError() throws {
        clearCache()
    }

    func mockEndpoint(mockedDataURL: URL, changesSince: Date? = nil) throws {
        Mock(
            url: Endpoint.items.url,
            dataType: .json,
            statusCode: 200,
            data: [.get: try Data(contentsOf: mockedDataURL)]
        ).register()

        Mock(
            url: Endpoint.polling(changesSince ?? .now).url,
            dataType: .json,
            statusCode: 200,
            data: [.get: try Data(contentsOf: mockedDataURL)]
        ).register()
    }

    func mockNonEmptyResponse() throws {
        try mockEndpoint(mockedDataURL: MockedRequestData.secrets)
    }

    func mockEmptyResponse() throws {
        try mockEndpoint(mockedDataURL: MockedRequestData.emptyArray)
    }

    func clearCache() {
        do {
            try ItemsCache().clear()
        } catch {
            XCTFail("Could not clear the ItemsCache")
        }
    }

    // Helper function to easily create items that are properly encrypted
    // using non-legacy encryption.
    func createItem(name: String, username: String? = nil, url: URL? = nil) -> Item {
        var itemWrapper = ItemWrapper(template: .loginV1)
        itemWrapper.name = name
        if let url {
            itemWrapper.url = url
        }
        if let username {
            itemWrapper.username = username
        }
        do {
            var item = try crypto.encrypt(itemWrapper: itemWrapper)
            // Mock an ID
            item.id = UUID()
            return item
        } catch {
            // XCTFail would be nicer, but wants us to return something
            fatalError("Failed to encrypt item in helper function")
        }
    }

    func testIsLoadingState() throws {
        // If
        let loadingState = viewModel
            .$loadingState
            .collect(3)
            .first()

        // When
        Task {
            await viewModel.fetchItems()
        }

        // Then
        let values = try awaitPublisher(loadingState, timeout: 5)
        XCTAssertEqual(values, [.fetchingInitial, .fetchingInitial, .idle])
    }

    func testUnauthenticatedQuery() async throws {
        // If
        viewModel = ItemListViewModel(
            crypto: .random(),
            tokenProvider: .mocked(failing: true)
        )

        // When
        var thrownError: Error?
        do {
            _ = try await viewModel.query()
        } catch {
            thrownError = error
        }

        // Then
        XCTAssertTrue(
            thrownError is AuthError,
            "Unexpected error type: \(type(of: thrownError))"
        )
    }

    func testQueryWithEmptyResponse() async throws {
        // If
        try mockEmptyResponse()

        // When
        let items = try await viewModel.query()

        // Then
        XCTAssertEqual(items.count, 0)
    }

    func testQueryWithNonEmptyResponse() async throws {
        // If
        try mockNonEmptyResponse()

        // When
        let items = try await viewModel.query()

        // Then
        XCTAssertEqual(items.count, 1)
    }

    func testFetchItemsWithEmptyResponse() async throws {
        // If
        try mockEmptyResponse()

        // When
        await viewModel.fetchItems()

        // Then
        XCTAssertEqual(viewModel.items.count, 0)
    }

    func testFetchItemsWithoutCustomTemplate() async throws {
        Mock(
            url: Endpoint.items.url,
            dataType: .json,
            statusCode: 200,
            data: [.get: try Data(contentsOf: MockedRequestData.mixedItems)]
        ).register()

        // When
        let items = try await viewModel.query()

        // Then
        XCTAssertNotNil(items)

        let templates = items.map({ $0.template })
        XCTAssertFalse(templates.contains(.customV1))
    }

    func testFetchItemsWithNonEmptyResponse() async throws {
        // If
        try mockNonEmptyResponse()

        // When
        await viewModel.fetchItems()

        // Then
        XCTAssertEqual(viewModel.items.count, 1)
    }

    func testFetchChangesWithEmptyResponse() async throws {
        // If
        try mockEmptyResponse()
        await viewModel.add(encryptedItem: createItem(name: "Bert"))
        await viewModel.add(encryptedItem: createItem(name: "bert"))
        XCTAssertEqual(viewModel.items.count, 2)

        // When
        await viewModel.fetchChanges()

        // Then
        XCTAssertEqual(viewModel.items.count, 2)
    }

    func testFetchChangesWithNonEmptyResponse() async throws {
        // If
        try mockNonEmptyResponse()
        await viewModel.add(encryptedItem: createItem(name: "Bert"))
        await viewModel.add(encryptedItem: createItem(name: "bert"))
        XCTAssertEqual(viewModel.items.count, 2)
        try mockEndpoint(
            mockedDataURL: MockedRequestData.updatedSecrets,
            changesSince: viewModel.lastRefresh
        )

        // When
        await viewModel.fetchChanges()

        // Then
        XCTAssertEqual(viewModel.items.count, 3)
    }

    func testUpdateItemsDuringFetchChanges() async throws {
        // If
        try mockNonEmptyResponse()
        await viewModel.fetchItems()
        XCTAssertEqual(viewModel.items.count, 1)
        XCTAssertEqual(viewModel.items.map { $0.name }, ["Airbnb"])
        try mockEndpoint(
            mockedDataURL: MockedRequestData.updatedSecrets,
            changesSince: viewModel.lastRefresh
        )

        // When
        await viewModel.fetchChanges()

        // Then
        XCTAssertEqual(viewModel.items.count, 1)
        XCTAssertEqual(viewModel.items.map { $0.name }, ["Amazon"])
    }

    func testDeleteItemsDuringFetchChanges() async throws {
        // If
        try mockNonEmptyResponse()
        await viewModel.fetchItems()
        XCTAssertEqual(viewModel.items.count, 1)
        XCTAssertEqual(viewModel.items.map { $0.name }, ["Airbnb"])
        try mockEndpoint(
            mockedDataURL: MockedRequestData.deletedSecrets,
            changesSince: viewModel.lastRefresh
        )

        // When
        await viewModel.fetchChanges()

        // Then
        XCTAssertEqual(viewModel.items.count, 0)
    }

    func testAddItemsDuringFetchChanges() async throws {
        // If
        try mockNonEmptyResponse()
        await viewModel.fetchItems()
        XCTAssertEqual(viewModel.items.count, 1)
        XCTAssertEqual(viewModel.items.map { $0.name }, ["Airbnb"])
        try mockEndpoint(
            mockedDataURL: MockedRequestData.addedSecrets,
            changesSince: viewModel.lastRefresh
        )

        // When
        await viewModel.fetchChanges()

        // Then
        XCTAssertEqual(viewModel.items.count, 2)

        let filteredNames = viewModel.items.map { $0.name }
        XCTAssertEqual(filteredNames, ["Airbnb", "Amazon"])
    }

    func testFetchChangesForMalformedResponse() throws {
        // If
        try mockEndpoint(
            mockedDataURL: MockedRequestData.malformed,
            changesSince: viewModel.lastRefresh
        )
        let loadingState = viewModel
            .$loadingState
            .collect(3)
            .first()
        XCTAssertNil(viewModel.lastError)
        XCTAssertEqual(viewModel.items.count, 0)

        // When
        Task {
            await viewModel.fetchChanges()
        }

        // Then
        let values = try awaitPublisher(loadingState, timeout: 5)
        XCTAssertEqual(values, [.fetchingInitial, .refreshing, .idle])

        XCTAssertEqual(viewModel.items.count, 0)
        XCTAssertNotNil(viewModel.lastError)
    }

    func testSetLastRefreshed() throws {
        // If
        let date = Date.now

        // When
        viewModel.setLastRefreshed(date)

        // Then
        XCTAssertEqual(
            UserDefaults.standard.double(forKey: UserDefaults.LastRefreshed),
            date.timeIntervalSince1970
        )
        XCTAssertEqual(
            viewModel.lastRefresh,
            Date(timeIntervalSince1970: date.timeIntervalSince1970)
        )
    }

    func testFetchItemsWhenLastRefreshIsExpired() async throws {
        // If
        try mockNonEmptyResponse()
        guard let twoDaysAgo = Calendar.current.date(byAdding: .day, value: -2, to: Date()) else {
            XCTFail("Could not get date for two days ago")
            return
        }

        // When
        viewModel.setLastRefreshed(twoDaysAgo)
        await viewModel.fetchChanges()

        // Then
        XCTAssertEqual(viewModel.items.count, 1)
        XCTAssertNotEqual(
            viewModel.lastRefresh,
            Date(timeIntervalSince1970: twoDaysAgo.timeIntervalSince1970)
        )
    }

    func testFetchChangesWhenLastRefreshIsNotExpired() async throws {
        // If
        try mockNonEmptyResponse()
        guard let sixHoursAgo = Calendar.current.date(byAdding: .hour, value: -6, to: Date()) else {
            XCTFail("Could not get date for two days ago")
            return
        }
        await viewModel.fetchItems()
        XCTAssertEqual(viewModel.items.count, 1)
        XCTAssertEqual(viewModel.items.map { $0.name }, ["Airbnb"])

        viewModel.setLastRefreshed(sixHoursAgo)
        try mockEndpoint(
            mockedDataURL: MockedRequestData.deletedSecrets,
            changesSince: viewModel.lastRefresh
        )

        // When
        await viewModel.fetchChanges()

        // Then
        XCTAssertEqual(viewModel.items.count, 0)
        XCTAssertNotEqual(
            viewModel.lastRefresh,
            Date(timeIntervalSince1970: sixHoursAgo.timeIntervalSince1970)
        )
    }

    func testNewlyAddedItem() async throws {
        let newItem = createItem(name: "Test")
        await viewModel.add(encryptedItem: newItem)
        XCTAssertEqual(viewModel.newlyAddedItem, newItem)
    }

    func testSorting() async throws {
        // When
        await viewModel.add(encryptedItem: createItem(name: "Bert"))
        await viewModel.add(encryptedItem: createItem(name: "bert"))
        await viewModel.add(encryptedItem: createItem(name: "C"))
        await viewModel.add(encryptedItem: createItem(name: "abbb"))
        await viewModel.add(encryptedItem: createItem(name: "Aaaa"))

        // Then
        let names = viewModel.items.map { $0.name }
        XCTAssertEqual(names, ["Aaaa", "abbb", "Bert", "bert", "C"])

        let filteredNames = viewModel.items.map { $0.name }
        XCTAssertEqual(filteredNames, ["Aaaa", "abbb", "Bert", "bert", "C"])
    }

    func testAddItem() async {
        // When
        let item = createItem(name: "X")
        await viewModel.add(encryptedItem: item)

        // Then
        XCTAssertEqual(viewModel.newlyAddedItem, item)
        XCTAssertTrue(viewModel.items.map({ $0.name }).contains("X"))
    }

    func testUpdatesCacheWhenAddingItems() async throws {
        // If
        let countBefore = try cache.getCachedItems().count

        // When
        let item = Item.random(name: "Added to cache")
        await viewModel.add(encryptedItem: item)

        // Then
        let countAfter = try cache.getCachedItems().count
        XCTAssertEqual(countAfter, countBefore + 1)
        XCTAssertNotNil(try cache.getCachedItems().first(where: { $0 == item }))
    }

    func testSortingAfterAddingItem() async {
        // If
        await viewModel.add(encryptedItem: createItem(name: "Bert"))
        await viewModel.add(encryptedItem: createItem(name: "bert"))
        await viewModel.add(encryptedItem: createItem(name: "C"))
        await viewModel.add(encryptedItem: createItem(name: "abbb"))
        await viewModel.add(encryptedItem: createItem(name: "Aaaa"))

        // When
        let item = createItem(name: "Baylor")
        await viewModel.add(encryptedItem: item)

        // Then
        let names = viewModel.items.map { $0.name }
        XCTAssertEqual(names, ["Aaaa", "abbb", "Baylor", "Bert", "bert", "C"])
    }

    func testUpdateItem() async {
        // If
        let item = createItem(name: "C")
        await viewModel.add(encryptedItem: createItem(name: "Bert"))
        await viewModel.add(encryptedItem: createItem(name: "bert"))
        await viewModel.add(encryptedItem: item)
        await viewModel.add(encryptedItem: createItem(name: "abbb"))
        await viewModel.add(encryptedItem: createItem(name: "Aaaa"))

        // When
        var updatedOverview = DecryptedOverview()
        updatedOverview.name = "Charlie"
        let updatedItemWrapper = ItemWrapper(
            item: item,
            overview: updatedOverview,
            details: DecryptedDetails()
        )
        viewModel.update(itemWrapper: updatedItemWrapper)

        // Then
        let names = viewModel.items.map { $0.name }
        XCTAssertEqual(names, ["Aaaa", "abbb", "Bert", "bert", "Charlie"])
    }

    // Note: test needs to be updated/fixed after fixing mocks/random inits for
    // items encrypted with new encryption
//    func testUpdatesCacheWhenUpdatingItems() async throws {
//        // If
//        let countBefore = try cache.getCachedItems().count
//        var item = Item.random(name: "Added to cache")
//        await viewModel.add(encryptedItem: item)
//
//        // When
//        item.name = "Changed"
//        await viewModel.update(item: item)
//
//        // Then
//        let countAfter = try cache.getCachedItems().count
//        XCTAssertEqual(countAfter, countBefore)
//        let cachedItem = try cache.getCachedItems().first(where: { $0 == item })
//        XCTAssertEqual(cachedItem.name, "Changed")
//    }

    func testDeleteItem() async throws {
        // If
        let bertItem = createItem(name: "Bert")
        let bertId = bertItem.id

        await viewModel.add(encryptedItem: bertItem)
        await viewModel.add(encryptedItem: createItem(name: "bert"))
        await viewModel.add(encryptedItem: createItem(name: "C"))
        await viewModel.add(encryptedItem: createItem(name: "abbb"))
        await viewModel.add(encryptedItem: createItem(name: "Aaaa"))

        // When
        viewModel.remove(item: bertItem)

        // Then
        XCTAssertFalse(viewModel.items.map({ $0.id }).contains(bertId))
    }

    func testUpdatesCacheAfterDeletingItems() async throws {
        // If
        let item = Item.random(name: "Cached item")
        await viewModel.add(encryptedItem: item)
        let countBefore = try cache.getCachedItems().count

        // When
        viewModel.remove(item: item)

        // Then
        let countAfter = try cache.getCachedItems().count
        XCTAssertEqual(countAfter, countBefore-1)
    }

    func testFilterItems() async throws {
        // if
        await viewModel.add(encryptedItem: createItem(name: "Bert"))
        await viewModel.add(encryptedItem: createItem(name: "bert"))
        await viewModel.add(encryptedItem: createItem(name: "abbb"))
        await viewModel.add(encryptedItem: createItem(name: "Aaaa"))

        // When
        viewModel.searchText = "Ber"

        // Then
        let names = viewModel.filteredItems.map { $0.name }
        XCTAssertEqual(names, ["Bert", "bert"])
    }

    func testFilterItemsUsingContains() async throws {
        // if
        await viewModel.add(encryptedItem: createItem(name: "Bert Harold"))
        await viewModel.add(encryptedItem: createItem(name: "bert"))
        await viewModel.add(encryptedItem: createItem(name: "abbb"))
        await viewModel.add(encryptedItem: createItem(name: "Aaaa"))

        // When
        viewModel.searchText = "Har"

        // Then
        let names = viewModel.filteredItems.map { $0.name }
        XCTAssertEqual(names, ["Bert Harold"])
    }

    func testFilterItemsByURL() async throws {
        // if
        await viewModel.add(encryptedItem: createItem(name: "Bert Harold"))
        await viewModel.add(encryptedItem: createItem(name: "bert"))
        await viewModel.add(encryptedItem: createItem(name: "abbb"))
        await viewModel.add(encryptedItem: createItem(name: "Aaaa", url: URL(string: "bright@gmail.com")))

        // When
        viewModel.searchText = "gmail"

        // Then
        let names = viewModel.filteredItems.map { $0.name }
        XCTAssertEqual(names, ["Aaaa"])
    }

    func testFilterItemsByInfo() async throws {
        // if
        await viewModel.add(encryptedItem: createItem(name: "Bert Harold"))
        await viewModel.add(encryptedItem: createItem(name: "bert"))
        await viewModel.add(encryptedItem: createItem(name: "abbb", username: "Bright"))
        await viewModel.add(encryptedItem: createItem(name: "Aaaa", url: URL(string: "bright@gmail.com")))

        // When
        viewModel.searchText = "bright"

        // Then
        let names = viewModel.filteredItems.map { $0.name }
        XCTAssertEqual(names, ["Aaaa", "abbb"])
    }

    func testFetchItemsForMalformedResponse() throws {
        // If
        try mockEndpoint(mockedDataURL: MockedRequestData.malformed)
        let loadingState = viewModel
            .$loadingState
            .collect(3)
            .first()
        XCTAssertNil(viewModel.lastError)
        XCTAssertEqual(viewModel.items.count, 0)

        // When
        Task {
            await viewModel.fetchItems()
        }

        // Then
        let values = try awaitPublisher(loadingState, timeout: 5)
        XCTAssertEqual(values, [.fetchingInitial, .fetchingInitial, .idle])
        // Should be same as it was before fetching
        XCTAssertEqual(viewModel.items.count, 0)
        XCTAssertNotNil(viewModel.lastError)
    }

    func testLoadsItemsFromCacheWhenMalformedResponse() throws {
        // If
        try mockEndpoint(mockedDataURL: MockedRequestData.malformed)
        let cachedItem = Item.random()
        try ItemsCache().cacheAll(items: [cachedItem])

        // When
        Task {
            await viewModel.fetchItems()
        }

        // Then
        let items = viewModel.$items.map { listItem in
            listItem.map { $0.encryptedItem }
        }
        waitForFirstValue(from: items, matching: [cachedItem])
    }

    func testActivateTaskLocking() throws {
        // If
        try mockNonEmptyResponse()
        let loadingState = viewModel
            .$loadingState
            .collect(3)
            .first()

        // When
        _ = viewModel.activate()
        _ = viewModel.activate()

        // Then
        let values = try awaitPublisher(loadingState, timeout: 5)
        XCTAssertEqual(values, [.fetchingInitial, .fetchingInitial, .idle])
        // And when TrackEvent mocking works: It shouldn't send `TrackingEvent.itemListViewed.send()` twice
    }
}
