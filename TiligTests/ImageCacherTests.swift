//
//  ImageCacheTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 19/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Tilig

final class ImageCacherTests: TiligTestCase {
    var imageCacher: ImageCacher!

    override func setUpWithError() throws {
        imageCacher = ImageCacher.shared
    }

    func testExample() throws {
        let url = URL(string: "https://asset.brandfetch.io/idHz7IYk0Z/id4QUosA_I.jpeg")!
        guard let image = UIImage(named: "logo_dark_transparent") else {
            XCTFail("Could not find asset")
            return
        }
        imageCacher.set(url: url, image: image)
        XCTAssertEqual(imageCacher.get(url: url), image)
    }
}
