//
//  DecryptedDetailsTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 26/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest

@testable import Tilig

class DecryptedDetailsTests: TiligTestCase {

    override func setUp() {
        super.setUp()
    }

    func testDetailsEncoding() throws {
        // If
        let details = DecryptedDetails(
            notes: "Hooray",
            main: [Field(kind: .username, value: "henk")],
            history: [
                HistoricVersion(kind: .password, value: "welcome123", replacedAt: Date(timeIntervalSince1970: 0))
            ],
            customFields: [
                CustomField(name: "test", kind: "random", value: "value")
            ]
        )

        // When
        let data = try JSONEncoder.snakeCaseConverting.encode(details)
        let string = String(data: data, encoding: .utf8)

        // Then
        // swiftlint:disable line_length
        XCTAssertEqual(string, "{\"custom_fields\":[{\"name\":\"test\",\"kind\":\"random\",\"value\":\"value\"}],\"main\":[{\"kind\":\"username\",\"value\":\"henk\"}],\"notes\":\"Hooray\",\"history\":[{\"kind\":\"password\",\"value\":\"welcome123\",\"replaced_at\":\"1970-01-01T00:00:00Z\"}]}")
    }

    func testFieldsWithNilValuesEncoding() throws {
        // If
        let details = DecryptedDetails(
            notes: nil,
            main: [
                Field(kind: .username, value: nil)
            ],
            customFields: [
                CustomField(name: "EmptyField", kind: "custom-field")
            ]
        )

        // When
        let data = try JSONEncoder.snakeCaseConverting.encode(details)
        let string = String(data: data, encoding: .utf8)

        // Then
        // For interop with web and Android, nil values should be encoded as `null`
        // swiftlint:disable line_length
        XCTAssertEqual(string, "{\"custom_fields\":[{\"name\":\"EmptyField\",\"kind\":\"custom-field\",\"value\":null}],\"main\":[{\"kind\":\"username\",\"value\":null}],\"notes\":null,\"history\":[]}")
    }

    func testDetailsDecoding() throws {
        // If
        let json = "{\"custom_fields\":[{\"name\":\"test\",\"kind\":\"random\",\"value\":\"value\"}],\"main\":[{\"kind\":\"username\",\"value\":\"henk\"}],\"notes\":\"Hooray\",\"history\":[{\"kind\":\"password\",\"value\":\"welcome123\",\"replaced_at\":\"1970-01-01T00:00:00Z\"}]}"

        // when
        let data = try XCTUnwrap(json.data(using: .utf8))
        let jsonDecoder = JSONDecoder.snakeCaseConverting
        let details = try jsonDecoder.decode(DecryptedDetails.self, from: data)

        // Then
        XCTAssertEqual(details, DecryptedDetails(
            notes: "Hooray",
            main: [Field(kind: .username, value: "henk")],
            history: [
                HistoricVersion(kind: .password, value: "welcome123", replacedAt: Date(timeIntervalSince1970: 0))
            ],
            customFields: [CustomField(name: "test", kind: "random", value: "value")]
        ))
    }

    func testUpdateFromLegacyFields() throws {
        // If
        var details = DecryptedDetails(
            notes: "Le detail",
            main: [Field(kind: .password, value: "super_secure")],
            history: [
                HistoricVersion(kind: .password, value: "welcome123", replacedAt: Date())
            ]
        )
        let legacyFields = LegacyFields(
            username: "fred_durst",
            password: "nookie",
            notes: "red_hat",
            otp: "1232"
        )

        // When
        details.update(fromLegacyFields: legacyFields)

        // Then
        XCTAssertEqual(
            details.main.first(where: { $0.kind == .username })?.value,
            legacyFields.username
        )
        XCTAssertEqual(
            details.main.first(where: { $0.kind == .password })?.value,
            legacyFields.password
        )
        XCTAssertEqual(
            details.notes, legacyFields.notes
        )
        XCTAssertEqual(
            details.main.first(where: { $0.kind == .totp })?.value,
            legacyFields.otp
        )
    }

    func testFindInMain() throws {
        // If
        let details = DecryptedDetails(
            notes: "Le detail",
            main: [
                Field(kind: .password, value: "super_secure"),
                Field(kind: .username, value: "SexyFrog123"),
                Field(kind: .ssid, value: "RandomSSID")
            ],
            history: [
                HistoricVersion(kind: .password, value: "welcome123", replacedAt: Date())
            ]
        )

        // When
        let usernameField = details.findInMain(kind: .username)
        let passwordField = details.findInMain(kind: .password)
        let ssidField = details.findInMain(kind: .ssid)

        // Then
        XCTAssertEqual(passwordField?.kind, .password)
        XCTAssertEqual(passwordField?.value, "super_secure")
        XCTAssertEqual(usernameField?.kind, .username)
        XCTAssertEqual(usernameField?.value, "SexyFrog123")
        XCTAssertEqual(ssidField?.kind, .ssid)
        XCTAssertEqual(ssidField?.value, "RandomSSID")
    }

    func testPasswordHistoryDecryption() {
        let timestamp = "2022-09-20T14:27:47.382Z"
        let password = "test"

        // TODO
    }

    func testTrackPasswordChangeWithNoHistory() throws {
        // If
        let previousDetails = DecryptedDetails(
            notes: "Hooray",
            main: [
                Field(kind: .username, value: "henk"),
                Field(kind: .password, value: "welcome123")
            ]
        )

        var newDetails = DecryptedDetails(
            notes: "Hooray",
            main: [
                Field(kind: .username, value: "henk"),
                Field(kind: .password, value: "randomPassword123")
            ]
        )

        // When
        newDetails.trackPasswordChange(previousDetails: previousDetails)

        // Then
        XCTAssertEqual(newDetails.history.count, 1)
        XCTAssertEqual(newDetails.history.last?.value, "welcome123")
    }

    func testTrackPasswordChangeWithHistory() throws {
        // If
        let previousDetails = DecryptedDetails(
            notes: "Hooray",
            main: [
                Field(kind: .username, value: "henk"),
                Field(kind: .password, value: "welcome123")
            ],
            history: [
                HistoricVersion(kind: .password, value: "oldPassword123", replacedAt: Date(timeIntervalSince1970: 0))
            ]
        )

        var newDetails = DecryptedDetails(
            notes: "Hooray",
            main: [
                Field(kind: .username, value: "henk"),
                Field(kind: .password, value: "randomPassword123")
            ],
            history: [
                HistoricVersion(kind: .password, value: "oldPassword123", replacedAt: Date(timeIntervalSince1970: 0))
            ]
        )

        // When
        newDetails.trackPasswordChange(previousDetails: previousDetails)

        // Then
        XCTAssertEqual(newDetails.history.count, 2)
        XCTAssertEqual(newDetails.history.last?.value, "welcome123")
    }
}
