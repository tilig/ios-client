//
//  KeyPairTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 16/12/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Sodium

@testable import Tilig

class KeyPairTests: TiligTestCase {

    override func setUp() {
        super.setUp()
    }

    func testKeyPairEncoding() throws {
        // If
        let sodium = Sodium()
        let keyPair = try XCTUnwrap(sodium.box.keyPair())

        // When
        let data = try JSONEncoder().encode(keyPair)
        let decodedKeyPair = try JSONDecoder().decode(Box.KeyPair.self, from: data)

        // Then
        XCTAssertEqual(decodedKeyPair.publicKey, keyPair.publicKey)
        XCTAssertEqual(decodedKeyPair.secretKey, keyPair.secretKey)
        XCTAssertEqual(decodedKeyPair, keyPair)
    }
}
