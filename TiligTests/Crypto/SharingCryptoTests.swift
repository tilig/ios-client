//
//  SharingCryptoTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 07/02/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import XCTest
import Sodium

final class SharingCryptoTests: TiligTestCase {

    var crypto: Crypto!
    var sharingCrypto: SharingCrypto!

    var item: Item!
    var shareeKeyPair: Box.KeyPair!

    override func setUpWithError() throws {
        try super.setUpWithError()

        crypto = Crypto(
            keyPair: .random(),
            legacyKeyPair: .random()
        )

        sharingCrypto = SharingCrypto(crypto: crypto)

        shareeKeyPair = try Box.KeyPair.generate()

        let dek = try crypto.generateDek()
        let encryptedDek = try crypto.encrypt(dek: dek)
        item = Item(id: UUID(), encryptedDek: encryptedDek)
    }

    func testUpgradedFolderDecryption() throws {
        // When
        let payload = try sharingCrypto.upgradeToFolder(
            item: item,
            shareePublicKey: shareeKeyPair.publicKey
        )

        // Then
        XCTAssertEqual(item.id, payload.itemID)
        XCTAssertNotEqual(item.encryptedDek, payload.encryptedFolderDek)

        let decryptedKek = try crypto.decrypt(
            encryptedPrivateKek: payload.ownEncryptedPrivateKek,
            withKeyPair: crypto.keyPair
        )
    }

    func testPrivateKekEncryption() throws {
        // When
        let payload = try sharingCrypto.upgradeToFolder(
            item: item,
            shareePublicKey: shareeKeyPair.publicKey
        )

        // Then
        // Decrypt the user's private KEK
        let privateKek = try crypto.decrypt(
            encryptedPrivateKek: payload.ownEncryptedPrivateKek,
            withKeyPair: crypto.keyPair
        )
        // Decrypt the other user's private KEK
        let otherPrivateKek = try crypto.decrypt(
            encryptedPrivateKek: payload.otherEncryptedPrivateKek ?? "",
            withKeyPair: shareeKeyPair
        )
        XCTAssertEqual(privateKek, otherPrivateKek)
    }

    func testKEK() throws {
        // When
        let payload = try sharingCrypto.upgradeToFolder(
            item: item,
            shareePublicKey: shareeKeyPair.publicKey
        )

        // Then
        let privateKek = try crypto.decrypt(
            encryptedPrivateKek: payload.ownEncryptedPrivateKek,
            withKeyPair: crypto.keyPair
        )
        let publicKek = payload.publicKek
        let folderKekPair = Box.KeyPair(
            publicKey: publicKek,
            secretKey: privateKek
        )

        let decryptedFolderDek = try crypto.decrypt(
            encryptedDek: payload.encryptedFolderDek,
            withKeyPair: folderKekPair
        )
    }
}
