//
//  LegacyCryptoTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 07/02/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import XCTest

final class LegacyCryptoTests: TiligTestCase {

    var crypto: Crypto!

    override func setUpWithError() throws {
        try super.setUpWithError()

        crypto = Crypto(
            keyPair: .random(),
            legacyKeyPair: .random()
        )
    }

    // MARK: Legacy sync

    func testLegacyFieldsDecryption() throws {
        // If
        var item = Item.random()
        item.username = try crypto.legacyKeyPair.encrypt(plainText: "Bono2000")
        item.password = try crypto.legacyKeyPair.encrypt(plainText: "U2")
        item.notes = try crypto.legacyKeyPair.encrypt(plainText: "I love Ireland")
        item.otp = try crypto.legacyKeyPair.encrypt(plainText: "123")

        // When
        let decryptedLegacyFields = try crypto.decryptLegacyFields(item: item)

        XCTAssertEqual(decryptedLegacyFields.username, "Bono2000")
        XCTAssertEqual(decryptedLegacyFields.password, "U2")
        XCTAssertEqual(decryptedLegacyFields.notes, "I love Ireland")
        XCTAssertEqual(decryptedLegacyFields.otp, "123")
    }

    func testDecryptsFullItemWhenLegacyEncryptionEnabled() throws {
        // If
        var item = Item.random()
        item.username = try crypto.legacyKeyPair.encrypt(plainText: "Bono2000")
        item.password = try crypto.legacyKeyPair.encrypt(plainText: "U2")
        item.notes = try crypto.legacyKeyPair.encrypt(plainText: "I love Ireland")
        item.otp = try crypto.legacyKeyPair.encrypt(plainText: "123")

        // When
        let decrypted = try crypto.decrypt(fullItem: item)

        XCTAssertEqual(decrypted.details.findInMain(kind: .username)?.value, "Bono2000")
        XCTAssertEqual(decrypted.details.findInMain(kind: .password)?.value, "U2")
        XCTAssertEqual(decrypted.details.notes, "I love Ireland")
        XCTAssertEqual(decrypted.details.findInMain(kind: .totp)?.value, "123")
    }

    func testDoesNotDecryptLegacyFieldsWhenDisabled() throws {
        // If
        var item = Item.random()
        item.legacyEncryptionDisabled = true
        item.username = try crypto.legacyKeyPair.encrypt(plainText: "Bono2000")
        item.password = try crypto.legacyKeyPair.encrypt(plainText: "U2")
        item.notes = try crypto.legacyKeyPair.encrypt(plainText: "I love Ireland")
        item.otp = try crypto.legacyKeyPair.encrypt(plainText: "123")

        // When
        let decrypted = try crypto.decrypt(fullItem: item)

        XCTAssertNotEqual(decrypted.details.findInMain(kind: .username)?.value, "Bono2000")
        XCTAssertNotEqual(decrypted.details.findInMain(kind: .password)?.value, "U2")
        XCTAssertNotEqual(decrypted.details.notes, "I love Ireland")
        XCTAssertNotEqual(decrypted.details.findInMain(kind: .totp)?.value, "123")
    }

    func testUpdateOverviewFromLegacyFields() throws {
        // If
        var overview = DecryptedOverview(
            name: "Overwrite me",
            info: nil,
            urls: [],
            androidAppIds: []
        )
        // Decrypted legacy fields
        let legacyFields = LegacyFields(
            username: "legacy_field_username",
            password: "legacy_field_password",
            notes: "legacy_field_note",
            otp: "legacy_otp"
        )
        // Legacy item with encrypted fields
        var item = Item.random(
            encryptedWithKeyPair: .random(),
            name: "Item name",
            plainUsername: legacyFields.username,
            plainPassword: legacyFields.password,
            url: URL(string: "www.legacy-url.com")!,
            notes: legacyFields.notes
        )

        item.androidAppIds = ["legacy_app_id"]

        // When
        overview.update(fromLegacyFields: legacyFields, andItem: item)

        // Then
        XCTAssertEqual(overview.name, "Item name")
        XCTAssertEqual(overview.info, "legacy_field_username")
        // Overview URL should not have a name, because it's the primary URL
        XCTAssertEqual(overview.urls, [
            OverviewURL(url: URL(string: "www.legacy-url.com")!)
        ])
        XCTAssertEqual(overview.androidAppIds, ["legacy_app_id"])
    }

    func testUpdateFromLegacyHistory() throws {
        // If
        let legacyKeyPair = LegacyKeyPair.random()
        let password1 = try legacyKeyPair.encrypt(plainText: "legacy-1")
        let password2 = try legacyKeyPair.encrypt(plainText: "legacy-2")

        let legacyHistory = [
            LegacyPasswordVersion(password: password1, createdAt: "2022-08-01T12:28:37.423Z"),
            LegacyPasswordVersion(password: password2, createdAt: "2022-07-31T23:28:37.423Z")
        ]
        var details = DecryptedDetails()

        // When
        try details.update(fromLegacyPasswordHistory: legacyHistory, legacyKeyPair: legacyKeyPair)

        // Then
        XCTAssertEqual(details.history.count, 2)
        XCTAssertEqual(details.history[0].value, "legacy-1")
        XCTAssertEqual(details.history[1].value, "legacy-2")
        XCTAssertNotNil(details.history[0].replacedAt)
        XCTAssertNotNil(details.history[1].replacedAt)
    }

    func testOverwritingLegacyFieldsWithLegacyEncryption() throws {
        // If
        let legacyKeyPair = crypto.legacyKeyPair
        let item = Item(
            template: .loginV1,
            name: "The name",
            // Note: legacy encryption enabled!
            legacyEncryptionDisabled: false,
            username: try legacyKeyPair.encrypt(plainText: "le_username"),
            password: try legacyKeyPair.encrypt(plainText: "le_password"),
            notes: try legacyKeyPair.encrypt(plainText: "notes"),
            otp: try legacyKeyPair.encrypt(plainText: "otp")
        )

        // When
        let encryptedItem = try crypto.encrypt(
            item: item,
            // Overwriting with an empty overview
            overview: DecryptedOverview(),
            // Overwriting with empty details
            details: DecryptedDetails()
        )

        // Then
        XCTAssertNotNil(encryptedItem.rsaEncryptedDek)
        XCTAssertNotNil(encryptedItem.encryptedOverview)
        XCTAssertNotNil(encryptedItem.encryptedDetails)
        // All encryped legacy fields should be nill
        XCTAssertNil(encryptedItem.username)
        XCTAssertNil(encryptedItem.password)
        XCTAssertNil(encryptedItem.notes)
        XCTAssertNil(encryptedItem.otp)
    }

    func testOverwritingLegacyFieldsWithoutLegacyEncryption() throws {
        // If
        let legacyKeyPair = crypto.legacyKeyPair
        let item = Item(
            template: .loginV1,
            name: "The name",
            // Note: no legacy encryption!
            legacyEncryptionDisabled: true,
            username: try legacyKeyPair.encrypt(plainText: "le_username"),
            password: try legacyKeyPair.encrypt(plainText: "le_password"),
            notes: try legacyKeyPair.encrypt(plainText: "notes"),
            otp: try legacyKeyPair.encrypt(plainText: "otp")
        )

        // When
        let encryptedItem = try crypto.encrypt(
            item: item,
            // Overwriting with an empty overview
            overview: DecryptedOverview(),
            // Overwriting with empty details
            details: DecryptedDetails()
        )

        // Then
        XCTAssertNotNil(encryptedItem.rsaEncryptedDek)
        XCTAssertNotNil(encryptedItem.encryptedOverview)
        XCTAssertNotNil(encryptedItem.encryptedDetails)
        // All encryped legacy fields should be unchanged
        XCTAssertEqual(encryptedItem.username, item.username)
        XCTAssertEqual(encryptedItem.password, item.password)
        XCTAssertEqual(encryptedItem.notes, item.notes)
        XCTAssertEqual(encryptedItem.otp, item.otp)
    }

    func testDoesNotEncryptLegacyFieldsWhenLegacyEncryptionDisabled() throws {
        // If
        let keyPair = crypto.legacyKeyPair
        let item = Item(
            template: .loginV1,
            name: "The name",
            legacyEncryptionDisabled: true,
            username: try keyPair.encrypt(plainText: "le_username"),
            password: try keyPair.encrypt(plainText: "le_password"),
            notes: try keyPair.encrypt(plainText: "notes"),
            otp: try keyPair.encrypt(plainText: "otp")
        )

        // When
        // Overwriting with an empty overview
        let overview = DecryptedOverview()
        // Overwriting with empty details
        let details = DecryptedDetails()
        let encryptedItem = try crypto.encrypt(
            item: item,
            overview: overview,
            details: details
        )

        // Then
        XCTAssertNotNil(encryptedItem.rsaEncryptedDek)
        XCTAssertNotNil(encryptedItem.encryptedOverview)
        XCTAssertNotNil(encryptedItem.encryptedDetails)
        // All encryped legacy fields should be the same as they were
        XCTAssertEqual(encryptedItem.username, item.username)
        XCTAssertEqual(encryptedItem.password, item.password)
        XCTAssertEqual(encryptedItem.notes, item.notes)
        XCTAssertEqual(encryptedItem.otp, item.otp)
    }

}
