//
//  LegacyItem.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 29/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest

class LegacyItemTests: TiligTestCase {

    override func setUpWithError() throws {
        try super.setUpWithError()
    }

    func testEncryptLegacyFieldsWhenChangingAllFields() throws {
        // If
        let keyPair = LegacyKeyPair.random()
        let originalFields = LegacyFields(
            username: "gertje",
            password: "secure123",
            notes: "Don't eat yellow snow.",
            otp: "otp-123"
        )
        let item = Item.random(
            encryptedWithKeyPair: keyPair,
            name: "Test",
            plainUsername: originalFields.username,
            plainPassword: originalFields.password,
            otp: originalFields.otp,
            notes: originalFields.notes
        )

        // When
        // Changing all encrypted fields
        let newFields = LegacyFields(
            username: "henkje",
            password: "lalala",
            notes: "Don't eat blue snow.",
            otp: "kroketjes"
        )
        var updatedItem = item
        try updatedItem.encryptLegacyFields(newFields: newFields, originalFields: originalFields, legacyKeyPair: keyPair)

        // Then
        XCTAssertNotEqual(updatedItem.username, item.username)
        XCTAssertNotEqual(updatedItem.password, item.password)
        XCTAssertNotEqual(updatedItem.notes, item.notes)
        XCTAssertNotEqual(updatedItem.otp, item.otp)

        XCTAssertEqual(
            try keyPair.decrypt(cipherText: updatedItem.username ?? ""),
            "henkje"
        )
        XCTAssertEqual(
            try keyPair.decrypt(cipherText: updatedItem.password ?? ""),
            "lalala"
        )
        XCTAssertEqual(
            try keyPair.decrypt(cipherText: updatedItem.notes ?? ""),
            "Don't eat blue snow."
        )
        XCTAssertEqual(
            try keyPair.decrypt(cipherText: updatedItem.otp ?? ""),
            "kroketjes"
        )
    }

    func testEncryptLegacyFieldsWithNilValues() throws {
        // If
        let legacyKeyPair = try LegacyKeyPair.generateRSAKeyPair()
        let originalFields = LegacyFields(
            username: "gertje",
            password: "secure123",
            notes: "Don't eat yellow snow.",
            otp: "otp-123"
        )
        let item = Item.random(
            encryptedWithKeyPair: legacyKeyPair,
            name: "Test",
            plainUsername: originalFields.username,
            plainPassword: originalFields.password,
            otp: originalFields.otp,
            notes: originalFields.notes
        )

        // When
        // Changing only the username
        let newFields = LegacyFields(username: "bono2000")
        var updatedItem = item
        try updatedItem.encryptLegacyFields(
            newFields: newFields,
            originalFields: originalFields,
            legacyKeyPair: legacyKeyPair
        )

        // Then
        XCTAssertNotEqual(updatedItem.username, item.username)
        XCTAssertNil(updatedItem.password)
        XCTAssertNil(updatedItem.notes)
        XCTAssertNil(updatedItem.otp)
    }

}
