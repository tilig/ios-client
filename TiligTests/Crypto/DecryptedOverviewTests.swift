//
//  DecryptedOverviewTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 26/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest

class DecryptedOverviewTests: TiligTestCase {

    override func setUp() {
        super.setUp()
    }

    func testOverviewEncoding() throws {
        // If
        let overview = DecryptedOverview(
            name: "Volkswagen",
            info: "Some random info",
            urls: [
                OverviewURL(
                    name: "Volkswagen",
                    url: URL(string: "www.volkswagen.nl")!
                )
            ],
            androidAppIds: ["volkswagen-android-app-id"]
        )

        // When
        let data = try JSONEncoder.snakeCaseConverting.encode(overview)
        let string = String(data: data, encoding: .utf8)

        // Then
        // swiftlint:disable line_length
        XCTAssertEqual(string, "{\"info\":\"Some random info\",\"android_app_ids\":[\"volkswagen-android-app-id\"],\"name\":\"Volkswagen\",\"urls\":[{\"name\":\"Volkswagen\",\"url\":\"www.volkswagen.nl\"}]}")
    }

    func testOverviewWithNilValuesEncoding() throws {
        // If
        let overview = DecryptedOverview(name: nil, info: nil, urls: [], androidAppIds: [])

        // When
        let data = try JSONEncoder.snakeCaseConverting.encode(overview)
        let string = String(data: data, encoding: .utf8)

        // Then
        // For interop with web and Android, nil values should be encoded as `null`
        XCTAssertEqual(string, "{\"info\":null,\"android_app_ids\":[],\"name\":null,\"urls\":[]}")
    }

}
