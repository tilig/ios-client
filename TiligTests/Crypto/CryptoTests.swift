//
//  CryptoTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 13/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Sodium

@testable import Tilig

class CryptoTests: TiligTestCase {

    // swiftlint:disable line_length
    let knownPrivateKey = Box.KeyPair.PublicKey(arrayLiteral: 74, 128, 112, 63, 186, 16, 170, 79, 232, 30, 126, 87, 192, 168, 73, 155, 91, 26, 223, 144, 250, 218, 20, 112, 198, 120, 151, 181, 118, 99, 253, 245)
    // swiftlint:disable line_length
    let knownPublicKey = Box.KeyPair.SecretKey(arrayLiteral: 250, 42, 11, 40, 18, 146, 64, 81, 110, 153, 175, 57, 239, 10, 191, 161, 59, 75, 34, 99, 208, 24, 95, 34, 191, 209, 80, 201, 237, 216, 41, 116)

    /// The keypair to use for this test.
    /// It's also possible to use Box.KeyPair.random().
    var knownKeyPair: Box.KeyPair {
        Box.KeyPair(publicKey: knownPublicKey, secretKey: knownPrivateKey)
    }

    var crypto: Crypto!

    override func setUpWithError() throws {
        try super.setUpWithError()

        crypto = Crypto(
            keyPair: knownKeyPair,
            legacyKeyPair: .random()
        )
    }

    // MARK: DEK encryption/decryption

    func testDekEncryptionRoundTrip() throws {
        // If
        let dek = try crypto.generateDek()

        // When
        let encryptedDek = try crypto.encrypt(dek: dek)
        let decryptedDek = try crypto.decrypt(encryptedDek: encryptedDek)

        // Then
        XCTAssertEqual(decryptedDek, dek)
    }

    // MARK: Legacy DEK encryption/decryption

    func testLegacyDekEncryptionRoundTrip() throws {
        // If
        let dek = try crypto.generateDek()

        // When
        let rsaEncryptedDek = try crypto.legacyEncrypt(dek: dek)
        let decryptedDek = try crypto.legacyDecrypt(rsaEncryptedDek: rsaEncryptedDek)

        // Then
        XCTAssertEqual(decryptedDek, dek)
    }

    // MARK: Failures

    func testEncryptingItemWithTheWrongLegacyKeyPairFails() throws {
        // If
        let wrongLegacyKeyPair = LegacyKeyPair.random()
        let item = Item(
            template: .loginV1,
            name: "The name",
            // Note: using legacy encryption
            legacyEncryptionDisabled: false,
            username: try wrongLegacyKeyPair.encrypt(plainText: "le_username"),
            password: try wrongLegacyKeyPair.encrypt(plainText: "le_password")
        )

        // When
        XCTAssertThrowsError(try crypto.encrypt(
            item: item,
            overview: DecryptedOverview(),
            details: DecryptedDetails()
        ))
    }

    // MARK: Payload encryption/decryption

    let knownDek: Bytes = [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1
    ]

    func testThrowsWhenDecryptingWithPadding() throws {
        // If
        let cipherText = "LMMpHMCsjCPq5eMHsMn-fPJHDNOuMrwY0JK10MiJVdDiwppGJRpMZWs-1DKD6myxrLg="

        // When + Then
        XCTAssertThrowsError(try crypto.decrypt(payload: cipherText, dek: knownDek))
    }

    func testDecryptsPayloadFromiOS() throws {
        // If
        // Known plaintext: 'my payload' (dek = random, nonce = no idea)
        // Created with `try crypto.encrypt(payload: "my payload", dek: knownDek)`
        let cipherText = "WwpjH6gGd_77-TfwOyrycnx05EPNMkdVDSNCv1NfG0Fy0ddYb20d_XiUjIuRW-Wj22w"

        // When
        let payload = try crypto.decrypt(payload: cipherText, dek: knownDek)

        // Then
        XCTAssertEqual(payload, "my payload")
    }

    func testDecryptsPayloadFromWeb() throws {
        // If
        // Known plaintext: 'my payload' (dek = knownDek, nonce = 1)
        let cipherText = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB25a-x4NcK3dXZsBP9UCEU3_2eJ2JeN6EfLQ"

        // When
        let payload = try crypto.decrypt(payload: cipherText, dek: knownDek)

        // Then
        XCTAssertEqual(payload, "my payload")
    }

    func testDecryptsPayloadsWithNewlines() throws {
        // If
        // Known plaintext: 'my payload' (dek = knownDek, nonce = 1)
        let cipherText = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB25a\n-x4NcK3dXZsBP9UCEU3_2eJ2JeN6EfLQ"

        // When
        let payload = try crypto.decrypt(payload: cipherText, dek: knownDek)

        // Then
        XCTAssertEqual(payload, "my payload")
    }

    func testPayloadEncryptionRoundTrip() throws {
        // If
        let payload = "Some string"
        let dek = try crypto.generateDek()

        // When
        let encrypted = try crypto.encrypt(payload: payload, dek: dek)
        let decrypted = try crypto.decrypt(payload: encrypted, dek: dek)

        // Then
        XCTAssertEqual(decrypted, payload)
    }

    // MARK: Overiew encryption/decryption

    func testOverviewEncryptionRoundTrip() throws {
        // If
        let dek = try crypto.generateDek()
        let url = URL(string: "https://www.tilig.com/")!
        let overview = DecryptedOverview(
            name: "Test",
            info: "Info",
            urls: [OverviewURL(name: "Test", url: url)]
        )

        // When
        let encryptedOverview = try crypto.encrypt(overview: overview, dek: dek)
        let decryptedOverview = try crypto.decrypt(encryptedOverview: encryptedOverview, dek: dek)

        // Then
        XCTAssertEqual(overview, decryptedOverview)
    }

    func testDecryptsOverviewForV1Item() throws {
        // If
        // TODO: encrypt with knownKeyPair and a known item instance,
        // instead of relying on the .random() one.
        let v1 = Item.random(
            encryptedWithKeyPair: crypto.legacyKeyPair,
            name: "Old name",
            url: URL(string: "old.website")!
        )

        // When
        let overview = try crypto.decryptOverview(encryptedItem: v1)

        // Then
        XCTAssertEqual(overview.name, "Old name")
        XCTAssertEqual(overview.urls.first?.url?.absoluteString, "old.website")
    }

    func testDecryptsOverviewForV2Item() throws {
        // TODO
    }

    // MARK: Details encryption/decryption

    func testDetailsEncryptionRoundTrip() throws {
        // If
        let dek = try crypto.generateDek()
        let details = DecryptedDetails(
            notes: "Hooray",
            main: [Field(kind: .username, value: "henk")],
            history: [
                HistoricVersion(kind: .password, value: "welcome123", replacedAt: Date(timeIntervalSince1970: 0))
            ],
            customFields: [CustomField(name: "test", kind: "random", value: "value")]
        )

        // When
        let encryptedDetails = try crypto.encrypt(details: details, dek: dek)
        let decryptedDetails = try crypto.decrypt(encryptedDetails: encryptedDetails, dek: dek)

        // Then
        XCTAssertEqual(details, decryptedDetails)
    }

    // MARK: Item encryption/decryption

    func testAssignsEncryptedDEKsToItem() throws {
        // If
        let item = Item()
        XCTAssertNil(item.encryptedDek)
        XCTAssertNil(item.rsaEncryptedDek)

        // When
        let updatedItem = try crypto.encrypt(
            item: item,
            overview: DecryptedOverview(),
            details: DecryptedDetails()
        )

        // Then
        XCTAssertNotNil(updatedItem.encryptedDek)
        XCTAssertNotNil(updatedItem.rsaEncryptedDek)
        // Test for equality too
        let encryptedDek = try XCTUnwrap(updatedItem.encryptedDek)
        let rsaEncryptedDek = try XCTUnwrap(updatedItem.rsaEncryptedDek)
        let decryptedDek = try crypto.decrypt(encryptedDek: encryptedDek)
        let legacyDecryptedDek = try crypto.legacyDecrypt(rsaEncryptedDek: rsaEncryptedDek)
        XCTAssertEqual(decryptedDek, legacyDecryptedDek)
    }

    /// Test full decryption using Libsodium keypair, so not using the RSA keypair
    func testDecryptFullItem() throws {
        // If
        var encrypted = try crypto.encrypt(
            item: Item(),
            overview: DecryptedOverview(),
            details: DecryptedDetails(notes: "Test notes")
        )
        // Make sure there's no legacy encrypted DEK
        encrypted.rsaEncryptedDek = nil
        XCTAssertNotNil(encrypted.encryptedDek)

        // When
        let decrypted = try crypto.decrypt(fullItem: encrypted)

        // Then
        XCTAssertEqual(decrypted.details.notes, "Test notes")
    }

    // MARK: Folder KEK encryption

    func testKekEncryptionRoundTrip() throws {
        // If
        let somePrivateKey = try Box.KeyPair.generate().secretKey

        // When
        let encryptedDek = try crypto.encrypt(
            privateKek: somePrivateKey,
            withPublicKey: knownPublicKey
        )
        //        print("----- Encrypted key -----")
        //        print(encryptedDek)
        //        print("-------------------------")
        let decryptedKey = try crypto.decrypt(
            encryptedPrivateKek: encryptedDek,
            withKeyPair: knownKeyPair
        )

        // Then
        XCTAssertEqual(decryptedKey, somePrivateKey)
    }

    func testDekEncryptionRoundtripWithKek() throws {
        // If
        let someDek = try crypto.generateDek()
        let folderKek = try Box.KeyPair.generate()

        // When
        let encryptedDek = try crypto.encrypt(
            key: someDek, withKeyPair: folderKek
        )
        let decryptedDek = try crypto.decrypt(
            encryptedDek: encryptedDek,
            withKeyPair: folderKek
        )

        // Then
        XCTAssertEqual(decryptedDek, someDek)
    }

}
