//
//  CryptoInteropTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 20/12/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Sodium

@testable import Tilig

class CryptoInteropTests: TiligTestCase {

    override func setUpWithError() throws {
        try super.setUpWithError()
    }

    let knownDek: Bytes = [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1
    ]

    func testDecryptsPayloadFromAndroid() throws {
        // If
        let intArr: [Int8] = [
            111, -97, 73, 127, 110, -75, -25, 9, -6, -17, 17, 77, 89, -16, 69, 36, -73,
            -46, 73, 54, 126, -2, 125, 15, -32, 73, -50, 42, -12, 101, -70, 98
        ]
        // In Java, byte is an 8-bit signed data type, so convert it to UInt.
        let dek = unsafeBitCast(intArr, to: [UInt8].self)
        let cipherText = "eX0sefzE1yKmDLIwgxtPFb_OLd0501FbBnbjXf87H8_igqzcwenXXfm706ZU_JPjfQ6QjumkWikGq_1u"

        // When
        let payload = try Crypto.random().decrypt(payload: cipherText, dek: dek)

        // Then
        XCTAssertEqual(payload, "payload from android")
    }

    // MARK: Android interop

    let androidPrivateKey = "HVJt_MxHlE1T7JK9nQO9Z5uwumsOEaMYYYKSChmag7s"
    let androidPublicKey = "3imoVUEjSEB_jr48iCivmYem2fIuxxdGWmMw9QmUz3k"
    // swiftlint:disable line_length
    let androidEncryptedKey = "yuy1J6bgEYEaG0cWAqzsoFmUB5uSdB7H1UWUyj5Ut6IdojKna4Jz370zv5-REvhCI7jDmTQ3_mbV2j4hZVJlJx5DLSQVXX6z"
    let payloadEncrypted = "gVRv39EIFnnT0ZqJUNx7hl5xayBfZcJVsvLKnMKzVboURXXJwDHETuPcd8SCJLMXHMSj8F2A__v2CPA0"

    func testAndroidKeyPair() throws {
        // If
        let publicKeyBytes = try XCTUnwrap(Box.KeyPair.base642bin(androidPublicKey))
        let privateKeyBytes = try XCTUnwrap(Box.KeyPair.base642bin(androidPrivateKey))
        let androidKeyPair = Box.KeyPair(publicKey: publicKeyBytes, secretKey: privateKeyBytes)

        // When
        let crypto = Crypto(keyPair: androidKeyPair, legacyKeyPair: .random())

        // Then
        XCTAssertNoThrow(try crypto.encrypt(dek: knownDek))
    }

    func testAndroidDekAndPayloadDecryption() throws {
        // If
        let publicKeyBytes = try XCTUnwrap(Box.KeyPair.base642bin(androidPublicKey))
        let privateKeyBytes = try XCTUnwrap(Box.KeyPair.base642bin(androidPrivateKey))
        let androidKeyPair = Box.KeyPair(publicKey: publicKeyBytes, secretKey: privateKeyBytes)
        let crypto = Crypto(keyPair: androidKeyPair, legacyKeyPair: .random())

        // When
        let dek = try crypto.decrypt(encryptedDek: androidEncryptedKey)
        let payload = try crypto.decrypt(payload: payloadEncrypted, dek: dek)

        // Then
        XCTAssertEqual(payload, "Message from android")
    }

    // MARK: Web interop

    let webPrivateKey = "OVkDL2pzAemehYACNdc7tB7wYB3qejxKiZ2WpPOnr50"
    let webPublicKey = "7gpk9z5P-M1LrmMR4upBOCKWsIiSbEno3RlTqOufBA4"
    // swiftlint:disable line_length
    let webEncryptedKey = "btoctTEaM0M6S8zEkrlZX5o8Ww_ssZTaMXDpAhVZVy9NBCN0PMktI2RodL-s1KHydqiTjfnBtUBmJ49o57pCPK6VVpVUipU_"
    // let payloadEncrypted = "eX0sefzE1yKmDLIwgxtPFb_OLd0501FbBnbjXf87H8_igqzcwenXXfm706ZU_JPjfQ6QjumkWikGq_1u"

    func testWebKeyPair() throws {
        // If
        let publicKeyBytes = try XCTUnwrap(Box.KeyPair.base642bin(webPublicKey))
        let privateKeyBytes = try XCTUnwrap(Box.KeyPair.base642bin(webPrivateKey))
        let webKeyPair = Box.KeyPair(publicKey: publicKeyBytes, secretKey: privateKeyBytes)

        // When
        let crypto = Crypto(keyPair: webKeyPair, legacyKeyPair: .random())
        XCTAssertNoThrow(try crypto.encrypt(dek: knownDek))
    }

    //    func testWebDekDecryption() throws {
    //        // If
    //        let publicKeyBytes = try XCTUnwrap(Box.KeyPair.base642bin(androidPublicKey))
    //        let privateKeyBytes = try XCTUnwrap(Box.KeyPair.base642bin(androidPrivateKey))
    //        let androidKeyPair = Box.KeyPair(publicKey: publicKeyBytes, secretKey: privateKeyBytes)
    //        let crypto = Crypto(keyPair: androidKeyPair, legacyKeyPair: .random())
    //
    //        let test = try crypto.encrypt(dek: [0, 0, 0])
    //
    //        // When
    //        let dek = try crypto.decrypt(rsaEncryptedDek: androidEncryptedKey)
    //        let payload = try crypto.decrypt(payload: payloadEncrypted, dek: dek)
    //
    //        // Then
    //        XCTAssertEqual(payload, "")
    //    }

    // MARK: iOS payloads

    let iosPublicKey = "DN25970xN8lE-h6jBJ_cCidduNlamFyIkc73hBCX9gs"
    let iosPrivateKey = "8lgIsUJ30ds21RyZwwJfxIx98j4ymzM6Ws1MxHmXCng"
    // swiftlint:disable line_length
    let iosEncryptedKey = "EcKWbo80Z099cN47-UPTN7C4dchSCgOK0hp4v1fL8CZDyBF018uj6dAyn0zLp5N5FGPmXnlHINX5QguU93Mnn2x5PlxYBnaP"
    let iosPayloadEncrypted = "90Iub29FZgZiJWOy1bCssmTM7b6lDpbKhgnfbE1yvXrfl-8_RDaF4Og0FZgg8cWW4YU--kaiEA"

    func testiOSKeyPair() throws {
        // If
        let publicKeyBytes = try XCTUnwrap(Box.KeyPair.base642bin(iosPrivateKey))
        let privateKeyBytes = try XCTUnwrap(Box.KeyPair.base642bin(iosPublicKey))
        let iOSKeyPair = Box.KeyPair(publicKey: publicKeyBytes, secretKey: privateKeyBytes)
        let crypto = Crypto(keyPair: iOSKeyPair, legacyKeyPair: .random())

        // When
        let dek = try crypto.decrypt(encryptedDek: iosEncryptedKey)
        let payload = try crypto.decrypt(payload: iosPayloadEncrypted, dek: dek)

        // Then
        XCTAssertEqual(payload, "iOS is tha 💣")
    }

    // Code to generate more iOS test data:
    //    func generateiOSKeyPair() throws {
    //        let keyPair = try Box.KeyPair.generate()
    //        let publicString = Box.KeyPair.bin2base(keyPair.publicKey)!
    //        let privateString = Box.KeyPair.bin2base(keyPair.secretKey)!
    //
    //        print("public key: \(publicString)")
    //        print("private key: \(privateString)")
    //
    //        let crypto = Crypto(keyPair: keyPair, legacyKeyPair: .random())
    //
    //        let dek = try crypto.generateDek()
    //        let rsaEncryptedDek = try crypto.encrypt(dek: dek)
    //        print("rsaEncryptedDek: \(rsaEncryptedDek)")
    //
    //        let payload = "iOS is tha 💣"
    //        let cipherText = try crypto.encrypt(payload: payload, dek: dek)
    //        print("cipherText: \(cipherText)")
    //    }
}
