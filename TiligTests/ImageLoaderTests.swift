//
//  ImageLoaderTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 19/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Mocker
import XCTest

final class ImageLoaderTests: TiligTestCase {
    var imageLoader: ImageLoader!

    override func setUpWithError() throws {
        imageLoader = ImageLoader()
    }

    func testLoad() throws {
        let imageURL = URL(string: "https://asset.brandfetch.io/idHz7IYk0Z/id4QUosA_I.png")!
        let mockedData = MockedRequestData.imageDownload.data
        guard let mockedUIImage = UIImage(data: mockedData) else {
            XCTFail("Mock UI Image generation failed")
            return
        }
        let mock = Mock(url: imageURL, dataType: .imagePNG, statusCode: 200, data: [
            .get: mockedData
        ])
        mock.register()

        let imagePublisher = imageLoader.$image
            .dropFirst()
            .collect(1)
            .first()

        imageLoader.load(url: imageURL)

        let result = try awaitPublisher(imagePublisher, timeout: 5)

        guard let output = result.first,
            let image = output else {
            XCTFail("Image loading failed")
            return
        }

        XCTAssertEqual(image.pngData(), mockedUIImage.pngData())
    }

}
