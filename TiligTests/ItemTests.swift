//
//  ItemTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 04/11/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import XCTest
import Sodium

@testable import Tilig

class ItemTests: TiligTestCase {
    var item: Item!

    override func setUp() {
        super.setUp()
        item = Item.random()
    }

    func testPrependsHttpsToUrlWithoutProtocol() {
        item.website = URL(string: "noprotocol.com")!
        XCTAssertEqual(item.website?.withProtocol, URL(string: "https://noprotocol.com")!)
    }

    func testDoesNotPrependProtocolToUrlWithProtocol() {
        item.website = URL(string: "https://www.protocolpresent.com")!
        XCTAssertEqual(item.website?.withProtocol, URL(string: "https://www.protocolpresent.com")!)
    }

    func testIconURLFromBrand() {
        let icon = BrandImageIcon(png: BrandImageExtension(original: "https://somelogo.com/source"))
        item.brand = Brand(
            name: "Henk",
            domain: "www.henk.com",
            totp: true,
            images: BrandImageDetails(icon: icon)
        )
        XCTAssertEqual(item.iconURL, URL(string: "https://somelogo.com/source")!)
    }

    func testItemEncodingRoundtrip() throws {
        // If
        // We should create a better abstraction for all of this. This is a mess.
        let keyPair = Box.KeyPair.random()
        let legacyKeyPair = LegacyKeyPair.random()
        let crypto = Crypto(
            keyPair: keyPair,
            legacyKeyPair: legacyKeyPair
        )
        let loginItem = ItemWrapper.random(
            encryptedWithKeyPair: legacyKeyPair,
            name: "Encoded",
            url: URL(string: "url.com"),
            username: "some_username",
            password: "some_password",
            notes: "important notes",
            totp: "1234",
            brand: nil
        )
        var encryptedItem = try crypto.encrypt(itemWrapper: loginItem)
        encryptedItem.androidAppIds = ["some-app-id"]
        encryptedItem.id = nil
        encryptedItem.legacyEncryptionDisabled = nil
        // Make fields that won't be encoded nil, to make an equality test possible
        XCTAssertNil(encryptedItem.id)
        XCTAssertNil(encryptedItem.domain)
        XCTAssertNil(encryptedItem.brand)

        // When
        let encoded = try JSONEncoder().encode(encryptedItem)
        let decoded = try JSONDecoder().decode(Item.self, from: encoded)

        // Then
        XCTAssertEqual(decoded.encryptionVersion, 2)
        XCTAssertEqual(decoded.template, .loginV1)
        XCTAssertEqual(decoded.name, "Encoded")
        XCTAssertEqual(decoded.username, encryptedItem.username)
        XCTAssertEqual(decoded.password, encryptedItem.password)
        XCTAssertEqual(decoded.otp, encryptedItem.otp)
        XCTAssertEqual(decoded.notes, encryptedItem.notes)
        XCTAssertEqual(decoded.website, encryptedItem.website)
        XCTAssertEqual(decoded.encryptedDetails, encryptedItem.encryptedDetails)
        XCTAssertEqual(decoded.encryptedOverview, encryptedItem.encryptedOverview)
        XCTAssertEqual(decoded.rsaEncryptedDek, encryptedItem.rsaEncryptedDek)
        XCTAssertEqual(decoded.androidAppIds, encryptedItem.androidAppIds)
        XCTAssertEqual(decoded.legacyEncryptionDisabled, encryptedItem.legacyEncryptionDisabled)
        XCTAssertEqual(decoded.updatedAt, encryptedItem.updatedAt)
        XCTAssertEqual(decoded.createdAt, encryptedItem.createdAt)
        XCTAssertEqual(decoded.encryptedDek, encryptedItem.encryptedDek)
        XCTAssertEqual(decoded.rsaEncryptedDek, encryptedItem.rsaEncryptedDek)
        XCTAssertEqual(decoded, encryptedItem)
    }
}
