////
////  PasswordGeneratorSpec.swift
////  TiligTests
////
////  Created by Gertjan Jansen on 21/07/2021.
////  Copyright © 2021 SubsLLC. All rights reserved.
////
//
// import Foundation
// import Quick
// import Nimble
//
// @testable import Tilig
//
// class PasswordGeneratorSpecs: QuickSpec {
//    override func spec() {
//        describe("password generation") {
//            let password = PasswordGenerator.randomPassword()
//
//            it("has 10 characters") {
//                expect(password.count).to(equal(10))
//            }
//
//            it("has a lowercase group of 7 characters") {
//                for index in 0...6 {
//                    let character = password[password.index(password.startIndex, offsetBy: index)]
//                    if let scalar = Unicode.Scalar(String(character)) {
//                        expect(CharacterSet.lowercaseLetters.contains(scalar)).to(beTrue())
//                    } else {
//                        fail("Character \(index) should be scalar")
//                    }
//                }
//            }
//
//            it("has one uppercase character at the 8th position") {
//                let character = password[password.index(password.startIndex, offsetBy: 7)]
//                if let scalar = Unicode.Scalar(String(character)) {
//                    expect(CharacterSet.uppercaseLetters.contains(scalar)).to(beTrue())
//                } else {
//                    fail("Character should be scalar")
//                }
//            }
//
//            it("has one digit at the 9th position") {
//                let character = password[password.index(password.startIndex, offsetBy: 8)]
//                if let scalar = Unicode.Scalar(String(character)) {
//                    expect(CharacterSet.decimalDigits.contains(scalar)).to(beTrue())
//                } else {
//                    fail("Character should be scalar")
//                }
//            }
//
//            it("has a special character $#! on the 10th position") {
//                let character = password[password.index(password.startIndex, offsetBy: 9)]
//                expect("$#!".contains(character)).to(beTrue())
//            }
//        }
//    }
// }
