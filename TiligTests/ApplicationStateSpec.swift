//
//  GoogleSignInDelegateSpecs.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 17/11/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import XCTest
import Nimble
import Quick
import Mixpanel
// import Mocker

@testable import Tilig

class ApplicationStatespec: QuickSpec {
    // TODO: fix for v3
//    override func spec() {
//        var appState: ApplicationState!
//
//        beforeEach {
//            appState = ApplicationState()
//            appState.user = UserModel(id: 10, email: "me@tilig.com")
//        }
//
//        describe("Mixpanel identification") {
//            describe("before identification") {
//                describe("currentMixpanelSessionIsAnonymous()") {
//                    it("returns true when not identified yet") {
//                        Mixpanel.mainInstance().reset()
//                        expect(appState.currentMixpanelSessionIsAnonymous()).toEventually(be(true))
//                    }
//                }
//            }
//
//            describe("after identification") {
//                beforeEach {
//                    appState.setMixpanelIdentity()
//                }
//
//                it("sets the Mixpanel distinctId") {
//                    expect(Mixpanel.mainInstance().distinctId).toEventually(be(String(appState.user!.id)))
//                }
//
//                it("is not anonymous anymore") {
//                    expect(appState.currentMixpanelSessionIsAnonymous()).toEventually(be(false))
//                }
//            }
//        }
//    }
}
