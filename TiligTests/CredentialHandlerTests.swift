//
//  CredentialHandlerTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 19/10/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import AuthenticationServices

final class CredentialHandlerTests: TiligTestCase {

    override func setUpWithError() throws {
        try super.setUpWithError()
    }

    var error: Error?

    func testThrowsWhenKeyPairIsNil() throws {
        // If
        error = nil
        let handler = CredentialHandler(
            extensionContext: ASCredentialProviderExtensionContext(),
            keyPair: nil,
            legacyKeyPair: nil,
            onError: { error in
                self.error = error
            }
        )
        let v1Item = Item.random(
            encryptedWithKeyPair: .random(),
            plainUsername: "Le Username",
            plainPassword: "Le Password"
        )

        // When
        handler.completeRequest(withItem: v1Item)

        // Then
        XCTAssertNotNil(error)
    }

    func testDoesNotThrowWithValidItem() throws {
        // If
        error = nil
        let legacyKeyPair = try LegacyKeyPair.generateRSAKeyPair()
        let v1Item = Item.random(
            encryptedWithKeyPair: legacyKeyPair,
            plainUsername: "Le Username",
            plainPassword: "Le Password"
        )
        let handler = CredentialHandler(
            extensionContext: ASCredentialProviderExtensionContext(),
            keyPair: nil,
            legacyKeyPair: legacyKeyPair
        )

        // When
        handler.completeRequest(withItem: v1Item)

        // Then
        XCTAssertNil(error)
    }
}
