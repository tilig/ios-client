//
//  TiligTestCase.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 03/11/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import XCTest
import Combine
import Mocker

@MainActor class TiligTestCase: XCTestCase {

    @MainActor override func setUpWithError() throws {
        try super.setUpWithError()
        configureMocker()
    }

    @MainActor override func setUp() async throws {
        try await super.setUp()
        configureMocker()
    }

    func configureMocker() {
        // Mocker setup
        let configuration = URLSessionConfiguration.default
        configuration.protocolClasses = [MockingURLProtocol.self]
        URLSession.current = URLSession(configuration: configuration)
    }

    // Source: https://www.swiftbysundell.com/articles/unit-testing-combine-based-swift-code/
    func awaitPublisher<T: Publisher>(
        _ publisher: T,
        timeout: TimeInterval = 10,
        file: StaticString = #file,
        line: UInt = #line
    ) throws -> T.Output {
        // Use Swift's Result type to keep track
        // of the result of our Combine pipeline:
        var result: Result<T.Output, Error>?
        let expectation = self.expectation(description: "Awaiting Publisher")

        let cancellable = publisher.sink(
            receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    result = .failure(error)
                case .finished:
                    break
                }

                expectation.fulfill()
            },
            receiveValue: { value in
                result = .success(value)
            }
        )

        // We await the expectation that we
        // created at the top of our test, and once done, we
        // also cancel our cancellable to avoid getting any
        // unused variable warnings:
        waitForExpectations(timeout: timeout)
        cancellable.cancel()

        // Here we pass the original file and line number that
        // our utility was called at, to tell XCTest to report
        // any encountered errors at that original call site:
        let unwrappedResult = try XCTUnwrap(
            result,
            "Awaited publisher did not produce any output",
            file: file,
            line: line
        )

        return try unwrappedResult.get()
    }
}
