//
//  CombineTestExtensions.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 04/11/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import Combine

// Source: https://www.swiftbysundell.com/articles/unit-testing-combine-based-swift-code/
extension Published.Publisher {
    /// Drop the first published value, collect the next `count` values, then finish.
    func collectNext(_ count: Int) -> AnyPublisher<[Output], Never> {
        self.dropFirst()
            .collect(count)
            .first()
            .eraseToAnyPublisher()
    }
}
