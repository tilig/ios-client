//
//  XCTestCase+WaitForFirstValue.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 12/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Combine
import XCTest

extension XCTestCase {
    /// Wait until the given publisher publishes the matching value
    /// Use the callback to execute any code that should be executed after setting up the subscription,
    /// which is useful for testing PassthroughSubjects
    func waitForFirstValue<T: Publisher>(
        from publisher: T,
        matching: T.Output,
        timeout: TimeInterval = 1,
        callback: @escaping () -> Void = {}
    ) where T.Output: Equatable, T.Failure == Never {
        let expectation = self.expectation(description: "waitForFirstValue")

        var cancellable: AnyCancellable?

        cancellable = publisher
            .filter { $0 == matching }
            .first()
            .sink { _ in
                expectation.fulfill()
                cancellable?.cancel()
            }

        callback()

        waitForExpectations(timeout: timeout, handler: nil)
    }

    func waitForFirstValue<T: Publisher>(
        from publisher: T,
        timeout: TimeInterval = 1,
        callback: @escaping () -> Void = {}
    ) where T.Failure == Never {
        let expectation = self.expectation(description: "waitForFirstValue")

        var cancellable: AnyCancellable?

        cancellable = publisher
            .first()
            .sink { _ in
                expectation.fulfill()
                cancellable?.cancel()
            }

        callback()

        waitForExpectations(timeout: timeout, handler: nil)
    }
}
