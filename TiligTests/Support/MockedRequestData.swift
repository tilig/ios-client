//
//  GoogleSignInDelegateSpecs.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 17/11/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import Foundation

public final class MockedRequestData {
    public static let authenticateSuccess: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "authenticate", withExtension: "json")!

    public static let profileSuccess: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "profile", withExtension: "json")!

    public static let postKeyPairSuccess: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "postKeyPairSuccess", withExtension: "json")!

    public static let empty: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "empty", withExtension: "json")!

    public static let emptyArray: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "emptyArray", withExtension: "json")!

    public static let refresh: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "refresh", withExtension: "json")!

    public static let delete: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "delete", withExtension: "json")!

    public static let brands: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "brands", withExtension: "json")!

    public static let brand: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "brand", withExtension: "json")!

    public static let addItemSuccess: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "addItemSuccess", withExtension: "json")!

    public static let addNoteSuccess: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "addNoteSuccess", withExtension: "json")!

    public static let editItemSuccess: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "editItemSuccess", withExtension: "json")!

    public static let addWifiSuccess: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "addWifiSuccess", withExtension: "json")!

    public static let addCardSuccess: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "addCardSuccess", withExtension: "json")!

    public static let editNoteSuccess: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "editNoteSuccess", withExtension: "json")!

    public static let passwordHistory: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "passwordHistory", withExtension: "json")!

    public static let secrets: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "secrets", withExtension: "json")!

    public static let addedSecrets: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "addedSecrets", withExtension: "json")!

    public static let updatedSecrets: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "updatedSecrets", withExtension: "json")!

    public static let folderMembership: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "folderMembership", withExtension: "json")!

    public static let folder: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "folder", withExtension: "json")!

    public static let publicKey: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "publicKey", withExtension: "json")!

    public static let deletedSecrets: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "deletedSecrets", withExtension: "json")!

    public static let mixedItems: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "mixedItems", withExtension: "json")!

    public static let malformed: URL = Bundle(for: MockedRequestData.self)
        .url(forResource: "malformed", withExtension: "json")!

    public static let imageDownload: URL = Bundle.module
        .url(forResource: "tilig_logo", withExtension: "png")!
}

extension Bundle {
#if !SWIFT_PACKAGE
    static let module = Bundle(for: MockedRequestData.self)
#endif
}

internal extension URL {
    /// Returns a `Data` representation of the current `URL`. Force unwrapping as it's only used for tests.
    var data: Data {
        return try! Data(contentsOf: self)
    }
}
