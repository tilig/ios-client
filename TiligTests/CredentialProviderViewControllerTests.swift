//
//  CredentialViewProviderTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 17/01/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import XCTest
import AuthenticationServices
import Mocker

final class CredentialProviderViewControllerTests: TiligTestCase {
    var controller: CredentialProviderViewController!
    var credentialHandlerMock: CredentialHandlerMock!

    override func setUp() {
        super.setUp()

        credentialHandlerMock = CredentialHandlerMock()

        // This is deprecated, but I didn't figure our how to make this work
        // with the recommended NSKeyedUnarchiver(forReadingFrom:) method
        let archiver = NSKeyedUnarchiver(forReadingWith: Data())
        controller = CredentialProviderViewController(coder: archiver)
        controller.credentialHandlerMock = credentialHandlerMock
    }

    override func tearDownWithError() throws {
        try ItemsCache().clear()
    }

    func mockAuthenticatedState() {
        do {
            try AuthState.cache(authState: .random())
        } catch {
            XCTFail("Could not mock auth state")
        }
    }

    /// Mock a failed server response.
    /// We're most interested in the scenario where there's no connection at all,
    /// but I didn't know how to mock that, and it behaves the same for any type of error.
    func mockConnectionError(forItemId itemId: UUID) {
        let mock = Mock(
            url: Endpoint.item(itemId).url,
            dataType: .html,
            statusCode: 500,
            data: [.get: Data()]
        )
        mock.register()
    }

    func mockItemCache(itemId: UUID, urlString: String) throws {
        let item = Item(
            id: itemId,
            website: CodableOptionalURL(string: urlString)
        )
        try ItemsCache().cacheAll(items: [item])
    }

    func testReturnsItemFromCacheOnConnectionFailure() throws {
        // If
        mockAuthenticatedState()

        let identifier = "booking.com"
        let user = "gertjan@tilig.com"
        let itemId = UUID()

        let recordIdentifier = itemId.uuidString
        let serviceIdentifier = ASCredentialServiceIdentifier(
            identifier: identifier,
            type: .URL
        )
        let identity = ASPasswordCredentialIdentity(
            serviceIdentifier: serviceIdentifier,
            user: user,
            recordIdentifier: recordIdentifier
        )

        mockConnectionError(forItemId: itemId)
        try mockItemCache(itemId: itemId, urlString: identifier)

        // When
        controller.provideCredentialWithoutUserInteraction(for: identity)

        // Then
        waitForFirstValue(
            from: credentialHandlerMock.$requestCompleted,
            matching: true
        )
    }
}
