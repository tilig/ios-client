//
//  BiometricHelperTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 04/10/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import LocalAuthentication

class LAContextMock: LAContextProtocol {
    var biometryType: LABiometryType = .faceID
    var canEvaluate = true
    var evaluateSuccess = true

    func canEvaluatePolicy(_: LAPolicy, error: NSErrorPointer) -> Bool {
        return canEvaluate
    }

    func evaluatePolicy(_ policy: LAPolicy, localizedReason: String, reply: @escaping (Bool, Error?) -> Void) {
        // Returning nil implies that there are no errors, therefore we are authenticated
        reply(evaluateSuccess, nil)
    }
}

final class BiometricHelperTests: TiligTestCase {

    var biometricHelper: BiometricHelper!

    override func setUpWithError() throws {
        let context = LAContextMock()
        context.canEvaluate = true
        biometricHelper = BiometricHelper(context: context)
    }

    override func tearDownWithError() throws {
        biometricHelper = nil
    }

    func testBiometricCanEvaluate() {
        let contextMock = LAContextMock()
        biometricHelper = BiometricHelper(context: contextMock)
        XCTAssertEqual(biometricHelper.canEvaluate, contextMock.canEvaluate)
    }

    func testAuthenticateBiometricFailure() async throws {
        let contextMock = LAContextMock()
        contextMock.canEvaluate = true
        contextMock.evaluateSuccess = false

        biometricHelper = BiometricHelper(context: contextMock)
        let authResult = try await biometricHelper.authenticateBiometric()

        XCTAssertEqual(authResult, false)
    }

    func testAuthenticateBiometricSuccess() async throws {
        let contextMock = LAContextMock()
        contextMock.canEvaluate = true
        contextMock.evaluateSuccess = true

        biometricHelper = BiometricHelper(context: contextMock)
        let authResult = try await biometricHelper.authenticateBiometric()

        XCTAssertEqual(authResult, true)
    }

}
