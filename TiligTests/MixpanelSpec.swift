//
//  MixpanelSpec.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 15/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation

import XCTest
import Nimble
import Quick
import Mixpanel

@testable import Tilig

class MixpanelSpec: QuickSpec {
    override func spec() {
        describe("distinctId") {
            let authState = AuthState.random()
            it("is equal to the user's profile id") {
                expect(authState.mixpanelDistinctId).to(
                    equal(authState.userProfile.id.uuidString.lowercased())
                )
            }
        }
    }
}
