//
//  URLOpenTrackerSpec.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 11/12/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import XCTest
import Nimble
import Quick

@testable import Tilig

class URLOpenTrackerSpec: QuickSpec {
    override func spec() {
        let urlOpenTracker = URLOpenTracker()

        describe("settings URL") {
            it("returns the expected URL") {
                let url = URL(string: "\(urlOpenTracker.getSettingsBase()):\(urlOpenTracker.getSettingsPage())")!

                expect(url).to(equal(URL(string: "App-prefs:PASSWORDS")))
            }
        }
    }
}
