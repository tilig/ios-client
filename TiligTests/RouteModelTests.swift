//
//  RouteModelTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 31/05/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import XCTest

import Foundation
import Nimble
import Quick

@testable import Tilig

class RouteModelTests: TiligTestCase {
    var routeModel: RouteModel!

    override func setUpWithError() throws {
        routeModel = RouteModel(initialRoute: .itemList)
    }

    func testInit() {
        let initialRoute: Route = .itemList
        routeModel = RouteModel(initialRoute: initialRoute)
        XCTAssertEqual(routeModel.currentRoute, initialRoute)
    }

    func testPushRoute() {
        routeModel.pushRoute(.itemList)
        XCTAssertEqual(routeModel.currentRoute, .itemList)
    }

    // Disabled now that we have only one route
//    func testPersistInitialRoute() {
//        routeModel.pushRoute(.autoFillSetup)
//        XCTAssertEqual(routeModel.initialRoute, .itemList)
//    }
 }
