//
//  ExperimentsSpec.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 20/01/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import Nimble
import Quick

@testable import Tilig

class ExperimentsSpec: QuickSpec {
    override func spec() {

        describe("abVariant(forKey:)") {

            it("picks both A and B approximately 50 out of 100 times") {
                var aCount = 0
                var bCount = 0

                for _ in 0..<100 {
                    let outcome = Experiments.abVariant(forKey: "testCount")
                    if outcome == .a {
                        aCount += 1
                    } else {
                        bCount += 1
                    }

                    Experiments.resetVariant(forKey: "testCount")
                }

                expect(Double(aCount)) ≈ 50 ± 20
                expect(Double(bCount)) ≈ 50 ± 20
                expect(aCount + bCount).to(equal(100))
            }

            it("persists the chosen variant") {
                let initialValue = Experiments.abVariant(forKey: "persistanceTest")

                for _ in 0..<10 {
                    expect(initialValue).to(equal(Experiments.abVariant(forKey: "persistanceTest")))
                }
            }
        }
    }
}
