//
//  VersionIdentifierTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 17/11/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import XCTest

@testable import Tilig

class VersionIdentifierTests: TiligTestCase {
    func testTiligPlatform() throws {
        // Not a very useful test, but soit...
        if UIDevice.current.userInterfaceIdiom == .phone {
            XCTAssertEqual(VersionIdentifier.tiligPlatform, "iOS")
        } else {
            XCTAssertEqual(VersionIdentifier.tiligPlatform, "iPadOS")
        }
    }
}
