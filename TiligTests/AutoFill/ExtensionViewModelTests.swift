//
//  ExtensionViewModelTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 22/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import XCTest
import AuthenticationServices

@testable import Tilig

class ExtensionViewModelTests: TiligTestCase {
    var viewModel: ExtensionViewModel!
    var credentialHandlerMock: CredentialHandlerMock!
    var identifier: ASCredentialServiceIdentifier!

    override func setUpWithError() throws {
        try setupViewModel()
    }

    func setupViewModel(identifierDomain: String = "cnn.com") throws {
        credentialHandlerMock = CredentialHandlerMock()

        identifier = ASCredentialServiceIdentifier(
            identifier: identifierDomain,
            type: .domain
        )

        viewModel = try ExtensionViewModel(
            serviceIdentifiers: [identifier],
            credentialHandler: credentialHandlerMock,
            tokenProvider: .mocked()
        )
    }

    func testDoesNotRaiseErrorOnInit() throws {
        XCTAssertNil(viewModel.error)
    }

    func testIsLoadingInitially() throws {
        XCTAssertTrue(viewModel.loading)
    }

    func testHandlesError() throws {
        let error = AuthError.unauthorized
        viewModel.handleError(error)
        waitForFirstValue(from: viewModel.$error)
        XCTAssertNotNil(viewModel.error)
        if case AuthError.unauthorized = viewModel.error! {
            // success
            return
        } else {
            XCTFail("Failed")
        }
    }

    func testCompleteRequest() throws {
        // If
        let item = Item.random()

        // When
        viewModel.completeRequest(withItem: item)

        // Then
        XCTAssertEqual(credentialHandlerMock.completedWithItem, item)
        XCTAssertTrue(credentialHandlerMock.requestCompleted)
    }

    func testCancelRequest() throws {
        try setupViewModel()
        viewModel.cancelRequest(withError: .userCanceled)
        XCTAssertEqual(credentialHandlerMock.cancelledWithError, .userCanceled)
        XCTAssertTrue(credentialHandlerMock.requestCancelled)
    }

    func testFilterMatchingItemsWhenMatches() throws {
        // If
        let items = [
            Item.random(domain: "cnn.com"),
            Item.random()
        ]

        // When
        let filtered = viewModel.filterMatching(items: items)

        // Then
        XCTAssertEqual(filtered.count, 1)
        XCTAssertEqual(filtered.first?.domain, "cnn.com")
    }

    func testFilterMatchingItemsWhenNoMatches() throws {
        // If
        let items = [
            Item.random(),
            Item.random()
        ]

        // When
        let filtered = viewModel.filterMatching(items: items)

        // Then
        XCTAssertEqual(filtered.count, 0)
    }

    func testIsMatchingEncryptedItem() throws {
        // If
        try setupViewModel(identifierDomain: "amazon.com")
        let matching = ["amazon.nl", "amazon.com", "amazon.de"]
        let notMatching = ["google.com"]

        // When
        let matched = matching.map {
            viewModel.isMatching(encryptedItem: Item.random(domain: $0))
        }
        let notMatched = notMatching.map {
            !viewModel.isMatching(encryptedItem: Item.random(domain: $0))
        }

        // Then
        XCTAssertEqual(matched, [true, true, true])
        XCTAssertEqual(notMatched, [true])
    }

    // TODO: instead of testing `filterMatching(items:)`, we should test `findMatchingItems`
    // func testFindMatchingItems() throws { }

}
