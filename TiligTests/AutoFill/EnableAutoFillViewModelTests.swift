//
//  AutoFillViewModelTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 25/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

 import XCTest
 @testable import Tilig

class EnableAutoFillViewModelTests: TiligTestCase {

    var viewModel: EnableAutoFillViewModel!
    let mockCredentialIdentityStateProvider = MockedCredentialIdentityStateProvider()

    override func setUpWithError() throws {
        initViewModel()
    }

    func initViewModel(clearUserDefaults: Bool = true) {
        if clearUserDefaults {
            UserDefaults.standard.reset()
        }
        viewModel = EnableAutoFillViewModel(
            credentialIdentityStateProvider: mockCredentialIdentityStateProvider
        )
        mockCredentialIdentityStateProvider.triggerStateChange(isEnabled: false)
    }

    func testCheckAutoFillEnabled() throws {
        // If
        viewModel = EnableAutoFillViewModel(credentialIdentityStateProvider: mockCredentialIdentityStateProvider)
        XCTAssertFalse(viewModel.autoFillEnabled)

        // When
        mockCredentialIdentityStateProvider.triggerStateChange(isEnabled: true)
        viewModel.updateAutoFillEnabledFlag()

        // Then
        waitForFirstValue(from: viewModel.$autoFillEnabled, matching: true)
    }

    func testCheckAutoFillDisabled() {
        // If
        viewModel = EnableAutoFillViewModel(credentialIdentityStateProvider: mockCredentialIdentityStateProvider)
        XCTAssertFalse(viewModel.autoFillEnabled)

        // When
        mockCredentialIdentityStateProvider.triggerStateChange(isEnabled: false)
        viewModel.updateAutoFillEnabledFlag()

        // Then
        waitForFirstValue(from: viewModel.$autoFillEnabled, matching: false)
    }

    func testPresentAutoFillExplanation() {
        viewModel.presentAutoFillExplanation()
        XCTAssertTrue(viewModel.presentAsSheet)
    }

    func testStoreLastAutoFillEnabledState() {
        viewModel.storeLastAutoFillEnabledState(true)
        XCTAssertTrue(viewModel.lastTrackedAutoFillEnabledState)
    }

    func testCompleteFinalStep() {
        // If
        initViewModel()

        // When
        viewModel.completeFinalStep()

        // Then
        waitForFirstValue(from: viewModel.$onboardingCompleted, matching: true)
    }

    func testCompleteFinalStepFromSheetFlow() {
        // If
        initViewModel()
        viewModel.presentAutoFillExplanation()

        // When
        viewModel.completeFinalStep()

        // Then
        waitForFirstValue(from: viewModel.$onboardingCompleted, matching: true)
        waitForFirstValue(from: viewModel.$presentAsSheet, matching: false)
    }

    func testOnboardingCompletedInitializesWithUserDefaultsFlag() throws {
        // If
        UserDefaults.standard.setValue(true, forKey: UserDefaults.AutoFillOnboardingCompleted)
        // When
        initViewModel(clearUserDefaults: false)
        // Then
        XCTAssertTrue(viewModel.onboardingCompleted)

        // If
        UserDefaults.standard.setValue(false, forKey: UserDefaults.AutoFillOnboardingCompleted)
        // When
        initViewModel()
        // Then
        XCTAssertFalse(viewModel.onboardingCompleted)
    }

    func testSkipAutoFillUpdatesOnboardingCompletedFlag() {
        // If
        UserDefaults.standard.setValue(true, forKey: UserDefaults.AutoFillOnboardingCompleted)
        initViewModel()

        // When
        viewModel.skipAutoFill()

        // Then
        waitForFirstValue(from: viewModel.$onboardingCompleted, matching: true)
        let userDefaultsFlag = UserDefaults.standard.bool(forKey: UserDefaults.AutoFillOnboardingCompleted)
        XCTAssertTrue(userDefaultsFlag)
    }

    func testEnableAutoFillOutsideOnboardingFlow() {
        // If
        initViewModel()

        // When
        mockCredentialIdentityStateProvider.triggerStateChange(isEnabled: true)

        // Then
        waitForFirstValue(from: viewModel.$onboardingCompleted, matching: true)
    }
}
