//
//  BaseServiceSpec.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 13/01/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import XCTest
import Nimble
import Quick
import AuthenticationServices

@testable import Tilig

class EndpointSpec: QuickSpec {
    override func spec() {
        describe("dataTaskPublishers") {
            describe("defaultHeaders") {
                it("includes the platform header") {
                    let platformHeader = URLSession.current.defaultHeaders["x-tilig-platform"]
                    if UIDevice.current.userInterfaceIdiom == .phone {
                        expect(platformHeader).to(equal("iOS"))
                    } else {
                        expect(platformHeader).to(equal("iPadOS"))
                    }
                }
            }

            describe("overriding the default headers") {
                it("overrides the authorization header") {
                    let merged = URLSession.current.headers(withAuthorizationHeader: "Koekje")
                    expect(merged["Authorization"]).to(equal("Koekje"))
                }
            }
        }

        describe("endpoint URLs") {
            it("creates the correct URL for item") {
                let item = Item(id: UUID(), name: "Gurt")
                let url = Endpoint.item(item.id!).url
                let host = UserDefaults.standard.string(forKey: "backendURL") ?? "https://tilig-dev.loca.lt/api"
                expect(url).to(equal(URL(string: "\(host)/v3/items/\(item.id!.uuidString)")!))
            }
        }
    }
}
