//
//  LoginDetailsTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 05/10/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest

final class ItemWrapperTests: TiligTestCase {

    override func setUpWithError() throws {
        try super.setUpWithError()
    }

    func testEmptyInitializer() throws {
        // If
        let itemWrapper = ItemWrapper(template: .loginV1)

        // Then
        XCTAssertEqual(itemWrapper.encryptedItem.template, .loginV1)
        XCTAssertEqual(itemWrapper.overview, DecryptedOverview())
        XCTAssertEqual(itemWrapper.details, DecryptedDetails())
    }

    func testInitWithDecryptedPayload() throws {
        // If
        var details = DecryptedDetails()
        details.notes = "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
        details.setInMain(kind: .password, value: "yolo")

        let overview = DecryptedOverview(name: "Pieces of a man")

        // When
        let itemWrapper = ItemWrapper(
            item: Item(template: .loginV1),
            overview: overview,
            details: details
        )

        // Then
        XCTAssertEqual(itemWrapper.notes, "Lorem ipsum dolor sit amet, consectetur adipiscing elit")
        XCTAssertEqual(itemWrapper.name, "Pieces of a man")
        XCTAssertEqual(itemWrapper.password, "yolo")
    }

    func testSetsBrandWithDefaultInitializer() throws {
        // If
        let brand = Brand.random(name: "Brand test")

        // When
        let itemWrapper = ItemWrapper(
            item: Item(brand: brand),
            overview: DecryptedOverview(),
            details: DecryptedDetails()
        )

        // Then
        XCTAssertEqual(itemWrapper.brand, brand)
    }

    func testSetsNameWithLegacyEncryption() throws {
        // If
        let legacyItem = Item(legacyEncryptionDisabled: false)
        var itemWrapper = ItemWrapper(
            item: legacyItem,
            overview: DecryptedOverview(),
            details: DecryptedDetails()
        )
        XCTAssertTrue(itemWrapper.encryptedItem.legacyEncryption)

        // When
        itemWrapper.name = "Some name"

        // Then
        XCTAssertEqual(itemWrapper.encryptedItem.name, "Some name")
        XCTAssertEqual(itemWrapper.overview.name, "Some name")
    }

    func testSetsNameWithoutLegacyEncryption() throws {
        // If
        var itemWrapper = ItemWrapper(template: .loginV1)
        XCTAssertTrue(itemWrapper.encryptedItem.newEncryption)

        // When
        itemWrapper.name = "Some name"

        // Then
        XCTAssertEqual(itemWrapper.overview.name, "Some name")
        // It shouldn't set the legacy field
        XCTAssertNil(itemWrapper.encryptedItem.name)
    }

    func testSettingPropertiesUpdatesDetails() throws {
        // If
        var itemWrapper = ItemWrapper(template: .loginV1)

        // When
        itemWrapper.username = "bono"
        itemWrapper.password = "loves_apples"
        itemWrapper.notes = "I love sunglasses"
        itemWrapper.totp = "ireland_123"

        // Then
        XCTAssertEqual(itemWrapper.details.findInMain(kind: .username)?.value, "bono")
        XCTAssertEqual(itemWrapper.details.findInMain(kind: .password)?.value, "loves_apples")
        XCTAssertEqual(itemWrapper.details.findInMain(kind: .totp)?.value, "ireland_123")
        XCTAssertEqual(itemWrapper.details.notes, "I love sunglasses")
    }

    func testURLFallsBackToLegacyURL() throws {
        // If there's no URL present on the overview, but a website is set on the encrypted item
        let item = Item.random(url: URL(string: "www.fallback.com")!)

        // When
        let itemWrapper = ItemWrapper(
            item: item,
            overview: DecryptedOverview(),
            details: DecryptedDetails()
        )

        // Then
        XCTAssertEqual(itemWrapper.url?.absoluteString, "www.fallback.com")
    }

    func testSettingURLUpdatesOverview() throws {
        // If
        var itemWrapper = ItemWrapper(template: .loginV1)

        // When
        itemWrapper.url = URL(string: "https://www.harrypotter.com")!

        // Then
        XCTAssertEqual(
            itemWrapper.overview.urls,
            [OverviewURL(url: URL(string: "https://www.harrypotter.com")!)]
        )
    }

    func testUpdatingURLUpdatesOverview() throws {
        // If
        var itemWrapper = ItemWrapper(
            item: Item(),
            overview: DecryptedOverview(urls: [
                OverviewURL(url: URL(string: "old-url.com")!),
                OverviewURL(name: "Secondary", url: URL(string: "secondary.com")!)
            ]),
            details: DecryptedDetails()
        )

        // When
        itemWrapper.url = URL(string: "new-url.com")!

        // Then
        XCTAssertEqual(
            itemWrapper.overview.urls,
            [
                OverviewURL(url: URL(string: "new-url.com")!),
                OverviewURL(name: "Secondary", url: URL(string: "secondary.com")!)
            ]
        )
    }

    func testLegacyEncryption() throws {
        // If
        let overview = DecryptedOverview(
            name: "Fred Durst's Overview",
            info: "fred_durst",
            urls: [OverviewURL(url: URL(string: "www.freddurst.com")!)],
            androidAppIds: ["android-durst"]
        )
        let details = DecryptedDetails(
            notes: "Nookie",
            main: [
                .init(kind: .username, value: "fred_durst"),
                .init(kind: .password, value: "is_awesome"),
                .init(kind: .totp, value: "123")
            ]
        )
        let itemWrapper = ItemWrapper(
            item: Item(legacyEncryptionDisabled: false),
            overview: overview,
            details: details
        )
        let crypto = Crypto.random()
        XCTAssertTrue(itemWrapper.encryptedItem.legacyEncryption)

        // When
        let encryptedItem = try crypto.encrypt(itemWrapper: itemWrapper)

        // Then
        XCTAssertNotEqual(encryptedItem.username, "fred_durst")

        let legacyFields = try crypto.decryptLegacyFields(item: encryptedItem)
        XCTAssertEqual(legacyFields.username, "fred_durst")
        XCTAssertEqual(legacyFields.password, "is_awesome")
        XCTAssertEqual(legacyFields.notes, "Nookie")
        XCTAssertEqual(legacyFields.otp, "123")
    }

    func testWithDisabledLegacyEncryption() throws {
        // If
        let crypto = Crypto.random()
        let encryptedItem = Item.random(
            encryptedWithKeyPair: crypto.legacyKeyPair,
            legacyEncryptionDisabled: true,
            name: "Old name"
        )
        XCTAssertTrue(encryptedItem.newEncryption)
        var itemWrapper = ItemWrapper(
            item: encryptedItem,
            overview: DecryptedOverview(),
            details: DecryptedDetails()
        )

        // When
        itemWrapper.name = "New name"

        // Then
        // It should have the old name as the legacy field
        XCTAssertEqual(itemWrapper.encryptedItem.name, "Old name")
    }

    func testUpdatedAt() throws {
        // If
        var item = ItemWrapper.random(name: "Henk", notes: "Je suis le henk!").encryptedItem
        item.updatedAt = "1970-01-01T00:00:00Z"

        // When
        let itemWrapper = ItemWrapper(
            item: item,
            overview: DecryptedOverview(),
            details: DecryptedDetails()
        )

        // Then
        XCTAssertEqual(itemWrapper.updatedAt, "1970-01-01T00:00:00Z")
    }

    func testEqualityWhenSettingNilURL() throws {
        // If
        let initialItemWrapper = ItemWrapper(
            item: Item(),
            overview: DecryptedOverview(),
            details: DecryptedDetails()
        )
        var itemWrapperCopy = initialItemWrapper
        XCTAssertEqual(initialItemWrapper, itemWrapperCopy)

        // When
        itemWrapperCopy.url = nil

        // Then
        XCTAssertEqual(initialItemWrapper, itemWrapperCopy)
    }
}
