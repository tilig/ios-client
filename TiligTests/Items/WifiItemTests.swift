//
//  WifiItemTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 18/11/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest

final class WifiItemTests: TiligTestCase {

    override func setUpWithError() throws {
        try super.setUpWithError()
    }

    func testEmptyInitializer() throws {
        // If
        let wifiItem = ItemWrapper(template: .wifiV1)

        // Then
        XCTAssertEqual(wifiItem.encryptedItem.template, .wifiV1)
        XCTAssertEqual(wifiItem.overview, DecryptedOverview())
        XCTAssertEqual(wifiItem.details, DecryptedDetails())
    }

    func testSetsName() throws {
        // If
        var wifiItem = ItemWrapper(template: .wifiV1)

        // When
        wifiItem.name = "Some name"

        // Then
        XCTAssertEqual(wifiItem.overview.name, "Some name")
    }

    func testSettingPropertiesUpdatesDetails() throws {
        // If
        var wifiItem = ItemWrapper(template: .wifiV1)

        // When
        wifiItem.ssid = "iPhone Hotspot"

        // Then
        XCTAssertEqual(wifiItem.details.findInMain(kind: .ssid)?.value, "iPhone Hotspot")
    }
}
