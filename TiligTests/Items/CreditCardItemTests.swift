//
//  CardItemTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 18/11/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest

final class CreditCardItemTests: TiligTestCase {

    override func setUpWithError() throws {
        try super.setUpWithError()
    }

    func testEmptyInitializer() throws {
        // If
        let cardItem = ItemWrapper(template: .cardV1)

        // Then
        XCTAssertEqual(cardItem.encryptedItem.template, .cardV1)
        XCTAssertEqual(cardItem.overview, DecryptedOverview())
        XCTAssertEqual(cardItem.details, DecryptedDetails())
    }

    func testSettingPropertiesUpdatesDetails() throws {
        // If
        var cardItem = ItemWrapper(template: .cardV1)

        // When
        cardItem.ccnumber = "1111000022223333"
        cardItem.ccholder = "Jalen Brunsen"
        cardItem.ccexp = "11/24"
        cardItem.cvv = "123"
        cardItem.pin = "1234"
        cardItem.zipcode = "00233"

        // Then
        XCTAssertEqual(cardItem.details.findInMain(kind: .ccnumber)?.value, "1111000022223333")
        XCTAssertEqual(cardItem.details.findInMain(kind: .ccholder)?.value, "Jalen Brunsen")
        XCTAssertEqual(cardItem.details.findInMain(kind: .ccexp)?.value, "11/24")
        XCTAssertEqual(cardItem.details.findInMain(kind: .cvv)?.value, "123")
        XCTAssertEqual(cardItem.details.findInMain(kind: .pin)?.value, "1234")
        XCTAssertEqual(cardItem.details.findInMain(kind: .zipcode)?.value, "00233")
    }
}
