//
//  AddLoginFormFieldTests.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 30/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest

final class AddLoginFormFieldTests: TiligTestCase {

    override func setUpWithError() throws {
        try super.setUpWithError()
    }

    // TODO: re-enable this test
//    func testItemInit() throws {
//        // if
//        let formFields = AddLoginFormFields(
//            name: "test",
//            website: URL(string: "www.website.com"),
//            username: "gurt",
//            password: "pass"
//        )
//        let keyPair = LegacyKeyPair.random()
//
//        // When
//        let item = try Item(formFields: formFields, keyPair: keyPair)
//
//        // Then
//        XCTAssertEqual(item.name, formFields.name)
//        XCTAssertEqual(item.website, formFields.website)
//
//        let username = try XCTUnwrap(item.username)
//        let decryptedUsername = try keyPair.decrypt(cipherText: username)
//        XCTAssertEqual(decryptedUsername, "gurt")
//
//        let password = try XCTUnwrap(item.password)
//        let decryptedPassword = try keyPair.decrypt(cipherText: password)
//        XCTAssertEqual(decryptedPassword, "pass")
//    }

}
