//
//  NoteDetailsTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 18/11/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest

final class NoteItemTests: TiligTestCase {

    override func setUpWithError() throws {
        try super.setUpWithError()
    }

    func testEmptyInitializer() throws {
        // If
        let noteItem = ItemWrapper(template: .noteV1)

        // Then
        XCTAssertEqual(noteItem.encryptedItem.template, .noteV1)
        XCTAssertEqual(noteItem.overview, DecryptedOverview())
        XCTAssertEqual(noteItem.details, DecryptedDetails())
    }

    func testInitWithDecryptedPayload() throws {
        // If
        let details = DecryptedDetails(
            notes: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
        )
        let overview = DecryptedOverview(
            name: "Pieces of a man"
        )

        // When
        let noteItem = ItemWrapper(
            item: Item(template: .noteV1),
            overview: overview,
            details: details
        )

        // Then
        XCTAssertEqual(noteItem.notes, "Lorem ipsum dolor sit amet, consectetur adipiscing elit")
        XCTAssertEqual(noteItem.name, "Pieces of a man")
    }

    // Note: Legacy encryption shouldn't do anything for note items!
    func testNeverAppliesLegacyEncryption() throws {
        // If
        var noteItem = ItemWrapper(
            item: Item(template: .noteV1, legacyEncryptionDisabled: false),
            overview: DecryptedOverview(),
            details: DecryptedDetails()
        )

        // When
        noteItem.name = "Some name"

        // Then
        XCTAssertNil(noteItem.encryptedItem.name)
        XCTAssertEqual(noteItem.overview.name, "Some name")
    }

    func testSetsNameWithoutLegacyEncryption() throws {
        // If
        var noteItem = ItemWrapper(template: .noteV1)

        // When
        noteItem.name = "Some name"

        // Then
        XCTAssertEqual(noteItem.overview.name, "Some name")
        // It shouldn't set the legacy field
        XCTAssertNil(noteItem.encryptedItem.name)
    }

    func testSettingPropertiesUpdatesDetails() throws {
        // If
        var noteItem = ItemWrapper(template: .noteV1)

        // When
        noteItem.notes = "I love sunglasses"

        // Then
        XCTAssertEqual(noteItem.details.notes, "I love sunglasses")
    }
}
