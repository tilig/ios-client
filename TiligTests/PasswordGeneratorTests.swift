//
//  PasswordGeneratorTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 10/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest

final class PasswordGeneratorTests: TiligTestCase {

    func testRandomPass() throws {
        let password = PasswordGenerator.randomPassword()
        XCTAssertEqual(password.count, 13)
        XCTAssertTrue(password.contains("-"))
    }

    func testRandomPassRegex() throws {
        let password = PasswordGenerator.randomPassword()
        let firstGroup = password[password.startIndex..<(password.index(password.startIndex, offsetBy: 6))]
        let secondGroup = password[(password.index(password.endIndex, offsetBy: -6))..<password.endIndex]

        XCTAssertTrue(
            firstGroup.range(of: "[0-9]", options: .regularExpression) != nil ||
            firstGroup.range(of: "[A-Z]", options: .regularExpression) != nil
        )
        XCTAssertTrue(
            secondGroup.range(of: "[0-9]", options: .regularExpression) != nil ||
            secondGroup.range(of: "[A-Z]", options: .regularExpression) != nil
        )
    }

}
