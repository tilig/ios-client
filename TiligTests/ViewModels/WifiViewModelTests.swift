//
// WifiViewModelTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 26/07/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Toast

@testable import Tilig

class WifiViewModelTests: TiligTestCase {
    var viewModel: ItemViewModel!
    var wifiItem: ItemWrapper!

    override func setUpWithError() throws {
        try super.setUpWithError()
        initWifiItem()
        initViewModel()
    }

    func initWifiItem() {
        wifiItem = ItemWrapper.randomWifiItem(name: "Living room Wifi", ssid: "Huawei 8612", notes: "Extra information")
    }

    func initViewModel() {
        viewModel = ItemViewModel(itemWrapper: wifiItem)
    }

    func testInitialWifiProperties() {
        XCTAssertEqual(viewModel.itemWrapper.name, "Living room Wifi")
        XCTAssertEqual(viewModel.itemWrapper.ssid, "Huawei 8612")
        XCTAssertEqual(viewModel.itemWrapper.notes, "Extra information")
    }

    func testCopySSID() {
        viewModel.copy(field: .ssid)
        XCTAssertEqual(UIPasteboard.general.string, "Huawei 8612")
    }

    func testUpdatingWifiItem() {
        // If
        let newItem = ItemWrapper.randomWifiItem(name: "New Wifi", ssid: "Home network", notes: "Dont connect with your phone")

        // When
        viewModel.update(with: newItem)

        // Then
        XCTAssertEqual(viewModel.itemWrapper.name, "New Wifi")
        XCTAssertEqual(viewModel.itemWrapper.ssid, "Home network")
        XCTAssertEqual(viewModel.itemWrapper.notes, "Dont connect with your phone")
    }
}
