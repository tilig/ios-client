//
//  AddCardViewModelTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 26/07/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Toast
import Mocker
import Combine

@testable import Tilig

class AddCardViewModelTests: TiligTestCase {
    var viewModel: AddCardViewModel!
    private var cancellables: Set<AnyCancellable> = []

    override func setUpWithError() throws {
        try setupViewModel()
    }

    func setupViewModel() throws {
        self.viewModel = AddCardViewModel(
            crypto: .random(),
            tokenProvider: .mocked()
        )

        // Mocker setup
        let configuration = URLSessionConfiguration.default
        configuration.protocolClasses = [MockingURLProtocol.self]
        URLSession.current = URLSession(configuration: configuration)

        Mock(url: Endpoint.items.url,
             dataType: .json,
             statusCode: 200,
             data: [.post: try Data(contentsOf: MockedRequestData.addCardSuccess)]
        ).register()
    }

    func testGetEncryptedCardItem() throws {
        // If
        viewModel.formFields = AddCardFormFields(
            name: "My Credit Card",
            ccnumber: "1112222333344445",
            ccholder: "J Bruno",
            ccexp: "12/24",
            cvv: "123",
            pin: "1234",
            zipcode: "00233"
        )

        // When
        let encryptedCard = try viewModel.getEncryptedItem()

        // Then
        XCTAssertEqual(encryptedCard.itemType, .card)
        XCTAssertNotNil(encryptedCard.rsaEncryptedDek)
        XCTAssertNotNil(encryptedCard.encryptedOverview)
        XCTAssertNotNil(encryptedCard.encryptedDetails)
    }

    func testSaveSetsIsSubmitting() throws {
        // If
        let isSubmitting = viewModel.$isSubmitting.collect(3).first()

        // When
        Task {
            await viewModel.save()
        }

        // Then
        let values = try awaitPublisher(isSubmitting, timeout: 5)
        XCTAssertEqual(values, [false, true, false])
    }

    func testSavePublishesItem() throws {
        // If
        // All fields present
        viewModel.formFields = AddCardFormFields(
            name: "IOS Test 5",
            ccnumber: "1111222233332222",
            ccholder: "Card Holder Gerald",
            ccexp: "12/23",
            cvv: "010",
            pin: "0203",
            zipcode: "00233"
        )

        // When
        Task {
            await viewModel.save()
        }

        // Then
        let card = try awaitPublisher(viewModel.itemCreated.first(), timeout: 2)
        XCTAssertEqual(card.id?.uuidString, "3D814FED-9574-4B28-B728-EE19114EC247")
        // XCTAssertNotNil(card.rsaEncryptedDek)
        XCTAssertNotNil(card.encryptedOverview)
        XCTAssertNotNil(card.encryptedDetails)
    }

    func testIsSaveButtonNotDisabledByDefault() {
        XCTAssertEqual(viewModel.saveButtonDisabled, true)
    }
}
