//
//  AddLoginViewModelTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 26/07/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Toast
import Mocker
import Combine

@testable import Tilig

class AddNoteViewModelTests: TiligTestCase {
    var viewModel: AddNoteViewModel!
    private var cancellables: Set<AnyCancellable> = []
    var prefix = "facebook"
    var crypto: Crypto!

    override func setUpWithError() throws {
        try setupViewModel()
    }

    func setupViewModel() throws {
        crypto = .random()

        viewModel = AddNoteViewModel(
            crypto: crypto,
            tokenProvider: .mocked()
        )

        // Mocker setup
        let configuration = URLSessionConfiguration.default
        configuration.protocolClasses = [MockingURLProtocol.self]
        URLSession.current = URLSession(configuration: configuration)

        Mock(url: Endpoint.items.url,
             dataType: .json,
             statusCode: 200,
             data: [.post: try Data(contentsOf: MockedRequestData.addNoteSuccess)]
        ).register()
    }

    func testGetEncryptedNoteItem() throws {
        // If
        viewModel.formFields = AddNoteFormFields(
            name: "Lorem Ipsum",
            notes: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
        )

        // When
        let item = try viewModel.getEncryptedItem()
        let decryped = try crypto.decrypt(fullItem: item)

        // Then
        XCTAssertEqual(decryped.name, "Lorem Ipsum")
        XCTAssertNotNil(decryped.notes)
    }

    func testSaveSetsIsSubmitting() throws {
        // If
        let isSubmitting = viewModel.$isSubmitting.collect(3).first()

        // When
        Task {
            await viewModel.save()
        }

        // Then
        let values = try awaitPublisher(isSubmitting, timeout: 5)
        XCTAssertEqual(values, [false, true, false])
    }

    func testSavePublishesItem() throws {
        // If
        // All fields present
        viewModel.formFields = AddNoteFormFields(
            name: "Lorem Ipsum",
            notes: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
        )

        // When
        Task {
            await viewModel.save()
        }

        // Then
        let note = try awaitPublisher(viewModel.itemCreated.first(), timeout: 2)
        // Should return the mocked data for the post request, which is Amazon at the moment
        XCTAssertEqual(note.name, "Lorem Ipsum")
        XCTAssertEqual(note.id?.uuidString, "74BD369A-CF97-47EE-AE60-BDEE68A74613")
        // XCTAssertNotNil(note.rsaEncryptedDek)
        XCTAssertNotNil(note.encryptedOverview)
        XCTAssertNotNil(note.encryptedDetails)
        XCTAssertNotNil(note.notes)
    }

    func testIsSaveButtonNotDisabledByDefault() {
        XCTAssertEqual(viewModel.saveButtonDisabled, true)
    }
}
