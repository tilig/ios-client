//
// LoginItemViewModelTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 26/07/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Toast
import Mocker
import Combine

@testable import Tilig

class EditItemViewModelTests: TiligTestCase {
    let authState = AuthState.random()
    var viewModel: EditItemViewModel!
    var itemWrapper: ItemWrapper!
    var crypto = Crypto.random()

    private var cancellables: Set<AnyCancellable> = []

    override func setUpWithError() throws {
        try setupViewModel()
    }

    func setupViewModel() throws {
        self.itemWrapper = ItemWrapper.random(
            encryptedWithKeyPair: crypto.legacyKeyPair,
            name: "Test",
            url: URL(string: "disneyplus.com"),
            notes: "Note to self",
            brand: nil
            // TODO: add url, username, password, etc.
        )

        guard let id = itemWrapper.id else {
            XCTFail("Login item should have an ID")
            return
        }

        self.viewModel = EditItemViewModel(
            id: id,
            itemWrapper: itemWrapper,
            crypto: crypto,
            tokenProvider: MockedTokenProvider()
        )

        // Mocker setup
        let configuration = URLSessionConfiguration.default
        configuration.protocolClasses = [MockingURLProtocol.self]

        Mock(
            url: Endpoint.item(id).url,
            dataType: .json,
            statusCode: 200,
            data: [.put: try Data(contentsOf: MockedRequestData.editItemSuccess)]
        )
        .register()
    }

    func testInitialItemProperties() {
        XCTAssertEqual(viewModel.itemWrapper.name, itemWrapper.name)
        XCTAssertEqual(viewModel.itemWrapper.url, itemWrapper.url)
        XCTAssertEqual(viewModel.itemWrapper.username, itemWrapper.username)
        XCTAssertEqual(viewModel.itemWrapper.password, itemWrapper.password)
        XCTAssertEqual(viewModel.itemWrapper.totp, itemWrapper.totp)
        XCTAssertEqual(viewModel.itemWrapper.notes, itemWrapper.notes)
        XCTAssertEqual(viewModel.itemWrapper.brand, itemWrapper.brand)
    }

    func testHasChangesInitialValue() {
        viewModel = EditItemViewModel(
            id: itemWrapper.id!,
            itemWrapper: itemWrapper,
            crypto: crypto,
            tokenProvider: MockedTokenProvider()
        )

        XCTAssertFalse(viewModel.hasChanges)
    }

    func testDoesNotHaveChangesWhenInitializedWithNote() {
        // If
        let initialItemWrapper = ItemWrapper(
            item: Item(id: UUID(), template: .noteV1),
            overview: DecryptedOverview(),
            details: DecryptedDetails()
        )

        // When
        viewModel = EditItemViewModel(
            id: initialItemWrapper.id!,
            itemWrapper: initialItemWrapper,
            crypto: crypto,
            tokenProvider: MockedTokenProvider()
        )

        // Then
        XCTAssertFalse(viewModel.hasChanges)
    }

    func testDoesNotHaveChangesWhenInitializedWithWIfi() {
        // If
        let initialItemWrapper = ItemWrapper(
            item: Item(id: UUID(), template: .wifiV1),
            overview: DecryptedOverview(),
            details: DecryptedDetails()
        )

        // When
        viewModel = EditItemViewModel(
            id: initialItemWrapper.id!,
            itemWrapper: initialItemWrapper,
            crypto: crypto,
            tokenProvider: MockedTokenProvider()
        )

        // Then
        XCTAssertFalse(viewModel.hasChanges)
    }

    func testHasChanges() {
        let keyPaths = [
            \EditItemViewModel.name,
            \EditItemViewModel.username,
            \EditItemViewModel.password,
            \EditItemViewModel.notes,
            \EditItemViewModel.totp
        ]

        for keyPath in keyPaths {
            XCTAssertFalse(viewModel.hasChanges)
            viewModel[keyPath: keyPath] = "Some change"
            XCTAssertTrue(viewModel.hasChanges)
            viewModel.undoChanges()
        }

        viewModel.url = URL(string: "somechange.com")
        XCTAssertTrue(viewModel.hasChanges)
    }

    func testUndoChanges() {
        // If
        let initialName = viewModel.name
        viewModel.name = "Gerald"
        waitForFirstValue(from: viewModel.$name, matching: "Gerald")
        XCTAssertEqual(viewModel.name, "Gerald")

        // When
        viewModel.undoChanges()

        // Then
        XCTAssertNotEqual(viewModel.name, "Gerald")
        XCTAssertEqual(viewModel.name, initialName)
    }

    // TODO: fix mocking for update API call, then enable this test
//    func testHasChangesIsFalseAfterUpdating() async throws {
//        // If
//        self.viewModel = EditItemViewModel(
//            id: itemWrapper.id!,
//            itemWrapper: itemWrapper,
//            crypto: crypto,
//            tokenProvider: MockedTokenProvider()
//        )
//        viewModel.name = "Some change"
//        // XCTAssertEqual(viewModel.hasChanges, true)
//
//        // When
//        await viewModel.update(trigger: .saveButton)
//
//        // Then
//        XCTAssertEqual(viewModel.hasChanges, false)
//    }

    func testRestorePassword() async {
        let version = HistoricVersion(
            kind: .password,
            value: "history_repeats",
            replacedAt: Date(timeIntervalSince1970: 0)
        )

        await viewModel.restorePassword(version)

        XCTAssertEqual(viewModel.password, version.value)
    }

    func testUpdateOTP() async {
        await viewModel.updateOTP("025059")
        XCTAssertEqual(viewModel.totp, "025059")
    }

    var triggerPublisher: AnyPublisher<UpdateTrigger, Never> {
        viewModel.itemUpdated.map { (_, trigger) in
            return trigger
        }.eraseToAnyPublisher()
    }

    // TODO: fix mocked responses to re-enable these tests. GL issue #289
//    func testUpdateTriggerMethod() async {
//        await viewModel.update(trigger: .saveButton)
//        waitForFirstValue(from: triggerPublisher, matching: .saveButton)
//    }
//
//    func testUpdateTriggerForOTP() async {
//        await viewModel.updateOTP("0123123")
//        waitForFirstValue(from: triggerPublisher, matching: .twoFactorChange)
//    }
//
//    func testUpdateTriggerForPasswordHistory() async throws {
//        let encryptedPassword = try authState.keyPair.encrypt(plainText: "newPassword")
//        await viewModel.restorePassword(
//            LegacyPasswordVersion(id: "1", password: encryptedPassword, createdAt: "1023123")
//        )
//        waitForFirstValue(from: triggerPublisher, matching: .restorePassword)
//    }

    // Disable flaky tests again, no idea why mocker stopped working again
//   func testUpdatesInitialItem() throws {
//       try setupViewModel()
//       XCTAssertNotEqual(viewModel.initialItem?.name, "Amazon")
//
//       // Request is mocked with "Amazon"
//       viewModel.name = "Amazon"
//       let publisher = viewModel.itemUpdated.map { $0.name }
//       viewModel.update()
//
//       waitForFirstValue(from: publisher, matching: "Amazon")
//       XCTAssertEqual(viewModel.initialItem?.name, "Amazon")
//   }
//
//    func testItemUpdated() throws {
//        let expectation = XCTestExpectation(description: "Item is updated")
//
//        // add item details
//        viewModel.name = "Amazon"
//        viewModel.website = URL(string: "https://amazon.com")!
//
//        viewModel
//            .itemUpdated
//            .sink { updatedItem in
//                XCTAssertNotNil(updatedItem)
//                XCTAssertEqual(updatedItem.name, "Amazon")
//                expectation.fulfill()
//            }
//            .store(in: &cancellables)
//
//        viewModel.update()
//
//        wait(for: [expectation], timeout: 1)
//    }
}
