//
// LoginItemViewModelTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 26/07/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Toast

@testable import Tilig

class LoginItemViewModelTests: TiligTestCase {
    var viewModel: ItemViewModel!
    var loginItem: ItemWrapper!
    var brand = Brand.random()

    override func setUpWithError() throws {
        try super.setUpWithError()
        initLoginItem()
        initViewModel()
    }

    func initLoginItem() {
        loginItem = ItemWrapper.random(
            name: "Limp Bizkit",
            url: URL(string: "www.limpbizkit.com")!,
            username: "Fred",
            password: "welcome123",
            notes: "Don't forget to call mom",
            totp: "123-123",
            brand: brand
        )
    }

    func initViewModel() {
        viewModel = ItemViewModel(itemWrapper: loginItem)
    }

    func testInitialItemProperties() {
        XCTAssertEqual(viewModel.itemWrapper.name, "Limp Bizkit")
        XCTAssertEqual(viewModel.itemWrapper.url, URL(string: "www.limpbizkit.com")!)
        XCTAssertEqual(viewModel.itemWrapper.url, URL(string: "www.limpbizkit.com")!)
        XCTAssertEqual(viewModel.itemWrapper.username, "Fred")
        XCTAssertEqual(viewModel.itemWrapper.password, "welcome123")
        XCTAssertEqual(viewModel.itemWrapper.totp, "123-123")
        XCTAssertEqual(viewModel.itemWrapper.notes, "Don't forget to call mom")
        XCTAssertEqual(viewModel.itemWrapper.brand, brand)
    }

    func testCopyNotes() {
        viewModel.copy(field: .notes)
        XCTAssertEqual(UIPasteboard.general.string, "Don't forget to call mom")
    }

    func testCopyUsername() {
        viewModel.copy(field: .username)
        XCTAssertEqual(UIPasteboard.general.string, "Fred")
    }

    func testCopyPassword() {
        viewModel.copy(field: .password)
        XCTAssertEqual(UIPasteboard.general.string, "welcome123")
    }

    func testCopyURL() {
        XCTAssertNotNil(viewModel.itemWrapper.url)
        viewModel.copy(field: .website)
        // Note: prefixed with https://
        XCTAssertEqual(UIPasteboard.general.string, "https://www.limpbizkit.com")
    }

    func testUpdatingLoginItem() {
        // If
        let newItem = ItemWrapper.random(name: "New name", notes: "New notes")

        // When
        viewModel.update(with: newItem)

        // Then
        XCTAssertEqual(viewModel.itemWrapper.name, "New name")
        XCTAssertEqual(viewModel.itemWrapper.notes, "New notes")
    }

    func testCopyPasswordVersion() {
        let version = HistoricVersion(
            kind: .password,
            value: "history_repeats",
            replacedAt: Date(timeIntervalSince1970: 0)
        )
        viewModel.copyPasswordVersion(version)
        XCTAssertEqual(UIPasteboard.general.string, "history_repeats")
    }
}
