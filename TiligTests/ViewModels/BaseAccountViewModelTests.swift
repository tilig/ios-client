//
//  AccountViewModelTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 26/07/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Toast
import Mocker

@testable import Tilig

class BaseAccountViewModelTests: TiligTestCase {
    var viewModel: BaseAccountViewModel!

    override func setUpWithError() throws {
        let authState = AuthState.random()
        if let account = try? Account.random(encryptedWithKeyPair: authState.keyPair) {
            viewModel = BaseAccountViewModel(account: account, keyPair: authState.keyPair)
        } else {
            XCTFail("Generating random account failed")
            return
        }
    }

    func testGetValidAccount() {
        XCTAssertNotNil(viewModel.getValidAccount())
    }

    func testUpdateIconWhenWebsiteChanges() throws {
        let iconURL = viewModel
            .$iconURL
            .collect(2)
            .first()

        viewModel.updateIconWhenWebsiteChanges()
        viewModel.website = URL(string: "https://amazon.com")

        let values = try awaitPublisher(iconURL, timeout: 10)
        XCTAssertEqual(values, [URL(string: "https://icon.tilig.com/missing.png"), URL(string: "https://asset.brandfetch.io/idawOgYOsG/idkXDhRL_u.jpeg")])
    }

    func testEncrypt() throws {
        let notes = "This is a sample note"
        let encryptedNotes = try viewModel.encrypt(notes)
        let decryptedNotes = viewModel.decrypt(encryptedNotes)

        XCTAssertEqual(decryptedNotes, notes)
        XCTAssertNotEqual(encryptedNotes, notes)
    }

    func testDecryptAll() throws {
        let notes = "This is a sample note"
        let password = "randomPassword"
        let username = "fitzgerald@tilig.com"
        let name = "Gerald"

        let encryptedNotes = try viewModel.encrypt(notes)
        let encryptedPassword = try viewModel.encrypt(password)
        let encryptedUsername = try viewModel.encrypt(username)

        let account = Account(
            name: name,
            username: encryptedUsername,
            notes: encryptedNotes,
            password: encryptedPassword
        )
        viewModel.setInitialValues(fromAccount: account)

        try viewModel.decryptAll()

        XCTAssertEqual(viewModel.plainTextNotes, notes)
        XCTAssertEqual(viewModel.plainTextPassword, password)
        XCTAssertEqual(viewModel.plainTextUsername, username)
    }

    override func tearDownWithError() throws {
        viewModel = nil
    }
}
