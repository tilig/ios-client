//
//  AddLoginViewModelTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 26/07/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Toast
import Mocker
import Combine

@testable import Tilig

class AddLoginViewModelTests: TiligTestCase {
    var viewModel: AddLoginViewModel!
    var crypto: Crypto!

    private var cancellables: Set<AnyCancellable> = []
    var prefix = "facebook"

    override func setUpWithError() throws {
        try setupViewModel()
    }

    func setupViewModel() throws {
        self.crypto = .random()

        self.viewModel = AddLoginViewModel(
            crypto: crypto,
            tokenProvider: .mocked()
        )

        // Mocker setup
        let configuration = URLSessionConfiguration.default
        configuration.protocolClasses = [MockingURLProtocol.self]
        URLSession.current = URLSession(configuration: configuration)

        Mock(url: Endpoint.brandSearch(prefix).url,
             dataType: .json,
             statusCode: 200,
             data: [.get: try Data(contentsOf: MockedRequestData.brands)]
        ).register()

        Mock(url: Endpoint.getBrand(URL(string: "facebook.com")!).url,
             dataType: .json,
             statusCode: 200,
             data: [.get: try Data(contentsOf: MockedRequestData.brand)]
        ).register()

        Mock(url: Endpoint.items.url,
             dataType: .json,
             statusCode: 200,
             data: [.post: try Data(contentsOf: MockedRequestData.addItemSuccess)]
        ).register()
    }

    func testContinueWithPrefilledWebsite() {
        let website = PrefillableWebsite(
            name: "Airbnb",
            url: URL(string: "https://airbnb.com")!,
            localAssetName: "airbnb"
        )

        viewModel.continueWithPrefilledWebsite(website)

        XCTAssertEqual(website.name, viewModel.formFields.name)
        XCTAssertEqual(website.url, viewModel.formFields.website)
        XCTAssertEqual(website.localAssetName, "airbnb")
    }

    func testContinueWithBrand() {
        let brand = Brand(
            name: "Google",
            domain: "www.google.com",
            totp: false
        )

        viewModel.continueWithBrand(brand)

        XCTAssertEqual(viewModel.formFields.name, "Google")
        XCTAssertEqual(viewModel.formFields.website, URL(string: "www.google.com")!)
        XCTAssertNil(viewModel.localIconAssetName)
        XCTAssertEqual(viewModel.brand, brand)
    }

    func testContinueWithSearchText() {
        viewModel.searchText = "Henkje"
        viewModel.continueWithSearchText()

        XCTAssertEqual(viewModel.formFields.name, "Henkje")
        // we'll want to change this in a future release
        XCTAssertEqual(viewModel.formFields.website?.absoluteString, "Henkje")
        XCTAssertNil(viewModel.brand)
        XCTAssertNil(viewModel.localIconAssetName)
    }

    func testStepsChanging() throws {
        try setupViewModel()
        XCTAssertEqual(viewModel.currentStep, .addWebsite)
        viewModel.goToNextStep()
        XCTAssertEqual(viewModel.currentStep, .addOtherDetails)
        viewModel.goToPreviousStep()
        XCTAssertEqual(viewModel.currentStep, .addWebsite)
    }

    func testSaveSetsIsSubmitting() throws {
        // If
        let isSubmitting = viewModel.$isSubmitting.collect(3).first()

        // When
        Task {
            await viewModel.save()
        }

        // Then
        let values = try awaitPublisher(isSubmitting, timeout: 5)
        XCTAssertEqual(values, [false, true, false])
    }

    func testSavePublishesItem() throws {
        // If
        // All fields present
        viewModel.formFields = AddLoginFormFields(
            name: "Save test",
            website: URL(string: "www.google.com"),
            username: "henkje",
            password: "password"
        )

        // When
        Task {
            await viewModel.save()
        }

        // Then
        let login = try awaitPublisher(viewModel.itemCreated.first(), timeout: 2)
        // Should return the mocked data for the post request, which is Amazon at the moment
        XCTAssertEqual(login.name, "Amazon")
        XCTAssertEqual(login.id?.uuidString, "D60F9626-3348-4C81-9D24-C3D06C1C486C")
        // XCTAssertNotNil(login.rsaEncryptedDek)
        XCTAssertNotNil(login.encryptedOverview)
        XCTAssertNotNil(login.encryptedDetails)

    }

    func testFetchBrand() async {
        // If
        let url = URL(string: "facebook.com")!
        viewModel.brand = nil
        viewModel.formFields = AddLoginFormFields(
            name: "brandfetch",
            website: url,
            username: "henkje",
            password: "password"
        )

        // When
        let brand = await viewModel.fetchBrand(for: url)

        // Then
        // swiftlint:disable line_length
        let pngURL = "https://proxy.img.infra.tilig.com/C6RvwxaBbTLYfgCLcm2-FLEmSB6f8tpSZd_NzeHG8ME/aHR0cHM6Ly9hc3Nl/dC5icmFuZGZldGNo/LmlvL2lkNk8yb0d6/di0vaWQtRHVPdG9y/Xy5wbmc.png"
        // swiftlint:disable line_length
        let svgURL = "https://proxy.img.infra.tilig.com/ytvfCgMTjnvRMFHKxAi8qpELPGiydBA3svabZ9WYUEU/aHR0cHM6Ly9hc3Nl/dC5icmFuZGZldGNo/LmlvL2lkNk8yb0d6/di0vaWR2TklRUjNw/Ny5zdmc.svg"
        XCTAssertEqual(
            brand,
            Brand(
                name: "Facebook",
                domain: "facebook.com",
                totp: true,
                mainColorHex: "#1877f2",
                images: BrandImageDetails(
                    icon: BrandImageIcon(
                        svg: BrandImageExtension(original: svgURL),
                        png: BrandImageExtension(original: pngURL)
                    )
                )
            )
        )
    }

    func testIsSaveButtonNotDisabledByDefault() {
        XCTAssertEqual(viewModel.saveButtonDisabled, false)
    }

    func testGetEncryptedItem() throws {
        // If
        viewModel.formFields = AddLoginFormFields(
            name: "Test",
            website: URL(string: "test.com")!,
            username: "user123",
            password: "pass123"
        )

        // When
        let encryptedItem = try viewModel.getEncryptedItem()

        // Then
        let itemWrapper = try crypto.decrypt(fullItem: encryptedItem)
        XCTAssertEqual(itemWrapper.username, "user123")
        XCTAssertEqual(itemWrapper.password, "pass123")
        XCTAssertEqual(itemWrapper.urlString, "https://test.com")
        XCTAssertEqual(itemWrapper.name, "Test")
    }

    // @Fitz please fix
//    func testItemCreated() throws {
//        let expectation = XCTestExpectation(description: "Item is created")
//
//        // add item details
//        viewModel.name = "Amazon"
//        viewModel.website = URL(string: "https://amazon.com")!
//
//        viewModel.itemCreated
//            .sink { newItem in
//                XCTAssertNotNil(newItem)
//                XCTAssertEqual(newItem.name, "Amazon")
//                expectation.fulfill()
//            }.store(in: &cancellables)
//
//        viewModel.save()
//
//        wait(for: [expectation], timeout: 10)
//    }
}
