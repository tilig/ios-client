//
//  DeleteItemViewModelTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 26/07/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Toast
import Mocker
import Combine

@testable import Tilig

class DeleteItemViewModelTests: TiligTestCase {
    var viewModel: DeleteItemViewModel!
    var item: Item!
    private var cancellables: Set<AnyCancellable> = []

    override func setUpWithError() throws {
        let authState = AuthState.random()

        let configuration = URLSessionConfiguration.default
        configuration.protocolClasses = [MockingURLProtocol.self]
        URLSession.current = URLSession(configuration: configuration)

        let item = Item.random(encryptedWithKeyPair: authState.legacyKeyPair)

        setupViewModel(withItem: item)
    }

    func setupViewModel(withItem item: Item) {
        self.item = item

        viewModel = DeleteItemViewModel(item: item, tokenProvider: .mocked())

        // Mock request for this specific item
        if let id = item.id {
            guard let data = try? Data(contentsOf: MockedRequestData.delete) else {
                XCTFail("Can't delete mock json file")
                return
            }

            Mock(
                url: Endpoint.item(id).url,
                dataType: .json,
                statusCode: 200,
                data: [.delete: data]
            )
            .register()
        }
    }

    func testIsDeleting() throws {
        let deleted = viewModel
            .$isDeleting
            .collect(3)
            .first()

        Task {
            await viewModel.delete()
        }

        let values = try awaitPublisher(deleted, timeout: 10)
        XCTAssertEqual(values, [false, true, false])
    }

    // FIX: flaky tests
//    func testThrowsErrorForNonpersistedItem() throws {
//        let item = Item(name: "test")
//        XCTAssertNil(item.id)
//        setupViewModel(withItem: item)
//        let publisher = NotificationCenter.default.publisher(for: .globalError)
//            .map { _ in
//                return "test"
//            }
//        viewModel.delete()
//        waitForFirstValue(from: publisher, matching: "test")
//    }
//
//    func testItemDeleted() throws {
//        let expectation = XCTestExpectation(description: "Item is deleted")
//        viewModel.itemDeleted
//            .sink { output in
//                XCTAssertEqual(output, self.item)
//                expectation.fulfill()
//            }.store(in: &cancellables)
//
//        viewModel.delete()
//        wait(for: [expectation], timeout: 10)
//    }
}
