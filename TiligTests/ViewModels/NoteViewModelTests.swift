//
// LoginItemViewModelTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 26/07/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Toast

@testable import Tilig

class NoteViewModelTests: TiligTestCase {
    var viewModel: ItemViewModel!
    var noteItem: ItemWrapper!

    override func setUpWithError() throws {
        try super.setUpWithError()
        initNoteItem()
        initViewModel()
    }

    func initNoteItem() {
        noteItem = ItemWrapper.randomNote(
            name: "Lorem Ipsum",
            notes: "Don't forget to call mom"
        )
    }

    func initViewModel() {
        viewModel = ItemViewModel(itemWrapper: noteItem)
    }

    func testInitialNoteProperties() {
        XCTAssertEqual(viewModel.itemWrapper.name, "Lorem Ipsum")
        XCTAssertEqual(viewModel.itemWrapper.notes, "Don't forget to call mom")
    }

    func testCopyNotes() {
        viewModel.copy(field: .notes)
        XCTAssertEqual(UIPasteboard.general.string, "Don't forget to call mom")
    }

    func testUpdatingNoteItem() {
        // If
        let newItem = ItemWrapper.randomNote(name: "New name", notes: "New notes")

        // When
        viewModel.update(with: newItem)

        // Then
        XCTAssertEqual(viewModel.itemWrapper.name, "New name")
        XCTAssertEqual(viewModel.itemWrapper.notes, "New notes")
    }
}
