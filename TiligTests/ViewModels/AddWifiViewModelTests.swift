//
//  AddWifiViewModelTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 26/07/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Toast
import Mocker
import Combine

@testable import Tilig

class AddWifiViewModelTests: TiligTestCase {
    var viewModel: AddWifiViewModel!
    private var cancellables: Set<AnyCancellable> = []
    private var crypto: Crypto!

    override func setUpWithError() throws {
        try setupViewModel()
    }

    func setupViewModel() throws {
        crypto = .random()

        viewModel = AddWifiViewModel(
            crypto: crypto,
            tokenProvider: .mocked()
        )

        // Mocker setup
        let configuration = URLSessionConfiguration.default
        configuration.protocolClasses = [MockingURLProtocol.self]
        URLSession.current = URLSession(configuration: configuration)

        Mock(url: Endpoint.items.url,
             dataType: .json,
             statusCode: 200,
             data: [.post: try Data(contentsOf: MockedRequestData.addWifiSuccess)]
        ).register()
    }

    func testGetEncryptedNoteItem() throws {
        // If
        viewModel.formFields = AddWifiFormFields(
            name: "Living room Wifi",
            ssid: "Huawei 8612",
            password: "randompass",
            notes: "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
        )

        // When
        let encryptedWifi = try viewModel.getEncryptedItem()
        let decrypted = try crypto.decrypt(fullItem: encryptedWifi)

        // Then
        XCTAssertEqual(decrypted.overview.name, "Living room Wifi")
        XCTAssertNotNil(decrypted.details.notes)
    }

    func testSaveSetsIsSubmitting() throws {
        // If
        let isSubmitting = viewModel.$isSubmitting.collect(3).first()

        // When
        Task {
            await viewModel.save()
        }

        // Then
        let values = try awaitPublisher(isSubmitting, timeout: 5)
        XCTAssertEqual(values, [false, true, false])
    }

    func testSavePublishesItem() throws {
        // If
        // All fields present
        viewModel.formFields = AddWifiFormFields(
            name: "iPhone Hotspot",
            ssid: "iPhone 14 Pro Hotspot",
            password: "randompass",
            notes: "Extra information"
        )

        // When
        Task {
            await viewModel.save()
        }

        // Then
        let wifi = try awaitPublisher(viewModel.itemCreated.first(), timeout: 2)
        XCTAssertEqual(wifi.id?.uuidString, "3DBF9096-8F90-46AF-8713-5D81AD1EBDA8")
        // XCTAssertNotNil(wifi.rsaEncryptedDek)
        XCTAssertNotNil(wifi.encryptedOverview)
        XCTAssertNotNil(wifi.encryptedDetails)
    }

    func testIsSaveButtonNotDisabledByDefault() {
        XCTAssertEqual(viewModel.saveButtonDisabled, true)
    }
}
