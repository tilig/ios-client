//
//  BiometricViewModelTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 04/10/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Valet

final class BiometricViewModelTests: TiligTestCase {

    var viewModel: BiometricViewModel!
    var mockedContext: LAContextMock!

    override func setUp() async throws {
        viewModel = BiometricViewModel()
    }

    func initViewModel(enabled: Bool, locked: Bool = false) {
        // Clear any previous state
        try? Valet.auth.removeObject(forKey: ValetKeys.biometricLockEnabled.rawValue)

        // Mock context + helper
        mockedContext = LAContextMock()
        mockedContext.canEvaluate = true
        mockedContext.evaluateSuccess = true

        // Setup view model
        viewModel = BiometricViewModel(biometricHelper: BiometricHelper(context: mockedContext))
        XCTAssertFalse(viewModel.isEnabled)
        XCTAssertFalse(viewModel.isLocked)

        // Set initial state
        if enabled {
            viewModel.toggleEnabled()
            waitForFirstValue(from: viewModel.$isEnabled, matching: true)
            XCTAssertTrue(viewModel.isEnabled)
        } else {
            XCTAssertFalse(viewModel.isEnabled)
        }
        if locked {
            viewModel.lockIfEnabled()
            waitForFirstValue(from: viewModel.$isLocked, matching: true)
            XCTAssertTrue(viewModel.isLocked)
        } else {
            XCTAssertFalse(viewModel.isLocked)
        }
    }

    func testEnable() throws {
        // If
        initViewModel(enabled: false)
        try Valet.auth.removeObject(forKey: ValetKeys.biometricLockEnabled.rawValue)
        let dataBefore = try? Valet.auth.object(forKey: ValetKeys.biometricLockEnabled.rawValue)
        XCTAssertTrue(dataBefore == nil)
        XCTAssertFalse(viewModel.isEnabled)

        // When
        viewModel.toggleEnabled()

        // Then
        waitForFirstValue(from: viewModel.$isEnabled, matching: true)
        XCTAssertNotNil(try? Valet.auth.object(forKey: ValetKeys.biometricLockEnabled.rawValue))
    }

    func testDisable() throws {
        // If
        initViewModel(enabled: true)

        // When
        viewModel.toggleEnabled()

        // When
        waitForFirstValue(from: viewModel.$isEnabled, matching: false)
        XCTAssertNotNil(try? Valet.auth.object(forKey: ValetKeys.biometricLockEnabled.rawValue))
    }

    func testLockIfEnabled() {
        initViewModel(enabled: true)
        XCTAssertEqual(viewModel.isEnabled, true)
        viewModel.lockIfEnabled()
        XCTAssertEqual(viewModel.isLocked, true)
    }

    func testLockIfNotEnabled() {
        initViewModel(enabled: false)
        viewModel.lockIfEnabled()
        XCTAssertEqual(viewModel.isLocked, false)
    }

    // TODO: fix this test by finding a workaround for clearing the LAContext
//    func testTryToUnlockSuccess() {
//        // If
//        initViewModel(enabled: true, locked: true)
//        mockedContext.evaluateSuccess = true
//
//        // When
//        viewModel.tryToUnlock()
//
//        // Then
//        waitForFirstValue(from: viewModel.$isLocked, matching: false, timeout: 2)
//    }

    func testTryToUnlockFailure() {
        // If
        initViewModel(enabled: true, locked: true)
        mockedContext.evaluateSuccess = false

        // When
        viewModel.tryToUnlock()

        // Then
        waitForFirstValue(from: viewModel.$isLocked, matching: true, timeout: 2)
    }

}
