//
// LoginItemViewModelTests.swift
//  TiligTests
//
//  Created by Fitzgerald Afful on 26/07/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import XCTest
import Toast

@testable import Tilig

class GetStartedViewModelTests: TiligTestCase {
    var viewModel: GetStartedViewModel!

    override func setUpWithError() throws {
        try super.setUpWithError()
        initViewModel()
    }

    func initViewModel() {
        viewModel = GetStartedViewModel(
            userProfile: .random(),
            autoFillEnabled: false,
            biometricViewModel: BiometricViewModel(),
            tokenProvider: .mocked()
        )
    }

    func testCompleteEnableAutoFill() async {
        // when
        await viewModel.completeStep(type: .enableAutoFill)

        // then
        XCTAssertEqual(viewModel.enableAutoFillCompleted, true)
    }

    func testCompleteLearnAutoFill() async {
        // when
        await viewModel.completeStep(type: .learnAutoFill)

        // then
        XCTAssertEqual(viewModel.learntAutoFill, true)
    }

    func testCompleteBiometric() async {
        // when
        await viewModel.completeStep(type: .enableBiometric)

        // then
        XCTAssertEqual(viewModel.biometricCompleted, true)
    }

    func testCompleteAddLogin() async {
        // when
        await viewModel.completeStep(type: .addLogin)

        // then
        XCTAssertEqual(viewModel.firstLoginAdded, true)
    }

    func testDismissForever() {
        // when
        viewModel.dismissForever()

        // then
        XCTAssertEqual(viewModel.getStartedCompleted, true)
    }
}
