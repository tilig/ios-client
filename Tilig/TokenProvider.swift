//
//  TokenProvider.swift
//  Tilig
//
//  Created by Gertjan Jansen on 29/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

/// Useful to mock access tokens in tests
protocol TokenProvider {
    func getAccessToken() async throws -> Token
}
