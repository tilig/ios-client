//
//  PageView.swift
//  AsyncJam
//
//  Created by Gertjan Jansen on 27/11/2020.
//  Copyright © 2020 Gertjan Jansen. All rights reserved.
//

import SwiftUI
import os.log

struct PageView<Page: View>: View, Equatable {
    private var viewControllers: [UIHostingController<Page>]
    @State private var currentPage = 0
    var onCurrentPageChanged: (Int) -> Void = { _ in }

    init(views: [Page], onCurrentPageChanged: @escaping (Int) -> Void = { _ in }) {
        self.viewControllers = views.map { UIHostingController(rootView: $0) }
        self.onCurrentPageChanged = onCurrentPageChanged
    }

    var body: some View {
        VStack {
            PageViewController(controllers: viewControllers, currentPage: $currentPage)
            PageControl(numberOfPages: viewControllers.count, currentPage: $currentPage)
                .padding(.trailing)
        }
        .onChange(of: currentPage) { newPage in
            onCurrentPageChanged(newPage)
        }
    }

    // PageView is recreated during update of body of parent view when its content changes.
    // To avoid this, PageView must implement Equatable
    static func == (lhs: PageView<Page>, rhs: PageView<Page>) -> Bool {
        return lhs.currentPage == rhs.currentPage
    }
}

#if DEBUG
struct PageView_Previews: PreviewProvider {
    static var previews: some View {
        PageView(views: [
            IntroPage(title: "Test", subTitle: "Page 1", imageName: "Female+iPhone"),
            IntroPage(title: "Supersecure fancyness", subTitle: "Page 2", imageName: "Female+iPhone")
        ])
    }
}
#endif
