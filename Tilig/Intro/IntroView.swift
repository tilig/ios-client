//
//  IntroView.swift
//  AsyncJam
//
//  Created by Gertjan Jansen on 29/11/2020.
//  Copyright © 2020 Gertjan Jansen. All rights reserved.
//

import SwiftUI
import Sentry

struct IntroView: View {
    @State var showPager = false
    @State private var selectedItem = 1
    @State private var itemCount = 4
    @State private var showAuthScreen = false

    var body: some View {
        ZStack {
            if showPager {
                if showAuthScreen {
                    SignInScreen(backAction: { showAuthScreen = false })
                } else {
                    // This can't be a Group to make the `.task()` work
                    VStack(spacing: 0) {
                        HStack {
                            Spacer()
                            Button(action: { showAuthScreen = true }) {
                                Text("Skip")
                                    .defaultButtonFont(color: .mediumPurple, size: 16)
                            }
                        }
                        .frame(height: 40)
                        .responsivePadding(.horizontal, compact: 16, regular: 32)

                        TabView(selection: $selectedItem) {
                            IntroPage(
                                title: "Safely store passwords",
                                subTitle:
                                     """
                                     A single place to store all your passwords safely.
                                     """,
                                imageName: "intro_1"
                            )
                            .tag(1)

                            IntroPage(
                                title: "Move freely",
                                subTitle:
                                     """
                                     Login to your accounts on-the-go, \
                                     from anywhere and from any device.
                                     """,
                                imageName: "intro_2"
                            )
                            .tag(2)

                            IntroPage(
                                title: "Notes, cards & more",
                                subTitle:
                                     """
                                     Safely store secure notes, credit \
                                     card details, and more in Tilig.
                                     """,
                                imageName: "intro_3"
                            )
                            .tag(3)

                            IntroPage(
                                title: "Extra security",
                                subTitle:
                                     """
                                     Enjoy an extra layer of security with \
                                     2FA for your accounts, in any device.
                                     """,
                                imageName: "intro_4"
                            )
                            .tag(4)
                            .onAppear {
                                TrackingEvent.showWelcomeInstructionsCompleted.send()
                            }
                        }
                        .tabViewStyle(.page)
                        .padding(.bottom, 20)
                        .preferredColorScheme(.light)

                        CircularProgressButton(
                            action: {
                                withAnimation {
                                    if selectedItem < itemCount {
                                        selectedItem += 1
                                    } else {
                                        showAuthScreen = true
                                    }
                                }
                            },
                            progress: selectedItem
                        )
                        .padding(.bottom, 60)
                    }
                }
            }
        }
        .task(delayPager)
    }

    // delay pageview appearance by 0.5s to fix bug where pageview doesnt swipe
    @Sendable private func delayPager() async {
        do {
            try await Task.sleep(nanoseconds: 500_000_000)
            withAnimation {
                showPager = true
            }
        } catch {
            SentrySDK.capture(error: error)
            Log("Sleep task failed wih error: \(error)", type: .error)
        }
    }
}

#if DEBUG
struct IntroView_Previews: PreviewProvider {
    static var previews: some View {
        IntroView()
        // .environment(\.colorScheme, .dark)
    }
}
#endif
