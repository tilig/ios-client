//
//  IntroPopupView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 11/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct IntroPopupView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        VStack(spacing: 0) {
            PopupHeaderView(
                title: "More info about signing in",
                closeButtonAction: {
                    self.presentationMode.wrappedValue.dismiss()
                }
            )

            VStack {
                HStack(alignment: .center) {
                    Image("apple")
                        .resizable()
                        .scaledToFit()

                    Image("google")
                        .resizable()
                        .scaledToFit()

                    Image("microsoft")
                        .resizable()
                        .scaledToFit()
                        .padding(8)
                }
                .responsiveHeight(compact: 80, regular: 160)
                .padding(.top, 40)
                .padding(.bottom, 20)

                Text("Why do we use Google, Apple or Microsoft to set up your Tilig account?")
                    .defaultBoldFont(size: 22)
                    .multilineTextAlignment(.leading)
                    .padding(.vertical)
                    .frame(alignment: .leading)

                Text("""
                    There’s a very high chance that you already have an account with Google, Apple or Microsoft.

                    By signing-up with one of these accounts, you have one less password to memorise.

                    So, signing-up is simple and secure. We call that a win-win.
                    """)
                    .subTextFont(size: 16)
                    .multilineTextAlignment(.leading)
                    .lineSpacing(4)
                    .fixedSize(horizontal: false, vertical: true)
            }
            .responsivePadding(.horizontal, compact: 40, regular: 80)
            .responsivePadding(.vertical, compact: 20, regular: 40)

            Spacer()
        }
    }
}
