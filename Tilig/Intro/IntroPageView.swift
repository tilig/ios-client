//
//  IntroPage.swift
//  Tilig
//
//  Created by Gertjan Jansen on 30/11/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import SwiftUI
import AuthenticationServices

struct IntroPage: View {
    @Environment(\.horizontalSizeClass) var sizeClass
    @Environment(\.colorScheme) var colorScheme

    var title: String
    var subTitle: String
    var imageName: String

    var body: some View {
        GeometryReader { reader in
            VStack {
                Spacer()
                    .frame(height: 20)

                Image(imageName)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .responsiveHeight(compact: 320, regular: reader.size.height * 0.7)
                    .if(sizeClass == .regular) { view in
                        view.clipped()
                    }

                VStack(spacing: 16) {
                    Text(title)
                        .largeBoldFont(color: .blue, size: 24)
                        .multilineTextAlignment(.center)
                        .lineLimit(2)

                    Text(subTitle)
                        .defaultFont(color: .subTextLightPurple, size: 16)
                        .multilineTextAlignment(.center)
                        .lineSpacing(4)
                        .lineLimit(4)
                }
                .minimumScaleFactor(0.2)
                .padding(.horizontal, 30)
                .padding(.top, 60)
                .padding(.bottom, 30)
            }
        }
    }
}

#if DEBUG
struct IntroPage_Previews: PreviewProvider {
    static var previews: some View {
        IntroPage(
            title: "World class security",
            subTitle: "Welcome to Tilig. We store and fill your", imageName: "intro_1"
        )
    }
}
#endif
