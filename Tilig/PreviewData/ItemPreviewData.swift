//
//  ItemPreviewData.swift
//  Tilig
//
//  Created by Jacob Schatz on 1/23/20.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import Foundation

struct ItemPreviewData {
    let data = [
        Item(id: UUID(uuidString: "e46d8e06-0667-4864-ae6d-007a5702b507")!,
                name: "Yahoo",
                website: CodableOptionalURL(string: "https://www.nytimes.com"),
                username: "jschatz1",
                password: "hahapassword123",
                notes: "this is good",
                otp: "",
                createdAt: nil,
                updatedAt: nil)

    ]
}
