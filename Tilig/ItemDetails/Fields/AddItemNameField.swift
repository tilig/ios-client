//
//  AddItemNameField.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 24/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

struct AddItemNameField: View {
    var placeholder: String?
    @Binding var value: String?
    @State var focused: Bool = false

    var body: some View {
        ZStack(alignment: .leading) {
            FocussableTextField(text: Binding<String>(
                get: { self.value ?? "" },
                set: { self.value = $0 }
            ), isFirstResponder: $focused) {
                $0.autocapitalizationType = .sentences
                $0.autocorrectionType = .no
                $0.font = .defaultBoldFont(size: 24)
                $0.textColor = UIColor(Color.blue)
            }
            .frame(height: 21)

            if value == nil || value == "", let placeholder {
                Text(placeholder)
                    .defaultButtonFont(color: .gray.opacity(0.5), size: 16)
                    .frame(alignment: .leading)
                    .lineSpacing(4)
            }
        }
        .padding()
        .frame(height: 48)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(focused ? Color.mediumPurple : .brightGray, lineWidth: 1)
        )
        .onTapGesture {
            focused = true
        }
    }
}
