//
//  InvisibleInputField.swift
//  Tilig
//
//  Created by Gertjan Jansen on 15/02/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

/// This is used to add an invisible field in the item view,
/// which prevents iOS from autofilling the username field.
struct InvisibleInputField: View {
    var body: some View {
        TextField("", text: Binding<String>(
            get: { "" },
            set: { _ in }
        ))
        .disabled(true)
        .frame(height: 0)
    }
}
