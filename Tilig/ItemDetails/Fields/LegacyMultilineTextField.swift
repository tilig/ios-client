//
//  MultilineTextField.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 23/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

struct LegacyMultilineTextField: UIViewRepresentable {
    @Binding public var isFirstResponder: Bool
    @Binding public var text: String

    public var configuration = { (_: UITextView) in }

    public init(
        text: Binding<String>,
        isFirstResponder: Binding<Bool>,
        configuration: @escaping (UITextView) -> Void = { _ in }
    ) {
        self.configuration = configuration
        self._text = text
        self._isFirstResponder = isFirstResponder
    }

    public func makeUIView(context: Context) -> UITextView {
        let view = UITextView()
        view.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(Coordinator.textViewDidChange(_:)),
            name: UITextView.textDidChangeNotification,
            object: nil
        )
        view.delegate = context.coordinator
        return view
    }

    public func updateUIView(_ uiView: UITextView, context: Context) {
        uiView.text = text
        configuration(uiView)
        Task { @MainActor in
            switch isFirstResponder {
            case true:
                _ = uiView.becomeFirstResponder()
            case false:
                _ = uiView.resignFirstResponder()
            }
        }
    }

    public func makeCoordinator() -> Coordinator {
        Coordinator($text, isFirstResponder: $isFirstResponder)
    }

    public class Coordinator: NSObject, UITextViewDelegate {
        var text: Binding<String>
        var isFirstResponder: Binding<Bool>

        init(_ text: Binding<String>, isFirstResponder: Binding<Bool>) {
            self.text = text
            self.isFirstResponder = isFirstResponder
        }

        public func textViewDidBeginEditing(_ textView: UITextView) {
            self.isFirstResponder.wrappedValue = true
        }

        public func textViewDidEndEditing(_ textView: UITextView) {
            Task { @MainActor in
                self.isFirstResponder.wrappedValue = false
            }
        }

        public func textViewDidChange(_ textView: UITextView) {
            self.text.wrappedValue = textView.text ?? ""
        }

        public func textFieldDidBeginEditing(_ textField: UITextField) {
            self.isFirstResponder.wrappedValue = true
        }

        // Added this myself. In most cases we want to resign input on return
        public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            self.isFirstResponder.wrappedValue = false
            return false
        }
    }
}
