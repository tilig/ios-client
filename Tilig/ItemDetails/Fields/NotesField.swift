//
//  FormTextField.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 07/01/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct NotesField: View {
    var label: String
    @Binding var value: String? {
        didSet {
            if hasLimit {
                truncateValue()
            }
        }
    }
    var onFocusChange: (Bool) -> Void = { _ in }
    @State private var focused: Bool = false
    var hasLimit: Bool = false
    private let characterLimit = 190

    /// Truncate the value when it exceeds the character limit
    /// Can be removed when we migrated to the v3 encryption model.
    func truncateValue() {
        guard let value = value else { return }
        if value.count > characterLimit {
            self.value = String(value.prefix(characterLimit))
        }
    }

    var body: some View {
        if #available(iOS 16.0, *) {
            MultilineTextField(
                label: label,
                value: $value,
                onFocusChange: onFocusChange,
                hasLimit: hasLimit
            )
        } else {
            LegacyMultilineTextField(text: Binding<String>(
                get: { self.value ?? "" },
                set: { self.value = $0 }
            ), isFirstResponder: $focused) {
                $0.autocapitalizationType = .sentences
                $0.autocorrectionType = .yes
                $0.font = .defaultButtonFont(size: 16)
                $0.textColor = UIColor(Color.blue)
            }
            .frame(maxWidth: .infinity, minHeight: 72, alignment: .leading)
            .foregroundColor(Color.blue)
            .padding(8)
            .defaultButtonFont(size: 16)
            .lineSpacing(4)
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(focused ? Color.mediumPurple : .brightGray, lineWidth: 1)
            )
            .background(
                RoundedRectangle(cornerRadius: 10)
                    .fill(Color.white)
            )
            .onChange(of: focused) { hasFocus in
                onFocusChange(hasFocus)
            }
        }
    }
}

struct NotesFields_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            NotesField(
                label: "test",
                value: .constant("Hey!")
            )
            .padding()
        }
    }

}
