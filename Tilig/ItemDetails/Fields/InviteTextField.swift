//
//  InviteTextField.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 03/03/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct InviteTextField: View {
    var label: String
    @Binding var value: String
    @Binding var focused: Bool
    var showIcon = true
    var iconName = "invite-share"
    var focusedBorderColor: Color = .mediumPurple
    var sendAction: () -> Void

    var body: some View {
        HStack {
            FocussableTextField(placeholder: label, text: Binding<String>(
                get: { self.value },
                set: { self.value = $0 }
            ), isFirstResponder: $focused) {
                $0.autocapitalizationType = .none
                $0.autocorrectionType = .no
                $0.font = .defaultButtonFont(size: 14)
                $0.textColor = UIColor(Color.blue)
            }
            .frame(height: 21)

            if showIcon {
                Button(action: sendAction) {
                    Image(iconName)
                        .frame(width: 12, height: 12, alignment: .center)
                        .foregroundColor(.blue.opacity(0.5))
                        .padding(.trailing, 8)
                }
            }
        }
        .padding(8)
        .frame(height: 44)
        .overlay(
            RoundedRectangle(cornerRadius: 8)
                .stroke(focused ? focusedBorderColor : .brightGray, lineWidth: 2)
        )
        .background(
            RoundedRectangle(cornerRadius: 8, style: .continuous)
                .fill(.white)
        )
        .onTapGesture {
            focused = true
        }
    }
}
