//
//  PasswordField.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 06/01/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct PasswordField: View {
    var placeholder = "Enter a password"
    @Binding var password: String?
    @State var isHidden = true {
        didSet {
            if !isHidden {
                TrackingEvent.passwordRevealed.send()
            }
        }
    }
    // Note that isHidden needs to be false in order for this to work
    @State var isFirstResponder = false
    // Useful to prevent interferance with other animations
    var setFirstResponderAfterDelay: Double?
    var showIcon = false
    var showGenerateButton = true
    var backgroundColor = Color.clear
    var keyboardType: UIKeyboardType = .default
    var characterLimit: Int?

    // This can be private when MR 308 gets merged
    var font: UIFont = .defaultButtonFont(size: 16)
    // For design reasons, we want a different font when the password is hidden
    private var fontWhenHidden: Font { .stolzlRegular(size: 16) }

    private var inputHeight: Double { 21.0 }
    private var cornerRadius: Double { 10.0 }
    private var permittedCharacterSet: CharacterSet {
        return keyboardType == .numberPad ? .decimalDigits : .alphanumerics
    }

    private func refreshPassword() {
        password = PasswordGenerator.randomPassword()
        isHidden = false
        TrackingEvent.passwordGenerated.send()
        isFirstResponder = false
    }

    var body: some View {
        ZStack(alignment: .leading) {
            HStack {
                if showIcon {
                    Image("extension_password")
                        .frame(width: 12, height: 12, alignment: .top)
                        .foregroundColor(.blue.opacity(0.5))
                        .padding(.trailing, 8)
                }

                if isHidden {
                    SecureField(placeholder, text: Binding<String>(
                        get: { self.password ?? "" },
                        set: { self.password = $0 }
                    ))
                    .font(fontWhenHidden)
                    .frame(height: inputHeight)
                } else {
                    FocussableTextField(text: Binding<String>(
                        get: { self.password ?? "" },
                        set: { self.password = $0 }
                    ), isFirstResponder: $isFirstResponder) {
                        $0.autocapitalizationType = .none
                        $0.autocorrectionType = .no
                        $0.textColor = UIColor(Color.blue)
                        $0.keyboardType = keyboardType
                        $0.placeholder = placeholder
                        $0.font = font
                    }
                    .frame(height: inputHeight)
                }

                // This syntax is necessary to make Button work in a List
                Button(action: { isHidden.toggle() }) {
                    Image(systemName: isHidden ? "eye.fill" : "eye.slash.fill")
                }
            }
            .onTapGesture {
                isHidden = false
                isFirstResponder = true
            }
            .padding()
            .overlay(
                RoundedRectangle(cornerRadius: cornerRadius)
                    .stroke(isFirstResponder ? Color.mediumPurple : .brightGray, lineWidth: 1)
            )
            .background(backgroundColor)
            // Apply corner radius to the background too
            .cornerRadius(radius: cornerRadius, corners: .allCorners)
            // Don't highlight this (List)item on tap
            .buttonStyle(PlainButtonStyle())

            if isFirstResponder && showGenerateButton {
                HStack {
                    Text(password.isEmptyOrNil ? "Generate password" : "Generate new password")
                        .defaultButtonFont(color: .white, size: 14)

                    Spacer()

                    Image("generatePassword")
                }
                .padding()
                .background(Color.mediumPurple)
                .padding(.top, 90)
                .cornerRadius(radius: 10, corners: [.bottomLeft, .bottomRight])
                .onTapGesture(perform: refreshPassword)
            }
        }
        .frame(height: 53)
        .onAppear {
            if let delay = setFirstResponderAfterDelay {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    isFirstResponder = true
                }
            }
        }
        .onTapGesture {
            isFirstResponder = true
        }
        .onChange(of: password) { newPassword in
            if let newPassword = newPassword {
                // Remove unpermitted characters from value
                password = newPassword.filter {
                    $0.unicodeScalars.allSatisfy(permittedCharacterSet.contains(_:))
                }

                // Truncate password if limit is set
                if let limit = characterLimit, newPassword.count > limit {
                    password = String(newPassword.prefix(limit))
                }
            }
        }
        .onChange(of: isFirstResponder) { isFirstResponder in
            isHidden = !isFirstResponder
        }
    }
}

#if DEBUG
struct PasswordField_Previews: PreviewProvider {

    static var previews: some View {
        VStack {
            PasswordField(password: .constant("012oskdoad0a"), showGenerateButton: false)

            PasswordField(password: .constant("012oskdoad0a"), backgroundColor: Color.extensionLightYellow)

            PasswordField(password: .constant("012oskdoad0a"), showGenerateButton: true)

            PasswordField(password: .constant("012oskdoad0a"), showIcon: true)
        }
        .padding(.horizontal, 20)
    }
}

#endif
