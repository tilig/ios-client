//
//  GeneratePasswordField.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 22/06/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct GeneratePasswordField: View {
    @Binding var password: String?
    @State var isFirstResponder = false
    @State var editMode = false
    @State var isHidden = true {
        didSet {
            if !isHidden {
                TrackingEvent.passwordRevealed.send()
            }
        }
    }

    private func generatePassword() {
        password = PasswordGenerator.randomPassword()
        isHidden = false
        TrackingEvent.passwordGenerated.send()
    }

    var body: some View {
        ZStack(alignment: .leading) {
            HStack {

                if isHidden {
                    SecureField("Type existing password", text: Binding<String>(
                        get: { self.password ?? "" },
                        set: { self.password = $0 }
                    ))
                    .font(.stolzlRegular(size: 14))
                } else {
                    FocussableTextField(text: Binding<String>(
                        get: { self.password ?? "" },
                        set: { self.password = $0 }
                    ), isFirstResponder: $isFirstResponder) {
                        $0.autocapitalizationType = .none
                        $0.autocorrectionType = .no
                        $0.textColor = UIColor(Color.blue)
                        $0.placeholder = "Enter a password"
                        $0.font = editMode ? .defaultButtonFont(size: 16) : .subTextFont(size: 14)
                    }.frame(height: 21)
                }

                if let password = password, !password.isEmpty, !editMode {
                    Button(action: { isHidden.toggle() }) {
                        Image(systemName: isHidden ? "eye.fill" : "eye.slash.fill")
                    }
                } else {
                    WhiteButton(
                        title: "Generate",
                        textSize: 12,
                        cornerRadius: 5,
                        maxHeight: 30,
                        action: generatePassword
                    )
                    .frame(height: 15)
                }
            }
            .onTapGesture {
                isHidden = false
                isFirstResponder = true
            }
            .onAppear {
                if editMode { isHidden = false }
            }
            .padding()
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(isFirstResponder ? Color.mediumPurple : .brightGray, lineWidth: 1)
            )
            .background(
                RoundedRectangle(cornerRadius: 10)
                    .fill(Color.white)
            )
            // Don't highlight this (List)item on tap
            .buttonStyle(PlainButtonStyle())
        }
        .frame(height: 48)
        .onTapGesture {
            isFirstResponder = true
        }
    }
}

#if DEBUG
struct GeneratePasswordField_Previews: PreviewProvider {

    static var previews: some View {
        GeneratePasswordField(password: .constant("012oskdoad0a"))
    }
}

#endif
