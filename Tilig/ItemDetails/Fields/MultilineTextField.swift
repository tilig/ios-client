//
//  MultilineTextField.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 13/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

// For use in iOS 16+. Has a placeholder, a focus property, linelimit and can expand beyond the limit

@available(iOS 16.0, *)
struct MultilineTextField: View {
    @FocusState private var isFocused: Bool

    var label: String
    @Binding var value: String? {
        didSet {
            if hasLimit {
                truncateValue()
            }
        }
    }
    var onFocusChange: (Bool) -> Void = { _ in }
    var hasLimit: Bool = false
    private let characterLimit = 190

    /// Truncate the value when it exceeds the character limit
    /// Can be removed when we migrated to the v3 encryption model.
    func truncateValue() {
        guard let value = value else { return }
        if value.count > characterLimit {
            self.value = String(value.prefix(characterLimit))
        }
    }

    var body: some View {
        TextField(label, text: Binding<String>(
            get: { self.value ?? "" },
            set: { self.value = $0 }
        ), axis: .vertical)
        .lineLimit(5...)
        .focused($isFocused)
        .padding(8)
        .defaultButtonFont(color: .blue, size: 16)
        .autocorrectionDisabled(false)
        .lineSpacing(4)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(isFocused ? Color.mediumPurple : .brightGray, lineWidth: 1)
        )
        .background(
            RoundedRectangle(cornerRadius: 10)
                .fill(Color.white)
        )
        .onChange(of: isFocused) { isFocused in
            onFocusChange(isFocused)
        }
    }
}
