//
//  FormTextField.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 07/01/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct FormTextField: View {
    var label: String
    @Binding var value: String?
    @State var focused: Bool = false
    var showIcon = false
    var editMode: Bool
    var keyboardType: UIKeyboardType
    var textContentType: UITextContentType?
    var focusedBorderColor: Color

    // Used to prevent pasting unpermitted characters in the textfield
    private var permittedCharacterSet: CharacterSet? {
        return keyboardType == .numberPad ? .decimalDigits : nil
    }

    init(
        _ label: String,
        value: Binding<String?>,
        showIcon: Bool = false,
        editMode: Bool = false,
        keyboardType: UIKeyboardType = .default,
        textContentType: UITextContentType?  = nil,
        focusedBorderColor: Color = .mediumPurple
    ) {
        self.label = label
        self._value = value
        self.showIcon = showIcon
        self.editMode = editMode
        self.keyboardType = keyboardType
        self.textContentType = textContentType
        self.focusedBorderColor = focusedBorderColor
    }

    var body: some View {
        HStack {
            if showIcon {
                Image("extension_username")
                    .frame(width: 12, height: 12, alignment: .top)
                    .foregroundColor(.blue.opacity(0.5))
                    .padding(.trailing, 8)
            }

            FocussableTextField(placeholder: label, text: Binding<String>(
                get: { self.value ?? "" },
                set: {
                    guard let charSet = permittedCharacterSet else {
                        self.value = $0
                        return
                    }
                    self.value = $0.filter { $0.unicodeScalars.allSatisfy(charSet.contains(_:)) }
                }
            ), isFirstResponder: $focused) {
                $0.autocapitalizationType = textContentType != .emailAddress ? .sentences : .none
                $0.autocorrectionType = .no
                $0.textContentType = textContentType
                $0.keyboardType = keyboardType
                $0.font = editMode ? .defaultButtonFont(size: 16) : .subTextFont(size: 14)
                $0.textColor = UIColor(Color.blue)
            }
            .frame(height: 21)
        }
        .padding()
        .frame(height: 48)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(focused ? focusedBorderColor : .brightGray, lineWidth: 1)
        )
        .background(
            RoundedRectangle(cornerRadius: 10, style: .continuous)
                .fill(.white)
        )
        .onTapGesture {
            focused = true
        }
    }
}
