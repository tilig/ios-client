//
//  ExpirationDateField.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 07/01/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct ExpirationDateField: View {
    var placeholder: String
    @Binding var value: String?
    @State var focused: Bool = false
    private var permittedCharacterSet: CharacterSet {
        var charSet = NSMutableCharacterSet()
        charSet.formUnion(with: .decimalDigits)
        charSet.addCharacters(in: "/")
        return charSet as CharacterSet
    }

    init(
        _ placeholder: String,
        value: Binding<String?>
    ) {
        self.placeholder = placeholder
        self._value = value
    }

    var body: some View {
        HStack {
            FocussableTextField(placeholder: placeholder, text: Binding<String>(
                get: { self.value ?? "" },
                set: { self.value = formatExpiryDate(oldValue: self.value ?? "", newValue: $0) }
            ), isFirstResponder: $focused) {
                $0.autocapitalizationType = .sentences
                $0.autocorrectionType = .no
                $0.keyboardType = .numberPad
                $0.font = .defaultButtonFont(size: 16)
                $0.textColor = UIColor(Color.blue)
            }
            .frame(height: 21)
        }
        .padding()
        .frame(height: 48)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(focused ? Color.mediumPurple : .brightGray, lineWidth: 1)
        )
        .background(
            RoundedRectangle(cornerRadius: 10, style: .continuous)
                .fill(.white)
        )
        .onTapGesture {
            focused = true
        }
    }

    func formatExpiryDate(oldValue: String, newValue: String) -> String {
        var value = newValue.filter { $0.unicodeScalars.allSatisfy(permittedCharacterSet.contains(_:)) }

        // delete slash when 3rd character is deleted
        if oldValue.count == 4 && value.count == 3 {
            value = String(value.prefix(2))
        }

        // append slash immediately 2nd character is entered
        if value.count == 2 && oldValue.count == 1 {
            value += "/"
        }

        // insert slash after 2nd character
        // (Fixes edge cases like pasting a value in the field)
        if newValue.count > 2 {
            value = value.replacingOccurrences(of: "/", with: "")
                        .separate(every: 2, with: "/")
        }

        // set max characters on field
        if value.count > 5 {
            value = String(value.prefix(5))
        }

        return value
    }
}
