//
//  FormTextField.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 07/01/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct ItemNameField: View {
    var label: String
    @Binding var value: String?
    var onEditingChanged: (Bool) -> Void = { _ in }
    @State var focused: Bool = false
    @State var isPlaceholderHidden = false

    init(_ label: String, value: Binding<String?>, onEditingChanged: @escaping (Bool) -> Void = { _ in }) {
        self.label = label
        self._value = value
        self.onEditingChanged = onEditingChanged
    }

    var body: some View {
        ZStack {
            if !isPlaceholderHidden {
                Text(label)
                    .defaultMediumFont(color: .white.opacity(0.5), size: 28)
            }

            FocussableTextField(text: Binding<String>(
                get: { self.value ?? "" },
                set: {
                    self.value = $0
                    isPlaceholderHidden = !value.isEmptyOrNil
                }
            ), isFirstResponder: $focused) {
                $0.autocorrectionType = .no
                $0.font = .defaultMediumFont(size: 28)
                $0.textAlignment = .center
                $0.textColor = .white
                $0.tintColor = .white
            }
            .frame(height: 50)
            .onAppear {
                // autofocus on new item name field first
                // when creating a new item
                if value == nil {
                    // delay autofocus animation
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        focused = true
                    }
                } else {
                    isPlaceholderHidden = true
                }
            }
        }
    }
}
