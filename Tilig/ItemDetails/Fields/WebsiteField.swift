//
//  WebsiteField.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 07/01/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct WebsiteField: View {
    var label: String
    @Binding var value: URL?
    @State var focused: Bool = false
    var editMode = false

    init(_ label: String, value: Binding<URL?>, editMode: Bool = false) {
        self.label = label
        self._value = value
        self.editMode = editMode
    }

    var body: some View {
        FocussableTextField(placeholder: label, text: Binding<String>(
            get: { self.value?.absoluteString ?? "" },
            set: { self.value = URL(string: $0) }
        ), isFirstResponder: $focused) {
            $0.autocapitalizationType = .none
            $0.autocorrectionType = .no
            $0.font = editMode ? .defaultButtonFont(size: 16) : .subTextFont(size: 14)
            $0.keyboardType = .webSearch
            $0.textContentType = .URL
            $0.textColor = UIColor(Color.blue)
        }
        .padding()
        .frame(height: 48)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(focused ? Color.mediumPurple : .brightGray, lineWidth: 1)
        )
        .background(
            RoundedRectangle(cornerRadius: 10)
                .fill(Color.white)
        )
        .onTapGesture {
            focused = true
        }
    }
}
