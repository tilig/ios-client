//
//  InnerItemView.swift
//  Tilig
//
//  Created by Gertjan Jansen on 31/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct LoginDetailsView: View {
    @EnvironmentObject var viewModel: ItemViewModel
    @State var isPasswordHidden = true

    var hasNotesLimit: Bool { viewModel.itemWrapper.encryptedItem.legacyEncryption }

    // Not moved to LoginItemViewModel.swift because `UIApplication.shared.open` is not available
    // in extension contexts, and FormViewModel is being used in the extension as well.
    func openWebsite() {
        guard let url = viewModel.itemWrapper.url?.withProtocol else { return }
        TrackingEvent.websiteOpened.send()
        UIApplication.shared.open(url)
    }

    var body: some View {
        VStack(spacing: 0) {
            VStack(spacing: 0) {
                Button(action: { viewModel.copy(field: .username) }) {
                    VStack(spacing: 0) {
                        Text(viewModel.itemWrapper.username?.isValidEmail == true ? "Email" : "Username")
                            .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.bottom, 4)

                        Text(viewModel.itemWrapper.username ?? "")
                            .defaultButtonFont(color: .blue, size: 16)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)
                    }
                }

                Divider()
                    .padding(.vertical)

                Button(action: { viewModel.copy(field: .password) }) {
                    VStack(spacing: 0) {
                        Text("Password")
                            .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.bottom, 4)

                        HStack {
                            Text(isPasswordHidden
                                 ? String(repeating: "•", count: 10)
                                 : (viewModel.itemWrapper.password ?? "")
                            )
                            .defaultButtonFont(color: .blue, size: 16)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .multilineTextAlignment(.leading)

                            Spacer()

                            Button(action: { isPasswordHidden.toggle() }) {
                                Image(systemName: isPasswordHidden ? "eye.fill" : "eye.slash.fill")
                                    .foregroundColor(.blue)
                            }
                        }
                    }
                }
            }
            .whiteRectangleBackground()

            if let website = viewModel.itemWrapper.url?.absoluteString {
                ItemDetailRowView(
                    label: "Website",
                    value: website,
                    action: { viewModel.copy(field: .website) },
                    rightButtonImage: Image("external_link"),
                    rightButtonAction: openWebsite
                )
            }

            TwoFactorView()
                .consumingViewModel(.twoFactor)

            if let notes = viewModel.itemWrapper.notes {
                ItemDetailRowView(
                    label: hasNotesLimit ? "Notes \(notes.count)/190" : "Notes",
                    value: notes,
                    action: { viewModel.copy(field: .notes) }
                )
            }
        }
        .onAppear {
            isPasswordHidden = true
            viewModel.activate()
        }
    }
}
