//
//  NoteDetailsView.swift
//  Tilig
//
//  Created by Gertjan Jansen on 31/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

 import SwiftUI

 struct NoteDetailsView: View {
    @EnvironmentObject var viewModel: ItemViewModel

    var body: some View {
        VStack {
            if let notes = viewModel.itemWrapper.notes {
                ItemDetailRowView(
                    label: "Note content",
                    value: notes,
                    action: { viewModel.copy(field: .notes) }
                )
            }
        }
    }
 }
