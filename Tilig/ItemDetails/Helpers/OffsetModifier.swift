//
//  OffsetModifier.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 19/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

// Modifier that updates the given offset
struct OffsetModifier: ViewModifier {

    @Binding var offset: CGFloat
    @State private var startOffset: CGFloat = 0

    // initial limit set to 1/2 the default height of the imageview
    var initialOffsetLimit: CGFloat = -60

    func body(content: Content) -> some View {
        content
            .overlay(
                GeometryReader {proxy in
                    Color.clear
                        .preference(key: OffsetKey.self, value: proxy.frame(in: .global).minY)
                }
            )
            .onPreferenceChange(OffsetKey.self) { offset in
                var newOffset = offset
                if startOffset == 0 || newOffset == 0 {
                    // initial offset should not be a huge negative number.
                    if offset < initialOffsetLimit {
                        newOffset = 0
                    }
                    startOffset = newOffset
                }
                self.offset = startOffset - offset
            }
    }
}

struct OffsetKey: PreferenceKey {
    static var defaultValue: CGFloat = 0

    static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
        value = nextValue()
    }
}
