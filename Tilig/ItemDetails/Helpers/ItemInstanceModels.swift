//
//  ItemInstanceModels.swift
//  Tilig
//
//  Created by Gertjan Jansen on 04/06/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI
import Sentry

/// A modifier that creates all view models for a specific item and sets those as the environment objects.
/// Basically a sync mechanism to make sure all item instance viewmodels are tied to the same item.
struct ItemInstanceModels: ViewModifier {
    @EnvironmentObject var authenticatedModelFactory: AuthenticatedModelFactory
    var item: Item

    func body(content: Content) -> some View {
        var threwError = false
        do {
            try authenticatedModelFactory.setInstance(forItem: item)
        } catch {
            SentrySDK.capture(error: error)
            threwError = true
        }

        return Group {
            if threwError {
                // TODO: add a better error view
                Text("Error: could not load item \(item.name ?? "")")
            } else {
                content
                    // Only put the item details view model in the env here.
                    // Other instance view models can be consumed when they're needed.
                    .consumingViewModel(.itemDetails)
            }
        }
    }
}

extension View {
    func setInstanceModels(forItem item: Item) -> some View {
        self.modifier(ItemInstanceModels(item: item))
    }
}
