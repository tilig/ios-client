//
//  View+GetSafeArea.swift
//  Tilig
//
//  Created by Gertjan Jansen on 31/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

// View extension to get the safe area
extension View {
    func getSafeArea() -> UIEdgeInsets {
        let null = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        guard let screen = UIApplication.shared.connectedScenes.first as? UIWindowScene else {
            return null
        }

        guard let safeArea = screen.windows.first?.safeAreaInsets else {
            return null
        }

        return safeArea
    }
}
