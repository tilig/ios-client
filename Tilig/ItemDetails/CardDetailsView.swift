//
//  CardDetailsView.swift
//  Tilig
//
//  Created by Gertjan Jansen on 31/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct CardDetailsView: View {
    @EnvironmentObject var viewModel: ItemViewModel
    @State var isCardNumberHidden = true
    @State var isSecureCodeHidden = true
    @State var isPinHidden = true

    var cardNumber: String {
        let number = viewModel.itemWrapper.overview.info ?? ""
        if isCardNumberHidden && !number.isEmpty {
            return (String(repeating: "•", count: 12) + number).separate(every: 4)
        }
        return viewModel.itemWrapper.ccnumber?.separate(every: 4) ?? ""
    }

    var secureCode: String {
        let code = viewModel.itemWrapper.cvv ?? ""
        if isSecureCodeHidden && !code.isEmpty {
            return String(repeating: "•", count: code.count)
        }
        return code
    }

    var body: some View {
        VStack(spacing: 0) {
            VStack(spacing: 0) {
                ItemDetailRowView(
                    label: "Card number",
                    value: cardNumber,
                    action: { viewModel.copy(field: .ccnumber) },
                    rightButtonImage: Image(systemName: isCardNumberHidden ? "eye.fill" : "eye.slash.fill"),
                    rightButtonAction: { isCardNumberHidden.toggle() },
                    transparent: true
                )

                Divider()
                    .padding(.vertical)

                ItemDetailRowView(
                    label: "Cardholder name",
                    value: viewModel.itemWrapper.ccholder ?? "-",
                    action: { viewModel.copy(field: .ccholder) },
                    transparent: true
                )

                Divider()
                    .padding(.vertical)

                ItemDetailRowView(
                    label: "Expiration date",
                    value: viewModel.itemWrapper.ccexp ?? "-",
                    action: { viewModel.copy(field: .ccexp) },
                    transparent: true
                )

                Divider()
                    .padding(.vertical)

                ItemDetailRowView(
                    label: "Secure code",
                    value: secureCode,
                    action: { viewModel.copy(field: .cvv) },
                    rightButtonImage: Image(systemName: isSecureCodeHidden ? "eye.fill" : "eye.slash.fill"),
                    rightButtonAction: { isSecureCodeHidden.toggle() },
                    transparent: true
                )
            }
            .whiteRectangleBackground()

            if let pin = viewModel.itemWrapper.pin {
                ItemDetailRowView(
                    label: "Pin code",
                    value: isPinHidden ? String(repeating: "•", count: 4) : pin,
                    action: { viewModel.copy(field: .pin) },
                    rightButtonImage: Image(systemName: isPinHidden ? "eye.fill" : "eye.slash.fill"),
                    rightButtonAction: { isPinHidden.toggle() }
                )
            }

            if let zipcode = viewModel.itemWrapper.zipcode {
                ItemDetailRowView(
                    label: "ZIP",
                    value: zipcode,
                    action: { viewModel.copy(field: .zipcode) }
                )
            }

            if let notes = viewModel.itemWrapper.notes, !notes.isEmpty {
                ItemDetailRowView(
                    label: "Notes",
                    value: notes,
                    action: { viewModel.copy(field: .notes) }
                )
            }
        }
        .onAppear {
            isCardNumberHidden = true
            isSecureCodeHidden = true
            isPinHidden = true
            TrackingEvent.creditCardViewed.send()
        }
    }
}
