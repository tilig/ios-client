//
//  TwoFactorExpandableView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 25/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct TwoFactorExpandableView: View {
    var itemName: String?
    @Binding var collapsed: Bool
    var scanAction: () -> Void = { }
    var learnMoreAction: () -> Void = { }

    var body: some View {
        VStack(spacing: 0) {
            Button(
                action: { self.collapsed.toggle() },
                label: {
                    HStack {
                        Text("Add Two-Factor Authentication")
                            .defaultButtonFont(size: 16)
                            .frame(maxWidth: .infinity, alignment: .leading)

                        Spacer()

                        Image(systemName: self.collapsed ? "chevron.down" : "chevron.up")
                    }
                    .padding()
                    .contentShape(Rectangle())
                }
            )
            .buttonStyle(PlainButtonStyle())
            .background(
                RoundedRectangle(cornerRadius: 10)
                    .fill(Color.backgroundPurple)
            )

            if !collapsed {
                VStack {
                    Text(
                        """
                        Add an additional security layer to\
                         \(itemName == nil ? "this" : "your \(itemName ?? "")") \
                        login by setting up Two-Factor Authentication (2FA).
                        """
                    )
                    .subTextFont(color: .blue.opacity(0.5), size: 14)
                    .lineSpacing(2)
                    .frame(maxWidth: .infinity, alignment: .leading)

                    HStack {
                        DefaultButton(
                            title: "Scan QR code",
                            textSize: 14,
                            image: Image("qr-code"),
                            padding: 8,
                            fullWidth: false,
                            action: scanAction
                        )
                        .frame(height: 50)

                        WhiteButton(title: "Learn More", maxHeight: 35, action: learnMoreAction)

                        Spacer()
                    }
                }
                .padding(.horizontal)
                .responsivePadding(.vertical, compact: 8, regular: 16)
                .background(
                    RoundedRectangle(cornerRadius: 10)
                        .fill(Color.backgroundPurple)
                )
            }
        }
        .padding(.bottom, 8)
    }
}

struct TwoFactorExpandableView_Previews: PreviewProvider {
    static var previews: some View {
        TwoFactorExpandableView(itemName: "Instagram", collapsed: .constant(true))
    }
}
