//
//  CollapsibleToolbar.swift
//  Tilig
//
//  Created by Gertjan Jansen on 31/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct CollapsibleToolbar<Content: View>: View {
    var collapsibleController: CollapsibleController {
        CollapsibleController(
            scrollOffset: scrollOffset,
            title: title,
            maxBackgroundHeight: maxBackgroundHeight
        )
    }
    /// The scroll offset of the parent's scroll view
    var scrollOffset: Double
    var title: String
    var brand: Brand?
    var maxBackgroundHeight: Double
    var backgroundZIndex: Double = 0
    var itemType: ItemType? = .login
    var titleZIndex: Double = 0
    var backgroundColor: Color? { brand?.mainColor }
    var fallbackColor: Color {
        switch itemType {
        case .login: return .mediumPurple
        case .card: return .creditCardGreen
        case .wifi: return .wifiBlue
        case .note: return .blue
        case .none, .custom: return .mediumPurple
        }
    }

    var content: () -> Content

    var body: some View {
        CollapsibleToolbarRenderingView(
            title: title,
            iconURL: brand?.iconURL,
            backgroundHeight: collapsibleController.backgroundHeight,
            backgroundOffset: collapsibleController.backgroundOffset,
            backgroundColor: backgroundColor ?? fallbackColor,
            backgroundZIndex: backgroundZIndex,
            titleZIndex: titleZIndex,
            contentOffset: collapsibleController.contentOffset,
            iconRect: collapsibleController.iconRect,
            iconOpacity: collapsibleController.iconOpacity,
            textOffset: collapsibleController.textOffset,
            textSize: collapsibleController.textSize,
            itemType: itemType,
            content: content
        )
    }
}

private struct CollapsibleToolbarRenderingView<Content: View>: View {
    var title: String
    var iconURL: URL?

    var backgroundHeight: Double
    var backgroundOffset: Double
    var backgroundColor: Color
    var backgroundZIndex: Double
    var titleZIndex: Double
    var contentOffset: Double
    var iconRect: CGRect
    var iconOpacity: Double
    var textOffset: Double
    var textSize: Double
    var itemType: ItemType? = .login
    var content: () -> Content

    var body: some View {
        // Note: this needs to be wrapped in a ZStack. Instead of using a `Group`
        // we could also a ZStack. However, that gives us less control over the
        // background layer zIndex.
        Group {
            CollapsibleToolbarBackground(
                color: backgroundColor,
                height: backgroundHeight
            )
            // zIndex = 0 makes sure the background can move behind a parent view's content (like the scrollview)
            .zIndex(backgroundZIndex)

            VStack(spacing: 0) {
                // The icon
                ListIcon(
                    iconURL: iconURL,
                    background: true,
                    backgroundColor: .white,
                    hasPadding: true,
                    cornerRadius: 8,
                    itemType: itemType
                )
                .frame(
                    width: iconRect.width,
                    height: iconRect.height
                )
                .clipped()
                .offset(x: iconRect.minX, y: iconRect.minY)
                .opacity(iconOpacity)
                .padding(.bottom, 8)

                Text(title)
                    .defaultMediumFont(color: .white, size: textSize)
                    .multilineTextAlignment(.center)
                    .lineLimit(1)
                    .padding(.horizontal, 32)
                    .padding(.bottom)
                    .offset(y: textOffset)
            }
            .zIndex(titleZIndex)

            // The header content
            content()
                .frame(width: getScreenBounds().width, height: 100)
                .offset(y: 20.0)
                // higher then everything else, to make sure all content is tappable
                .zIndex(10)
        }
    }
}

struct CollapsibleToolbarBackground: View {
    var color: Color
    var height: Double

    var body: some View {
        Image("header_background")
            .resizable()
            .scaledToFill()
            .background(color)
            .frame(height: height)
            .clipped()
            // .clipped clips the view but its content that extends over the view's bounds might still be tappable.
            // adding .contentShape defines the content shape / area for hit testing
            .contentShape(Rectangle())
    }
}

#if DEBUG
 struct CollapsibleToolbar_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            ZStack(alignment: .top) {
                CollapsibleToolbarRenderingView(
                    title: "Test",
                    iconURL: URL(string: "https://asset.brandfetch.io/ido5G85nya/idnsPRMIrl.jpeg"),
                    backgroundHeight: 50.0,
                    backgroundOffset: 0,
                    backgroundColor: .pink,
                    backgroundZIndex: 0,
                    titleZIndex: 1,
                    contentOffset: 10,
                    iconRect: CGRect(
                        x: 0,
                        y: 100,
                        width: 100,
                        height: 100
                    ),
                    iconOpacity: 0.9,
                    textOffset: -55,
                    textSize: 24,
                    content: {
                        HStack {
                            // The back button
                            BackButton(action: { })

                            Spacer()

                            Button(action: { }) {
                                Text("Save")
                                    .defaultButtonFont(color: .white, size: 16)
                                    .padding(.trailing)
                            }
                        }
                    }
                )
            }

            Spacer()
        }
        .background(Color.gray)
        .ignoresSafeArea()
    }
 }
#endif
