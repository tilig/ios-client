//
//  ItemView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 05/01/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//
import SwiftUI

struct ItemView: View {
    @EnvironmentObject var viewModel: ItemViewModel
    @EnvironmentObject var authenticatedModelFactory: AuthenticatedModelFactory
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>

    @State var scrollOffset: CGFloat = 0
    @State var isPresentingEditView = false

    func navigateBack() {
        authenticatedModelFactory.clearInstance()
        self.mode.wrappedValue.dismiss()
    }

    var body: some View {
        ZStack(alignment: .top) {
            CollapsibleToolbar(
                scrollOffset: scrollOffset,
                title: viewModel.itemWrapper.name ?? "Untitled",
                brand: viewModel.itemWrapper.brand,
                maxBackgroundHeight: 220,
                backgroundZIndex: 0,
                itemType: viewModel.itemType,
                titleZIndex: 2
            ) {
                HStack {
                    BackButton(hasPadding: true, action: navigateBack)

                    Spacer()

                    Button(action: { self.isPresentingEditView = true }) {
                        Text("Edit")
                            .defaultButtonFont(color: .white, size: 16)
                            .padding(.trailing)
                    }
                }
            }
            // Higher than the scrollview
            .zIndex(1)

            ScrollView(showsIndicators: false) {
                VStack(spacing: 0) {
                    Group {
                        switch viewModel.itemType {
                        case .login:
                            LoginDetailsView()
                                .consumingViewModel(.twoFactor)
                        case .note:
                            NoteDetailsView()
                        case .wifi:
                            WifiDetailsView()
                        case .card:
                            CardDetailsView()
                        case .custom:
                            EmptyView()
                        }
                    }
                    .id(viewModel.itemWrapper.updatedAt)
                    .padding(.top, viewModel.itemType == .login ? 170 : 200)

                    ShareItemView()
                        .consumingViewModel(.shareItem)

                    if viewModel.itemType == .login {
                        PasswordHistoryButton()
                    }
                }
                .frame(width: getScreenBounds().width - 32)
                .modifier(OffsetModifier(offset: $scrollOffset))
            }
            // Higher than the toolbar background
            .zIndex(3)

            NavigationLink(destination: EditItemView().consumingViewModel(.editItem),
                isActive: $isPresentingEditView
            ) {
                EmptyView()
            }

        }
        .background(Color.backgroundGray.edgesIgnoringSafeArea(.bottom))
        .swipeRightToDismiss()
        .ignoresSafeArea(.container, edges: .top)
        .navigationBarHidden(true)
    }
}
