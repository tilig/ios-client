//
//  ItemDetailRowView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 20/12/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct ItemDetailRowView: View {
    var label: String
    var value: String?
    var action: () -> Void
    var rightButtonImage: Image?
    var rightButtonAction: () -> Void = { }
    var transparent = false

    /// Returns the "-" if the value is nil or empty.
    private var valueOrEmptyPlaceholder: String {
        if let value, !value.isEmpty { return value }
        return "-"
    }

    var body: some View {
        Button(action: action) {
            VStack(spacing: 0) {
                Text(label)
                    .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.bottom, 8)

                HStack {
                    Text(valueOrEmptyPlaceholder)
                        .defaultButtonFont(color: .blue, size: 16)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .multilineTextAlignment(.leading)

                    Spacer()

                    if let rightButtonImage {
                        Button(action: rightButtonAction) {
                            rightButtonImage
                                .foregroundColor(.blue)
                        }
                    }
                }
            }
        }
        .if(!transparent, transform: { view in
            view.whiteRectangleBackground()
        })
    }
}

struct ItemDetailRowView_Previews: PreviewProvider {
    static var previews: some View {
        ItemDetailRowView(label: "Name", value: "Gerald", action: {})
    }
}
