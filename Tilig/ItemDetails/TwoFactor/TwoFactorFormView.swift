//
//  TwoFactorFormView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 06/01/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct TwoFactorView: View {
    @EnvironmentObject var viewModel: TwoFactorViewModel

    var body: some View {
        VStack {
            if viewModel.hasOneTimePassword {
                Button {
                    viewModel.copyOneTimePassword()
                } label: {
                    VStack(spacing: 0) {
                        Text("Two Factor Authentication")
                            .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.bottom, 4)

                        if let otpCode = viewModel.otpPassword,
                           let remainingSeconds = viewModel.remainingSeconds {
                            TwoFactorCodeView(
                                otpCode: otpCode,
                                remainingSeconds: remainingSeconds
                            )
                        }
                    }
                }
                .padding(.vertical, 16)
                .padding(.horizontal)
                .frame(maxWidth: .infinity, alignment: .center)
                .frame(height: 80)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.clear, lineWidth: 1)
                )
                .background(
                    RoundedRectangle(cornerRadius: 10)
                        .fill(.white)
                )
                .padding(.bottom)
            }
        }
        .onAppear(perform: {
            viewModel.activate()
        })
    }
}
