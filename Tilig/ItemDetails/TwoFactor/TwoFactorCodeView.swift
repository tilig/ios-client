//
//  TwoFactorCodeView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 28/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct TwoFactorCodeView: View {
    var otpCode: String
    var remainingSeconds: Int
    var editMode: Bool = false

    var body: some View {
        HStack {
            Text(otpCode.separate(every: 3, with: "-"))
                .defaultCodeFont(size: editMode ? 16 : 20)
                .multilineTextAlignment(.leading)

            Spacer()

            TimerView(remainingSeconds: remainingSeconds)
        }
    }
}
