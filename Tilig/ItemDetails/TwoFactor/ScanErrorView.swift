//
//  ScanErrorView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct ScanErrorView: View {
    @StateObject var viewModel: TwoFactorViewModel

    var body: some View {
        VStack(spacing: 0) {
            PopupHeaderView(
                title: "Something went wrong",
                closeButtonAction: viewModel.dismissScanErrorView
            )

            Spacer()

            VStack {
                Image("two-factor-step-1")
                    .responsiveHeight(compact: 150, regular: 300)

                Text("Hmm... something went wrong")
                    .defaultBoldFont(size: 24)
                    .multilineTextAlignment(.center)
                    .padding(.vertical, 8)
                    .padding(.horizontal, 40)

                Text(
                    """
                    It looks like this is not the correct QR code for 2FA. \
                    Can you please try it again and make sure its the QR code provided by {\(viewModel.label)}
                    """
                )
                .subTextFont(size: 16)
                .multilineTextAlignment(.center)
                .lineSpacing(4)
            }
            .responsivePadding(.horizontal, compact: 40, regular: 80)
            .responsivePadding(.vertical, compact: 20, regular: 40)

            Spacer()

            DefaultButton(title: "Try again", textSize: 14, action: viewModel.dismissScanErrorView)
                .responsivePadding(compact: 40, regular: 80)

            Spacer()
        }
    }
}
