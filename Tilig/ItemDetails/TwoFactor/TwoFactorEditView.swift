//
//  TwoFactorEditView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 11/10/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

struct TwoFactorEditView: View {
    @EnvironmentObject var viewModel: TwoFactorViewModel

    @State private var collapsed = true
    var isOtpPossible: Bool
    @State private var presentLearnMore = false
    @State private var presentScannerView = false

    func hideKeyboard() {
        UIApplication.shared.endEditing(true)
    }

    var body: some View {
        VStack {
            if viewModel.hasOneTimePassword {
                ZStack {
                    if let otpCode = viewModel.otpPassword,
                       let remainingSeconds = viewModel.remainingSeconds {
                        TwoFactorCodeView(
                            otpCode: otpCode,
                            remainingSeconds: remainingSeconds,
                            editMode: true
                        )
                    }

                    RoundedRectangle(cornerRadius: 10)
                        .fill(.white.opacity(0.4))
                }
                .padding(.vertical, 8)
                .padding(.horizontal)
                .frame(maxWidth: .infinity, alignment: .center)
                .frame(height: 48)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.brightGray, lineWidth: 1)
                )
                .background(
                    RoundedRectangle(cornerRadius: 10)
                        .fill(.white)
                )
                .padding(.bottom)
            } else if isOtpPossible {
                TwoFactorExpandableView(
                    collapsed: $collapsed,
                    scanAction: {
                        hideKeyboard()
                        viewModel.presentScannerView()
                    },
                    learnMoreAction: {
                        hideKeyboard()
                        viewModel.presentLearnMoreView()
                    }
                )
            }
        }
        .onAppear(perform: {
            viewModel.activate()
        })
        .fullScreenCover(isPresented: $presentScannerView) {
            ScannerView()
        }
        .sheet(isPresented: $presentLearnMore) {
            LearnMorePagerView(viewModel: viewModel, onTwoFactorEnabled: {
                // delay toggle for a second to allow popup to be dismissed
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    viewModel.presentScannerView()
                })
            })
        }
        // Using Binding values from viewModel in sheet/fullScreenCover throws a
        // 'Publishing changes from within view updates' warning. To fix these warnings,
        // introduce a local state in the view and update it when the viewModel's published
        // value changes
        // - https://developer.apple.com/forums/thread/711899?answerId=729307022#729307022
        .onChange(of: viewModel.showLearnMoreView) { value in
            presentLearnMore = value
        }
        .onChange(of: viewModel.showScannerView) { value in
            presentScannerView = value
        }
    }
}
