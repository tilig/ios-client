//
//  QRScannerView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import SwiftUI

struct QRCodeScannerView: UIViewRepresentable {

    var supportedBarcodeTypes: [AVMetadataObject.ObjectType] = [.qr]
    typealias UIViewType = CameraPreview

    private let session = AVCaptureSession()
    private weak var delegate: QRCameraDelegate?
    private let metadataOutput = AVCaptureMetadataOutput()

    init(delegate: QRCameraDelegate) {
        self.delegate = delegate
    }

    func interval(delay: Double) -> QRCodeScannerView {
        delegate?.scanInterval = delay
        return self
    }

    func codeFound(_ result: @escaping (String) -> Void) -> QRCodeScannerView {
        delegate?.onResult = result
        return self
    }

    func setupCamera(_ uiView: CameraPreview) {
        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        if let input = try? AVCaptureDeviceInput(device: backCamera) {
            session.sessionPreset = .photo

            if session.canAddInput(input) {
                session.addInput(input)
            }

            if session.canAddOutput(metadataOutput) {
                session.addOutput(metadataOutput)
                metadataOutput.metadataObjectTypes = supportedBarcodeTypes
                metadataOutput.setMetadataObjectsDelegate(delegate, queue: DispatchQueue.main)
            }

            let previewLayer = AVCaptureVideoPreviewLayer(session: session)
            uiView.backgroundColor = UIColor.gray
            previewLayer.videoGravity = .resizeAspectFill
            uiView.layer.addSublayer(previewLayer)
            uiView.previewLayer = previewLayer
            session.startRunning()
        }
    }

    func makeUIView(context: UIViewRepresentableContext<QRCodeScannerView>) -> QRCodeScannerView.UIViewType {
        let cameraView = CameraPreview(session: session)
        checkCameraAuthorizationStatus(cameraView)
        return cameraView
    }

    static func dismantleUIView(_ uiView: CameraPreview, coordinator: ()) {
        uiView.session.stopRunning()
    }

    private func checkCameraAuthorizationStatus(_ uiView: CameraPreview) {
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
        if cameraAuthorizationStatus == .authorized {
            setupCamera(uiView)
        }
    }

    func updateUIView(_ uiView: CameraPreview, context: UIViewRepresentableContext<QRCodeScannerView>) {
        uiView.setContentHuggingPriority(.defaultHigh, for: .vertical)
        uiView.setContentHuggingPriority(.defaultLow, for: .horizontal)
    }

}

class QRCameraDelegate: NSObject, AVCaptureMetadataOutputObjectsDelegate {

    var scanInterval: Double = 1.0
    var lastTime = Date(timeIntervalSince1970: 0)
    var onResult: (String) -> Void = { _  in }

    func metadataOutput(_ output: AVCaptureMetadataOutput,
                        didOutput metadataObjects: [AVMetadataObject],
                        from connection: AVCaptureConnection
    ) {
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            let now = Date()
            if now.timeIntervalSince(lastTime) >= scanInterval {
                lastTime = now
                self.onResult(stringValue)
            }
        }
    }
}

class CameraPreview: UIView {
    var previewLayer: AVCaptureVideoPreviewLayer?
    var session = AVCaptureSession()

    init(session: AVCaptureSession) {
        super.init(frame: .zero)
        self.session = session
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        previewLayer?.frame = self.bounds
    }
}
