//
//  ScannerView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI
import AVFoundation

struct ScannerView: View {
    @EnvironmentObject var viewModel: TwoFactorViewModel

    @State var presentLearnMoreView: Bool = false
    @State var presentManualCodeView: Bool = false
    @State var presentCameraAlertView: Bool = false
    @State var presentScanCompletedView: Bool = false
    @State var presentScanErrorView: Bool = false

    var body: some View {
        ZStack {
            if viewModel.cameraEnabled {
                ZStack {
                    QRCodeScannerView(delegate: self.viewModel)
                        .codeFound(self.viewModel.onQRCodeFound)
                        .interval(delay: self.viewModel.scanInterval)

                    VStack {

                        Spacer()

                        Text("Scan the QR code on \n\(viewModel.label)")
                            .defaultButtonFont(color: .white, size: 14)
                            .multilineTextAlignment(.center)
                            .padding()
                            .frame(maxWidth: .infinity, alignment: .center)
                            .roundedRectangleBackground(fillColor: .black.opacity(0.7))
                            .padding(.bottom)

                        Button(action: {
                            viewModel.presentLearnMoreView()
                            TrackingEvent.twoFactorReadMoreTapped.send()
                        }) {
                            Text("Need help? Click here")
                                .defaultButtonFont(color: Color.black, size: 16)
                                .padding()
                                .frame(maxWidth: .infinity, alignment: .center)
                                .roundedRectangleBackground(fillColor: .white)
                        }
                    }
                    .padding([.horizontal, .bottom])
                }
                .padding(.top, 60)
            }

            VStack {
                ScannerHeaderView(
                    showLearnMoreView: $viewModel.showLearnMoreView,
                    showManualView: $viewModel.showManualView,
                    dismissAction: viewModel.dismissScannerView
                )

                Spacer()

                ZStack {
                    Image("scanner_pointer")
                        .resizable()
                        .scaledToFit()
                        .padding(.top, viewModel.cameraEnabled ? -80 : 0)

                    if !viewModel.cameraEnabled {
                        VStack {
                            Image("camera")
                                .resizable()
                                .scaledToFit()
                                .foregroundColor(.white)
                                .frame(width: 40, height: 40)

                            Text("Please give Tilig permission to use your camera.")
                                .defaultFont(color: .white, size: 18)
                                .multilineTextAlignment(.center)
                        }
                    }
                }

                Spacer()

                if !viewModel.cameraEnabled {
                    // Bottom buttons
                    VStack {
                        DefaultButton(
                            title: "Enable camera",
                            image: Image(systemName: "camera.fill"),
                            action: viewModel.requestCameraPermission
                        )
                        .padding(.bottom, 12)

                        Button(action: {
                            viewModel.presentManualView()
                        }) {
                            HStack {
                                Text("No camera? Enter code manually")
                                    .defaultButtonFont(color: Color.white, size: 16)
                                    .padding()
                                    .frame(maxWidth: .infinity, alignment: .center)
                                    .background(
                                        RoundedRectangle(cornerRadius: 10)
                                            .stroke(Color.white)
                                            .shadow(color: .gray, radius: 0.5, x: 0, y: 1)
                                    )
                            }
                        }
                    }
                }
            }
            .padding(.horizontal, 16)
            .padding(.bottom, 16)
        }
        .background(
            Image("scanner_bg")
                .resizable()
                .scaledToFill()
                .edgesIgnoringSafeArea(.all)
        )
        .sheet(isPresented: $presentLearnMoreView) {
            LearnMorePagerView(viewModel: viewModel, onTwoFactorEnabled: {})
        }
        .sheet(isPresented: $presentManualCodeView) {
            TwoFactorManualView(viewModel: viewModel)
        }
        .sheet(isPresented: $presentScanCompletedView) {
            ScanCompletedView(viewModel: viewModel)
        }
        .sheet(isPresented: $presentScanErrorView) {
            ScanErrorView(viewModel: viewModel)
        }
        .sheet(isPresented: $presentCameraAlertView) {
            ScannerCameraAlertView(viewModel: viewModel)
        }
        .onAppear {
            // check for camera permissions
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
            viewModel.setCameraEnabled(cameraAuthorizationStatus == .authorized)
        }

        // Using Binding values from viewModel in sheet/fullScreenCover throws a 'Publishing changes from within view updates' warning
        // To fix these warnings, introduce a local state in the view and update it when the viewModel's published value changes
        // - https://developer.apple.com/forums/thread/711899?answerId=729307022#729307022
        .onChange(of: viewModel.showLearnMoreView) { value in
            presentLearnMoreView = value
        }
        .onChange(of: viewModel.showScanCompletedView) { value in
            presentScanCompletedView = value
        }
        .onChange(of: viewModel.showScanError) { value in
            presentScanErrorView = value
        }
        .onChange(of: viewModel.showManualView) { value in
            presentManualCodeView = value
        }
        .onChange(of: viewModel.showCameraAlert) { value in
            presentCameraAlertView = value
        }
    }
}
