//
//  ScannerHeaderView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 26/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct ScannerHeaderView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @Binding var showLearnMoreView: Bool
    @Binding var showManualView: Bool
    var dismissAction: () -> Void = { }

    var body: some View {
        HStack {
            Button(action: dismissAction) {
                Image(systemName: "xmark")
                    .resizable()
                    .foregroundColor(.white)
            }
            .frame(width: 20, height: 20)

            Spacer()

            Text("Scan QR Code")
                .defaultButtonFont(color: .white, size: 16)

            Spacer()

            Button(action: {
                showManualView = true
            }) {
                Image(systemName: "keyboard")
                    .resizable()
                    .foregroundColor(.white)
            }
            .frame(width: 22, height: 16)
            .padding(.trailing, 8)

            Button(action: {
                showLearnMoreView = true
            }) {
                Image(systemName: "questionmark.circle.fill")
                    .resizable()
                    .foregroundColor(.white)
            }
            .frame(width: 20, height: 20)
        }
        .frame(height: 55)
    }
}

struct ScannerHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        ScannerHeaderView(showLearnMoreView: .constant(false), showManualView: .constant(false))
    }
}
