//
//  LearnMorePage.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct LearnMorePage: View {
    var imageName: String
    var title: String
    var details: String

    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 0) {
                Image(imageName)
                    .responsiveHeight(compact: 150, regular: 300)
                    .padding(.vertical, 20)

                Text(title)
                    .defaultMediumFont()
                    .multilineTextAlignment(.leading)
                    .padding(.vertical, 8)

                Text(details)
                    .subTextFont(size: 16)
                    .multilineTextAlignment(.leading)
                    .lineSpacing(4)
            }
            .responsivePadding(.horizontal, compact: 40, regular: 80)
            .responsivePadding(.horizontal, compact: 20, regular: 40)
        }
    }
}
