//
//  ScanCompletedView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

struct ScanCompletedView: View {
    @StateObject var viewModel: TwoFactorViewModel

    var body: some View {
        VStack(spacing: 0) {
            PopupHeaderView(
                title: "Security Code",
                closeButtonAction: viewModel.dismissScanCompletedView
            )

            VStack {
                Image("two-factor-step-1")
                    .responsiveHeight(compact: 150, regular: 300)

                Text("Security code")
                    .defaultBoldFont(size: 24)
                    .multilineTextAlignment(.center)
                    .padding(.vertical, 8)

                Text("Please fill in this security code on the security page on \(viewModel.label)")
                    .subTextFont(size: 16)
                    .multilineTextAlignment(.center)
                    .lineSpacing(4)

                if let otp = viewModel.otpPassword, let seconds = viewModel.remainingSeconds {
                    TwoFactorScanCompletedCodeView(otp: otp, remainingSeconds: seconds)
                }

                Text("""
                    Note: this code changes every 30 seconds as an added security measure. \
                    Never give this code to anyone, even if they ask.
                    """
                )
                .subTextFont(color: .blue.opacity(0.75), size: 14)
                .multilineTextAlignment(.center)
                .lineSpacing(6)
            }
            .responsivePadding(.horizontal, compact: 40, regular: 80)
            .responsivePadding(.vertical, compact: 20, regular: 40)

            Spacer()

            DefaultButton(title: "Done", textSize: 14, action: viewModel.dismissScanCompletedView)
                .responsivePadding(compact: 40, regular: 80)
        }
    }
}

struct TwoFactorScanCompletedCodeView: View {
    var otp: String
    var remainingSeconds: Int

    var body: some View {
        HStack {
            Text(otp.separate(every: 3, with: " "))
                .defaultCodeFont(size: 40)
                .minimumScaleFactor(0.5)
                .padding(.trailing)

            TimerView(remainingSeconds: remainingSeconds)
                .frame(width: 32, height: 32)

        }
        .padding(.horizontal)
        .frame(maxWidth: .infinity, maxHeight: 88)
        .background(
            RoundedRectangle(cornerRadius: 8)
                .stroke(Color.mediumPurple)
                .background(Color.backgroundGray)
        )
        .padding(.vertical, 30)
    }
}

struct TwoFactorCodeView_Previews: PreviewProvider {
    static var previews: some View {
        TwoFactorScanCompletedCodeView(otp: "ABC-ABC", remainingSeconds: 20)
    }
}
