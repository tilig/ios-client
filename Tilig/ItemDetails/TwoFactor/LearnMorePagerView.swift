//
//  LearnMoreView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

// swiftlint:disable line_length
struct LearnMorePagerView: View {
    var viewModel: TwoFactorViewModel
    @State var showEnable2FAButton = false
    var onTwoFactorEnabled: () -> Void = { }
    var pages: [LearnMorePage] = []

    init(viewModel: TwoFactorViewModel, onTwoFactorEnabled: @escaping () -> Void = { }) {
        self.viewModel = viewModel
        self.onTwoFactorEnabled = onTwoFactorEnabled
        pages = [
            LearnMorePage(
                imageName: "two-factor-step-1",
                title: "Why should you enable Two-Factor Authentication?",
                details:
                    """
                    Two-Factor Authentication (2FA) makes your logins tougher to crack. If a hacker (or a pesky ex) signs in using your password, 2FA can keep them from accessing your Login. How? By asking for a security code that is valid for only a few seconds.

                    To generate these security codes, you need an authenticator app. By using Tilig as your authenticator app, you can be confident that you can always access your security codes. Even if you lose your phone!
                    """
            ),
            LearnMorePage(
                imageName: "two-factor-step-2",
                title: "Enabling 2FA - Part 1",
                details: """
                    Open your computer or tablet and go to the security settings in your \(viewModel.label) account.

                    Once you’re there, look for a Two-Factor Authentication (2FA) or One-Time Password (OTP) option.

                    Enable it. This will prompt you to scan a QR code.

                    (Some websites also offer the option manually type over a code here.)
                    """
            ),
            LearnMorePage(
                imageName: "two-factor-step-3",
                title: "Enabling 2FA - Part 2",
                details: """
                    Open Tilig on your phone. Scan the QR code with the Tilig's QR scanner.

                    (If they don't offer a QR code but show a code instead, type over that code in Tilig.)
                    """
            ),
            LearnMorePage(
                imageName: "two-factor-step-4",
                title: "Enabling 2FA - Part 3",
                details: """
                    Tilig will then show you a 6 digit verification code.

                    \(viewModel.label) will ask you to enter that code. So... enter it!

                    ...and that’s it. You’re all set with 2FA.

                    From now on, Tilig will generate a security code for \(viewModel.label) whenever you need one.

                    Congrats!
                    """
            )
        ]
    }
    func enableTwoFactor() {
        viewModel.dismissLearnMoreView()
        onTwoFactorEnabled()
    }

    var body: some View {
        VStack(spacing: 0) {
            PopupHeaderView(
                title: "Enable 2FA",
                closeButtonAction: {
                    viewModel.dismissLearnMoreView()
                }
            )

            PageView(views: pages, onCurrentPageChanged: { currentPage in
                self.showEnable2FAButton = currentPage == 3
            })
            .equatable()
            .padding(.bottom, 10)
            .preferredColorScheme(.light)

            if showEnable2FAButton {
                DefaultButton(title: "Enable 2FA", action: enableTwoFactor)
                    .responsivePadding(compact: 40, regular: 80)
            }
        }
    }
}
