//
//  TimerView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct TimerView: View {
    var remainingSeconds: Int
    var period: Int = 30

    var body: some View {
        VStack {
            ZStack {
                CircularProgressBar(value: remainingSeconds, total: period)
                Text("\(remainingSeconds)")
                    .subTextFont(color: .green, size: 12)
            }
        }
    }
}
