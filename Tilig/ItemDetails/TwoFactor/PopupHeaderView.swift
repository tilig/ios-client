//
//  PopupHeaderView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 26/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct PopupHeaderView: View {

    @State var title: String
    var closeButtonAction: () -> Void = { }
    var hasBackground = false
    var isBackEnabled = false

    var body: some View {
        ZStack {
            if hasBackground {
                Image("extension_header")
                    .resizable()
                    .scaledToFill()
                    .frame(height: 55)
                    .clipped()
                    .edgesIgnoringSafeArea(.top)
            }

            HStack {
                BackButton(
                    foregroundColor: hasBackground ? .white : .black,
                    usesXMarkIcon: !isBackEnabled,
                    hasPadding: false,
                    action: closeButtonAction
                )

                Spacer()

                Text(title)
                    .defaultButtonFont(color: hasBackground ? .white : .blue, size: 16)

                Spacer()
            }
            .padding(.horizontal, 16)
            .frame(height: 55)
        }

        if !hasBackground {
            Divider()
                .background(Color.brightGray)
        }
    }
}

struct PopupHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        PopupHeaderView(title: "Something went wrong")
    }
}
