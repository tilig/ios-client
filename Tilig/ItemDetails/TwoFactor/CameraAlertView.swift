//
//  CameraAlertView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct ScannerCameraAlertView: View {
    @StateObject var viewModel: TwoFactorViewModel

    func openSettings() {
        UIApplication.shared.open(URLOpenTracker.shared.getAppSettingsPage())
    }

    var body: some View {
        VStack {
            PopupHeaderView(
                title: "Camera access needed",
                closeButtonAction: viewModel.dismissCameraAlert
            )

            Spacer()

            VStack {
                Image("camera")
                    .responsiveHeight(compact: 60, regular: 120)
                    .foregroundColor(.black)

                Text("Camera access needed")
                    .defaultBoldFont(size: 24)
                    .multilineTextAlignment(.center)
                    .padding(.vertical, 8)
                    .padding(.horizontal, 40)

                Text("Tilig does not have access to your camera. To enable access, tap Settings and turn on Camera.")
                    .subTextFont(size: 16)
                    .multilineTextAlignment(.center)
                    .lineSpacing(4)
            }
            .responsivePadding(.horizontal, compact: 40, regular: 80)
            .responsivePadding(.vertical, compact: 20, regular: 40)

            Spacer()

            DefaultButton(title: "Go to settings", textSize: 14, action: openSettings)
                .responsivePadding(compact: 40, regular: 80)

            Spacer()
        }
    }
}
