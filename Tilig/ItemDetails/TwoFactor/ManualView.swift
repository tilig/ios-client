//
//  ManualView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

struct TwoFactorManualView: View, KeyboardReadable {
    @StateObject var viewModel: TwoFactorViewModel
    @State var manualCode: String = ""
    @State var isKeyboardVisible: Bool = false

    func submitCode() {
        if manualCode.isEmpty {
            TwoFactorValidationError.codeIsRequired.raiseGlobalAlert()
            return
        }

        viewModel.dismissManualView()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            viewModel.onManualSecretCodeEntered(secret: manualCode)
        })
    }

    // swiftlint:disable line_length
    var body: some View {
        VStack {
            PopupHeaderView(
                title: "Add code manually",
                closeButtonAction: viewModel.dismissManualView
            )

            ScrollView {
                VStack {
                    Image("manual_2FA_code")
                        .fixedHeight(100)
                        .padding(.top)

                    Text("Add QR code manually")
                        .defaultBoldFont(size: 24)
                        .multilineTextAlignment(.center)
                        .padding(.vertical, 8)
                        .padding(.horizontal)

                    Text("""
                            If you can’t scan the QR code, or if you haven’t given the Tilig App permission to access your phone camera, please fill in the authentication code manually.
                            """)
                        .subTextFont(size: 16)
                        .multilineTextAlignment(.center)
                        .lineSpacing(4)
                        .padding(.bottom)

                    if !isKeyboardVisible {
                        Text("""
                                Services often refer to this as a “key.” You will need to enter 8 sequences of 4 characters for this step.
                                """)
                            .subTextFont(size: 16)
                            .multilineTextAlignment(.center)
                            .lineSpacing(4)
                            .padding(.bottom)
                    }

                    TextField("Fill in the code", text: $manualCode)
                        .disableAutocorrection(true)
                        .autocapitalization(.none)
                        .font(.stolzlRegular(size: 16))
                        .foregroundColor(.blue)
                        .multilineTextAlignment(.center)
                        .padding()
                        .frame(height: 48)
                        .overlay(
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(Color.mediumPurple, lineWidth: 1)
                        )
                        .onReceive(keyboardPublisher) { newIsKeyboardVisible in
                            isKeyboardVisible = newIsKeyboardVisible
                        }
                }
                .padding(.horizontal)
                .padding(.vertical)

                DefaultButton(title: "Continue with this code", textSize: 14, action: submitCode)
                    .padding(.horizontal)
            }
        }
        .globalErrorHandler()
        .onAppear {
            TrackingEvent.twoFactorManualButtonTapped.send()
        }
    }
}

enum TwoFactorValidationError: Error {
    case codeIsRequired
}

extension TwoFactorValidationError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .codeIsRequired:
            return "Please enter authentication code"
        }
    }
}
