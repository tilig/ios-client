//
//  InviteUserScreen.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 01/03/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct InviteUserScreen: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var viewModel: ShareItemViewModel
    @State var isTextFieldFocused: Bool = false

    var body: some View {
        VStack(spacing: 0) {
            PopupHeaderView(
                title: "Share with",
                closeButtonAction: { presentationMode.wrappedValue.dismiss() }
            )

            ScrollView {
                VStack(alignment: .leading, spacing: 16) {
                    InviteTextField(
                        label: "Search emails or names",
                        value: $viewModel.recipientEmail,
                        focused: $isTextFieldFocused,
                        iconName: viewModel.hasEnteredValidEmail ? "invite-share-filled" : "invite-share",
                        sendAction: {
                            if viewModel.hasEnteredValidEmail {
                                viewModel.submitShareEmail()
                                presentationMode.wrappedValue.dismiss()
                            }
                        }
                    )

                    if viewModel.filteredContacts.isEmpty &&
                        !viewModel.recipientEmail.isEmpty {
                        Text(viewModel.hasEnteredValidEmail
                             ? "SELECT A PERSON"
                             : "KEEP TYPING TO SEND TO EMAIL"
                        )
                        .defaultButtonFont(color: .blue.opacity(0.3), size: 10)

                        if let user = FolderMembershipUser(id: UUID(), email: viewModel.recipientEmail) {
                            InviteUserCell(user: user, isSuggestion: true)
                                .onTapGesture {
                                    viewModel.recipientEmail = user.email
                                    isTextFieldFocused = true
                                }
                        }
                    }

                    if !viewModel.filteredContacts.isEmpty &&  !viewModel.hasEnteredValidEmail {
                        Text(viewModel.recipientEmail.isEmpty
                             ? "SUGGESTED CONTACTS"
                             : "SELECT A PERSON"
                        )
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 10)

                        LazyVStack(alignment: .leading, spacing: 0) {
                            ForEach(viewModel.filteredContacts) { contact in
                                InviteUserCell(user: contact)
                                    .onTapGesture {
                                        viewModel.recipientEmail = contact.email
                                        isTextFieldFocused = true
                                    }
                            }
                        }
                    }

                    Spacer()
                }
                .padding()
            }

            DefaultButton(title: "Send invite", action: {
                viewModel.submitShareEmail()
                presentationMode.wrappedValue.dismiss()
            })
            .disabled(!viewModel.hasEnteredValidEmail)
            .padding()
        }
    }
}

struct InviteUserCell: View {
    var user: FolderMembershipUser
    var isSuggestion = false

    var body: some View {
        HStack {
            if let picture = user.picture {
                ListIcon(
                    iconURL: picture,
                    backgroundColor: .clear,
                    cornerRadius: 12
                )
                .frame(width: 24, height: 24, alignment: .center)
            } else {
                Text(user.email.first!.uppercased())
                    .defaultButtonFont(color: isSuggestion ? .blue.opacity(0.5) : .white, size: 16)
                    .padding(8)
                    .background {
                        Circle()
                            .fill(isSuggestion ? .blue.opacity(0.05) : Color.purple)
                    }
            }

            VStack(alignment: .leading, spacing: 4) {
                Text(user.displayName ?? user.email)
                    .defaultButtonFont(
                        color: isSuggestion ? .blue.opacity(0.2) : .blue,
                        size: isSuggestion ? 12 : 14
                    )
                    .lineLimit(1)
                    .padding(.leading, 4)
            }

            Spacer()
        }
        .padding(.bottom)
    }
}

struct InviteUserScreen_Previews: PreviewProvider {
    static var previews: some View {
        InviteUserScreen()
    }
}
