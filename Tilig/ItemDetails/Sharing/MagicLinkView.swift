//
//  MagicLinkView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 08/02/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct MagicLinkView: View {

    var body: some View {
        VStack(alignment: .leading) {
            Text("Or share with a magic link that can be seen by anyone")
                .defaultButtonFont(color: .blue.opacity(0.5), size: 12)

            Button(action: {}) {
                HStack {
                    Image(systemName: "link")
                        .resizable()
                        .foregroundColor(.mediumPurple)
                        .frame(width: 15, height: 15)

                    Text("Create share-link")
                        .defaultButtonFont(color: .mediumPurple, size: 14)
                }
                .padding(.horizontal)
                .padding(.vertical, 8)
            }
            .overlay(
                RoundedRectangle(cornerRadius: 6)
                    .stroke(Color.mediumPurple, lineWidth: 1)
            )
        }
    }
}

struct MagicLinkView_Previews: PreviewProvider {
    static var previews: some View {
        MagicLinkView()
    }
}
