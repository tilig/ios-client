//
//  FolderMembershipCell.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 08/02/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct FolderMembershipCell: View {
    @EnvironmentObject var viewModel: ShareItemViewModel

    var membership: FolderMembership

    func displayRevokeAccessAlert(forMembership membership: FolderMembership) {
        let alertItem = AlertItem(
            id: UUID(),
            title: Text("Revoke access"),
            message: Text("""
                            Are you sure you want to revoke the access of \(membership.user.email) \
                            to \(viewModel.itemWrapper.name ?? "item")?
                            """),
            primaryButton: .destructive(
                Text("Revoke access"),
                action: { viewModel.revokeAccess(forMembership: membership) }
            ),
            secondaryButton: .cancel()
        )
        alertItem.post()
    }

    var body: some View {
        HStack {
            if let picture = membership.user.picture {
                ListIcon(
                    iconURL: picture,
                    backgroundColor: .clear,
                    cornerRadius: 12
                )
                .frame(width: 24, height: 24, alignment: .center)
            } else {
                Text(membership.user.email.first!.uppercased())
                    .defaultButtonFont(color: .white, size: 16)
                    .padding(8)
                    .background {
                        Circle()
                            .fill(Color.purple)
                    }
            }

            VStack(alignment: .leading, spacing: 4) {
                Text(membership.user.email)
                    .defaultButtonFont(color: .blue, size: 14)
                    .lineLimit(1)
                    .padding(.leading, 4)

                if membership.pending {
                    RoundedLabel(text: "Pending")
                }

            }

            Spacer()

            if viewModel.isRevoking(membership.id) {
                ActivityIndicator()
            } else {
                Button(action: { displayRevokeAccessAlert(forMembership: membership) }) {
                    HStack(spacing: 4) {
                        Image("revoke")
                            .resizable()
                            .frame(width: 14, height: 14)
                            .foregroundColor(.red)
                    }
                    .foregroundColor(.borderGray)
                }
            }
        }
        .padding(.bottom)
    }
}
