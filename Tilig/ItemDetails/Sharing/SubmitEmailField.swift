//
//  SubmitEmailField.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 08/02/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct SubmitEmailField: View {
    @Binding var email: String
    var isSubmitting: Bool
    @State var focused: Bool = false
    var action: () -> Void

    private var cornerRadius: Double { 10 }

    var body: some View {
        HStack {
            FocussableTextField(placeholder: "Type an email address", text: Binding<String>(
                get: { self.email },
                set: { self.email = $0 }
            ), isFirstResponder: $focused) {
                $0.autocapitalizationType = .none
                $0.autocorrectionType = .no
                $0.textContentType = .emailAddress
                $0.keyboardType = .emailAddress
                $0.font = .defaultButtonFont(size: 14)
                $0.textColor = UIColor(Color.blue)
            }
            .padding(.horizontal)
            .onTapGesture {
                focused = true
            }

            Group {
                if isSubmitting {
                    ActivityIndicator(color: .white)
                } else {
                    Button(action: {
                        action()

                        // Hide keyboard after submitting
                        UIApplication.shared.endEditing(true)
                    }) {
                        Text("Share")
                            .defaultButtonFont(color: .white, size: 14)
                    }
                }
            }
            .frame(width: 60)
            .frame(maxHeight: .infinity)
            .background(Color.mediumPurple)
            .cornerRadius(radius: cornerRadius, corners: [.topRight, .bottomRight])
        }
        .overlay(
            RoundedRectangle(cornerRadius: cornerRadius)
                .stroke(focused ? Color.mediumPurple : .brightGray, lineWidth: 1)
        )
        .frame(height: 48)
    }
}
