//
//  ShareItemView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 06/02/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct ShareItemView: View {
    @EnvironmentObject var viewModel: ShareItemViewModel
    @State var showInviteScreen = false

    var body: some View {
        VStack(alignment: .leading, spacing: 12) {
            HStack(alignment: .top) {
                VStack(alignment: .leading, spacing: 4) {
                    Text("Share this \(viewModel.itemType.label.lowercased())")
                        .defaultMediumFont(color: .black, size: 20)

                    Text("Give a friend or coworker full access to \(viewModel.itemWrapper.name ?? "this item")")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 14)
                }

                Spacer()

                Image("share_feature_img")
                    .fixedHeight(40)
            }

            ZStack {
                SubmitEmailField(
                    email: $viewModel.recipientEmail,
                    isSubmitting: viewModel.isSharing,
                    action: { viewModel.submitShareEmail() }
                )
                .disabled(true)

                DefaultButton(
                    title: "Share login",
                    foregroundColor: .clear,
                    backgroundColor: .clear
                ) {
                    showInviteScreen.toggle()
                }
            }

            if let warningText = viewModel.warningText,
               viewModel.showWarning {
                WarningLabel(text: warningText, buttonAction: { viewModel.showWarning.toggle() })
                    .layoutPriority(1)
            }

            if viewModel.hasOtherMemberships {
                Text("SHARED WITH")
                    .defaultButtonFont(color: .blue.opacity(0.3), size: 12)

                LazyVStack(alignment: .leading, spacing: 0) {
                    ForEach(viewModel.folderMemberships.filter { $0.isMe != true }) { member in
                        FolderMembershipCell(membership: member)
                    }
                }
            }

//             Not implemented on iOS yet
//            Divider()
//                .padding(.vertical)
//             MagicLinkView()
        }
        .whiteRectangleBackground()
        .sheet(isPresented: $showInviteScreen) {
            InviteUserScreen()
        }
    }
}

#if DEBUG
struct ShareItemView_Previews: PreviewProvider {

    static var previews: some View {
        ShareItemView()
    }
}
#endif
