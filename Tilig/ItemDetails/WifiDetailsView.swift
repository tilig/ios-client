//
//  WifiDetailsView.swift
//  Tilig
//
//  Created by Gertjan Jansen on 31/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct WifiDetailsView: View {
    @EnvironmentObject var viewModel: ItemViewModel
    @State var isPasswordHidden = true

    var body: some View {
        VStack(spacing: 0) {
            ItemDetailRowView(
                label: "Network name",
                value: viewModel.itemWrapper.ssid,
                action: { viewModel.copy(field: .ssid) }
            )

            ItemDetailRowView(
                label: "Network password",
                value: isPasswordHidden ? String(repeating: "•", count: 10) : viewModel.itemWrapper.password,
                action: { viewModel.copy(field: .password) },
                rightButtonImage: Image(systemName: isPasswordHidden ? "eye.fill" : "eye.slash.fill"),
                rightButtonAction: { isPasswordHidden.toggle() }
            )

            // Hide the notes field when it's empty. This is in line with how web does it.
            if let notes = viewModel.itemWrapper.notes, !notes.isEmpty {
                ItemDetailRowView(
                    label: "Additional notes",
                    value: notes,
                    action: { viewModel.copy(field: .notes) }
                )
            }
        }
        .onAppear {
            isPasswordHidden = true
            TrackingEvent.wifiViewed.send()
        }
    }
}
