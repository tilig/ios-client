//
//  PasswordHistoryView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 16/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI
import Sentry

struct PasswordHistoryButton: View {
    @EnvironmentObject var viewModel: ItemViewModel
    @State var isPresentingPasswordHistory = false

    var reversedPasswordVersions: [HistoricVersion] {
        viewModel.itemWrapper.details.history.reversed()
    }

    var body: some View {
        PasswordHistoryButtonRenderingView(
            hasPasswordVersions: !reversedPasswordVersions.isEmpty,
            onButtonToggle: {
                isPresentingPasswordHistory = true
            }
        )
        .sheet(isPresented: $isPresentingPasswordHistory) {
            PasswordHistoryList()
                .consumingViewModel(.itemDetails)
                .consumingViewModel(.editItem)
        }
        .onAppear {
            TrackingEvent.passwordHistoryViewed.send()
        }
    }
}

private struct PasswordHistoryButtonRenderingView: View {
    var hasPasswordVersions: Bool
    var onButtonToggle: () -> Void = { }

    var body: some View {
        Group {
            DefaultButton(
                title: "View Password History",
                foregroundColor: hasPasswordVersions
                    ? .mediumPurple
                    : .blue.opacity(0.3),
                backgroundColor: hasPasswordVersions
                    ? .white
                    : Color.brightGray,
                action: onButtonToggle
            )
            .disabled(!hasPasswordVersions)
            .frame(height: 50)
        }
    }
}

struct PasswordHistoryList: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var viewModel: ItemViewModel
    @EnvironmentObject var editItemViewModel: EditItemViewModel

    var body: some View {
        VStack(spacing: 0) {
            PopupHeaderView(
                title: "Password History",
                closeButtonAction: {
                    self.presentationMode.wrappedValue.dismiss()
                }
            )

            ScrollView(.vertical) {
                VStack(alignment: .leading) {
                    ForEach(viewModel.passwordVersions, id: \.replacedAt) { version in
                        PasswordHistoryListCell(
                            version: version,
                            restoreAction: {
                                Task {
                                    await editItemViewModel.restorePassword(version)
                                }
                                self.presentationMode.wrappedValue.dismiss()
                            },
                            copyAction: { viewModel.copyPasswordVersion(version) }
                        )
                    }
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
            }

        }
    }
}

struct PasswordHistoryListCell: View {
    var version: HistoricVersion
    var restoreAction: () -> Void
    var copyAction: () -> Void

    var body: some View {
        HStack {
            Button(action: copyAction) {
                VStack(alignment: .leading, spacing: 0) {
                    HStack {
                        Text(version.value)
                            .defaultButtonFont(color: .blue, size: 15)
                            .lineLimit(1)

                        Image("password_copy")
                            .fixedHeight(12)
                    }

                    Spacer()
                        .frame(height: 2)

                    Text(getFormattedDate(date: version.replacedAt))
                        .subTextFont(color: .blue.opacity(0.5), size: 13)
                        .lineLimit(1)
                }
            }

            Spacer()

            WhiteButton(title: "Restore", maxHeight: 30, action: restoreAction)

        }
        .padding(.vertical, 10)
        .padding(.horizontal, 16)
    }

    func getFormattedDate(date: Date) -> String {
        let dateFormatter = ISO8601DateFormatter()
        dateFormatter.formatOptions = [
            .withInternetDateTime,
            .withFractionalSeconds
        ]

        let newFormat = DateFormatter()
        newFormat.dateStyle = .medium
        newFormat.timeStyle = .short
        return newFormat.string(from: date)
    }
}
