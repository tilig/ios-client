//
//  CollapsibleController.swift
//  Tilig
//
//  Created by Gertjan Jansen on 31/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI
import simd

class CollapsibleController {
    init(scrollOffset: CGFloat, title: String, maxBackgroundHeight: Double) {
        self.scrollOffset = scrollOffset
        self.title = title
        self.maxBackgroundHeight = maxBackgroundHeight
    }

    /// Input: scroll offset
    private var scrollOffset: CGFloat = 0

    private var editMode = false

    private var moveIconNextToText = false

    /// Input: the string in the text field
    private var title: String

    var iconRect: CGRect {
        CGRect(
            x: interpolatedIconOffsetX,
            y: interpolatedIconOffsetY,
            width: interpolatedIconWidth,
            height: interpolatedIconHeight
        )
    }

    var textOffset: Double {
        (progress * collapsedTextOffsetY) + (1.0 - progress) * defaultTextOffsetY
    }

    // Disabled for now, since we don't really collapse anymore (at this moment)
    // and the text size animation isn't 100% smooth
    var animateTextSize = false

    var textSize: Double {
        if animateTextSize {
            return Double((progress * minTextSize) + (1.0 - progress) * maxTextSize)
        } else {
            return maxTextSize
        }
    }

    var iconOpacity: Double {
        if moveIconNextToText {
            return 1.0
        } else {
            return 1.0 - progress
        }
    }

    /// 1.0 is fully collapsed, 0.0 is fully uncollapsed
    var progress: CGFloat {
        // Make sure it stays within 0.0 and 1.0.
        // A scrollView can be dragged or pulled higher or lower than that.
        max(min(getRelativeScrollProgress(), 1.0), 0.0)
    }

    /// minimum Height beyond which the background cannot reduce
    private var minBackgroundHeight = 90.0
    private var maxBackgroundHeight: CGFloat // { editMode ? 180.0 : 220.0 }

    /// Returns the negative offset that should be applied to the header background
    /// It is based on the absolute amount scrolled by the scrollview, maxed at a specific value.
    var backgroundOffset: CGFloat {
        let offset = min(scrollOffset, minBackgroundHeight)
        return min(-offset, 0)
    }

    var backgroundHeight: Double { (progress * minBackgroundHeight) + (1.0 - progress) * maxBackgroundHeight }
    var contentOffset: Double { 20.0 }

    private var interpolatedIconOffsetX: CGFloat {
        if moveIconNextToText {
            // move to the left:
            return (progress * collapsedIconOffsetX) + (1.0 - progress) * defaultIconOffsetX
        } else {
            // keep in center:
            return defaultIconOffsetX
        }
    }

    private var interpolatedIconOffsetY: CGFloat {
        (progress * collapsedIconOffsetY) + (1.0 - progress) * defaultIconOffsetY
    }

    private var interpolatedIconWidth: CGFloat {
        (progress * collapsedIconWidth) + (1.0 - progress) * defaultIconWidth
    }

    private var interpolatedIconHeight: CGFloat {
        interpolatedIconWidth
    }

    private var maxScrollOffset: CGFloat = 115
    private var maxProgress = 0.4
    private var topHeight: CGFloat { maxScrollOffset - safeArea.top }

    private var safeArea: UIEdgeInsets {
        guard let screen = UIApplication.shared.connectedScenes.first as? UIWindowScene,
              let safeArea = screen.windows.first?.safeAreaInsets else {
            return .zero
        }
        return safeArea
    }

    private var textWidth: CGFloat {
        title.widthOfString(usingFont: UIFont.defaultMediumFont(size: 28))
    }

    private var defaultIconOffsetX: CGFloat { 0 }
    private var defaultIconOffsetY: CGFloat { 55 }

    private var collapsedIconOffsetY: CGFloat {
        if moveIconNextToText {
            return 45
        } else {
            return 20
        }
    }

    private var collapsedIconWidth: CGFloat = 55
    private var collapsedIconHeight: CGFloat { collapsedIconWidth }

    private var defaultIconWidth: CGFloat { 64 }

    // These are only used when positioning the icon next to the text:
    private var collapsedIconPadding: CGFloat = 5
    private var collapsedIconOffsetX: CGFloat {
        -textWidth / 2.0 - collapsedIconWidth / 2.0 - collapsedIconPadding
    }

    // This is a magic value, unfortunately. Note that changing the self.animateTextSize flag
    // influences the appropriate value for this property.
    private var collapsedTextOffsetY: CGFloat { -10 }
    private var defaultTextOffsetY: CGFloat { 60 }
    private var maxTextSize: CGFloat { 24 }
    private var minTextSize: CGFloat { 16 }

    /// returns relative amount scrolled by the scrollview to the topHeight
    private func getRelativeScrollProgress() -> CGFloat {
        min(1.0, scrollOffset / topHeight)
    }

    var isCollapsed: Bool { progress == 1 }
}
