//
//  DebugTools.swift
//  Tilig
//
//  Created by Gertjan Jansen on 14/02/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

#if DEBUG
struct DebugToolsView: View {
    let itemCreator = DummyItemCreator()
    let itemListCleaner = ItemListCleaner()

    var body: some View {
        if UserDefaults.standard.bool(forKey: "showDebugTools") {
            VStack {
                Button(action: itemCreator.createAll) {
                    Text("Create App Store items")
                }

                Button(action: itemListCleaner.deleteAllItems) {
                    Text("Delete all items")
                }
            }
            .padding(.bottom, 20)
        }
    }
}
#endif
