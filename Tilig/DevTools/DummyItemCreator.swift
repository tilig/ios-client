//
//  DummyItemCreator.swift
//  Tilig
//
//  Created by Gertjan Jansen on 14/02/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//
import Foundation
import Combine

#if DEBUG
class DummyItemCreator {
    var crypto: Crypto? {
        if let keyPair = AuthState.cached?.keyPair,
           let legacyKeyPair = AuthState.cached?.legacyKeyPair {
            return Crypto(keyPair: keyPair, legacyKeyPair: legacyKeyPair)
        }
        return nil
    }

    let username = "eric@tilig.com"
    var items: [Item] {
        [
            // Currently disabled because it shows an orange logo
//            Item(
//                name: "Amazon",
//                website: CodableOptionalURL(string: "https://www.amazon.co.uk"),
//                username: "eric@tilig.com",
//                notes: nil,
//                password: PasswordGenerator.randomPassword(),
//                otp: nil
//            ),
            Item(
                name: "Netflix",
                website: CodableOptionalURL(string: "https://www.netflix.com"),
                username: "bingewatcher@tilig.com",
                password: PasswordGenerator.randomPassword(),
                notes: nil,
                otp: nil
            ),
            Item(
                name: "Airbnb",
                website: CodableOptionalURL(string: "https://www.airbnb.com"),
                username: "holiday.lover@tilig.com",
                password: PasswordGenerator.randomPassword(),
                notes: "John's account",
                otp: nil
            ),
            Item(
                name: "Facebook",
                website: CodableOptionalURL(string: "https://www.facebook.com"),
                username: username,
                password: PasswordGenerator.randomPassword(),
                notes: nil,
                otp: nil
            ),
            Item(
                name: "Spotify",
                website: CodableOptionalURL(string: "https://www.spotify.com"),
                username: username,
                password: PasswordGenerator.randomPassword(),
                notes: nil,
                otp: nil
            ),
            Item(
                name: "YouTube",
                website: CodableOptionalURL(string: "https://www.youtube.com"),
                username: username,
                password: PasswordGenerator.randomPassword(),
                notes: nil,
                otp: nil
            ),
            Item(
                name: "Twitter",
                website: CodableOptionalURL(string: "https://www.twitter.com"),
                username: username,
                password: PasswordGenerator.randomPassword(),
                notes: nil,
                otp: nil
            ),
            Item(
                name: "Booking",
                website: CodableOptionalURL(string: "https://www.booking.com"),
                username: username,
                password: PasswordGenerator.randomPassword(),
                notes: nil,
                otp: nil
            ),
            Item(
                name: "Uber",
                website: CodableOptionalURL(string: "https://www.uber.com"),
                username: username,
                password: PasswordGenerator.randomPassword(),
                notes: nil,
                otp: nil
            )
        ]
    }

    func createAll() {
        guard let crypto = crypto else { return }
        for item in items {
            // swiftlint:disable force_try
            let encrypted = try! item.encrypt(withKeyPair: crypto.legacyKeyPair)
            // swiftlint:disable force_try
            let decryptedItemWrapper = try! crypto.decrypt(fullItem: encrypted)
            // swiftlint:disable force_try
            let v2Encrypted = try! crypto.encrypt(itemWrapper: decryptedItemWrapper)

            save(item: v2Encrypted)
        }
    }

    func save(item: Item) {
        guard let authState = AuthState.cached else { return }
        let client = Client(tokenProvider: authState)

        Task {
            do {
                _ = try await client.saveItem(item)

                Log("Item created => \(item)")
            } catch {
                error.raiseGlobalAlert()
            }
        }
    }
}

extension Item {
    func encrypt(withKeyPair keyPair: LegacyKeyPair) throws -> Item {
        var item = self
        if let username = item.username {
            item.username = try keyPair.encrypt(plainText: username)
        }
        if let password = item.password {
            item.password = try keyPair.encrypt(plainText: password)
        }
        if let notes = item.notes {
            item.notes = try keyPair.encrypt(plainText: notes)
        }
        return item
    }
}
#endif
