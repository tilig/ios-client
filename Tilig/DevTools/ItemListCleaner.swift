//
//  ItemListCleaner.swift
//  Tilig
//
//  Created by Gertjan Jansen on 18/02/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import Combine

#if DEBUG
class ItemListCleaner {
    var subscriptions = Set<AnyCancellable>()

    func deleteAllItems() {
        URLSession
            .current
            .dataTaskPublisher(for: .items, method: .get)
            .decodeResponse(as: [Item].self)
            .sink { _ in
                Log("Got items")
            } receiveValue: { [weak self] items in
                for item in items {
                    self?.remove(item: item)
                }
            }
            .store(in: &subscriptions)
    }

    private func remove(item: Item) {
        URLSession
            .current
            .dataTaskPublisher(for: .item(item.id!), method: .delete)
            .decodeResponse(as: DeleteResponse.self)
            .mapError { _ in DeleteItemError.deletionFailed }
            .sink(receiveCompletion: { completion in
                if case .failure(let error) = completion {
                    error.raiseGlobalAlert()
                }
            }, receiveValue: { _ in
                Log("Deleted item: \(item.id!)")
            })
            .store(in: &subscriptions)
    }
}
#endif
