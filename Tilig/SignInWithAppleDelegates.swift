import UIKit
import AuthenticationServices
import Contacts
import Combine
import os.log
import Valet

enum AppleAuthError {
    case tiligTokenIsMissing
    case tiligUserIsMissing
    case tokenResponseError(Error)
    case responseContainsErrorMessage(String)

    case savingUserDataFailed(Error)
    case savingUserIdentifierFailed(Error)
}

struct AppleAuthTokenResponse: Decodable, Encodable {
    let accessToken: String
    let verified: Bool
}

extension AppleAuthError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .tiligTokenIsMissing, .tiligUserIsMissing:
            return "Tilig authentication failed."
        case .tokenResponseError:
            return "Received an error from the Tilig server."
        case .responseContainsErrorMessage(let msg):
            return "Could not sign in with Apple. Error message: \(msg)."

        case .savingUserDataFailed, .savingUserIdentifierFailed:
            return "Could not create Apple account."
        }
    }
}

class SignInWithAppleDelegate: NSObject {

    // MARK: Published auth attributes

    @Published var tiligToken: String?
    @Published var tiligUser: UserModel?

    var appleAuthError: AppleAuthError? {
        didSet {
            if let error = appleAuthError {
                OSLog.error("googleAuthError: \(error)")
                NotificationCenter.default.post(name: .globalError, object: error)
            }
        }
    }

    // MARK: Window

    weak var window: UIWindow?

    private var subscriptions = Set<AnyCancellable>()

    init(window: UIWindow? = nil) {
        self.window = window
    }
}

extension SignInWithAppleDelegate: ASAuthorizationControllerDelegate {

    func authorizationController(
        controller: ASAuthorizationController,
        didCompleteWithAuthorization
        authorization: ASAuthorization
    ) {
        guard let credential = authorization.credential as? ASAuthorizationAppleIDCredential else {
            return
        }

        if saveUserIdentifier(credential.user) {
            registerNewAccount(credential: credential)
        }
    }

    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Silently ignore errors
        OSLog.error("Apple auth didCompleteWithError: \(error)")

        self.setSignedOutState()
    }

    func signInUser(appleUser: AppleAuthorizationModel) {
        guard let params = try? appleUser.asDictionary() else {
            OSLog.error("⚠️Failed creating params for apple sign in")
            return
        }

        BaseService.shared.responsePublisher("user/verify_id_token_and_create_user_for_apple",
                                             controller: .users,
                                             parameters: params,
                                             method: .post,
                                             decodableType: VerifyTokenResponse.self)
            .sink(receiveCompletion: { completion in
                if case .failure(let error) = completion {
                    // Until we have a better solution
                    // Silently ignore errors
                    // This also ignores bad responses from the server.
                    OSLog.error("verify_id_token_and_create_user_for_apple failed: \(error)")

                    self.setSignedOutState()
                }
            }) { (verifyResponse) in
                if let msg = verifyResponse.msg {
                    self.appleAuthFailed(error: .responseContainsErrorMessage(msg))
                    return
                }

                guard let tiligToken = verifyResponse.token else {
                    self.appleAuthFailed(error: .tiligTokenIsMissing)
                    return
                }

                guard let tiligUser = verifyResponse.user else {
                    self.appleAuthFailed(error: .tiligUserIsMissing)
                    return
                }

                self.setSignedInState(tiligToken: tiligToken, tiligUser: tiligUser)
            }
            .store(in: &subscriptions)

    }

    // MARK: Failure handling

    private func appleAuthFailed(error: AppleAuthError) {
        self.appleAuthError = error

        setSignedOutState()
    }

    // MARK: Signin and signout state changes

    func setSignedInState(tiligToken: String, tiligUser: UserModel) {
        self.tiligUser = tiligUser
        self.tiligToken = tiligToken
        self.appleAuthError = nil
    }

    func setSignedOutState() {
        tiligUser = nil
        tiligToken = nil
    }

    // MARK: 

    private func registerNewAccount(credential: ASAuthorizationAppleIDCredential) {
        let appleUser = AppleAuthorizationModel(
            email: credential.email,
            name: credential.fullName,
            identifier: credential.user,
            authorizationCode: credential.authorizationCode,
            identityToken: credential.identityToken
        )

        if saveUserData(appleUser) {
            signInUser(appleUser: appleUser)
        }
    }

    /// Error handler around saveAppleUserIdentifier()
    private func saveUserData(_ userData: AppleAuthorizationModel) -> Bool {
        do {
            try saveAppleUserData(userData: userData)

            return true
        } catch let error {
            self.appleAuthFailed(error: .savingUserDataFailed(error))

            return false
        }
    }

    /// Error handler around saveAppleUserIdentifier()
    private func saveUserIdentifier(_ identifier: String) -> Bool {
        do {
            try saveAppleUserIdentifier(identifier: identifier)

            return true
        } catch let error {
            self.appleAuthFailed(error: .savingUserIdentifierFailed(error))

            return false
        }
    }

    // MARK: Storing Apple User data

    private func saveAppleUserIdentifier(identifier: String) throws {
        try Valet.auth.setString(identifier, forKey: ValetKeys.savedAppleIdentifier)
    }

    private func saveAppleUserData(userData appleUser: AppleAuthorizationModel) throws {
        let encodedData = try JSONEncoder().encode(appleUser)
        try Valet.auth.setObject(encodedData, forKey: ValetKeys.appleUserData)
    }

}

extension SignInWithAppleDelegate: ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.window!
    }
}
