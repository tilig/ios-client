//
//  SceneDelegate.swift
//  Tilig
//
//  Created by Jacob Schatz on 1/22/20.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import UIKit
import SwiftUI

struct WindowKey: EnvironmentKey {
    struct Value {
        weak var value: UIWindow?
    }

    static let defaultValue: Value = .init(value: nil)
}

extension EnvironmentValues {
    var window: UIWindow? {
        get { return self[WindowKey.self].value }
        set { self[WindowKey.self] = .init(value: newValue) }
    }
}

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    let globalFactory = GlobalFactory()

    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        guard let windowScene = scene as? UIWindowScene else { return }
        let window = UIWindow(windowScene: windowScene)
        self.window = window

        let rootView = ContentView()
            .environment(\.window, window)
            .environmentObject(globalFactory)
            .environmentObject(globalFactory.routeModel)
            .environmentObject(globalFactory.authManager)
            .environmentObject(globalFactory.autoFillViewModel)
            .environmentObject(globalFactory.biometricViewModel)
            .environmentObject(NetworkMonitor())

        window.rootViewController = UIHostingController(rootView: rootView)
        window.makeKeyAndVisible()
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not
        // neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        globalFactory.autoFillViewModel.updateAutoFillEnabledFlag()

        // onAppear is not called when the the app did become active
        globalFactory.authenticatedModelFactory?.itemListViewModel.activate()

        // Try to unlock
        globalFactory.biometricViewModel.tryToUnlock()

        // Handle untracked autofill events
        AutoFillTrackingState.handleUntrackedEvents()
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        globalFactory.biometricViewModel.lockIfEnabled()
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        globalFactory.biometricViewModel.lockIfEnabled()
    }
}
