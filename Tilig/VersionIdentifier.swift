//
//  VersionManager.swift
//  Tilig
//
//  Created by Gertjan Jansen on 17/11/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import UIKit

struct VersionIdentifier {
    static var appVersion: String {
        // Force-optional is pretty safe here
        // swiftlint:disable force_cast
        Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
    }

    /// The x-tilig-version header value
    static var tiligVersion: String {
        "\(tiligPlatform) \(appVersion)"
    }

    /// The x-tilig-platform header value.
    /// We only support iOS and iPadOS at the moment.
    static var tiligPlatform: String {
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            return "iOS"
        case .pad:
            return "iPadOS"
        default:
            return "Apple (unspecified)"
        }
    }
}
