//
//  SignInScreen.swift
//  Bolted
//
//  Created by Jacob Schatz on 1/22/20.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import UIKit
import SwiftUI

struct SignInScreen: View {
    @EnvironmentObject var authManager: AuthManager
    @State private var isLogin = false
    var backAction: () -> Void

    private var subTitle: String {
        if isLogin {
            return "Login to Tilig for a fast and smooth experience navigating your accounts."
        }
        return "Increase your online security by using an account you already have, no master password needed!"
    }

    var body: some View {
        GeometryReader { reader in
            ZStack {
                Image("intro_signup")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .responsiveHeight(compact: 340, regular: reader.size.height * 0.3)
                    .padding(.leading, 80)
                    .padding(.bottom, 260)

                VStack(alignment: .leading, spacing: 20) {
                    HStack {
                        BackButton(
                            foregroundColor: .blue,
                            hasPadding: false,
                            action: {
                                if isLogin {
                                    isLogin = false
                                } else {
                                    backAction()
                                }
                            }
                        )

                        Spacer()

                        Button(action: { isLogin.toggle() }) {
                            Text(isLogin ? "Signup" : "Login")
                                .defaultButtonFont(color: .mediumPurple, size: 15)
                        }
                    }
                    .frame(height: 40)
                    .responsivePadding(.horizontal, compact: 16, regular: 32)

                    VStack {
                        ZStack {

                            VStack(alignment: .leading, spacing: 16) {
                                Text(isLogin ? "Welcome back!" : "Signup to Tilig")
                                    .largeBoldFont(color: .blue, size: 24)

                                Text(subTitle)
                                    .defaultButtonFont(color: .subTextLightPurple, size: 16)
                                    .multilineTextAlignment(.leading)
                                    .lineSpacing(4)
                                    .frame(maxWidth: .infinity, alignment: .leading)

                                Spacer()
                            }
                            .responsivePadding(.vertical, compact: 32, regular: 64)
                            .responsivePadding(.horizontal, compact: 16, regular: 32)
                        }

                        Spacer()

                        VStack(alignment: .center) {
                            if authManager.isAuthenticating {
                                HStack(alignment: .center) {
                                    Spacer()
                                    ActivityIndicator()
                                    Text(isLogin ? "Signing in..." : "Signing up...")
                                    Spacer()
                                }
                                .transition(.asymmetric(insertion: .opacity, removal: .identity))
                            } else {
                                VStack(alignment: .center, spacing: 25) {
                                    FirebaseAuth(authManager: authManager, isLogin: isLogin)
                                }
                            }
                        }
                        .responsivePadding(compact: 16, regular: 32)
                        .frame(height: 260)

                        Spacer()
                    }
                }
            }
        }
        .onAppear {
            TrackingEvent.showWelcomeInstructions.send()
        }
    }
}

#if DEBUG
struct SignInScreen_Previews: PreviewProvider {
    static let authManager = AuthManager()
    static var previews: some View {
        VStack {
            SignInScreen(backAction: {})
                .environmentObject(authManager)

            // To test out animations
            Button(action: {
                authManager.isAuthenticating.toggle()
            }) {
                Text("Toggle")
            }
        }
    }
}
#endif
