//
//  Experiments.swift
//  Tilig
//
//  Created by Gertjan Jansen on 20/01/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import Mixpanel

enum ABVariant: Int {
    // swiftlint:disable identifier_name
    case a = 0
    // swiftlint:disable identifier_name
    case b = 1
}

class Experiments {
    /// Decide if this user should get variant A or B for the given key
    /// Also stores the chosen variant in UserDefaults and Mixpanel profile
    static func abVariant(forKey key: String) -> ABVariant {
        let value = getOrCreateVariant(forKey: key)

        assert(value < 2)

        let abVariant = ABVariant.init(rawValue: value) ?? .a

        // Set variant on Mixpanel property
        Mixpanel.mainInstance().people.set(
            properties: [ key: abVariant.rawValue ]
        )

        Log("🎲 Variant for \(key) => \(abVariant)")

        return abVariant
    }

    static func resetVariant(forKey key: String) {
        UserDefaults.standard.setValue(nil, forKey: prefixedKey(key))
    }

    private

    /// Prefix the UserDefault's key to be sure it never mixes up with something else
    static func prefixedKey(_ key: String) -> String {
        return "variant_\(key)"
    }

    static func hasPersistedValue(forKey key: String) -> Bool {
        UserDefaults.standard.object(forKey: prefixedKey(key)) != nil
    }

    static func storeRandomValue(forKey key: String) {
        let value = Int.random(in: 0...1)
        UserDefaults.standard.set(value, forKey: prefixedKey(key))
    }

    static func getOrCreateVariant(forKey key: String) -> Int {
        // If no value has been stored, calculate a new one
        if !hasPersistedValue(forKey: key) {
            storeRandomValue(forKey: key)
        }

        return UserDefaults.standard.integer(forKey: prefixedKey(key))
    }
}
