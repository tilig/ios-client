//
//  BiometricHelper.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 23/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//
import Foundation
import LocalAuthentication
import Combine
import Toast

protocol LAContextProtocol {
    func canEvaluatePolicy(_: LAPolicy, error: NSErrorPointer) -> Bool
    func evaluatePolicy(_ policy: LAPolicy, localizedReason: String, reply: @escaping (Bool, Error?) -> Void)
    var biometryType: LABiometryType { get }
}

extension LAContext: LAContextProtocol {}

class BiometricHelper {
    private var context: LAContextProtocol
    private var error: NSError?

    init(context: LAContextProtocol = LAContext() ) {
        self.context = context
    }

    var canEvaluate: Bool {
        context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)
    }

    /// If the user has denied biometry access for our app, this returns true.
    var biometryPermissionDenied: Bool {
        // Note that canEvaluate needs to be called for error to be non-nil
        _ = canEvaluate
        if let error = error as? LAError, case LAError.biometryNotAvailable = error {
            return true
        }
        // Biometry is probably not enabled on device level in this scenario
        return false
    }

    // The type of biometrics that the device supports.
    // Note that when it has support, that does not mean that it is also enrolled.
    var biometryType: LABiometryType { context.biometryType }

    func authenticateBiometric() async throws -> Bool {
        let reason = "Please verify your identity to unlock Tilig."
        return try await withCheckedThrowingContinuation({ cont in
            self.context.evaluatePolicy(
                .deviceOwnerAuthenticationWithBiometrics,
                localizedReason: reason
            ) { result, error in
                if let error = error { return cont.resume(throwing: error) }
                cont.resume(returning: result)
            }
        })
    }
}

extension Error {
    func asBiometricError() -> BiometricError? {
        switch self {
        case LAError.appCancel, LAError.systemCancel, LAError.userCancel:
            return BiometricError.authenticationCancelled
        case LAError.passcodeNotSet, LAError.biometryNotEnrolled:
            return BiometricError.biometricNotSetup
        case LAError.biometryNotAvailable:
            return BiometricError.biometricNotSupported
        case LAError.userFallback, LAError.authenticationFailed:
            return BiometricError.authenticationFailed
        default:
            return nil
        }
    }
}

enum BiometricError: Error {
    case biometricNotSupported
    case biometricNotSetup
    case authenticationFailed
    case authenticationCancelled
}

extension BiometricError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .biometricNotSupported:
            return """
                Unlocking this app with face recognition or fingerprint \
                is unfortunately not supported on this device.
            """
        case .biometricNotSetup:
            return """
                You haven't enabled face or fingerprint recognition on this device. Please enable face ID or touch ID in your device's settings.
            """
        case .authenticationFailed:
            return "Authenticating user failed. Please try again"
        case .authenticationCancelled:
            return "Biometric authentication cancelled."
        }
    }
}
