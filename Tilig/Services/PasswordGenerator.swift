//
//  SimplePasswordGenerator.swift
//  Tilig
//
//  Created by Gertjan Jansen on 28/09/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import Foundation

class PasswordGenerator {

    static func randomPassword() -> String {
        let lowercaseChars = "abcdefghijklmnopqrstuvwxyz"
        let uppercaseChars = lowercaseChars.uppercased()
        let digits = (0...9).map { String($0) }

        var firstLowercaseGroup = (1...5).map { _ in lowercaseChars.randomElement()! }
        var secondLowercaseGroup = (1...5).map { _ in lowercaseChars.randomElement()! }
        let uppercaseGroup = uppercaseChars.randomElement()!
        let digitGroup = digits.randomElement()!

        if Bool.random() {
            firstLowercaseGroup.append(contentsOf: digitGroup)
        } else {
            firstLowercaseGroup.insert(contentsOf: digitGroup, at: 0)
        }

        if Bool.random() {
            secondLowercaseGroup.append(uppercaseGroup)
        } else {
            secondLowercaseGroup.insert(uppercaseGroup, at: 0)
        }

        var mergedGroup = [String(firstLowercaseGroup), String(secondLowercaseGroup)].shuffled()
        return mergedGroup.joined(separator: "-")
    }
}
