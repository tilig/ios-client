//
//  SettingsURLOpenTracker.swift
//  Tilig
//
//  Created by Gertjan Jansen on 08/12/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import UIKit

class URLOpenTracker {
    static let shared = URLOpenTracker()

    func getSettingsBase() -> String {
        return "App-prefs"
    }

    func getSettingsPage() -> String {
        return "PASSWORDS"
    }

    func getAndTrackUrl() -> URL {
        trackMixpanelUrlOpen()
        return URL(string: "\(getSettingsBase()):\(getSettingsPage())")!
    }

    /// returns url for settings page for Tilig
    /// - Returns: url for settings page for Tilig
    func getAppSettingsPage() -> URL {
        return URL(string: UIApplication.openSettingsURLString)!
    }

    func trackMixpanelUrlOpen() {
        TrackingEvent.systemSettingsOpened.send()
    }
}
