//
//  DebounceHelper.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 17/02/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import Foundation

class DebounceHelper {
    // MARK: Debounce helpers

    private var task: Task<(), Error>?

    func debounce(duration: Double, operation: @escaping () async -> Void) {
        task?.cancel()

        task = Task {
            try await sleep(duration: duration)
            await operation()
            task = nil
        }
    }

    private func sleep(duration: Double) async throws {
        try await Task.sleep(nanoseconds: UInt64(duration * .nanosecondsPerSecond))
    }
}
