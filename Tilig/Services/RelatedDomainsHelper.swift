//
//  RelatedDomainsHelper.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 24/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import Sentry

typealias RelatedDomains = [String]

class RelatedDomainsHelper {
    private(set) var domains: [RelatedDomains] = []
    private let resourceName = "related-domains"

    init() {
        do {
            guard let bundlePath = Bundle.main.path(forResource: resourceName, ofType: "json"),
                  let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) else {
                Log("Could not locate file or retrieve json data from resource file")
                return
            }
            self.domains = try JSONDecoder().decode([RelatedDomains].self, from: jsonData)
        } catch {
            SentrySDK.capture(error: error)
            Log(error.localizedDescription)
        }
    }

    func findRelatedDomains(_ domain: String) -> RelatedDomains {
        return domains.filter({ $0.contains(domain) }).first ?? [domain]
    }
}
