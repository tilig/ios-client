//
//  Logging.swift
//  Tilig
//
//  Created by Gertjan Jansen on 05/10/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import Foundation
import os.log

/// An enum with log categories we use ("modules")
enum LogCategory: String {
    /// Logs anything that's not specific to anything of the other subsystems
    case general

    // Specific categories:
    case authentication
    case biometrics
    case encryption
    case credentialIdentities
    case itemList
    case mixpanel
    case requests
    case routing
}

extension LogCategory {
    func toOSLog() -> OSLog {
        .forCategory(self)
    }
}

extension OSLog {
    private static var subsystem = Bundle.main.bundleIdentifier!

    static func forCategory(_ category: LogCategory) -> OSLog {
        OSLog(subsystem: subsystem, category: category.rawValue)
    }
}

extension OSLog {
    /// A convenience method for logging debug messages
    static func debug(_ message: String, log: OSLog = .forCategory(.general), type: OSLogType = .debug) {
        os_log("%@", log: log, type: .info, message)
    }

    /// A convenience method for logging info messages
    static func info(_ message: String, log: OSLog = .forCategory(.general), type: OSLogType = .info) {
        os_log("%@", log: log, type: .info, message)
    }

    /// A convenience method for logging error messages
    static func error(_ message: String, log: OSLog = .forCategory(.general), type: OSLogType = .error) {
        os_log("%@", log: log, type: .info, message)
    }
}

/// Convenience logging function, without having to import os.log
// swiftlint:disable identifier_name
func Log(_ message: String, category: LogCategory = .general, type: OSLogType = .debug) {
    // TODO: add some kind of flag/runargument system for this
    let isEnabled = category == .general || type == .error || category == .requests

    if isEnabled {
        OSLog.debug(type == .error ? "⚠️ ERROR: \(message)" : message, log: .forCategory(category), type: type)
    }
}
