//
//  Notifications.swift
//  Bolted
//
//  Created by Jacob Schatz on 1/22/20.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let globalError = Notification.Name("globalError")
}
