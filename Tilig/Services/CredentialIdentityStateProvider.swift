//
//  CredentialIdentityStateProvider.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 25/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import AuthenticationServices

/// A wrapper around `ASCredentialIdentityStore`'s `getState` callback, that gets called with a boolean
/// value indicating whether the credential identity store is enabled.
class CredentialIdentityStateProvider {

    /// Receives a callback that gets called with a boolean value
    /// indicating whether the credential identity store is enabled.
    func getState(_ onStateChange: @escaping (Bool) -> Void) {
        ASCredentialIdentityStore.shared.getState { state in
            onStateChange(state.isEnabled)
        }
    }
}

#if DEBUG
class MockedCredentialIdentityStateProvider: CredentialIdentityStateProvider {
    private var onStateChange: ((Bool) -> Void)?
    private var isEnabled: Bool = false

    override func getState(_ onStateChange: @escaping (Bool) -> Void) {
        self.onStateChange = onStateChange
        onStateChange(isEnabled)
    }

    /// Call this to simulate enabling or disabling the credential identity store.
    func triggerStateChange(isEnabled: Bool) {
        self.isEnabled = isEnabled
        if let onStateChange = onStateChange {
            onStateChange(isEnabled)
        }
    }
}
#endif
