//
//  CredentialStoreLoader.swift
//  Tilig
//
//  Created by Gertjan Jansen on 08/10/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import Foundation
import AuthenticationServices
import Combine
import Sentry

enum CredentialStoreError: Error {
    case adddingIdentityFailed(Error)
    case replacingIdentitiesFailed(Error)
}

/// This automatically loads and populates the ASCredentialIdentityStore.
/// It should make sure the loaded identities are always in sync with the user's login items.
/// This includes clearing the store when signing out.
class CredentialIdentitiesLoader {

    /// This loader updates the ASCredentialIdentityStore with the given items,
    /// and  updates it whenever the give publisher publishes a new item list.
    /// - crypto: necessary for decryption
    /// - items: the current list of item
    /// - itemsPublisher: a publisher that publishes a new item list
    /// - credentialIdentityStateProvider: useful to mock the ASCredentialIdentityStore
    init(crypto: Crypto,
         items: [Item],
         itemsPublisher: AnyPublisher<[Item], Never>,
         credentialIdentityStateProvider: CredentialIdentityStateProvider = CredentialIdentityStateProvider()) {
        self.credentialIdentityStateProvider = credentialIdentityStateProvider
        self.crypto = crypto
        self.items = items

        itemsCancellable = itemsPublisher
            .removeDuplicates()
            .sink { [weak self] items in
                self?.items = items
            }

        self.sync()
    }

    var credentialIdentityStateProvider: CredentialIdentityStateProvider

    private var keyPairCancellable: AnyCancellable?
    private var itemsCancellable: AnyCancellable?

    private var crypto: Crypto
    private var items: [Item] {
        didSet {
            self.sync()
        }
    }

    private func sync() {
        if !items.isEmpty {
            Log("🏪 Initializing store with \(items.count) items", category: .credentialIdentities)
            self.replaceCredentialIdenties(with: items)
        } else {
            Log("🏪 Clearing store", category: .credentialIdentities)
            self.clearCredentialIdentities()
        }
    }

    @Published var identities: [ASPasswordCredentialIdentity] = []

    /// Replace identities in store with given items
    private func replaceCredentialIdenties(with items: [Item]) {
        Log("🏪 Replacing store with \(items.count) identities", category: .credentialIdentities)

        let store = ASCredentialIdentityStore.shared

        credentialIdentityStateProvider.getState { [weak self] isEnabled in
            guard let self = self else { return }

            if !isEnabled {
                Log("🏪 Store is not enabled", category: .credentialIdentities)
                return
            }

            self.identities = items.flatMap { self.passwordCredentialIdentities(forItem: $0) }

            store.replaceCredentialIdentities(with: self.identities) { (_, error) in
                if let error = error {
                    self.handleError(.replacingIdentitiesFailed(error))
                    Log("🏪 Could not replace credential identities", category: .credentialIdentities, type: .error)
                } else {
                    Log("🏪 Replaced \(self.identities.count) credential identities", category: .credentialIdentities)
                }
            }
        }
    }

    /// Add one item to the identity store
    private func addCredentialIdentity(forItem item: Item) {
        let store = ASCredentialIdentityStore.shared

        credentialIdentityStateProvider.getState { [weak self] isEnabled in
            guard let self, isEnabled else { return }

            let identities = self.passwordCredentialIdentities(forItem: item)

            store.saveCredentialIdentities(identities) { (_, error) in
                if let error = error {
                    self.handleError(.adddingIdentityFailed(error))
                }
            }
        }
    }

    private func clearCredentialIdentities() {
        replaceCredentialIdenties(with: [])
    }

    // MARK: Password Identitiy initialization

    /// Convenience method for creating pasword ASPasswordCredentialIdentity for an item.
    /// When the given item does not contain a username/email address, then it returns nil.
    ///
    /// It's non-private for testing purposes.
    func passwordCredentialIdentities(forItem item: Item) -> [ASPasswordCredentialIdentity] {
        guard let itemID = item.id else {
            // Silently fail but send the error to Sentry
            Log("🏪 Cannot create a password identity for an item without id", type: .error)
            SentrySDK.capture(message: "Cannot create a password identity for an item without id")
            return []
        }
        SentrySDK.addSimpleBreadcrumb("Decrypting username for item \(item.id)")
        guard let username = decryptUsername(forItem: item) else {
            return []
        }

        // We have 2 options for service identifiers: .domain or .URL.
        // We previously used .URL, but .domain makes more sense. It's also what Apple
        // demonstrate themselves. I expect .domain to be less strict then .URL.
        // The domain we use is determined by our backend, which takes into item the
        // Public Suffix List.
        guard let domain = item.domain else {
            let itemString = "\(item.id?.uuidString ?? ""): domain is nil"
            Log("🏪 Could not create a password identity for \(itemString)", type: .error)
            return []
        }

        Log("🏪 identifier domain: \(domain)")
        return RelatedDomainsHelper().findRelatedDomains(domain).map { domain in
            let serviceIdentifier = ASCredentialServiceIdentifier(
                identifier: domain,
                type: .domain
            )

            return ASPasswordCredentialIdentity(
                serviceIdentifier: serviceIdentifier,
                user: username,
                recordIdentifier: itemID.uuidString
            )
        }
    }

    // Returns the username, if present. Fail silently, but sends decryption errors to Sentry.
    private func decryptUsername(forItem item: Item) -> String? {
        do {
            let overview = try crypto.decryptOverview(encryptedItem: item)
            let username = overview.info
            if username.isEmptyOrNil {
                Log("🏪 Could not decrypt username: it's empty", category: .credentialIdentities)
            }
            return username
        } catch {
            let errorMessage = "Cred loader failed to decrypt \(item.id?.uuidString ?? "-")"
            Log("🏪 \(errorMessage)", type: .error)
            SentrySDK.capture(message: errorMessage)
            return nil
        }
    }

    private func handleError(_ error: CredentialStoreError) {
        Log("🏪 Credential identies error: \(error)", type: .error)
        switch error {
        case .replacingIdentitiesFailed(let wrappedError):
            SentrySDK.addSimpleBreadcrumb("Store error: replacingIdentitiesFailed")
            SentrySDK.capture(error: wrappedError)
        case .adddingIdentityFailed(let wrappedError):
            SentrySDK.addSimpleBreadcrumb("Store error: adddingIdentityFailed")
            SentrySDK.capture(error: wrappedError)
        }
    }
}
