//
//  NetworkMonitor.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 26/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import Network
import SwiftUI

/// Listens to changes in Network Connection and publishes whether it has connection or not
class NetworkMonitor: ObservableObject {
    private let monitor = NWPathMonitor()
    private let queue = DispatchQueue(label: "TiligNetworkMonitor")

    @Published var hasConnection = true

    init() {
        monitor.pathUpdateHandler = { [weak self] path in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.hasConnection = path.status == .satisfied
                Log("Connected? : \(self.hasConnection)")
            }
        }
        monitor.start(queue: queue)
    }
}
