//
//  TwoFactorGenerator.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import OneTimePassword

class TwoFactorGenerator {

    private var period: Int = 30

    func getOneTimePassword(code: String) -> String? {
        guard let generatedToken = try? self.generateToken(code: code) else { return nil }
        return self.getPasswordForCurrentTime(token: generatedToken)
    }

    private func generateToken(code: String) throws -> OneTimePassword.Token {
        guard let url = URL(string: code),
              let token = try? OneTimePassword.Token(url: url) else {
            throw URLError(.badURL)
        }

        return token.updatedToken()
    }

    private func getPasswordForCurrentTime(token: OneTimePassword.Token) -> String? {
        let currentTime = Date()
        return try? token.generator.password(at: currentTime)
    }

    func getRemainingSeconds() -> Int {
        return period - getCounter()
    }

    private func getCounter() -> Int {
        return Int(Date().timeIntervalSince1970.truncatingRemainder(dividingBy: TimeInterval(period)))
    }

    func refreshCodeIfNecessary() -> Bool {
        return period - getRemainingSeconds() == 0
    }
}
