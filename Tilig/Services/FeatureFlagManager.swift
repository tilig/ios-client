//
//  FeatureFlagManager.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 16/12/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import UnleashProxyClientSwift
import Sentry

enum FeatureFlagKeys: String {
    case microsoftSignInEnabled = "clients-microsoft_login"
}

class FeatureFlagManager {

    private static var url: String {
        return environment == "staging"
            ? "https://flags-staging.tilig.com/proxy"
            : "https://flags.tilig.com/proxy"
    }

    private static var environment: String {
        #if DEBUG
        return "staging"
        #else
        return "production"
        #endif
    }

    private static var unleashClient = UnleashClient(
        unleashUrl: url,
        clientKey: "CkUeaMwHGCguBrfgy6JPLnz6v",
        refreshInterval: 0,
        appName: environment
    )

    static func setup() {
        Task {
            unleashClient.start(true) { error in
                if let error {
                    Log("Error starting Unleash: \(error)", category: .general)
                    SentrySDK.capture(error: error)
                }
            }
        }

        #if DEBUG
        unleashClient.subscribe(name: "ready", callback: handleReady)
        #endif
    }

    static func handleReady() {
        // do this when unleash is ready
        Log("Feature Flag setup ready", category: .general)
    }

    static func isFlagEnabled(flag: FeatureFlagKeys) -> Bool {
        return unleashClient.isEnabled(name: flag.rawValue)
    }

    static func setIdentity(authState: AuthState) {
        let userId = authState.userProfile.id.uuidString.lowercased()
        let property = ["userId": userId]
        unleashClient.updateContext(context: property)
    }
}
