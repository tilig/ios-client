//
//  SignedInView.swift
//  Tilig
//
//  Created by Gertjan Jansen on 10/11/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct SignedInView: View {
    @EnvironmentObject var enableAutoFillViewModel: EnableAutoFillViewModel

    var body: some View {
        Group {
            if !enableAutoFillViewModel.onboardingCompleted {
                AutoFillExplanation()
            } else {
                ItemListView()
                    .consumingViewModel(.itemList)
                    .consumingViewModel(.getStarted)
                    .autoFillSheetPresenter(enableAutoFillViewModel: enableAutoFillViewModel)
            }
        }
        .behindBiometricLock()
        .onAppear {
            enableAutoFillViewModel.activate()
        }
    }
}

#if DEBUG
struct SignedInView_Previews: PreviewProvider {
    static var previews: some View {
        SignedInView()
    }
}
#endif
