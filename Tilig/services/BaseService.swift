//
//  BaseService.swift
//  Bolted
//
//  Created by Jacob Schatz on 1/22/20.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import Foundation
import Alamofire
import Combine
import SwiftUI
import os.log

enum ResponseError: Error {
    case AFError(AFError)
    case unauthorized
    case badRequest
    case missingResponseCode

    // Legacy
    case invalidResponseFormat
}

extension ResponseError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .AFError(let error):
            return "AFError: \(error.localizedDescription)."
        case .badRequest:
            return "Bad request."
        case .unauthorized:
            return "You are not signed in."
        case .missingResponseCode:
            return "Invalid response from server."

        // legacy
        case .invalidResponseFormat:
            return "Received an invalid response from the server."
        }
    }
}

enum EndpointController: String {
    case secrets
    case users
}

class BaseService {
    static let shared = BaseService()

    private let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String

    private(set) var accessToken: String?

    private var baseURL: URL

    /// Not private to enable mocking requests
    var session: Session

    init() {
        session = Session.default
        baseURL = URL(string: ConfigLoader.loadConfig().backendURL)!
    }

    func setToken(_ token: String?) {
        accessToken = token
    }

    func removeToken() {
        setToken(nil)
    }

    func buildURL(controller: String, endpoint: String) -> URL {
        return URL(string: "\(baseURL)/v1/\(controller)/\(endpoint)")!
    }

    /// Creates a publisher for a request. Accepts a Encodable parameters.
    func responsePublisher<D: Decodable, Parameters: Encodable>(
        url: URL,
        parameters: Parameters? = nil,
        method: HTTPMethod = .get,
        decodableType: D.Type = D.self
    ) -> AnyPublisher<D, Error> {
        let encoder: ParameterEncoder = method == .get ? URLEncodedFormParameterEncoder.default :
            JSONParameterEncoder.default

        return session.request(url,
                   method: method,
                   parameters: parameters,
                   encoder: encoder,
                   headers: headers)
          .publishData()
          .mapResponse()
          .decode(type: D.self, decoder: JSONDecoder())
          .eraseToAnyPublisher()
    }

    /// Convenience method that take an Encodable as parameters
    func responsePublisher<D: Decodable, EncodableParameters: Encodable>(
        _ endpoint: String,
        controller: EndpointController,
        parameters: EncodableParameters,
        method: HTTPMethod = .get,
        decodableType: D.Type = D.self
    ) -> AnyPublisher<D, Error> {

        return responsePublisher(
            url: buildURL(controller: controller.rawValue, endpoint: endpoint),
            parameters: parameters,
            method: method,
            decodableType: decodableType
        )
    }

    /// Creates a publisher for a request. Accepts non-encodable parameters.
    func responsePublisher<D: Decodable>(url: URL,
                                         parameters: Parameters = [:],
                                         method: HTTPMethod = .get,
                                         decodableType: D.Type = D.self) -> AnyPublisher<D, Error> {

        // For put requests, JSON encoder doesn't work properly
        // TODO: fix encoder: JSONParameterEncoder.default,
        assert(method != .put)

        return session.request(url, method: method, parameters: parameters, headers: headers)
                .publishData()
                .mapResponse()
                .decode(type: D.self, decoder: JSONDecoder())
                .eraseToAnyPublisher()
    }

    /// Convenience method for creating request publisher for given controller + endpoint
    func responsePublisher<D: Decodable>(_ endpoint: String,
                                         controller: EndpointController,
                                         parameters: Parameters = [:],
                                         method: HTTPMethod = .get,
                                         decodableType: D.Type = D.self) -> AnyPublisher<D, Error> {

        return responsePublisher(url: buildURL(controller: controller.rawValue, endpoint: endpoint),
                                 parameters: parameters,
                                 method: method,
                                 decodableType: decodableType)
    }

    // NOTE: Only used for requestClearTiligServerError, which is probably obsolete.
    func requestJSON(url: String,
                     parameters: Parameters = [:],
                     method: HTTPMethod = .get) -> AnyPublisher<[String: Any], Error> {
        Deferred {
            Future { promise in
                self.session.request(url, method: method, parameters: parameters)
                    .validate()
                    .responseJSON { response in
                        switch response.result {
                        case .failure:
                            guard let error = response.error else {
                                fatalError("Unknown Error type")
                            }
                            promise(.failure(error))
                        case .success:
                            let value = response.value as? [String: Any]
                            guard let jsonValue = value else {
                                promise(.failure(ResponseError.InvalidResponseFormat))
                                return
                            }
                            promise(.success(jsonValue))
                        }
                }
            }
        }.eraseToAnyPublisher()
    }
}

extension BaseService {

    var headers: HTTPHeaders {
        var headers: HTTPHeaders = [
            "x-tilig-version": "iOS \(String(describing: self.appVersion!))",
            "x-tilig-platform": "iOS"
        ]

        if let token = accessToken {
            headers["Authorization"] = "Bearer \(token)"
            headers["Content-Type"] = "application/json"
            headers["Accept"] = "application/json"
        }

        return headers
    }
}

extension Publisher where Self == DataResponsePublisher<Data> {

    /// Operator that publishes errors in the response as ResponseError cases
    public func mapResponse() -> Publishers.TryMap<DataResponsePublisher<Data>, Data> {
        return tryMap { response throws -> Data in

            if let error = response.error {
                logError(message: "Response AFError: \(error)", response: response)
                throw ResponseError.AFError(error)
            }

            let statusCode = response.response?.statusCode

            switch statusCode {
            case nil:
                logError(message: "Missing response code", response: response)
                throw ResponseError.missingResponseCode
            case 401:
                logError(message: "Bad Request (401)", response: response)
                throw ResponseError.unauthorized
            case 400:
                logError(message: "Bad Request (400)", response: response)
                throw ResponseError.badRequest
            default:
                // This means it propagates to the failure case below, I guess?
                break
            }

            switch response.result {
            case .success(let data):
                logSuccess(response: response)
                return data
            case .failure(let afError):
                logError(message: "afError: \(afError)", response: response)
                throw ResponseError.AFError(afError)
            }
        }
    }

    /// Convenience method for logging errors
    private func logError(message errorMessage: String, response: DataResponsePublisher<Data>.Output) {
        let url = String(describing: response.request?.url)
        let token = response.request?.headers["Authorization"] ?? ""

        OSLog.error("⚠️ Request \(url) =>  \(errorMessage)  [token: \(token)]")
    }

    /// Convenience method for logging successful responses
    private func logSuccess(response: DataResponsePublisher<Data>.Output) {
        #if DEBUG
        if case .success(let data) = response.result {
//            let url = String(describing: response.request?.url)
//            OSLog.info("Response for \(url) => \(String(data: data, encoding: .utf8)!)")
        }
        #endif
    }
}
