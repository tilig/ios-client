//
//  SharingService.swift
//  TiligTests
//
//  Created by Gertjan Jansen on 08/02/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import Foundation
import Sodium

class SharingService {
    init(client: Client, crypto: Crypto, currentUserId: UUID) {
        self.client = client
        self.currentUserId = currentUserId
        self.crypto = crypto
        self.sharingCrypto = SharingCrypto(crypto: crypto)
    }

    private var currentUserId: UUID
    private var client: Client
    private var crypto: Crypto
    private var sharingCrypto: SharingCrypto

    func share(item: Item, name: String?, withEmail email: String) async throws -> Folder {
        // Get the public key for this user
        let otherUser = try? await self.getPublicKey(email: email)

        // Is this a shared item already, or not?
        if let folder = item.folder {
            if let otherUser {
                let shareePublicKey = try crypto.decode(otherUser.publicKey)
                let shareeEncryptedPrivateKek = try sharingCrypto.encryptPrivateKekForNewUser(
                    inFolder: folder,
                    shareePublicKey: shareePublicKey
                )

                return try await self.inviteTiligUserToFolder(
                    id: otherUser.id,
                    shareeEncryptedPrivateKek: shareeEncryptedPrivateKek,
                    folder: folder,
                    name: name
                )
            }

            // If public key doesnt exist for other user, invite using their email
            return try await self.inviteNonTiligUserToFolder(
                email: email,
                to: folder,
                name: name
            )
        } else {
            if let otherUser {
                let shareePublicKey = try crypto.decode(otherUser.publicKey)
                return try await self.createFolder(
                    name: name,
                    item: item,
                    otherUserId: otherUser.id,
                    shareePublicKey: shareePublicKey
                )
            }

            // If public key doesnt exist for other user, create folder with just the owner's keys
            let folder = try await self.createFolder(
                name: name,
                item: item,
                otherUserId: nil,
                shareePublicKey: nil
            )

            // and then invite other user using their email
            return try await self.inviteNonTiligUserToFolder(
                email: email,
                to: folder,
                name: name
            )
        }
    }

    private func createFolder(name: String?, item: Item, otherUserId: UUID?, shareePublicKey: Box.PublicKey?) async throws -> Folder {
        guard let itemId = item.id else {
            throw SharingError.invalidItem
        }

        let upgradePayload = try sharingCrypto.upgradeToFolder(
            item: item,
            shareePublicKey: shareePublicKey
        )

        let base64EncodedPublicKey = try crypto.encode(upgradePayload.publicKek)

        var folderMemberships = [
            CreateFolderMembershipPayload(
                userId: currentUserId,
                encryptedPrivateKey: upgradePayload.ownEncryptedPrivateKek
            )
        ]

        if let otherUserId, let otherEncryptedPrivateKek = upgradePayload.otherEncryptedPrivateKek {
            folderMemberships.append(
                CreateFolderMembershipPayload(
                    userId: otherUserId,
                    encryptedPrivateKey: otherEncryptedPrivateKek
                )
            )
        }

        let payload = CreateFolderPayloadWrapper(folder: .init(
            name: name,
            folderMemberships: folderMemberships,
            items: [
                CreateFolderItemsPayload(
                    id: itemId,
                    encryptedFolderDek: upgradePayload.encryptedFolderDek
                )
            ],
            publicKey: base64EncodedPublicKey
        ))

        let response: FolderResponse
        do {
            response = try await client.createFolder(payload)
        } catch {
            // If it returns a 404, the folder is probably already upgraded
            if error.isNotFoundError {
                throw SharingError.cannotUpgrade
            }
            throw error
        }

        return response.folder
    }

    private func getPublicKey(email: String) async throws -> ShareePublicKey? {
        let otherUser: ShareePublicKey
        do {
            otherUser = try await client.getPublicKey(for: email)
        } catch {
            // Dont throw an error, to be able to share with non-tilig users
            if error.isNotFoundError {
                return nil
            }
            throw error
        }
        return otherUser
    }

    func inviteTiligUserToFolder(id: UUID, shareeEncryptedPrivateKek: String, folder: Folder, name: String?) async throws -> Folder {
        let payload = CreateFolderMembershipPayloadWrapper(folderMembership: .init(
            userId: id,
            encryptedPrivateKey: shareeEncryptedPrivateKek
        ), folder: FolderNamePayload(name: name ?? folder.name))

        let response = try await client.createFolderInvitation(payload, forFolderId: folder.id)

        return updateFolderInvitations(folder: folder, invitation: response.folderMembership)
    }

    func inviteNonTiligUserToFolder(email: String, to folder: Folder, name: String?) async throws -> Folder {
        let payload = CreateNonTiligFolderMembershipPayload(email: email)
        let wrapper = CreateNonTiligFolderMembershipPayloadWrapper(
            folderMembership: payload,
            folder: FolderNamePayload(name: name ?? folder.name)
        )
        let response = try await client.createNonTiligFolderInvitation(wrapper, forFolderId: folder.id)

        return updateFolderInvitations(folder: folder, invitation: response.folderMembership)
    }

    func updateFolderInvitations(folder: Folder, invitation: FolderMembership) -> Folder {
        var newFolder = folder
        if !newFolder.folderMemberships.map({ $0.user }).contains(invitation.user) &&
            !newFolder.folderInvitations.map({ $0.user }).contains(invitation.user) {
            newFolder.folderInvitations.append(invitation)
        }
        return newFolder
    }
}
