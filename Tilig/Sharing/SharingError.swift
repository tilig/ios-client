//
//  SharingError.swift
//  Tilig
//
//  Created by Gertjan Jansen on 08/02/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//
import Foundation

enum SharingError: Error {
    case invalidEmail
    case invalidUser
    case invalidItem
    case encryptedDekMissing
    case alreadyUpgradedToFolder
    case publicKeyNotFound
    case cannotUpgrade
    case cannotShareWithYourself
    case alreadySharedWithUser
    case cannotRevoke
}

extension SharingError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .publicKeyNotFound:
            // swiftlint disable:line_length
            return "Unfortunately, this person does not have a Tilig account yet. Currently, sharing is only possible with other Tilig users."
        case .cannotUpgrade:
            return "This folder is already shared."
        case .cannotShareWithYourself:
            return "You cannot share an item with yourself."
        case .alreadySharedWithUser:
            return "This item is already shared with the user"
        case .invalidEmail:
            return "Please enter a valid email address."
        case .alreadyUpgradedToFolder:
            return "Already upgraded to folder."
        case .cannotRevoke:
            return "Cannot revoke this user's access."
        default:
            return nil
        }
    }
}
