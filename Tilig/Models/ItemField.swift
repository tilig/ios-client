//
//  ItemField.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 07/02/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import Foundation

enum ItemField {
    case name
    case notes
    case username
    case password
    case website
    case ssid
    case ccnumber
    case ccholder
    case ccexp
    case cvv
    case pin
    case zipcode

    var keyPath: KeyPath<ItemWrapper, String?> {
        switch self {
        case .name:
            return \.name
        case .notes:
            return \.notes
        case .username:
            return \.username
        case .password:
            return \.password
        case .website:
            return \.urlString
        case .ssid:
            return \.ssid
        case .ccnumber:
            return \.ccnumber
        case .ccholder:
            return \.ccholder
        case .ccexp:
            return \.ccexp
        case .cvv:
            return \.cvv
        case .pin:
            return \.pin
        case .zipcode:
            return \.zipcode
        }
    }

    var copyEvent: TrackingEvent {
        switch self {
        case .name:
            return .fieldCopied
        case .notes:
            return .notesCopied
        case .username:
            return .usernameCopied
        case .password:
            return .passwordCopied
        case .website:
            return .websiteCopied
        case .ssid:
            return .wifiNetworkNameCopied
        case .ccnumber:
            return .creditCardNumberCopied
        case .ccholder:
            return .creditCardHolderCopied
        case .ccexp:
            return .creditCardExpirationDateCopied
        case .cvv:
            return .creditCardSecurityCodeCopied
        case .pin:
            return .creditCardPinCodeCopied
        case .zipcode:
            return .creditCardZipCodeCopied
        }
    }

    var label: String {
        switch self {
        case .name:
            return "name"
        case .notes:
            return "note"
        case .username:
            return "username"
        case .password:
            return "password"
        case .website:
            return "website"
        case .ssid:
            return "network name"
        case .ccnumber:
            return "card number"
        case .ccholder:
            return "cardholder name"
        case .ccexp:
            return "expiration date"
        case .cvv:
            return "secure code"
        case .pin:
            return "pin code"
        case .zipcode:
            return "zip"
        }
    }
}
