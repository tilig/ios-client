//
// ItemWrapper.swift
//  Tilig
//
//  Created by Gertjan Jansen on 05/10/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation

struct ItemWrapper: Equatable {
    /// Initialize with an existing item, and the decrypted overview and details.
    /// Note that the overview and details have been decrypted before passing it
    /// to this initializer.
    init(item: Item,
         overview: DecryptedOverview,
         details: DecryptedDetails) {
        self.encryptedItem = item
        self.overview = overview
        self.details = details
        self.brand = item.brand
    }

    /// Initialize a new item with the given template
    init(template: Template) {
        self.encryptedItem = Item(template: template)
        self.overview = DecryptedOverview()
        self.details = DecryptedDetails()
    }

    // TODO: make this non-optional
    var itemType: ItemType? { encryptedItem.itemType }

    /// The underlying encrypted item, that also contains some important unencrypted fields,
    /// like brand, timestamps, etc.
    private(set) var encryptedItem: Item
    /// The underlying decrypted overview object. Gets encrypted.
    private(set) var overview: DecryptedOverview
    /// The underlying decrypted details object. Gets encrypted.
    private(set) var details: DecryptedDetails

    var id: UUID? { encryptedItem.id }

    var brand: Brand?

    var name: String? {
        get { overview.name }
        set {
            overview.name = newValue
            if encryptedItem.legacyEncryption {
                encryptedItem.name = newValue
            }
        }
    }

    var url: URL? {
        get { overview.urls.first?.url ?? encryptedItem.website }
        set {
            // Set the URL on the encrypted instance
            // This needs to happen, even if legacy encryption is disabled.
            encryptedItem.website = newValue

            // Set the URL on the decrypted overview
            // We only want to overwrite the main URL
            var urls = overview.urls

            // When setting the URL to nil, clear the main URL (index 0) and don't set a new one
            if urls.count > 0 { urls.remove(at: 0) }
            if newValue != nil {
                urls.insert(OverviewURL(url: newValue), at: 0)
            }
            overview.urls = urls
        }
    }

    var urlString: String? { url?.withProtocol?.absoluteString }

    var username: String? {
        get { details.findInMain(kind: .username)?.value ?? overview.info }
        set {
            if itemType == .login {
                details.setInMain(kind: .username, value: newValue)
            }
            overview.info = newValue
        }
    }

    var info: String? {
        get { overview.info }
        set { overview.info = newValue }
    }

    var ssid: String? {
        get { details.findInMain(kind: .ssid)?.value }
        set { details.setInMain(kind: .ssid, value: newValue) }
    }

    var password: String? {
        get { details.findInMain(kind: .password)?.value }
        set { details.setInMain(kind: .password, value: newValue) }
    }

    var notes: String? {
        get { details.notes }
        set { details.notes = newValue }
    }

    var totp: String? {
        get { details.findInMain(kind: .totp)?.value }
        set { details.setInMain(kind: .totp, value: newValue) }
    }

    var ccnumber: String? {
        get { details.findInMain(kind: .ccnumber)?.value }
        set { details.setInMain(kind: .ccnumber, value: newValue) }
    }

    var ccholder: String? {
        get { details.findInMain(kind: .ccholder)?.value }
        set { details.setInMain(kind: .ccholder, value: newValue) }
    }

    var ccexp: String? {
        get { details.findInMain(kind: .ccexp)?.value }
        set { details.setInMain(kind: .ccexp, value: newValue) }
    }

    var cvv: String? {
        get { details.findInMain(kind: .cvv)?.value }
        set { details.setInMain(kind: .cvv, value: newValue) }
    }

    var pin: String? {
        get { details.findInMain(kind: .pin)?.value }
        set { details.setInMain(kind: .pin, value: newValue) }
    }

    var zipcode: String? {
        get { details.findInMain(kind: .zipcode)?.value }
        set { details.setInMain(kind: .zipcode, value: newValue) }
    }

    var folder: Folder? {
        get { encryptedItem.folder }
        set { encryptedItem.folder = newValue }
    }

    var androidAppIds: [String] { overview.androidAppIds }

    var updatedAt: String? { encryptedItem.updatedAt }
}

extension ItemWrapper {
    mutating func reencrypt(history: [LegacyPasswordVersion], crypto: Crypto) throws {
        if !history.isEmpty {
            try details.update(
                fromLegacyPasswordHistory: history,
                legacyKeyPair: crypto.legacyKeyPair
            )
        }
    }
}

extension Crypto {
    func encrypt(itemWrapper: ItemWrapper) throws -> Item {
        var item = try encrypt(
            item: itemWrapper.encryptedItem,
            overview: itemWrapper.overview,
            details: itemWrapper.details
        )
        switch itemWrapper.itemType {
        case .login:
            item.template = .loginV1
        case .note:
            item.template = .noteV1
        case .wifi:
            item.template = .wifiV1
        case .card:
            item.template = .cardV1
        case .custom:
            item.template = .customV1
        case .none:
            assertionFailure("No item type set!")
            item.template = .loginV1
        }
        return item
    }
}
