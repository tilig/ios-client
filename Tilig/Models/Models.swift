//
//  Models.swift
//  Tilig
//
//  Created by Jacob Schatz on 3/11/20.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

// Schema definition here: https://gitlab.com/subshq/clients/webapp/-/snippets/2418754#LC17

struct Item: Decodable, Encodable, Identifiable, Equatable {
    var id: UUID?

    // Legacy items will have null for this. New encryption has v1.
    var template: Template? = Template.loginV1
    // Legacy items will have version 1. New items should have version 2.
    var encryptionVersion = 2

    @available(*, deprecated, message: "Only available with legacy encryption")
    var name: String?
    /// The website field mirrors the overview's main URL
    @CodableOptionalURL var website: URL?
    /// The server figures out the domain based on the website field,
    /// taking into account the Public Suffix List.
    var domain: String?
    var androidAppIds: [String]? // Legacy -> in overview now
    var brand: Brand?
    var iconURL: URL? { brand?.iconURL }

    // An implementation of our terrible legacyEncryptionDisabled field,
    // that also takes the itemType into account, as an extra safeguard.
    var legacyEncryption: Bool {
        if itemType != .login { return false }
        return legacyEncryptionDisabled == nil || legacyEncryptionDisabled == false
    }
    var newEncryption: Bool { !legacyEncryption }

    // Legacy encryption model
    var legacyEncryptionDisabled: Bool? = true
    @available(*, deprecated, message: "Only available with legacy encryption")
    var username: String?
    @available(*, deprecated, message: "Only available with legacy encryption")
    var password: String?
    @available(*, deprecated, message: "Only available with legacy encryption")
    var notes: String?
    @available(*, deprecated, message: "Only available with legacy encryption")
    var otp: String?
    /// The DEK, encrypted with a legacy RSA keypair
    @available(*, deprecated, message: "This will be removed in a next iteration, use encrypedKey instead.")
    var rsaEncryptedDek: String?

    /// The DEK for a private item, encrypted with the user's KEK
    var encryptedDek: String?
    /// The DEK for a shared item (folder), encrypted with the KEK of the membership
    var encryptedFolderDek: String?
    /// The overview, encrypted with the DEK
    var encryptedOverview: String?
    /// The details, encrypted with the DEK
    var encryptedDetails: String?

    // Timestamps
    var createdAt: String?
    var updatedAt: String?

    // Folder model for sharing
    var folder: Folder?

    var itemType: ItemType? {
        switch template {
        case .noteV1:
            return .note
        case .loginV1:
            return .login
        case .wifiV1:
            return .wifi
        case .cardV1:
            return .card
        case .customV1:
            return .custom
        default:
            assertionFailure("itemType should be non-nil")
            return nil
        }
    }
}

/// Only list templates that the web app actually supports
enum Template: String, Codable, Equatable {
    case loginV1 = "login/1"
    case noteV1 = "securenote/1"
    case wifiV1 = "wifi/1"
    case cardV1 = "creditcard/1"
    case customV1 = "custom/1"
    case unknown = "unknown"
}

extension Template {
    public init(from decoder: Decoder) throws {
        self = try Template(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .unknown
    }
}

/// An ItemType does not have a version, while a Template does
enum ItemType: Identifiable, CaseIterable {
    var id: Self { self }

    case login
    case note
    case wifi
    case card
    case custom

    var label: String {
        switch self {
        case .login:
            return "Login"
        case .note:
            return "Note"
        case .wifi:
            return "WiFi network"
        case .card:
            return "Credit card"
        case .custom:
            return "Custom"
        }
    }

    var shortLabel: String {
        switch self {
        case .login:
            return "Login"
        case .note:
            return "Note"
        case .wifi:
            return "WiFi"
        case .card:
            return "Card"
        case .custom:
            return "Custom"
        }
    }
}

struct Folder: Codable, Equatable, Identifiable {
    var id: UUID
    var myEncryptedPrivateKey: String
    var folderMemberships: [FolderMembership]
    var folderInvitations: [FolderMembership]
    var name: String?
    var publicKey: String?

    // TODO: decode these properly!
    var createdAt: String?
    var updatedAt: String?
}

struct FolderMembership: Codable, Equatable, Identifiable {
    var id: UUID
    var role: String
    var pending: Bool
    var accepted: Bool
    var isMe: Bool?
    var user: FolderMembershipUser
    var acceptedAt: String?
    var updatedAt: String?
    var createdAt: String?
}

struct FolderMembershipUser: Codable, Equatable, Identifiable {
    var id: UUID?
    var email: String
    var displayName: String?
    var picture: URL?
    var publicKey: String?
    var organization: Organization?
}

struct Organization: Codable, Equatable, Identifiable {
    var id: UUID
    var name: String
}

struct ShareePublicKey: Codable, Equatable {
    // userId?
    var id: UUID
    // base64 urlsafe encoded bytes
    var publicKey: String
    var email: String
}

/**
 * Overview payload for v2 Item
 *
 * The overview contains less sensitive information, which can
 * be decrypted as soon as it is received from the API.
 *
 * Any data that is needed for matching, sorting, displaying, etc.,
 * should be present in the overview.
 *
 **/
struct DecryptedOverview: Codable, Equatable {
    @NullCodable var name: String?
    /// A secondary name/label. E.g. a username for logins.
    @NullCodable var info: String?
    var urls: [OverviewURL] = []
    var androidAppIds: [String] = []
}

struct OverviewURL: Codable, Equatable {
    @NullCodable var name: String?
    @CodableOptionalURL var url: URL?
}

/// Convenience initializer to init with a default URL, skipping the CodableOptionalURL
extension OverviewURL {
    init(name: String?, url: URL?) {
        self.name = name
        self.url = url
    }

    init(url: URL?) {
        self.url = url
    }
}

enum FieldKind: String, Codable {
    case username
    case password
    case totp
    case ssid
    case ccnumber
    case ccholder
    case ccexp
    case cvv
    case pin
    case zipcode
}

struct Field: Codable, Equatable {
    var kind: FieldKind
    @NullCodable var value: String?
}

typealias CustomFieldKind = String

struct CustomField: Codable, Equatable {
    var name: String
    var kind: CustomFieldKind
    @NullCodable var value: String?
}

struct LegacyFields: Codable, Equatable {
    var username: String?
    var password: String?
    var notes: String?
    var otp: String?
    var passwordHistory: [LegacyPasswordVersion] = []
}

extension LegacyFields {
    init(overview: DecryptedOverview, details: DecryptedDetails) {
        username = details.findInMain(kind: .username)?.value
        password = details.findInMain(kind: .password)?.value
        otp = details.findInMain(kind: .totp)?.value
        notes = details.notes
    }
}

/**
 * Details payload for v2 Item
 *
 * The details contain the 'meat' of the Secret. This is more
 * sensitive data that sould only be decrypted Just-In-Time (e.g.
 * when (auto)filling or displaying a secret.)
 */
struct DecryptedDetails: Codable, Equatable {
    @NullCodable var notes: String?
    var main: [Field] = []
    var history: [HistoricVersion] = []
    /// These aren't used yet
    var customFields: [CustomField] = []
}

struct Brand: Codable, Hashable {
    var name: String?
    var domain: String
    var totp: Bool
    var mainColorHex: String?
    var images: BrandImageDetails?
    var isFetched: Bool?

    var mainColor: Color? {
        guard let mainColorHex = mainColorHex else {
            return nil
        }
        return Color(hex: mainColorHex)
    }
}

struct BrandImageDetails: Codable, Hashable {
    var icon: BrandImageIcon?
}

struct BrandImageIcon: Codable, Hashable {
    var svg: BrandImageExtension?
    var png: BrandImageExtension?
}

struct BrandImageExtension: Codable, Hashable {
    var original: String?
}

extension Brand {
    var iconURL: URL? {
        // Check if brandfetch returned an icon
        if let pngLogo = images?.icon?.png?.original, let url = URL(string: pngLogo) {
            return url
        }

        if let svgLogo = images?.icon?.svg?.original, let url = URL(string: svgLogo) {
            return url
        }

        // We could fallback to our legacy icon service.
        // However, that leads to inconsistent behaviour.
        // let host = try? DomainParser().parse(host: domain)?.domain
        // return URL(string: "https://icon.tilig.com/\(host ?? "missing.png")")!
        return nil
    }
}

/// An historic value for a specific field, like password history
struct HistoricVersion: Codable, Equatable {
    var kind: FieldKind
    var value: String
    var replacedAt: Date
}

extension HistoricVersion {
    enum CodingKeys: String, CodingKey {
        case kind, value, replacedAt
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        value = try container.decode(String.self, forKey: .value)
        kind = try container.decode(FieldKind.self, forKey: .kind)

        let dateString = try container.decode(String.self, forKey: .replacedAt)
        // For 1970-01-01T00:00:00Z format
        let formatter = ISO8601DateFormatter()
        // For 1970-01-01T00:00:00.000Z format (including ms)
        let fractionalFormatter = ISO8601DateFormatter()
        formatter.formatOptions = [
            .withInternetDateTime,
            .withDashSeparatorInDate,
            .withColonSeparatorInTime,
            // this one fixes parsing milliseconds:
            .withFractionalSeconds
        ]
        if let date = formatter.date(from: dateString) {
            replacedAt = date
        } else if let date = fractionalFormatter.date(from: dateString) {
            replacedAt = date
        } else {
            throw DecodingError.dataCorruptedError(
                forKey: .replacedAt,
                in: container,
                debugDescription: "Date string does not match format expected by formatter."
            )
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(kind, forKey: .kind)
        try container.encode(value, forKey: .value)

        let formatter = ISO8601DateFormatter()
        let isoFormat = formatter.string(from: replacedAt)
        try container.encode(isoFormat, forKey: .replacedAt)
    }
}

/// Password version for the legacy encryption model
struct LegacyPasswordVersion: Codable, Identifiable, Equatable {
    var id: String?
    var password: String
    var createdAt: String
}

extension LegacyPasswordVersion {
    struct DateDecodingError: Error { }

    func getFormattedDate() -> String {
        let dateFormatter = ISO8601DateFormatter()
        dateFormatter.formatOptions = [
            .withInternetDateTime,
            .withFractionalSeconds
        ]
        guard let date = dateFormatter.date(from: createdAt) else { return "" }
        let newFormat = DateFormatter()
        newFormat.dateStyle = .medium
        newFormat.timeStyle = .short
        return newFormat.string(from: date)
    }

    func getDate() throws -> Date {
        let dateFormatter = ISO8601DateFormatter()
        dateFormatter.formatOptions = [
            .withInternetDateTime,
            .withFractionalSeconds
        ]
        guard let date = dateFormatter.date(from: createdAt) else {
            throw DateDecodingError()
        }
        return date
    }
}
