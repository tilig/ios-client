//
//  MockedModels.swift
//  Tilig
//
//  Created by Gertjan Jansen on 04/11/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

#if DEBUG
import Foundation
import Firebase
import DomainParser
import Sodium

class MockedUser: FirebaseUser {
    var uid: String { "mocked-uid" }
    var email: String?
    var displayName: String?
    var isAnonymous: Bool { false }
    var isEmailVerified: Bool { true }
    var refreshToken: String? { "firebase-refresh" }
    var providerData: [UserInfo] = []
    var multiFactor: MultiFactor = MultiFactor()

    init(email: String, displayName: String) {
        self.email = email
        self.displayName = displayName
    }
}

extension String {
    static func randomEmail() -> String {
        ["test@tilig.com",
         "boss@tilig.com",
         "franc@barleduc.com",
         "swinkle@swinkledinkle.io"].randomElement()!
    }
}

extension CodeableFirebaseUser {
    static func random() -> CodeableFirebaseUser {
        CodeableFirebaseUser(
            uid: UUID().uuidString,
            email: "gertjan@email.com",
            displayName: "Guerderino",
            isAnonymous: false,
            isEmailVerified: true,
            refreshToken: "random-refresh-token",
            providerData: [UserInfo](),
            multiFactor: MultiFactor()
        )
    }
}

extension TiligAuthTokens {
    static func random() -> TiligAuthTokens {
        TiligAuthTokens(
            accessToken: Token.random(),
            refreshToken: Token.random()
        )
    }
}

extension Token {
    static func random() -> Token {
        // Expires in 2034
        // swiftlint:disable line_length
        Token(rawValue: "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI2M2QzN2Y4YS03MTYxLTRiZTktODBkYy04NzFhZWNiOWE3YTUiLCJ1aWQiOiI3MGM5NjNhOC0yMDc1LTQ5MDQtODY5ZC0xMTI4NGFjOGJlZTQiLCJleHAiOjIwMjM2NzQ5MjEsImlzcyI6Imh0dHBzOi8vYXBwLnRpbGlnLmNvbSIsImlhdCI6MTYyMzY3NDMyMX0.uy1A7FQ3gtS_v23OFf6f4K5Fq9CJWygsywucAjfIm4w")
    }
}

struct MockedTokenProvider: TokenProvider {
    var failing: Bool = false

    func getAccessToken() async throws -> Token {
        if failing {
            throw AuthError.unauthorized
        } else {
            return Token.random()
        }
    }
}

extension TokenProvider where Self == MockedTokenProvider {
    static func mocked(failing: Bool = false) -> MockedTokenProvider {
        MockedTokenProvider(failing: failing)
    }
}

extension LegacyKeyPair {
    static func random() -> LegacyKeyPair {
        // swiftlint:disable force_try
        try! LegacyKeyPair.generateRSAKeyPair()
    }
}

extension Crypto {
    static func random() -> Crypto {
        Crypto(keyPair: .random(), legacyKeyPair: .random())
    }
}

extension LegacyPublicKey {
    static func random() -> LegacyPublicKey {
        LegacyKeyPair.random().publicKey
    }
}

extension UserProfile {
    static func random() -> UserProfile {
        return UserProfile(id: UUID(),
                           email: "gurt@tilig.com",
                           displayName: "Gurt Johansson",
                           picture: nil,
                           locale: "en",
                           publicKey: LegacyPublicKey.random())
    }
}

extension FirebaseUserProfile {
    static func random() -> FirebaseUserProfile {
        FirebaseUserProfile(
            isNewUser: false,
            providerID: "google",
            username: "testhenk_2000",
            profile: ["given_name": NSString("Test"), "family_name": NSString("Henk")]
        )
    }
}

extension Box.KeyPair {
    static func random() -> Self {
        guard let keyPair = Sodium().box.keyPair() else {
            fatalError("Sodium failed to generate a keyPair.")
        }
        return keyPair
    }
}

extension AuthState {
    static func random() -> AuthState {
        let keyPair = Box.KeyPair.random()
        let legacyKeyPair = LegacyKeyPair.random()
        var userProfile = UserProfile.random()
        userProfile.publicKey = legacyKeyPair.publicKey
        return AuthState(
            firebaseUser: CodeableFirebaseUser.random(),
            tiligAuthTokens: TiligAuthTokens.random(),
            keyPair: keyPair,
            legacyKeyPair: legacyKeyPair,
            userProfile: userProfile
        )
    }
}

extension Item {
    static func random(encryptedWithKeyPair keyPair: LegacyKeyPair = .random(),
                       // Note: using legacy encryption
                       legacyEncryptionDisabled: Bool = false,
                       name: String = "Netflix",
                       plainUsername: String? = "franky@bingewatcher.com",
                       plainPassword: String? = PasswordGenerator.randomPassword(),
                       url: URL? = URL(string: "netflix.com"),
                       domain: String? = nil,
                       otp: String? = nil,
                       notes: String? = "Please enter a note",
                       brand: Brand? = .random(),
                       template: Template = .loginV1) -> Item {
        // Make it possible to overwrite domain, but use the websites's domain by default
        var domain = domain
        if let url, domain == nil {
            domain = getDomain(forWebsite: url)
        }

        var encryptedUsername: String?
        var encryptedPassword: String?
        var encryptedNotes: String?
        var encryptedOtp: String?
        do {
            if let plainUsername {
                encryptedUsername = try keyPair.encrypt(plainText: plainUsername)
            }
            if let plainPassword {
                encryptedPassword = try keyPair.encrypt(plainText: plainPassword)
            }
            if let notes {
                encryptedNotes = try keyPair.encrypt(plainText: notes)
            }
            if let otp {
                encryptedOtp = try keyPair.encrypt(plainText: otp)
            }
        } catch {
            assertionFailure("Failed to encrypt random item \(error)")
        }

        return Item(
            id: UUID(),
            template: template,
            name: name,
            // ARGH, I hate this CodableOptionalURL thing
            website: CodableOptionalURL(string: url?.absoluteString ?? ""),
            domain: domain,
            brand: brand,
            legacyEncryptionDisabled: legacyEncryptionDisabled,
            username: encryptedUsername,
            password: encryptedPassword,
            notes: encryptedNotes,
            otp: encryptedOtp
        )
    }

    // For testing purposes only, because the backend determines the domain
    // in actual use cases.
    static func getDomain(forWebsite website: URL) -> String? {
        guard let host = website.host else {
            return nil
        }
        let domain = try? DomainParser().parse(host: host)?.domain
        assert(domain != nil)
        return domain
    }
}

extension LegacyKeyPair {
    /// Convenience method of generating random passwords that doesn't throw errors
    func randomEncrypedPassword() -> String {
        // We're pretty sure this always works
        guard let password = try? encrypt(plainText: PasswordGenerator.randomPassword()) else {
            assertionFailure("randomEncrypedPassword() failed unexpectedly")
            return ""
        }
        return password
    }
}

extension Brand {
    static func random(name: String? = nil,
                       domain: String? = nil,
                       mainColorHex: String? = nil) -> Brand {
        Brand(
            name: name ?? "Random brand",
            domain: domain ?? "randombrand.com",
            totp: true,
            mainColorHex: mainColorHex ?? "#000000"
        )
    }
}

extension ItemWrapper {
    // Creates a random login item.
    // Note: not passing in a keypair yet, so could case encryption issues!
    static func random(encryptedWithKeyPair keyPair: LegacyKeyPair = .random(),
                       name: String = "Random Item",
                       url: URL? = nil,
                       domain: String? = nil,
                       username: String? = nil,
                       password: String? = nil,
                       notes: String? = nil,
                       totp: String? = nil,
                       brand: Brand? = .random(),
                       ccnumber: String? = nil,
                       ccholder: String? = nil,
                       ccexp: String? = nil,
                       cvv: String? = nil,
                       pin: String? = nil,
                       zipcode: String? = nil) -> ItemWrapper {
        var details = DecryptedDetails(notes: notes)
        details.setInMain(kind: .password, value: password)
        details.setInMain(kind: .username, value: username)
        details.setInMain(kind: .totp, value: totp)
        details.setInMain(kind: .ccnumber, value: ccnumber)
        details.setInMain(kind: .ccholder, value: ccholder)
        details.setInMain(kind: .ccexp, value: ccexp)
        details.setInMain(kind: .cvv, value: cvv)
        details.setInMain(kind: .pin, value: pin)
        details.setInMain(kind: .zipcode, value: zipcode)

        var overview = DecryptedOverview(
            name: name,
            info: username
        )

        if let url {
            overview.urls = [OverviewURL(url: url)]
        }

        return ItemWrapper(
            item: Item.random(
                encryptedWithKeyPair: keyPair,
                name: name,
                plainUsername: username,
                plainPassword: password,
                url: url,
                domain: domain,
                otp: totp,
                notes: notes,
                brand: brand
            ),
            overview: overview,
            details: details
        )
    }
}

extension ItemWrapper {
    // Note: not passing in a keypair yet, so could case encryption issues!
    static func randomNote(encryptedWithKeyPair keyPair: LegacyKeyPair = .random(),
                           name: String = "Random Note",
                           notes: String? = nil) -> ItemWrapper {
        return ItemWrapper(
            item: Item.random(
                encryptedWithKeyPair: keyPair,
                name: name,
                notes: notes,
                template: .noteV1
            ),
            overview: DecryptedOverview(name: name),
            details: DecryptedDetails(notes: notes)
        )
    }
}

extension ItemWrapper {
    // Note: not passing in a keypair yet, so could cause encryption issues!
    static func randomWifiItem(encryptedWithKeyPair keyPair: LegacyKeyPair = .random(),
                               name: String = "My Hotspot",
                               ssid: String = "Home Hotspot",
                               password: String = PasswordGenerator.randomPassword(),
                               notes: String? = nil) -> ItemWrapper {

        var details = DecryptedDetails(notes: notes)
        details.setInMain(kind: .password, value: password)
        details.setInMain(kind: .ssid, value: ssid)

        let overview = DecryptedOverview(name: name)

        return ItemWrapper(
            item: Item.random(
                encryptedWithKeyPair: keyPair,
                name: name,
                notes: notes,
                template: .wifiV1
            ),
            overview: overview,
            details: details
        )
    }
}

#endif
