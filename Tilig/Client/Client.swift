//
//  Client.swift
//  Tilig
//
//  Created by Gertjan Jansen on 17/01/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import Foundation

struct ItemPayload: Codable {
    var item: Item
}

struct ItemsPayload: Codable {
    var items: [Item]
}

struct PollingPayload: Codable {
    var updates: [Item]
    var deletions: [UUID?]
}

struct ShareePublicKeyPayload: Codable {
    var user: ShareePublicKey
}

struct CreateFolderMembershipPayload: Codable {
    var userId: UUID
    var encryptedPrivateKey: String
}

struct CreateNonTiligFolderMembershipPayload: Codable {
    var email: String
}

struct CreateNonTiligFolderMembershipPayloadWrapper: Codable {
    var folderMembership: CreateNonTiligFolderMembershipPayload
    var folder: FolderNamePayload
}

struct CreateFolderMembershipPayloadWrapper: Codable {
    var folderMembership: CreateFolderMembershipPayload
    var folder: FolderNamePayload
}

struct FolderNamePayload: Codable {
    var name: String?
}

struct CreateFolderItemsPayload: Codable {
    var id: UUID
    var encryptedFolderDek: String
}

struct CreateFolderPayload: Codable {
    var name: String?
    var folderMemberships: [CreateFolderMembershipPayload]
    var items: [CreateFolderItemsPayload]
    var publicKey: String
}

struct CreateFolderPayloadWrapper: Codable {
    var folder: CreateFolderPayload
}

struct FolderResponse: Codable {
    var folder: Folder
}

struct FolderMembershipResponse: Codable {
    var folderMembership: FolderMembership
}

struct ContactsPayload: Codable {
    var contacts: [FolderMembershipUser]
}

struct UserSettingsPayload: Codable {
    var userSettings: UserSettings
}

enum EditLoginItemError: Error {
    case idRequired
}

struct Client {
    var urlSession = URLSession.current
    var tokenProvider: TokenProvider

    init(tokenProvider: TokenProvider) {
        self.tokenProvider = tokenProvider
    }

    func getItem(id: UUID) async throws -> Item {
        let wrapped: ItemPayload = try await get(.item(id))
        return wrapped.item
    }

    func getItems() async throws -> [Item] {
        let wrapped: ItemsPayload = try await get(.items)
        return wrapped.items.filter { $0.template != .customV1 && $0.template != .unknown }
    }

    func getChanges(changedSince: Date) async throws -> PollingPayload {
        return try await get(.polling(changedSince))
    }

    func saveItem(_ item: Item) async throws -> Item {
        let wrapped: ItemPayload = try await post(ItemPayload(item: item), endpoint: .items)
        return wrapped.item
    }

    func updateItem(_ item: Item) async throws -> Item {
        guard let id = item.id else {
            throw EditLoginItemError.idRequired
        }
        let wrapped: ItemPayload = try await put(ItemPayload(item: item), endpoint: .item(id))
        return wrapped.item
    }

    func deleteItem(_ item: Item) async throws -> Item {
        guard let id = item.id else {
            throw DeleteItemError.cannotDeleteNonPersistedItem
        }
        let wrapped: ItemPayload = try await delete(endpoint: .item(id))
        return wrapped.item
    }

    func getLegacyPasswordHistory(id: UUID) async throws -> [LegacyPasswordVersion] {
        try await get(.passwordHistory(id.uuidString))
    }

    func getBrand(for url: URL) async throws -> Brand {
        try await get(.getBrand(url))
    }

    func getContacts() async throws -> [FolderMembershipUser] {
        let payload: ContactsPayload = try await get(.getContacts)
        return payload.contacts
    }

    func getPublicKey(for email: String) async throws -> ShareePublicKey {
        let payload: ShareePublicKeyPayload = try await get(.getPublicKey(email))
        return payload.user
    }

    func brandSearch(prefix: String) async throws -> [Brand] {
        try await get(.brandSearch(prefix))
    }

    func createFolder(_ payload: CreateFolderPayloadWrapper) async throws -> FolderResponse {
        try await post(payload, endpoint: .folders)
    }

    func revokeFolderMembership(forFolderId folderId: String,
                                memberId: String) async throws -> FolderMembershipResponse {
        try await delete(endpoint: .revokeFolderMembership(folderId: folderId, memberId: memberId))
    }

    func createFolderInvitation(_ payload: CreateFolderMembershipPayloadWrapper,
                                forFolderId folderId: UUID) async throws -> FolderMembershipResponse {
        try await post(payload, endpoint: .folderMemberships(folderId))
    }

    func createNonTiligFolderInvitation(_
                                        payload: CreateNonTiligFolderMembershipPayloadWrapper,
                                        forFolderId folderId: UUID) async throws -> FolderMembershipResponse {
        try await post(payload, endpoint: .folderMemberships(folderId))
    }

    func getUserProfile() async throws -> UserProfile {
        return try await get(.profile)
    }

    func updateUserSettings(payload: UserSettingsPayload) async throws -> UserSettings {
        try await put(payload, endpoint: .userSettings)
    }
}

/// Request helpers
extension Client {
    private func get<D: Decodable>(_ endpoint: Endpoint) async throws -> D {
        try await urlSession.request(
            for: endpoint,
            using: tokenProvider.getAccessToken()
        )
    }

    private func post<D: Decodable, E: Encodable>(_ item: E, endpoint: Endpoint) async throws -> D {
        try await urlSession.request(
            for: endpoint,
            method: .post,
            body: item,
            using: tokenProvider.getAccessToken()
        )
    }

    private func put<D: Decodable, E: Encodable>(_ item: E, endpoint: Endpoint) async throws -> D {
        try await urlSession.request(
            for: endpoint,
            method: .put,
            body: item,
            using: tokenProvider.getAccessToken()
        )
    }

    private func delete<D: Decodable>(endpoint: Endpoint) async throws -> D {
        try await urlSession.request(
            for: endpoint,
            method: .delete,
            using: tokenProvider.getAccessToken()
        )
    }

}
