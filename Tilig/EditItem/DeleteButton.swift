//
//  DeleteButton.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 20/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

struct DeleteButton: View {
    @EnvironmentObject var viewModel: DeleteItemViewModel
    @EnvironmentObject var routeModel: RouteModel

    var body: some View {
        DeleteButtonRenderingView(
            title: "Delete \(viewModel.item.itemType?.label ?? "")",
            isDeleting: viewModel.isDeleting,
            onDelete: { Task { await viewModel.delete() } }
        )
        .onReceive(viewModel.itemDeleted) { _ in
            routeModel.pushRoute(.itemList, forceOverwrite: true)
        }
    }
}

private struct DeleteButtonRenderingView: View {
    var title: String
    var isDeleting: Bool
    var onDelete: () -> Void = { }

    var body: some View {
        Button {
            let alertItem = AlertItem(
                id: UUID(),
                title: Text(title).defaultBoldFont(color: .mediumPurple),
                message: Text("Are you sure to delete this item? This cannot be undone"),
                primaryButton: .destructive(Text("Delete"), action: onDelete),
                secondaryButton: .cancel()
            )
            alertItem.post()
        } label: {
            HStack {
                if isDeleting {
                    ActivityIndicator()
                } else {
                    Text(title)
                        .defaultButtonFont(color: .mediumPurple, size: 16)
                }
            }
            .padding()
            .frame(alignment: .center)
        }
        .frame(maxWidth: .infinity, alignment: .center)
    }

}
