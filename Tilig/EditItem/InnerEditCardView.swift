//
//  InnerEditCardView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 31/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct InnerEditCardView: View {
    @EnvironmentObject var viewModel: EditItemViewModel
    @Binding var scrollOffset: CGFloat
    @State var isPasswordHidden = true

    var body: some View {
        ScrollViewReader { proxy in
            LazyVStack(alignment: .leading, spacing: 0) {
                InputGroup {
                    Text("Card title")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 4)

                    FormTextField(
                        "E.g. Work Credit Card",
                        value: $viewModel.name,
                        editMode: true
                    )
                    .padding(.bottom)
                }

                InputGroup {
                    Text("Card number")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 4)

                    FormTextField(
                        "0000 0000 0000 0000",
                        value: $viewModel.ccnumber,
                        editMode: true,
                        keyboardType: .numberPad
                    )
                    .padding(.bottom)
                }

                InputGroup {
                    Text("Cardholder name")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 8)

                    FormTextField(
                        "E.g. Gertjan Jansen",
                        value: $viewModel.ccholder,
                        editMode: true
                    )
                    .padding(.bottom)
                }

                InputGroup {
                    Text("Expiration date")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 8)

                    ExpirationDateField(
                        "MM/YY",
                        value: $viewModel.ccexp
                    )
                    .padding(.bottom)
                }

                InputGroup {
                    Text("Secure code")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 8)

                    PasswordField(
                        placeholder: "035",
                        password: $viewModel.cvv,
                        isHidden: true,
                        showGenerateButton: false,
                        backgroundColor: .white,
                        keyboardType: .numberPad,
                        characterLimit: 4
                    )
                    .padding(.bottom)
                }

                InputGroup {
                    Text("Pin code")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 8)

                    PasswordField(
                        placeholder: "1242",
                        password: $viewModel.pin,
                        isHidden: true,
                        showGenerateButton: false,
                        backgroundColor: .white,
                        keyboardType: .numberPad,
                        characterLimit: 4
                    )
                    .padding(.bottom)
                }

                InputGroup {
                    Text("ZIP")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 8)

                    FormTextField(
                        "Enter a ZIP or postal code",
                        value: $viewModel.zipcode,
                        editMode: true
                    )
                    .padding(.bottom)
                }

                InputGroup {
                    Text("Notes")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 8)

                    NotesField(
                        label: "Anything else you'd like to add.",
                        value: $viewModel.notes,
                        onFocusChange: { hasFocus in
                            if hasFocus {
                                withAnimation {
                                    proxy.scrollTo(4)
                                }
                            }
                        }
                    )
                    .id(4)
                    .padding(.bottom)
                }

                Spacer(minLength: 40)

                DeleteButton()
                    .consumingViewModel(.deleteItem)
            }
            .padding(.top, 200)
            .padding(.horizontal)
        }
        .modifier(OffsetModifier(offset: $scrollOffset))
    }
}
