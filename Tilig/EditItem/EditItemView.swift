//
//  EditItemView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 05/01/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//
import SwiftUI

struct EditItemView: View {
    @EnvironmentObject var viewModel: EditItemViewModel
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @State var scrollOffset: CGFloat = 0

    func backAction() {
        if viewModel.hasChanges {
            let alertItem = AlertItem(
                id: UUID(),
                title: Text("Unsaved changes").defaultBoldFont(color: .mediumPurple),
                message: Text("Are you sure you want to discard the changes you've made?"),
                primaryButton: .destructive(
                    Text("Discard"), action: {
                        viewModel.undoChanges()
                        stopPresenting()
                    }
                ),
                secondaryButton: .cancel(Text("Go back"))
            )
            alertItem.post()
        } else {
            self.stopPresenting()
        }
    }

    var body: some View {
        ZStack(alignment: .top) {
            CollapsibleToolbar(
                scrollOffset: scrollOffset,
                title: viewModel.name ?? "Untitled",
                brand: viewModel.brand,
                maxBackgroundHeight: 180,
                backgroundZIndex: 3,
                itemType: viewModel.itemWrapper.itemType ?? .login,
                titleZIndex: 6
            ) {
                HStack {
                    BackButton(hasPadding: true, action: backAction)

                    Spacer()

                    Button(action: {
                        Task { await viewModel.update(trigger: .saveButton) }
                    }) {
                        Text("Save")
                            .defaultButtonFont(color: .white, size: 16)
                            .padding(.trailing)
                    }
                }
            }
            .zIndex(1)

            ScrollView(showsIndicators: false) {
                Group {
                    switch viewModel.itemWrapper.itemType {
                    case .login:
                        InnerEditItemView(scrollOfset: $scrollOffset)
                            .consumingViewModel(.twoFactor)
                    case .note:
                        InnerEditNoteView(scrollOfset: $scrollOffset)
                    case .wifi:
                        InnerEditWifiView(scrollOffset: $scrollOffset)
                    case .card:
                        InnerEditCardView(scrollOffset: $scrollOffset)
                    case .none, .custom:
                        EmptyView()
                    }
                }
                .frame(width: getScreenBounds().width)
            }
            .zIndex(2)
        }
        .background(Color.backgroundGray.edgesIgnoringSafeArea(.bottom))
        .swipeRight(action: backAction)
        .ignoresSafeArea(.container, edges: .top)
        .navigationBarHidden(true)
        .onReceive(viewModel.itemUpdated, perform: { (_, trigger) in
            // A 2FA change is triggered by a different presented subview. Therefore
            // we don't want to stop presenting this sheet after an update that is triggered
            // by a 2FA change.
            if trigger != .twoFactorChange {
                stopPresenting()
            }
        })
    }

    /// Stop presenting this view, go back to details view
    func stopPresenting() {
        self.mode.wrappedValue.dismiss()
    }
}
