//
//  InnerEditNoteView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 31/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct InnerEditNoteView: View {
    @EnvironmentObject var viewModel: EditItemViewModel
    @Binding var scrollOfset: CGFloat
    @State var isPasswordHidden = true

    var body: some View {
        ScrollViewReader { (_: ScrollViewProxy) in
            LazyVStack(alignment: .leading, spacing: 0) {
                InputGroup {
                    Text("Note name")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 4)

                    FormTextField(
                        "Enter a name",
                        value: $viewModel.name,
                        editMode: true
                    )
                    .padding(.bottom)
                }

                InputGroup {
                    Text("Note content")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 8)

                    NotesField(
                        label: "",
                        value: $viewModel.notes
                    )
                    .padding(.bottom)
                }

                Spacer(minLength: 40)

                DeleteButton()
                    .consumingViewModel(.deleteItem)
            }
            .padding(.top, 200)
            .padding(.horizontal)
        }
        .modifier(OffsetModifier(offset: $scrollOfset))
    }
}
