//
//  InnerEditWifiView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 31/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct InnerEditWifiView: View {
    @EnvironmentObject var viewModel: EditItemViewModel
    @Binding var scrollOffset: CGFloat
    @State var isPasswordHidden = true

    var body: some View {
        ScrollViewReader { (_: ScrollViewProxy) in
            LazyVStack(alignment: .leading, spacing: 0) {
                InputGroup {
                    Text("Name")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 4)

                    FormTextField(
                        "E.g. \"WiFi at Mom & Dad\"",
                        value: $viewModel.name,
                        editMode: true
                    )
                    .padding(.bottom)
                }

                InputGroup {
                    Text("Network name")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 4)

                    FormTextField(
                        "E.g. \"Router3868\"",
                        value: $viewModel.ssid,
                        editMode: true
                    )
                    .padding(.bottom)
                }

                InputGroup {
                    Text("Network password")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 8)

                    PasswordField(
                        password: $viewModel.password,
                        isHidden: true,
                        showGenerateButton: false,
                        backgroundColor: .white
                    )
                    .padding(.bottom)
                }

                InputGroup {
                    Text("Notes")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 8)

                    NotesField(
                        label: "Anything else you'd like to add.",
                        value: $viewModel.notes
                    )
                    .padding(.bottom)
                }

                Spacer(minLength: 40)

                DeleteButton()
                    .consumingViewModel(.deleteItem)
            }
            .padding(.top, 200)
            .padding(.horizontal)
        }
        .modifier(OffsetModifier(offset: $scrollOffset))
    }
}
