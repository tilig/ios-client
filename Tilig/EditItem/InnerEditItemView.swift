//
//  InnerEditItemView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 31/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct InnerEditItemView: View {
    @EnvironmentObject var twoFactorViewModel: TwoFactorViewModel
    @EnvironmentObject var viewModel: EditItemViewModel
    @Binding var scrollOfset: CGFloat
    @State var isPasswordHidden = true

    var hasNotesLimit: Bool { viewModel.itemWrapper.encryptedItem.legacyEncryption }

    /// True = we know the app/website has support for 2FA.
    /// False = we don't know if the app/website has support for 2FA.
    var supportsTotp: Bool {
        viewModel.brand?.totp ?? false
    }

    var body: some View {
        ScrollViewReader { (proxy: ScrollViewProxy) in
            LazyVStack(alignment: .leading, spacing: 0) {
                InputGroup {
                    Text("Login name")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 4)

                    FormTextField(
                        "Enter a name",
                        value: $viewModel.name,
                        editMode: true
                    )
                    .padding(.bottom)
                }

                InputGroup {
                    Text("Email or Username")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.bottom, 4)

                    FormTextField(
                        "Enter an email or username",
                        value: $viewModel.username,
                        editMode: true,
                        textContentType: .emailAddress
                    )
                    .padding(.bottom)
                }

                InputGroup {
                    // A hack to prevent iOS from autofilling the username field
                    InvisibleInputField()

                    Text("Password")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 8)

                    GeneratePasswordField(
                        password: $viewModel.password,
                        editMode: true,
                        isHidden: true
                    )
                    .padding(.bottom)
                }

                InputGroup {
                    Text("Website")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 8)

                    WebsiteField("Enter a website address", value: $viewModel.url, editMode: true)
                        .padding(.bottom)
                }

                if supportsTotp || twoFactorViewModel.hasOneTimePassword {
                    Text("Two-Factor Authentication ")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 8)
                }

                TwoFactorEditView(isOtpPossible: viewModel.brand?.totp ?? false)
                    .padding(.bottom, 8)

                InputGroup {
                    Text(hasNotesLimit ? "Notes (\(viewModel.notes?.count ?? 0)/190)" : "Notes")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .padding(.bottom, 8)

                    NotesField(
                        label: "Please add any additional notes",
                        value: $viewModel.notes,
                        onFocusChange: { hasFocus in
                            if hasFocus {
                                withAnimation {
                                    proxy.scrollTo(4)
                                }
                            }
                        },
                        hasLimit: hasNotesLimit
                    )
                    // necessary to scroll the notes view into view programmatically
                    // when the keyboard pops up
                    .id(4)
                    .padding(.bottom)
                }

                PasswordHistoryButton()
                    .consumingViewModel(.itemDetails)

                if !supportsTotp && !twoFactorViewModel.hasOneTimePassword {
                    DefaultButton(
                        title: "Enable 2FA",
                        foregroundColor: .mediumPurple,
                        backgroundColor: .white,
                        action: twoFactorViewModel.presentScannerView
                    )
                    .frame(height: 50)
                    .padding(.vertical)
                }

                Group {
                    Spacer(minLength: 40)

                    DeleteButton()
                        .frame(maxWidth: .infinity, alignment: .center)
                        .consumingViewModel(.deleteItem)
                        .padding(.bottom, 40)
                }
            }
            .padding(.bottom, 40)
            .padding(.top, 200)
            .padding(.horizontal)
        }
        .modifier(OffsetModifier(offset: $scrollOfset))
    }
}

/// A wrapper to put around each input + label in the form
public struct InputGroup<Content: View>: View {
    let content: Content

    init(@ViewBuilder content: () -> Content) {
        self.content = content()
    }

    public var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            content
        }
    }
}
