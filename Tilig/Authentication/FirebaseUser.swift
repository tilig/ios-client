//
//  FirebaseUser.swift
//  Tilig
//
//  Created by Gertjan Jansen on 11/05/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Firebase
import FirebaseAuth
import Foundation

// Firebase user mocking protocols.
// These were created to make it possible to mock Firebase users in our tests.
// This was necessary because it's not possible to instantiate a Firebase User instance.

protocol FirebaseUser {
    var uid: String { get }
    var email: String? { get }
    var displayName: String? { get }
    var isAnonymous: Bool { get }
    var isEmailVerified: Bool { get }
    var refreshToken: String? { get }
    var providerData: [UserInfo] { get }
    /// UserMetadata can't be mocked easily.
    /// it contains lastSignInDate and creationData
    /// so if we want to use that, we should assign them manually.
    // var metadata: UserMetadataProtocol { get }
    var multiFactor: MultiFactor { get }
}

extension User: FirebaseUser { }

// Also useful for tests
extension FirebaseUser where Self: Equatable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        // ignore providerData in comparison because it's not
        // equatable
        return lhs.uid == rhs.uid &&
            lhs.email == rhs.email &&
            lhs.displayName == rhs.displayName &&
            lhs.isAnonymous == rhs.isAnonymous &&
            lhs.isEmailVerified == rhs.isEmailVerified &&
            lhs.refreshToken == rhs.refreshToken
            // lhs.providerData == rhs.providerData
            // lhs.multiFactor == rhs.multiFactor
    }
}

// MARK: FirebaseUser Codable support

/// It was impossible to implement Codable support for the FirebaseUser protocol.
/// That's why I created a class that mirrors the protocol, which does contain codable support.

enum FirebaseUserCodingKeys: String, CodingKey {
    case uid, email, displayName, isAnonymous, isEmailVerified, refreshToken
    // providerData, multiFactor,
}

// Make encodeable for caching
extension FirebaseUser where Self: Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: FirebaseUserCodingKeys.self)
        try container.encode(uid, forKey: .uid)
        try container.encode(email, forKey: .email)
        try container.encode(displayName, forKey: .displayName)
        try container.encode(isAnonymous, forKey: .isAnonymous)
        try container.encode(isEmailVerified, forKey: .isEmailVerified)
        try container.encode(refreshToken, forKey: .refreshToken)
        // try container.encode(providerData, forKey: .providerData)
        // try container.encode(multiFactor, forKey: .multiFactor)
    }
}

final class CodeableFirebaseUser: FirebaseUser, Equatable {
    var uid: String
    var email: String?
    var displayName: String?
    var isAnonymous: Bool
    var isEmailVerified: Bool
    var refreshToken: String?
    var providerData: [UserInfo]
    var multiFactor: MultiFactor

    init(uid: String,
         email: String?,
         displayName: String?,
         isAnonymous: Bool,
         isEmailVerified: Bool,
         refreshToken: String?,
         providerData: [UserInfo],
         multiFactor: MultiFactor) {
        self.uid = uid
        self.email = email
        self.displayName = displayName
        self.isAnonymous = isAnonymous
        self.isEmailVerified = isEmailVerified
        self.refreshToken = refreshToken
        self.providerData = providerData
        self.multiFactor = multiFactor
    }
}

extension CodeableFirebaseUser: Encodable { }

// Make encodeable for caching
extension CodeableFirebaseUser: Decodable {
    convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: FirebaseUserCodingKeys.self)
        let uid = try container.decode(String.self, forKey: .uid)
        let email = try container.decode(String.self, forKey: .email)
        let displayName = try container.decodeIfPresent(String.self, forKey: .displayName)
        let isAnonymous = try container.decode(Bool.self, forKey: .isAnonymous)
        let isEmailVerified = try container.decode(Bool.self, forKey: .isEmailVerified)
        let refreshToken = try container.decode(String.self, forKey: .refreshToken)
        let providerData = [UserInfo]()
        let multiFactor = MultiFactor()
        self.init(uid: uid,
                  email: email,
                  displayName: displayName,
                  isAnonymous: isAnonymous,
                  isEmailVerified: isEmailVerified,
                  refreshToken: refreshToken,
                  providerData: providerData,
                  multiFactor: multiFactor
        )
    }
}

extension FirebaseUser {
    func toCodable() -> CodeableFirebaseUser {
        CodeableFirebaseUser(
            uid: uid,
            email: email,
            displayName: displayName,
            isAnonymous: isAnonymous,
            isEmailVerified: isEmailVerified,
            refreshToken: refreshToken,
            providerData: providerData,
            multiFactor: multiFactor
        )
    }
}
