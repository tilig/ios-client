//
//  TokenExpired.swift
//  Tilig
//
//  Created by Gertjan Jansen on 10/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation

struct Token: RawRepresentable, Codable, Equatable, Hashable {
    var rawValue: String
}

/// Token isExpired extension
extension Token {
    enum TokenDecodeError: Error {
        case expirationInvalid
        case badToken
        case other
    }

    func isExpired() -> Bool {
        #if DEBUG
        if UserDefaults.standard.bool(forKey: "accessTokenExpired") {
            Log("🕰 Enforcing token expiration!")
            return true
        }
        #endif
        guard let payload = try? decode(jwtToken: rawValue),
            let exp = payload["exp"] as? Int else {
            Log("Failed to read exp from JWT", type: .error)
            // make it fail silently since in the end it will sign the user out
            // (and an invalid token can also be considered expired)
            // In dev I can encounter cases like these, when some state is initialized
            // with unreadable tokens, resulting from XCTests.
            return true
        }
        /// expire 1 minutes earlier than end time, to be sure
        let currentTime = Date().timeIntervalSince1970 + 60
        Log("🕰 currentTime: \(currentTime) vs exp: \(exp)", category: .authentication)
        if exp <= Int(currentTime) {
             Log("🕰 Expired!", category: .authentication)
            return true
        } else {
             Log("🕰 Not expired", category: .authentication)
            return false
        }
    }

    private func decode(jwtToken jwt: String) throws -> [String: Any] {
        func base64Decode(_ base64: String) throws -> Data {
            let padded = base64.padding(toLength: ((base64.count + 3) / 4) * 4, withPad: "=", startingAt: 0)
            guard let decoded = Data(base64Encoded: padded) else {
                throw TokenDecodeError.badToken
            }
            return decoded
        }

        func decodeJWTPart(_ value: String) throws -> [String: Any] {
            let bodyData = try base64Decode(value)
            let json = try JSONSerialization.jsonObject(with: bodyData, options: [])
            guard let payload = json as? [String: Any] else {
                throw TokenDecodeError.other
            }
            return payload
        }
        let segments = jwt.components(separatedBy: ".")
        if segments.count < 2 {
            throw TokenDecodeError.badToken
        }
        return try decodeJWTPart(segments[1])
    }
}
