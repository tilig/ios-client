//
//  AuthStateWrapper.swift
//  Tilig
//
//  Created by Gertjan Jansen on 01/06/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Combine

/// A wrapper that always contains a non-nil authstate
/// https://www.swiftbysundell.com/tips/combining-dynamic-member-lookup-with-key-paths/
@dynamicMemberLookup
class AuthStateWrapper: ObservableObject {
    @Published private(set) var wrappedValue: AuthState

    private var subscriptions = Set<AnyCancellable>()

    /// Pass the initial `authState`, with an optional `authStatePublisher` that updates the wrapped value.
    init(authState: AuthState, authStatePublisher: AnyPublisher<AuthState?, Never>? = nil) {
        self.wrappedValue = authState

        authStatePublisher?
            .compactMap { $0 }
            .assign(to: &$wrappedValue)
    }

    subscript<T>(dynamicMember keyPath: KeyPath<AuthState, T>) -> T {
        wrappedValue[keyPath: keyPath]
    }
}
