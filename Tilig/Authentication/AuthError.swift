//
//  AuthError.swift
//  Tilig
//
//  Created by Gertjan Jansen on 10/05/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation

enum AuthError: Error {
    /// Indicates an internal error in the auth pipeline
    case partialAuthPipelineError
    /// The user signed out from Firebase
    case signedOutFromFirebase
    /// The user is not authorzed
    case unauthorized
    /// The authentication call to the Tilig Service failed
    case tiligServiceConnectionFailed(Error)
    /// Key Service related errors
    case keyServiceError(KeyServiceError)
    /// Refreshing the access token failed
    case refreshFailed(Error)
    /// Received an unexpected Firebase result
    case unexpectedFirebaseResult
    /// User already has a Tilig account with a different Provider
    case accountExistsWithDifferentProvider
    /// User has not verified their email address
    case userEmailNotVerified
}

extension AuthError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .partialAuthPipelineError:
            return "An internal authentication error occured. If this happens again, please contact our support."
        case .unexpectedFirebaseResult:
            return """
                An internal authentication error occured (Firebase). If this happens again, please contact our support.
                """
        case .signedOutFromFirebase:
            return "Signed out from Firebase"
        case .unauthorized:
            return "You are not authorized to perform this action."
        case .tiligServiceConnectionFailed:
            // Could be the result of not having an internet connection, but
            // to be sure tell the user to contact Tilig support.
            return "Could not connect to the Tilig server. Please contact Tilig support."
        case .keyServiceError(let error):
            return error.localizedDescription
        case .refreshFailed:
            return "Your session could not be continued. Please sign in again."
        case .accountExistsWithDifferentProvider:
            return "Can't log in using Microsoft. Please continue with Google or Apple when using this email."
        case .userEmailNotVerified:
            return """
                Your email address is not verified. \
                Please check your inbox or junk mail for your verification email and try again.
                """
        }
    }
}
