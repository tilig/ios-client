//
//  KeyPair.swift
//  Tilig
//
//  Created by Gertjan Jansen on 14/12/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import Sodium
import Combine
import Firebase
import FirebaseFunctions

/// The encrypted KeyPair in the shape of a JWT
struct KeyPairJWT: RawRepresentable, Codable, Equatable, Hashable {
    var rawValue: String
}

extension Box.KeyPair {
    enum KeyPairError: Error {
        case failedToDecryptKeyPair
        case failedToGenerateKeyPair
        case failedToEncryptKeyPair
    }

    static var mockedPublisher: AnyPublisher<Box.KeyPair, Error>?

    /// Fetches or generates a KeyPair and publishes it
    static func publish(usingAccessToken accessToken: Token) -> AnyPublisher<Box.KeyPair, Error> {
        if let mockedPublisher { return mockedPublisher }

        return Future {
            try await generateOrFetch(usingAccessToken: accessToken)
        }
        .eraseToAnyPublisher()
    }

    /// This does the following:
    ///
    /// Step 1: Try to fetch keypair from profile API
    /// If it exists:
    ///     Step 2: Decrypt the private part
    ///     Step 3: Return the keypair
    /// It it doesn't exist:
    ///     Step 2: Generate a keypair
    ///     Step 3: Send the keypair + some metadata to the encrypt cloud function (returns JSON web token)
    ///       meta data here: https://gitlab.com/subshq/clients/webapp/-/blob/main/core/src/clients/firebase.ts#L59
    ///     Step 4: Store the JSON web token in the backend
    ///     Step 5: Return the keypair
    static func generateOrFetch(usingAccessToken accessToken: Token) async throws -> Box.KeyPair {
        if let fetched = try await fetch(usingAccessToken: accessToken) {
            return fetched
        }
        return try await generateAndStore(usingAccessToken: accessToken)
    }

    /// - Fetch the KeyPair from the API
    /// - Decrypt the private key, using the Firebase cloud function
    /// - Return the decrypted KeyPair
    static func fetch(usingAccessToken accessToken: Token) async throws -> Self? {
        let profile: UserProfile = try await URLSession.current.request(for: .profile, using: accessToken)

        if let encryptedKeyPair = profile.keypair {
            let payload: [String: String] = ["encrypted_private_key": encryptedKeyPair.encryptedPrivateKey]
            let result = try await decryptFunction.call(payload)

            guard let data = result.data as? NSDictionary,
                  let privateKey = data["private_key"] as? String,
                  let secretKey = base642bin(privateKey),
                  let publicKey = base642bin(encryptedKeyPair.publicKey) else {
                throw KeyPairError.failedToDecryptKeyPair
            }

            Log("🔑🔑 Fetched keypair", category: .authentication)
            return Box.KeyPair(publicKey: publicKey, secretKey: secretKey)
        }

        Log("🔑🔑 No keypair present", category: .authentication)
        return nil
    }

    // MARK: Generation

    static func generateAndStore(usingAccessToken accessToken: Token) async throws -> Self {
        let keyPair = try generate()

        let token = try await encrypt(keyPair: keyPair)

        try await store(token: token, usingAccessToken: accessToken)

        return keyPair
    }

    /// Generate a new KeyPair using Libsodium
    static func generate() throws -> Self {
        Log("🔑🔑 Generating keypair", category: .authentication)
        guard let keyPair = Sodium().box.keyPair() else {
            throw KeyPairError.failedToGenerateKeyPair
        }
        return keyPair
    }

    /// Let the cloud function encrypt the given keypair.
    /// Returns the encrypted pair as a JSON web token.
    static func encrypt(keyPair: Self) async throws -> KeyPairJWT {
        Log("🔑🔑 Encrypting keypair", category: .authentication)
        guard let privateKeyString = bin2base(keyPair.secretKey) else {
            throw KeyPairError.failedToEncryptKeyPair
        }
        guard let publicKeyString = bin2base(keyPair.publicKey) else {
            throw KeyPairError.failedToEncryptKeyPair
        }

        let payload: [String: [String: String]] = [
            "key": [
                "private_key": privateKeyString,
                "public_key": publicKeyString,
                "key_type": "x25519"
            ],
            "meta": [
                "app_platform": VersionIdentifier.tiligPlatform,
                "app_version": VersionIdentifier.tiligVersion
            ]
        ]

        if let mocked = mockedFirebaseEncryptResponse {
            return mocked
        }

        let response = try await encryptFunction.call(payload)

        guard let data = response.data as? NSDictionary,
              let jwt = data["token"] as? String else {
            throw KeyPairError.failedToEncryptKeyPair
        }

        Log("[☁️ encrypted keyPair] response: \(String(describing: jwt))")

        return KeyPairJWT(rawValue: jwt)
    }

    struct StoreKeyPairPayload: Codable {
        // The API spelled it Keypair instead of KeyPair here
        let encryptedKeypair: KeyPairJWT
    }

    struct StoreKeyPairResponse: Codable {
        let encryptedPrivateKey: String
        let publicKey: String
    }

    /// Store the encrypted keypair in the API
    static func store(token: KeyPairJWT, usingAccessToken accessToken: Token) async throws {
        Log("🔑🔑 Storing keypair", category: .authentication)
        let payload = StoreKeyPairPayload(encryptedKeypair: token)
        let data = try JSONEncoder.snakeCaseConverting.encode(payload)

        let response: StoreKeyPairResponse = try await URLSession.current.request(
            for: .keypairs,
            method: .post,
            data: data,
            using: accessToken
        )
        Log("🔑🔑 Stored keypair: \(response.encryptedPrivateKey)", category: .authentication)
        // assert(response.encryptedKeypair == token)
    }

    // MARK: Helper functions

    /// Helper function
    static func base642bin(_ value: String) -> Bytes? {
        // Remove any newlines that other clients might have sneaked in
        let value = value.replacingOccurrences(of: "\n", with: "")
        return Sodium().utils.base642bin(value, variant: .URLSAFE_NO_PADDING)
    }

    /// Helper function
    static func bin2base(_ value: Bytes) -> String? {
        Sodium().utils.bin2base64(value, variant: .URLSAFE_NO_PADDING)
    }

    // MARK: Cloud functions

    private static var decryptFunction: HTTPSCallable = {
        return Functions.functions().httpsCallable("decryptPrivateKey")
    }()

    private static var encryptFunction: HTTPSCallable = {
        return Functions.functions().httpsCallable("encryptKeypair")
    }()

    // Mock the response from the Firebase Cloud function
    static var mockedFirebaseEncryptResponse: KeyPairJWT?

    // TODO: implement this
    // static var mockedFirebaseDecryptResponse: String?
}
