//
//  KeyPair+Codable.swift
//  Tilig
//
//  Created by Gertjan Jansen on 16/12/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Sodium

extension Box.KeyPair: Codable {
    enum CodingKeys: String, CodingKey {
        case publicKey, secretKey
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let publicKey = try container.decode([UInt8].self, forKey: .publicKey)
        let secretKey = try container.decode([UInt8].self, forKey: .secretKey)
        self.init(publicKey: publicKey, secretKey: secretKey)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(publicKey, forKey: .publicKey)
        try container.encode(secretKey, forKey: .secretKey)
    }
}

extension Box.KeyPair: Equatable {
    public static func == (lhs: Box.KeyPair, rhs: Box.KeyPair) -> Bool {
        lhs.secretKey == rhs.secretKey && lhs.publicKey == rhs.publicKey
    }
}
