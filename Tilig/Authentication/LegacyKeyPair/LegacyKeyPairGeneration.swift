//
//  NewKeyPairGenerator.swift
//  Tilig
//
//  Created by Gertjan Jansen on 28/04/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Combine
import Security
import Foundation
import SwiftyRSA

enum KeyGenerationError: Error {
    case couldNotCreatePublicKey
    case couldNotDecode
    case couldNotDecodeX509Format
    case couldNotEncodeX509Format
    case integrityCheckFailed(Error)
}

extension KeyGenerationError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .couldNotCreatePublicKey:
            return "Could not create a public key."
        case .couldNotDecode:
            return "Could not read (decode) the key."
        case .couldNotDecodeX509Format:
            return "The key is encoded in an invalid X509 format."
        case .integrityCheckFailed:
            return "Integrity check of key pair failed."
        case .couldNotEncodeX509Format:
            return "Could not encode the key in X509 format."
        }
    }
}

extension LegacyKeyPair {
    static func generatedKeyPairPublisher(firebaseUserID: String, accessToken: Token) -> AnyPublisher<LegacyKeyPair, Error> {
        // 1: Generate a new RSA key pair
        let keyPair: LegacyKeyPair
        do {
            keyPair = try LegacyKeyPair.generateRSAKeyPair()
        } catch {
            Log("🔐 Error generating a new key pair: \(error.localizedDescription)", type: .error)
            return Fail(error: error).eraseToAnyPublisher()
        }
        Log("🔐 Generated a new key pair: \(keyPair)")

        // 2: Store private key in Firestore
        return keyPair.privateKey
            .storeInFirebase(forFirebaseUserID: firebaseUserID)
            .handleEvents(receiveOutput: { storedPrivateKey in
                Log("🔐 Stored private key in Firestore")
                assert(storedPrivateKey == keyPair.privateKey)
            })
            .flatMap({ _ -> AnyPublisher<LegacyKeyPair, Error> in
                // 3: Store public key at TS
                keyPair.publicKey
                    .storeInTiligService(accessToken: accessToken)
                    .handleEvents(receiveOutput: { storedPublicKey in
                        Log("🔐 Stored public key in TS")
                        assert(storedPublicKey == keyPair.publicKey)
                    })
                    .map { _ -> LegacyKeyPair in
                        return keyPair
                    }
                    .eraseToAnyPublisher()
            })
            .eraseToAnyPublisher()
    }

    func removePublicAndPrivateKeys(firebaseUserID: String, tokenProvider: TokenProvider) async throws {
        let accessToken = try await tokenProvider.getAccessToken()

        try await publicKey
            .deleteFromTiligService(accessToken: accessToken)
            .singleOutput()

        Log("🔐 Deleted public key in TS")

        _ = try await privateKey
            .deleteFromFirebase(forFirebaseUserID: firebaseUserID)
            .singleOutput()

        Log("🔐 Deleted private key from Firestore")
    }

//    func removePublicAndPrivateKeys(firebaseUserID: String, accessToken: Token) -> AnyPublisher<Void, Error> {
//        // 1: Remove Private Key in Firestore
//        return self.privateKey
//            .deleteFromFirebase(forFirebaseUserID: firebaseUserID)
//            .handleEvents(receiveOutput: { _ in
//                Log("🔐 Deleted private key from Firestore")
//            })
//            .flatMap({ _ -> AnyPublisher<Void, Error> in
//                // 2: Remove public key in TS
//                self.publicKey
//                    .deleteFromTiligService(accessToken: accessToken)
//                    .handleEvents(receiveOutput: { _ in
//                        Log("🔐 Deleted public key in TS")
//                    })
//                    .eraseToAnyPublisher()
//            })
//            .eraseToAnyPublisher()
//    }

    static func generateRSAKeyPair() throws -> LegacyKeyPair {
        var error: Unmanaged<CFError>?
        guard let privateKey = SecKeyCreateRandomKey(LegacyKeyPair.attributes, &error) else {
            throw error!.takeRetainedValue() as Error
        }
        let encodedPrivateKey = try privateKey.x509Format()

        Log(encodedPrivateKey)

        // Derive the public key from the private key
        guard let publicKey = SecKeyCopyPublicKey(privateKey) else {
            throw KeyGenerationError.couldNotCreatePublicKey
        }
        let encodedPublicKey = try publicKey.x509Format()

        return LegacyKeyPair(privateKey: LegacyPrivateKey(rawValue: encodedPrivateKey),
                       publicKey: LegacyPublicKey(rawValue: encodedPublicKey))
    }
}

extension LegacyKeyPair {
    static let defaultKeySizeInBits: Int = 2048

    static var attributes: CFDictionary {
        let tagPublic = "com.tilig.encryption.public"
        let tagPrivate = "com.tilig.encryption.private"
        let storeInKeyChain = false

        let publicKeyAttributes: [String: Any] = [
            kSecAttrIsPermanent as String: storeInKeyChain,
            kSecAttrApplicationTag as String: tagPublic
        ]

        let privateKeyAttributes: [String: Any] = [
            /// Don't store it in the default keychain
            kSecAttrIsPermanent as String: storeInKeyChain,
            kSecAttrApplicationTag as String: tagPrivate
        ]

        return [
            kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
            kSecAttrKeySizeInBits as String: LegacyKeyPair.defaultKeySizeInBits,
            kSecPublicKeyAttrs as String: publicKeyAttributes,
            kSecPrivateKeyAttrs as String: privateKeyAttributes
        ] as CFDictionary
    }
}

enum KeyType {
    case privateKey
    case publicKey
}

extension SecKey {
    /// Convenience initializer to create a SecKey from a X509 string and keyType
    /// `convenience init(...) cannot be used on extended CF types, but this acts like one.
    static func create(fromX509String x509String: String, type keyType: KeyType) throws -> SecKey {
        if keyType == .privateKey {
            let swiftyPrivateKey = try PrivateKey(pemEncoded: x509String)
            return swiftyPrivateKey.reference
        } else {
            let swiftyPublicKey = try PublicKey(pemEncoded: x509String)
            return swiftyPublicKey.reference
        }

        // Remove the X.509 prefix and suffix
//        let lines = x509String.components(separatedBy: "\n")
//        let base64String = lines[1...lines.count-2].joined()
//        // decode with .ignoreUnknownCharacters to skip newlines and read it as one string
//        guard let keyData = Data(base64Encoded: base64String, options: .ignoreUnknownCharacters) else {
//            throw KeyGenerationError.couldNotDecode
//        }
//        return try SecKey.create(fromData: keyData, type: keyType)
    }

    /// Convenience initializer to create a SecKey from keyData and keyType.
    /// `convenience init(...) cannot be used on extended CF types, but this acts like one.
//    static func create(fromData keyData: Data, type keyType: KeyType) throws -> SecKey {
//        var error: Unmanaged<CFError>?
//        let keyClassType = keyType == .privateKey ? kSecAttrKeyClassPrivate : kSecAttrKeyClassPublic
//        let attributes: [String: Any] = [
//            kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
//            kSecAttrKeyClass as String: keyClassType,
//            kSecAttrKeySizeInBits as String: LegacyKeyPair.defaultKeySizeInBits,
//        ]
//        guard let key = SecKeyCreateWithData(keyData as CFData, attributes as CFDictionary, &error) else {
//            throw error!.takeRetainedValue() as Error
//        }
//        return key
//    }

    /// Convert this SecKey key to data
    /// Note this is the data of the actual key, not the data of the full X.509 formatted string
    func data() throws -> Data {
        var publicKeyExportError: Unmanaged<CFError>?
        guard let cfData = SecKeyCopyExternalRepresentation(self, &publicKeyExportError) else {
            throw publicKeyExportError!.takeRetainedValue() as Error
        }
        return cfData as Data
    }

    /// Convert this SecKey key to x.509 format, which includes a base64 encoded string
    func x509Format() throws -> String {
        // get the type of key
        guard let attributes = SecKeyCopyAttributes(self) as? [CFString: Any] else {
            throw KeyGenerationError.couldNotEncodeX509Format
        }
        guard let keyClass = attributes[kSecAttrKeyClass] as? String else {
            throw KeyGenerationError.couldNotEncodeX509Format
        }
        let keyType: KeyType = keyClass == kSecAttrKeyClassPrivate as String ? .privateKey : .publicKey
        // generate the X.509 string
        return appendPrefixSuffixTo(
            try data().base64EncodedString(options: .lineLength64Characters),
            prefix: "-----BEGIN RSA \(keyType == .privateKey ? "PRIVATE" : "PUBLIC") KEY-----\n",
            suffix: "\n-----END RSA \(keyType == .privateKey ? "PRIVATE" : "PUBLIC") KEY-----"
        )
    }

    /// Helper function
    private func appendPrefixSuffixTo(_ string: String, prefix: String, suffix: String) -> String {
        "\(prefix)\(string)\(suffix)"
    }
}

extension LegacyPublicKey {
    /// The public key represented as Apple's SecKey type
    func secKey() throws -> SecKey {
        return try SecKey.create(fromX509String: rawValue, type: .publicKey)
    }
}

extension LegacyPrivateKey {
    /// The public key represented as Apple's SecKey type
    func secKey() throws -> SecKey {
        return try SecKey.create(fromX509String: rawValue, type: .privateKey)
    }
}
