//
//  PublicKeyManager.swift
//  Tilig
//
//  Created by Gertjan Jansen on 28/04/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import Combine

struct LegacyPublicKey: RawRepresentable, Codable, Equatable {
    var rawValue: String
}

extension LegacyPublicKey: ExpressibleByStringLiteral {
    init(stringLiteral: String) {
        rawValue = stringLiteral
    }
}

enum LegacyPublicKeyError: Error {
    case notFound
    case storingFailed
}

enum DeleteLegacyPublicKeyError: Error {
    case deletionFailed
}

extension LegacyPublicKeyError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .notFound:
            return "Your public key was not found. Please contact us for support."
        case .storingFailed:
            return "Could not store your public key. Please contact us for support."
        }
    }
}

extension LegacyPublicKey {
    static var mockedPublisher: AnyPublisher<LegacyPublicKey, Error>?

    /// Contrary to the PrivateKey publisher, the PublicKey cannot be optional.
    /// It should raise an error instead.
    static func publisher(forAccessToken accessToken: Token) -> AnyPublisher<LegacyPublicKey, Error> {
        if let mocked = mockedPublisher {
            return mocked
        }

        return URLSession
            .current
            .dataTaskPublisher(for: .profile,
                               authorizationHeader: "Bearer \(accessToken.rawValue)")
            .decodeResponse(as: UserProfile.self)
            .mapError { error in
                Log(error.localizedDescription, type: .error)
                return LegacyPublicKeyError.notFound
            }
            .tryMap({ profile in
                guard let publicKey = profile.publicKey else {
                    throw LegacyPublicKeyError.notFound
                }
                return publicKey
            })
            .eraseToAnyPublisher()
    }

    // MARK: Storing a public key

    func storeInTiligService(accessToken: Token) -> AnyPublisher<LegacyPublicKey, Error> {
        struct UserWrapper: Encodable {
            var user: PublicKeyWrapper
        }
        struct PublicKeyWrapper: Codable {
            var publicKey: String
        }
        let postData = UserWrapper(user: PublicKeyWrapper(publicKey: rawValue))

        return URLSession
            .current
            .dataTaskPublisher(for: .profile,
                               method: .put,
                               encodable: postData,
                               authorizationHeader: "Bearer \(accessToken.rawValue)")
            .decodeResponse(as: PublicKeyWrapper.self)
            .map(\.publicKey)
            .map { LegacyPublicKey(rawValue: $0) }
            .mapError { error in
                Log(error.localizedDescription, type: .error)
                return LegacyPublicKeyError.storingFailed
            }
            .eraseToAnyPublisher()
    }

    // MARK: Deleting a public key

    func deleteFromTiligService(accessToken: Token) -> AnyPublisher<Void, Error> {
        return URLSession
            .current
            .dataTaskPublisher(for: .profile,
                               method: .delete,
                               authorizationHeader: "Bearer \(accessToken.rawValue)")
            .map { _ in () }
            .mapError { error in
                Log(error.localizedDescription, type: .error)
                return DeleteLegacyPublicKeyError.deletionFailed
            }
            .eraseToAnyPublisher()
    }
}
