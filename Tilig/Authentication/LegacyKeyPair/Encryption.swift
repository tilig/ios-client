//
//  Encryption.swift
//  Tilig
//
//  Created by Gertjan Jansen on 09/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import Sentry

enum EncryptionError: Error {
    case encryptionFailed(String)
    case keyPairIsMissing
}

// We haven't localized these yet, so "DecryptionError 1" is .blockSize,
// 2 is .base64DecodingFailed, etc.
enum DecryptionError: Error {
    case blockSize
    case base64DecodingFailed
    case decryptionFailed(String)
    case stringEncodingFailed
}

extension LegacyKeyPair {
    // TODO, write tests for:
    // - special characters
    // - long strong
    // - invalid or missing keys
    // and then try to trigger different errors for each of those cases
    func encrypt(plainText: String) throws -> String {
        let buffer = [UInt8](plainText.utf8)
        return try encrypt(buffer: buffer)
    }

    func encrypt(buffer: [UInt8]) throws -> String {
        let publicSecKey = try publicKey.secKey()
        var keySize = SecKeyGetBlockSize(publicSecKey)
        var keyBuffer = [UInt8](repeating: 0, count: keySize)

        let status = SecKeyEncrypt(
            publicSecKey, // public key
            SecPadding.OAEP, // padding
            buffer, // plain text
            buffer.count, // plain text length
            &keyBuffer, // On return, the encrypted text (cipherText)
            &keySize // cipherText length
        )
        guard status == errSecSuccess else {
            let errorDescription = SecCopyErrorMessageString(status, nil)
            // Sentry doesn't give useful information what went wrong exactly,
            // therefore send this to sentry directly.
            // TODO: write test for this
            let messageString = String(describing: errorDescription)
            SentrySDK.capture(message: messageString)
            throw EncryptionError.encryptionFailed(messageString)
        }
        let data = Data(bytes: keyBuffer, count: keySize)
        return data.base64EncodedString()
    }

    /// Decrypt the given cipherText, return as string
    func decrypt(cipherText base64Encoded: String) throws -> String {
        let result = try decryptBuffer(cipherText: base64Encoded)
        let plainTextBuffer = result.buffer
        let bufferSize = result.bufferSize

        let outputData = Data(bytes: plainTextBuffer, count: bufferSize)
        guard let string = String(data: outputData, encoding: .utf8) else {
            let error = DecryptionError.stringEncodingFailed
            SentrySDK.capture(error: error)
            throw error
        }
        return string
    }

    private func fixBase64(_ base64: String) -> String {
        // Remove newlines
        var base64 = base64.replacingOccurrences(of: "\n", with: "")

        // Convert URL safe to non-URL safe
        base64 = base64
            .replacingOccurrences(of: "_", with: "/")
            .replacingOccurrences(of: "-", with: "+")
        // Add any necessary padding with `=`
        if base64.count % 4 != 0 {
            base64.append(String(repeating: "=", count: 4 - base64.count % 4))
        }

        return base64
    }

    /// Decrypt the given cipherText, return as buffer (array of UInt8). Also returns the buffer size.
    func decryptBuffer(cipherText base64Encoded: String) throws -> (buffer: [UInt8], bufferSize: Int) {
        // Android can add newlines and/or URL safe formatting, so fix that.
        let base64Encoded = fixBase64(base64Encoded)

        if base64Encoded == "" { return ([], 0) }

        guard let decodedData = Data(base64Encoded: base64Encoded, options: .ignoreUnknownCharacters) else {
            let error = DecryptionError.base64DecodingFailed
            SentrySDK.capture(error: error)
            throw error
        }
        let privateSecKey = try privateKey.secKey()

        let blockSize = SecKeyGetBlockSize(privateSecKey)
        // In some edge cases, the payload is 255 bytes instead of 256. That's why we allow blockSize-1 too.
        guard decodedData.count == blockSize || decodedData.count == blockSize-1 else {
            // block size doesn't match
            let error = DecryptionError.blockSize
            SentrySDK.capture(error: error)
            throw error
        }

        let cipherTextBytes = [UInt8](decodedData)
        let cipherTextSize = cipherTextBytes.count

        // TODO: Use private or public key block size here?
        // var publicKeyBlockSize = SecKeyGetBlockSize(publicKey)
        // var plainTextBuffer = [UInt8](repeating: 0, count: publicKeyBlockSize)
        // var plainTextSize: Int = publicKeyBlockSize
        let privateKeyBlockSize = SecKeyGetBlockSize(privateSecKey)
        var plainTextBuffer = [UInt8](repeating: 0, count: privateKeyBlockSize)
        var plainTextSize: Int = privateKeyBlockSize

        let decryptStatus = SecKeyDecrypt(
            // private key with which to decrypt the data.
            privateSecKey,
            // The type of padding used.
            SecPadding.OAEP,
            // The data to decrypt.
            cipherTextBytes,
            // Length in bytes of the data in the cipherText buffer. This must be less than or equal to
            // the value returned by the SecKeyGetBlockSize(_:) function.
            cipherTextSize,
            // On return, the decrypted text.
            &plainTextBuffer,
            // On entry, the size of the buffer provided in the plainText parameter.
            // On return, the amount of data actually placed in the buffer.
            &plainTextSize
        )

        guard decryptStatus == errSecSuccess else {
            let errorDescription = SecCopyErrorMessageString(decryptStatus, nil)
            let messageString = String(describing: errorDescription)
            SentrySDK.capture(message: messageString)
            throw DecryptionError.decryptionFailed(String(describing: errorDescription))
        }

        return (plainTextBuffer, plainTextSize)
    }
}
