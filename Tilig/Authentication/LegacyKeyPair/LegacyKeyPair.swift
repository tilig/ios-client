//
//  LegacyKeyPairManager.swift
//  Tilig
//
//  Created by Gertjan Jansen on 22/03/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Firebase
import Combine

struct LegacyKeyPair: Equatable, Codable {
    var privateKey: LegacyPrivateKey
    var publicKey: LegacyPublicKey
}

extension LegacyKeyPair {
    static var mockedKeyPair: LegacyKeyPair?

    static func loadKeyPair(
        forFirebaseUserID firebaseUserID: String,
        accessToken: Token
    ) -> AnyPublisher<LegacyKeyPair, Error> {

        if let mocked = mockedKeyPair {
            return Just(mocked)
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        }

        return LegacyPrivateKey
            .publisher(forFirebaseUserID: firebaseUserID)
            .flatMap { privateKey -> AnyPublisher<LegacyKeyPair, Error> in
                if let privateKey = privateKey {
                    return LegacyPublicKey
                        .publisher(forAccessToken: accessToken)
                        .map { publicKey in
                            LegacyKeyPair(privateKey: privateKey, publicKey: publicKey)
                        }.eraseToAnyPublisher()
                } else {
                    return LegacyKeyPair.generatedKeyPairPublisher(firebaseUserID: firebaseUserID,
                                                   accessToken: accessToken)
                }
            }
            .eraseToAnyPublisher()
    }
}
