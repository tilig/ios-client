//
//  FirestorePrivateKeyManager.swift
//  Tilig
//
//  Created by Gertjan Jansen on 15/03/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift
import FirebaseFunctions
import Combine
import os.log

enum LegacyPrivateKeyError: Error {
    case empty
    case decodingFailed
    case decodingError(Error)
}

extension LegacyPrivateKeyError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .empty:
            return "Please contact Tilig support with the following error code: PK-E."
        case .decodingFailed:
            return "Please contact Tilig support with the following error code: PK-DF."
        case .decodingError:
            return "Please contact Tilig support with the following error code: PK-DE."
        }
    }
}

struct LegacyPrivateKey: RawRepresentable, Codable, Equatable {
    var rawValue: String
}

extension LegacyPrivateKey: ExpressibleByStringLiteral {
    init(stringLiteral: String) {
        rawValue = stringLiteral
    }
}

extension LegacyPrivateKey {
    static var mockedPublisher: AnyPublisher<LegacyPrivateKey?, Error>?

    internal struct PrivateKeyDocument: Codable {
        var uid: String
        var privateKey: String
    }

    static func publisher(forFirebaseUserID firebaseUserID: String) -> AnyPublisher<LegacyPrivateKey?, Error> {
        if let mocked = mockedPublisher {
            return mocked
        }

        #if DEBUG
        if UserDefaults.standard.bool(forKey: "generateNewKeyPair") == true {
            Log("🔑⚠️ Overwriting with new key pair (development mode)", category: .authentication)
            return Just(nil).setFailureType(to: Error.self).eraseToAnyPublisher()
        }
        #endif

        return documentPublisher(forFirebaseUserID: firebaseUserID)
            .tryMap { document in
                if !document.exists {
                    return nil // Don't error
                }
                do {
                    if document.get("privateKey") == nil {
                        Log("🔐 Private Key is empty", type: .error)
                        throw LegacyPrivateKeyError.empty
                    }

                    let privateKeyDoc = try document.data(as: PrivateKeyDocument.self)

                    return LegacyPrivateKey(rawValue: privateKeyDoc.privateKey)
                } catch {
                    Log("🔐 Private Key decoding error: \(error)", type: .error)
                    throw LegacyPrivateKeyError.decodingError(error)
                }
            }
            .eraseToAnyPublisher()
    }

    /// Fetch and publish document from Firestore
    static func documentPublisher(forFirebaseUserID firebaseUserID: String) -> AnyPublisher<DocumentSnapshot, Error> {
        Future { promise in
            LegacyPrivateKey.getDocumentReference(firebaseUserID: firebaseUserID)
                .getDocument { (document, err) in
                    if let document = document {
                        promise(.success(document))
                    }
                    if let error = err {
                        Log("🔐 Firestore raised an error: \(error)", type: .error)
                        promise(.failure(error))
                    }
                }
        }
        .eraseToAnyPublisher()
    }

    /// Get the Firebase document reference that's being used for this user
    static func getDocumentReference(firebaseUserID: String) -> DocumentReference {
        Firestore
            .firestore()
            .collection("keys")
            .document(firebaseUserID)
    }

    // MARK: Storing a private key

    /// Store this private key in Firebase
    func storeInFirebase(forFirebaseUserID firebaseUserID: String) -> AnyPublisher<LegacyPrivateKey, Error> {
        // TODO: throw error if the document already exists
        // meaning: if a key for this user already exists,
        // because overwriting a Firestore key is very dangerous.
        let document = LegacyPrivateKey.getDocumentReference(firebaseUserID: firebaseUserID)

        let privateKeyDocument = PrivateKeyDocument(uid: firebaseUserID,
                                                    privateKey: self.rawValue)

        return Future { promise in
            do {
                try document.setData(from: privateKeyDocument) { error in
                    if let error = error {
                        Log("Error writing private key to Firestore: \(error)", type: .error)
                        promise(.failure(error))
                    }
                    promise(.success(self))
                }
            } catch let error {
                Log("Error writing private key to Firestore: \(error)", type: .error)
                promise(.failure(error))
            }
        }.eraseToAnyPublisher()
    }

    // MARK: Deleting a private key

    /// Deletes this private key from Firebase
    func deleteFromFirebase(forFirebaseUserID firebaseUserID: String) -> AnyPublisher<LegacyPrivateKey, Error> {
        let document = LegacyPrivateKey.getDocumentReference(firebaseUserID: firebaseUserID)
        return Future { promise in
            document.delete(completion: { error in
                if let error = error {
                    Log("Error deleting private key from Firestore: \(error)", type: .error)
                    promise(.failure(error))
                }
                promise(.success(self))
            })
        }.eraseToAnyPublisher()
    }
}
