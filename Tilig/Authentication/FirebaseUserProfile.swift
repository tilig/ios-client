//
//  FirebaseUserProfile.swift
//  Tilig
//
//  Created by Gertjan Jansen on 17/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import FirebaseAuth

/**
 FirebaseUserProfile is a wrapper around user info received from Firebase/Google.
 An example of `additionalUserInfo.profile`:
 [0]    (null)    "at_hash" : "zS0_yzjyIjmowkS-DODenQ"
 [1]    (null)    "hd" : "tilig.com"
 [2]    (null)    "exp" : Int64(1623855336)
 [3]    (null)    "azp" : "765031685617-eur38jubjlmsfc40snp8at10vl60i0ek.apps.googleusercontent.com"
 [4]    (null)    "nonce" : "iCHzsSwvBaCzV6Wz7jcs1EYme3E7e7jZDTdV2q1WbM4"
 [5]    (null)    "picture" : "https://lh3.googleusercontent.com/a/AATXAJwVjKegnMUEjGJiRVhAtdPg-Ad1EpwYxf3v8Dk=s96-c"
 [6]    (null)    "locale" : "en"
 [7]    (null)    "iss" : "https://accounts.google.com"
 [8]    (null)    "email_verified" : YES
 [9]    (null)    "sub" : "114841814089936296887"
 [10]    (null)    "aud" : "765031685617-eur38jubjlmsfc40snp8at10vl60i0ek.apps.googleusercontent.com"
 [11]    (null)    "family_name" : "Jansen"
 [12]    (null)    "iat" : Int64(1623851736)
 [13]    (null)    "email" : "gertjan@tilig.com"
 [14]    (null)    "name" : "Gertjan Jansen"
 [15]    (null)    "given_name" : "Gertjan"
*/

struct FirebaseUserProfile {
    /// Initialize with Firebase's AdditionalUserInfo object
    init(additionalUserInfo: AdditionalUserInfo) {
        isNewUser = additionalUserInfo.isNewUser
        providerID = additionalUserInfo.providerID
        username = additionalUserInfo.username
        profile = additionalUserInfo.profile
    }

    var isNewUser: Bool
    var providerID: String
    var username: String?
    private var profile: [String: NSObject]?

    var givenName: String? { (profile ?? [:])["given_name"] as? String }
    var familyName: String? { (profile ?? [:])["family_name"] as? String }
    var name: String? { (profile ?? [:])["name"] as? String }
    var locale: String? { (profile ?? [:])["locale"] as? String }
    var iss: String? { (profile ?? [:])["iss"] as? String }
    var picture: String? { (profile ?? [:])["picture"] as? String }
}

/// Convencience initializer, useful for testsf
extension FirebaseUserProfile {
    init(isNewUser: Bool,
         providerID: String,
         username: String?,
         profile: [String: NSObject]?) {
        self.isNewUser = isNewUser
        self.providerID = providerID
        self.username = username
        self.profile = profile
    }
}

extension FirebaseUserProfile {
    func debugLog() {
        #if DEBUG
        Log("""
isNewUser = \(isNewUser)
providerID = \(providerID)
username = \(username ?? "nil")
givenName = \(givenName ?? "nil")
familyName = \(familyName ?? "nil")
locale = \(locale ?? "nil")
iss = \(iss ?? "nil")
picture = \(picture ?? "nil")
""", category: .authentication)
        #endif
    }
}
