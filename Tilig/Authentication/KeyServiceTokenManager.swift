//
//  KeyServiceTokenManager.swift
//  Tilig
//
//  Created by Gertjan Jansen on 28/04/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFunctions
import Combine

enum KeyServiceError: Error {
    case decodingFailed
    case responseError(Error)
}

extension KeyServiceError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .decodingFailed:
            return "An internal error occured."
        case .responseError:
            return "Could not connect to the server."
        }
    }
}

struct DataReciever {
    let data: Data
}

struct KeyServiceTokenManager {
    /// Mock the response
    // Tried to make this a Result<HTTPSCallableResult, Error>?, but couldn't make that work
    static var mockedResponse: Result<NSDictionary, KeyServiceError>?

    static func fetchJWT() -> AnyPublisher<String, Error> {
        call()
            .mapError { AuthError.keyServiceError($0) }
            .tryMap { dict -> String in
                guard let jwt = dict["token"] as? String else {
                    throw AuthError.keyServiceError(.decodingFailed)
                }
                return jwt
            }
            .eraseToAnyPublisher()
    }

    private static var endpoint: HTTPSCallable = {
        let functions = Functions.functions()
        #if DEBUG
        if UserDefaults.standard.bool(forKey: "useEmulators") {
            let hostname = UserDefaults.standard.string(forKey: "localIP") ?? "localhost"
            functions.useEmulator(withHost: "http://\(hostname)", port: 5001)
        } else {
            Log("NOTE: not using emulators for Key Service.")
        }
        #endif
        return functions.httpsCallable("generateTiligToken")
    }()

    /// Wrap the cloud function call in a Combine Future publisher
    private static func call() -> Future<NSDictionary, KeyServiceError> {
        Future { promise in
            endpoint.call { result, error in
                if let mockedResponse = mockedResponse {
                    promise(mockedResponse)
                } else if let error = error {
                    promise(Result.failure(.responseError(error)))
                } else if let result = result {
                    guard let data = result.data as? NSDictionary else {
                        promise(Result.failure(.decodingFailed))
                        return
                    }
                    promise(Result.success(data))
                }
            }
        }
    }
}
