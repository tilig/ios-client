//
//  FirebaseAuthView.swift
//  Tilig
//
//  Created by Gertjan Jansen on 10/03/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import SwiftUI
import FirebaseAuthUI
import AuthenticationServices

/// Hacky way of customizing Firebase's FUIAuthPickerViewController
class CustomFUIAuthPickerViewController: FUIAuthPickerViewController {
    var isLogin: Bool = true {
        didSet { setupButtons() }
    }
    let screenHeight = UIScreen.main.bounds.size.height
    let screenWidth = UIScreen.main.bounds.size.width

    // This constant is used to calculate screens with height higher/lower to adjust subviews' paddings
    private var minimumScreenHeight: CGFloat { 850 }

    private var isScreenHeightLargerThanMinimum: Bool {
        screenHeight >= minimumScreenHeight
    }

    private var getButtonHeight: CGFloat = 50

    override func viewDidLoad() {
        super.viewDidLoad()

        setupButtons()
    }

    func setupButtons() {
        // Set clear bg color
        view.backgroundColor = .clear
        if let scrollView = view.subviews[0] as? UIScrollView {
            scrollView.backgroundColor = .clear
            let contentView = scrollView.subviews[0]
            contentView.backgroundColor = .clear
            scrollView.isScrollEnabled = false
        }

        // Customize buttons
        let buttons = view.subviews[0].subviews[0].subviews[0].subviews
        let viewWidth = UIDevice.current.userInterfaceIdiom == .phone ? (screenWidth - 32) : (screenWidth - 64)

        buttons[0].isHidden = true
        guard let googleImage = UIImage(named: "google") else { return }
        let googleButton = createButton(
            title: isLogin ? "Login with Google" : "Signup with Google",
            image: googleImage
        )
        googleButton.frame = CGRect(x: 0, y: 10, width: viewWidth, height: getButtonHeight)
        googleButton.backgroundColor = .white
        googleButton.addTarget(self, action: #selector(googleButtonAction), for: .touchUpInside)
        self.view.addSubview(googleButton)
        googleButton.translatesAutoresizingMaskIntoConstraints = false
        googleButton.layer.cornerRadius = 0.5 * googleButton.bounds.size.height
        NSLayoutConstraint.activate([
            googleButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            googleButton.heightAnchor.constraint(equalToConstant: getButtonHeight),
            googleButton.widthAnchor.constraint(equalToConstant: viewWidth)
        ])

        buttons[1].isHidden = true
        guard let appleImage = UIImage(named: "apple") else { return }
        let appleButton = createButton(
            title: isLogin ? "Login with Apple" : "Signup with Apple",
            image: appleImage
        )
        appleButton.frame = CGRect(x: 0, y: buttons[1].frame.minY+10, width: viewWidth, height: getButtonHeight)
        appleButton.backgroundColor = .white
        appleButton.addTarget(self, action: #selector(appleButtonAction), for: .touchUpInside)
        self.view.addSubview(appleButton)
        appleButton.translatesAutoresizingMaskIntoConstraints = false
        appleButton.layer.cornerRadius = 0.5 * appleButton.bounds.size.height
        NSLayoutConstraint.activate([
            appleButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            appleButton.heightAnchor.constraint(equalToConstant: getButtonHeight),
            appleButton.widthAnchor.constraint(equalToConstant: viewWidth),
            appleButton.centerYAnchor.constraint(equalTo: buttons[0].centerYAnchor, constant: 55)
        ])

        buttons[2].isHidden = true
        if FeatureFlagManager.isFlagEnabled(flag: .microsoftSignInEnabled) {
            guard let microsoftImage = UIImage(named: "microsoft") else { return }
            let microsoftButton = createButton(
                title: isLogin ? "Login with Microsoft" : "Signup with Microsoft",
                image: microsoftImage)
            microsoftButton.frame = CGRect(x: 0, y: buttons[2].frame.minY+10, width: viewWidth, height: getButtonHeight)
            microsoftButton.backgroundColor = .white
            microsoftButton.addTarget(self, action: #selector(microsoftButtonAction), for: .touchUpInside)
            self.view.addSubview(microsoftButton)
            microsoftButton.translatesAutoresizingMaskIntoConstraints = false
            microsoftButton.layer.cornerRadius = 0.5 * microsoftButton.bounds.size.height
            NSLayoutConstraint.activate([
                microsoftButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                microsoftButton.heightAnchor.constraint(equalToConstant: getButtonHeight),
                microsoftButton.widthAnchor.constraint(equalToConstant: viewWidth),
                microsoftButton.centerYAnchor.constraint(equalTo: buttons[0].centerYAnchor, constant: 120)
            ])
        }
    }

    func createButton(title: String, image: UIImage, textColor: UIColor = .black) -> UIButton {
        let button = UIButton(type: .system)
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.borderWidth = 1.0
        button.layer.shadowPath = UIBezierPath(rect: button.bounds).cgPath
        button.clipsToBounds = true
        button.contentVerticalAlignment = .center

        let image1Attachment = NSTextAttachment()
        let yValue = (UIFont.defaultButtonFont().capHeight - 22.5).rounded() / 2
        image1Attachment.bounds = CGRect(x: 0, y: yValue, width: 20, height: 20)
        image1Attachment.image = image
        let image1String = NSAttributedString(attachment: image1Attachment)

        let fullString = NSMutableAttributedString()
        fullString.append(image1String)
        let attributes = [NSAttributedString.Key.font: UIFont.defaultButtonFont(size: 16),
                          NSAttributedString.Key.foregroundColor: textColor]
        fullString.append(NSMutableAttributedString(string: "  " + title, attributes: attributes))
        button.setAttributedTitle(fullString, for: .normal)
        return button
    }

    @objc func googleButtonAction() {
        if let button = view.subviews[0].subviews[0].subviews[0].subviews[0] as? UIButton {
            button.sendActions(for: .touchUpInside)
        }
    }

    @objc func microsoftButtonAction() {
        if let button = view.subviews[0].subviews[0].subviews[0].subviews[2] as? UIButton {
            button.sendActions(for: .touchUpInside)
        }
    }

    @objc func appleButtonAction() {
        if let button = view.subviews[0].subviews[0].subviews[0].subviews[1] as? UIButton {
            button.sendActions(for: .touchUpInside)
        }
    }
}

/// SwiftUI wrapper around Firebase it's authViewController
/// (Which is a UINavigationController)
struct FirebaseAuth: UIViewControllerRepresentable {
    var authManager: AuthManager
    var isLogin: Bool

    func makeUIViewController(context: Context) -> some CustomFUIAuthPickerViewController {
        let vc = CustomFUIAuthPickerViewController(authUI: authManager.authUI)
        vc.isLogin = isLogin
        return vc
    }

    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        uiViewController.isLogin = isLogin
        uiViewController.authUI.delegate = authManager
    }
}

#if DEBUG
struct FirebaseAuthView_Previews: PreviewProvider {
    static var previews: some View {
        FirebaseAuth(authManager: AuthManager(), isLogin: true)
    }
}
#endif
