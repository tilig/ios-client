//
//  TiligTokenPublisher.swift
//  Tilig
//
//  Created by Gertjan Jansen on 29/04/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import Combine

struct TiligAuthTokens: Codable, Equatable, Hashable {
    var accessToken: Token
    var refreshToken: Token
}

struct TiligServiceAuthentication {
    static var urlSession = URLSession.shared

    /// Authenticate at Tilig Service and publish auth tokens
    static func authenticate(jwt: String) -> AnyPublisher<TiligAuthTokens, Error> {
        urlSession
            .dataTaskPublisher(for: .authenticate,
                               method: .post,
                               // Authentication endpoint uses Token instead of Bearer.
                               authorizationHeader: "Token \(jwt)")
            .decodeResponse(as: TiligAuthTokens.self)
            .mapError({ error in
                AuthError.tiligServiceConnectionFailed(error)
            })
            .eraseToAnyPublisher()
    }

    static var mockedRefreshTokenPublisher: AnyPublisher<TiligAuthTokens, Error>?

    /// Refresh the access (and refresh) token at Tilig Service
    static func refresh(refreshToken: Token) -> AnyPublisher<TiligAuthTokens, Error> {
        if let mockedPublisher = mockedRefreshTokenPublisher {
            return mockedPublisher
        }

        return urlSession
            .dataTaskPublisher(for: .refresh,
                               method: .post,
                               // Refresh token endpoint uses Token instead of Bearer.
                               authorizationHeader: "Token \(refreshToken.rawValue)")
            .decodeResponse(as: TiligAuthTokens.self)
            .mapError({ error in
                AuthError.tiligServiceConnectionFailed(error)
            })
            .eraseToAnyPublisher()
    }
}
