//
//  FirestoreTesting.swift
//  Tilig
//
//  Created by Gertjan Jansen on 28/04/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation

// MARK: testing

// TODO: move this to specs? 

// extension FirestorePrivateKeyManager {
//    
//    // MARK: Functions
//    
//    lazy var functions = Functions.functions()
//    
//    func firestoreTest() throws {
//        
//        print("exists?")
//        privateKeyExists { exists, error in
//            print("exists: \(String(describing: exists)) - error: \(String(describing: error))")
//        }
//        
//        collectionRef.whereField("uid", isEqualTo: firebaseUserID)
//            .getDocuments() { (querySnapshot, err) in
//                if let err = err {
//                    print("Error getting documents: \(err)")
//                } else {
//                    for document in querySnapshot!.documents {
//                        print("Testkey \(document.documentID) => \(document.data())")
//                    }
//                }
//            }
//    }
//    
//    // Function call to get JWT
//    func testFunctionCall() {
//        functions.useFunctionsEmulator(origin: "http://0.0.0.0:5001")
//        
//        let data = ["userId": "other-user-id"]
//        
//        print("data: \(data)")
//        
//        functions.httpsCallable("getPrivateKeyOfOtherUser").call(data) { result, error in
//            if let error = error as NSError? {
//                if error.domain == FunctionsErrorDomain {
//                    let code = FunctionsErrorCode(rawValue: error.code)
//                    let message = error.localizedDescription
//                    let details = error.userInfo[FunctionsErrorDetailsKey]
//                    
//                    OSLog.info("Failed: \(code!) - \(String(describing: message)) - \(String(describing: details))")
//                    return
//                }
//            }
//            
//            if let text = result?.data as? String {
//                OSLog.info("🔐 output: \(text)")
//            } else if let data = result?.data as? Data {
//                OSLog.info("data: \(String(data: data, encoding: .utf8)!)")
//            } else if let data = result?.data as? NSDictionary {
//                OSLog.info("dict: \(data)")
//            } else {
//                
//                OSLog.info("unknown output: \(result!.data)")
//            }
//        }
//    }
//    
//    func tryToFetchKeyOfOtherUser() {
//        let otherDocumentRef = collectionRef.document("other-user-id")
//        
//        otherDocumentRef.getDocument { (document, err) in
//            if let document = document, document.exists {
//                OSLog.info("😡 Fetched the key of another user!: \(document)")
//            } else {
//                OSLog.info("😅 Could not fetch the key of another user! - err: \(err!.localizedDescription)")
//            }
//        }
//    }
// }
