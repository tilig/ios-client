//
//  TiligAuth.swift
//  Tilig
//
//  Created by Gertjan Jansen on 22/03/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Firebase
import Combine
import FirebaseAuth
import Sentry
import Sodium

/// Auth Process steps (legacy - needs to be updated with the libsodium keypairs):
/// 1) Sign in to Firebase
/// 2) Fetch JWT token from Firebase Cloud Function (Key Service)
/// 3) Validate JWT token at Tilig Service and get refresh + access token
/// 4) Get or create Key Pair (from Firestore and Tilig Service)
/// 5) Get or update user profile from TS

struct AuthState: Codable, Equatable {
    /// User info: firebase ID, email, name
    var firebaseUser: CodeableFirebaseUser
    /// The access and refresh tokens for the Tilig Service
    var tiligAuthTokens: TiligAuthTokens
    /// The public and private key
    var keyPair: Box.KeyPair
    /// The legacy public and private key
    var legacyKeyPair: LegacyKeyPair
    /// The profile of this user, as stored on the TS
    var userProfile: UserProfile
}

struct LegacyAuthState: Codable, Equatable {
    /// User info: firebase ID, email, name
    var firebaseUser: CodeableFirebaseUser
    /// The access and refresh tokens for the Tilig Service
    var tiligAuthTokens: TiligAuthTokens
    /// The legacy public and private key
    var keyPair: LegacyKeyPair
    /// The profile of this user, as stored on the TS
    var userProfile: UserProfile
}

/// For keeping track of auth state during
/// the different steps of the auth process.
struct PartialAuthState {
    var firebaseUser: FirebaseUser?
    /// The Key Service JWT to authenticate at the Tilig Service
    /// Only required during authentication procedure.
    var keyServiceJWT: String?
    var tiligAuthTokens: TiligAuthTokens?
    var keyPair: Box.KeyPair?
    var legacyKeyPair: LegacyKeyPair?
    var userProfile: UserProfile?
}

/// Publisher of the complete authentication pipeline
extension AuthState {
    static func authenticateFromCache() -> AnyPublisher<AuthState, Error> {
        Log("🔑 Checking cached authState...", category: .authentication)

        // TODO: remove this when all iOS users have migrated to the new keypairs
        // If a legacy AuthState was cached, `cached` is nil (fails silently)
        // In that case, try to read it as `cachedLegacy` and migrate to the new AuthState type.
        if cached == nil, let legacyAuthState = cachedLegacy {
            Log("🔑 Found legacy cached auth state! Migrating...", category: .authentication)
            return Future {
                try await migrateCached(legacyAuthState: legacyAuthState)
            }
            .cacheAuthState()
            .mapError({ $0 as Error })
            .eraseToAnyPublisher()
        } else {
            if cached == nil {
                Log("🔑 No cached auth state.", category: .authentication)
            } else {
                Log("🔑 Cached (non-legacy) auth state.", category: .authentication)
            }
        }

        return Just(cached)
            // Note: this compactMap causes the subscription to complete without publishing any value
            // when `cached` is nil. It does not throw an error. Maybe that's not what we want.
            .compactMap { $0 }
            .refreshTokens()
            .cacheAuthState()
            .mapError({ $0 as Error })
            .eraseToAnyPublisher()
    }

    /// Use the legacy cached auth state to:
    /// 1) Get fresh tokens
    /// 2) Generate and store a new keypair
    /// 3) Return a non-legacy AuthState
    static func migrateCached(legacyAuthState: LegacyAuthState) async throws -> AuthState {
        // Get a fresh accessToken
        let refreshedTokens = try await TiligServiceAuthentication
            .refresh(refreshToken: legacyAuthState.tiligAuthTokens.refreshToken)
            .singleOutput()

        // Generate and store a new keypair
        let newKeyPair = try await Box.KeyPair.generateOrFetch(
            usingAccessToken: refreshedTokens.accessToken
        )

        // Map to our new AuthState type
        return AuthState(
            firebaseUser: legacyAuthState.firebaseUser,
            tiligAuthTokens: refreshedTokens,
            keyPair: newKeyPair,
            legacyKeyPair: legacyAuthState.keyPair,
            userProfile: legacyAuthState.userProfile
        )
    }

    /// Performs the full authentication pipeline or returns the cached authentication state, if present.
    // swiftlint:disable function_body_length
    static func authenticate(firebaseUser: FirebaseUser,
                             firebaseUserProfile: FirebaseUserProfile) -> AnyPublisher<AuthState, Error> {
        return Just(firebaseUser)
            .setFailureType(to: Error.self)
            .map { PartialAuthState(firebaseUser: $0) }
            .logPartialAuthState(step: 1)
            // 2: Get JWT from Key Service
            .combineLatest(KeyServiceTokenManager.fetchJWT())
            .map { partialAuthState, keyServiceJWT -> PartialAuthState in
                PartialAuthState(
                    firebaseUser: partialAuthState.firebaseUser,
                    keyServiceJWT: keyServiceJWT
                )
            }
            .logPartialAuthState(step: 2)
            // 3: Authenticate at Tilig Service
            .flatMap({ partialAuthState -> AnyPublisher<PartialAuthState, Error> in
                guard let jwt = partialAuthState.keyServiceJWT else {
                    return Fail(error: AuthError.partialAuthPipelineError)
                        .eraseToAnyPublisher()
                }
                return TiligServiceAuthentication
                    .authenticate(jwt: jwt)
                    .map { tiligAuthTokens in
                        PartialAuthState(
                            firebaseUser: partialAuthState.firebaseUser,
                            keyServiceJWT: partialAuthState.keyServiceJWT,
                            tiligAuthTokens: tiligAuthTokens
                        )
                    }
                    .eraseToAnyPublisher()
            })
            .logPartialAuthState(step: 3)
            // 4: Get or generate KeyPair(s)
            .flatMap({ partialAuthState -> AnyPublisher<PartialAuthState, Error> in
                guard let tiligAuthTokens = partialAuthState.tiligAuthTokens else {
                    return Fail(error: AuthError.partialAuthPipelineError)
                        .eraseToAnyPublisher()
                }

                return Box.KeyPair
                    .publish(usingAccessToken: tiligAuthTokens.accessToken)
                    .map { keyPair in
                        PartialAuthState(
                            firebaseUser: partialAuthState.firebaseUser,
                            keyServiceJWT: partialAuthState.keyServiceJWT,
                            tiligAuthTokens: partialAuthState.tiligAuthTokens,
                            keyPair: keyPair
                        )
                    }
                    .eraseToAnyPublisher()
            })
            .logPartialAuthState(step: 4)
            // 5: Legacy keypair
            .flatMap({ partialAuthState -> AnyPublisher<PartialAuthState, Error> in
                guard let firebaseUserID = partialAuthState.firebaseUser?.uid,
                      let tiligAuthTokens = partialAuthState.tiligAuthTokens else {
                    return Fail(error: AuthError.partialAuthPipelineError)
                        .eraseToAnyPublisher()
                }

                return LegacyKeyPair
                    .loadKeyPair(forFirebaseUserID: firebaseUserID, accessToken: tiligAuthTokens.accessToken)
                    .map { legacyKeyPair in
                        PartialAuthState(
                            firebaseUser: partialAuthState.firebaseUser,
                            keyServiceJWT: partialAuthState.keyServiceJWT,
                            tiligAuthTokens: partialAuthState.tiligAuthTokens,
                            keyPair: partialAuthState.keyPair,
                            legacyKeyPair: legacyKeyPair
                        )
                    }
                    .eraseToAnyPublisher()
            })
            .logPartialAuthState(step: 5)
            .eraseToAnyPublisher()
            // 6: Get or update user profile
            .syncUserProfile(firebaseUserProfile: firebaseUserProfile)
            // 7: Map to final auth state
            .tryMap { partialAuthState -> AuthState in
                guard let firebaseUser = partialAuthState.firebaseUser,
                      let tiligAuthTokens = partialAuthState.tiligAuthTokens,
                      let keyPair = partialAuthState.keyPair,
                      let legacyKeyPair = partialAuthState.legacyKeyPair,
                      let userProfile = partialAuthState.userProfile else {
                    throw AuthError.partialAuthPipelineError
                }
                return AuthState(
                    firebaseUser: firebaseUser.toCodable(),
                    tiligAuthTokens: tiligAuthTokens,
                    keyPair: keyPair,
                    legacyKeyPair: legacyKeyPair,
                    userProfile: userProfile
                )
            }
            .cacheAuthState()
            .eraseToAnyPublisher()
    }
}

extension Publisher where Output == PartialAuthState {
    fileprivate func logPartialAuthState(step: Int) -> Publishers.HandleEvents<Self> {
        handleEvents(receiveOutput: { (partialAuthState: PartialAuthState) in
            #if DEBUG
            Log("""
🔑 Partial Auth State [\(step)]:
1 - firebaseUserID: \(partialAuthState.firebaseUser?.uid ?? "nil")
2 - keyServiceJWT: \(partialAuthState.keyServiceJWT ?? "nil")
3 - tiligAuthTokens: \(String(describing: partialAuthState.tiligAuthTokens))
4 - keyPair: \(String(describing: partialAuthState.keyPair))
5 - legacyKeyPair: \(String(describing: partialAuthState.legacyKeyPair))
""", category: .authentication)
            #endif
        })
    }
}
