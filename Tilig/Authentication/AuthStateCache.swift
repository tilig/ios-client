//
//  AuthStateCache.swift
//  Tilig
//
//  Created by Gertjan Jansen on 10/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import Combine
import Valet

extension Publisher where Output == AuthState {
    func cacheAuthState() -> Publishers.HandleEvents<Self> {
        handleEvents(receiveOutput: { authState in
            do {
                try AuthState.cache(authState: authState)
            } catch {
                Log("Failed cache AuthState: \(error.localizedDescription)", type: .error)
            }
        })
    }
}

extension AuthState {
    /// The cached authentication state. This is important to reduce authentication requests
    /// as much as possible.
    static var cached: AuthState? {
        do {
            let data = try Valet.auth.object(forKey: ValetKeys.authState.rawValue)
            let decoder = JSONDecoder()
            return try decoder.decode(AuthState.self, from: data)
        } catch KeychainError.itemNotFound {
            return nil
        } catch {
            Log("Failed to decode AuthState: \(error.localizedDescription)", type: .error)
            return nil
        }
    }

    static var cachedLegacy: LegacyAuthState? {
        do {
            let data = try Valet.auth.object(forKey: ValetKeys.authState.rawValue)
            let decoder = JSONDecoder()
            return try decoder.decode(LegacyAuthState.self, from: data)
        } catch KeychainError.itemNotFound {
            return nil
        } catch {
            Log("Failed to decode LegacyAuthState: \(error.localizedDescription)", type: .error)
            return nil
        }
    }

    /// Store the given authState in Valet/KeyChain
    static func cache(authState: AuthState) throws {
        let encoder = JSONEncoder()
        let data = try encoder.encode(authState)
        try Valet.auth.setObject(data, forKey: ValetKeys.authState.rawValue)
        // Set a flag that the cache should be persisted
        KeyChainClearer.setPersistenceFlag()
    }

    /// Remove the cached authState.
    /// Be very careful with calling this method. Make sure to not render any views that depend
    /// on this state. First redirect to the login screen instead. See `AuthStateEnvironment` for
    /// an explanation why.
    static func clearCache() throws {
        try Valet.auth.removeAllObjects()
    }
}
