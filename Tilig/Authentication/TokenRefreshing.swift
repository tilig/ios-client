//
//  JWTExpiration.swift
//  Tilig
//
//  Created by Gertjan Jansen on 10/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import Combine
import Sentry

extension Publisher where Output == AuthState, Self.Failure == Never {
    /// An operator that works on a published AuthState and refreshes it
    /// Note it takes a non-failable ouput as upstream publisher and makes it failable,
    /// since the refresh API call can fail.
    func refreshTokens() -> AnyPublisher<AuthState, AuthError> {
        return setFailureType(to: AuthError.self) // iOS 13 fix
            .flatMap { authState -> AnyPublisher<AuthState, AuthError> in
                if authState.isExpired() {
                    // refresh the token and return the updated state
                    return TiligServiceAuthentication
                        .refresh(refreshToken: authState.tiligAuthTokens.refreshToken)
                        .map { refreshedTokens in
                            AuthState(
                                firebaseUser: authState.firebaseUser,
                                tiligAuthTokens: refreshedTokens,
                                keyPair: authState.keyPair,
                                legacyKeyPair: authState.legacyKeyPair,
                                userProfile: authState.userProfile
                            )
                        }
                        .mapError { error in
                            Log("Refresh failed", type: .error)
                            // Send cause of refresh failure to Sentry
                            SentrySDK.capture(error: error)
                            // Raise a refreshFailed error
                            return AuthError.refreshFailed(error)
                        }
                        // Cache the new auth state
                        .eraseToAnyPublisher()
                }
                return Just(authState)
                    .setFailureType(to: AuthError.self)
                    .eraseToAnyPublisher()
            }
            // Simulating refresh token failures by setting the `simulateFailingTokenRefresh` run flag
            .tryMap { state -> AuthState in
                #if DEBUG
                let simulateFailure = UserDefaults.standard.integer(forKey: "simulateFailingTokenRefresh")
                if simulateFailure > 0 {
                    // Fail every 1/x of requests
                    if Int.random(in: 0...simulateFailure) == 1 {
                        throw AuthError.refreshFailed(AuthError.unauthorized)
                    }
                }
                #endif
                return state
            }
            // Necessary because `tryMap` (above) can only throw generic `Error` types
            .mapError { error -> AuthError in
                return error as! AuthError
            }
            .eraseToAnyPublisher()
    }
}

extension AuthState {
    func isExpired() -> Bool {
        tiligAuthTokens.accessToken.isExpired()
    }
}
