//
//  UserProfile.swift
//  Tilig
//
//  Created by Gertjan Jansen on 14/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import Combine

enum UserProfileError: Error {
    case syncFailed
}

struct EncryptedKeyPair: Codable, Equatable {
    var encryptedPrivateKey: String
    var publicKey: String
    // Commented out because not used atm:
    // var kid: String
    // var keyType: String
}

struct UserProfile: Codable, Equatable {
    var id: UUID
    var email: String
    var displayName: String
    var givenName: String?
    var familyName: String?
    var picture: URL?
    var locale: String?
    var publicKey: LegacyPublicKey?
    var keypair: EncryptedKeyPair?
    var applicationSettings: ApplicationSettings?
    var userSettings: UserSettings?
}

struct ApplicationSettings: Codable, Equatable {
    var clientSignedUp: String?
    var clientsSignedIn: [String]
}

struct UserSettings: Codable, Equatable {
    var onboarding: OnboardingSetup?
    var ios: OnboardingSetup?
}

struct OnboardingSetup: Codable, Equatable {
    var completed: Bool
    var dismissed: Bool
    var stepsCompleted: [String]
}

extension UserProfile {
    static var mockedPublisher: AnyPublisher<UserProfile, Error>?

    static func save(firebaseUser: FirebaseUser,
                     firebaseUserProfile: FirebaseUserProfile,
                     accessToken: Token) -> AnyPublisher<UserProfile, Error> {
        if let mockedPublisher = mockedPublisher {
            return mockedPublisher
        }

        struct UserWrapper: Encodable {
            var user: ProfileData
        }
        struct ProfileData: Encodable {
            var displayName: String?
            var givenName: String?
            var familyName: String?
            var picture: URL?
            var locale: String?
            var providerID: String?
        }
        let postData = UserWrapper(
            user: ProfileData(
                displayName: firebaseUser.displayName,
                givenName: firebaseUserProfile.givenName,
                familyName: firebaseUserProfile.familyName,
                picture: URL(string: firebaseUserProfile.picture ?? ""),
                locale: firebaseUserProfile.locale,
                providerID: firebaseUserProfile.providerID
            )
        )

        return URLSession.current
            .dataTaskPublisher(for: .profile,
                               method: .put,
                               encodable: postData,
                               authorizationHeader: "Bearer \(accessToken.rawValue)")
            .decodeResponse(as: UserProfile.self)
            .eraseToAnyPublisher()
    }
}

// MARK: AuthState pipeline step

extension Publisher where Output == PartialAuthState, Self.Failure == Error {
    /// An operator that receives a partial auth state that should contain at minimum
    /// a `firebaseUser` and `accessToken`, and appends the partial auth state with
    /// a `userProfile`.
    func syncUserProfile(firebaseUserProfile: FirebaseUserProfile) -> AnyPublisher<PartialAuthState, Error> {
        flatMap({ partialAuthState -> AnyPublisher<PartialAuthState, Error> in
            guard let firebaseUser = partialAuthState.firebaseUser,
                  let accessToken = partialAuthState.tiligAuthTokens?.accessToken else {
                return Fail(error: AuthError.partialAuthPipelineError)
                    .eraseToAnyPublisher()
            }

            return UserProfile
                .save(firebaseUser: firebaseUser,
                      firebaseUserProfile: firebaseUserProfile,
                      accessToken: accessToken)
                .map({ userProfile in
                    PartialAuthState(
                        firebaseUser: firebaseUser,
                        keyServiceJWT: partialAuthState.keyServiceJWT,
                        tiligAuthTokens: partialAuthState.tiligAuthTokens,
                        keyPair: partialAuthState.keyPair,
                        legacyKeyPair: partialAuthState.legacyKeyPair,
                        userProfile: userProfile
                    )
                })
                .eraseToAnyPublisher()
        })
        .eraseToAnyPublisher()
    }
}
