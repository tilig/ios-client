//
//  AccessToken.swift
//  Tilig
//
//  Created by Gertjan Jansen on 29/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Combine

extension AuthState: TokenProvider {
    /// Async method that returns an access token, and refreshes if necesary.
    /// Throws an error if not authenticated, or refreshing fails.
    func getAccessToken() async throws -> Token {
        do {
            return try await AuthState
                .authenticateFromCache()
                .map { $0.tiligAuthTokens.accessToken }
                .singleOutput()
        } catch {
            // This is a workaround / patch for an issue where
            // AuthState.authenticateFromCache() does not throw an error
            // when AuthState.cached is nil, which results in `singleOutput()`
            // throwing a MissingOutputError, which is not very descriptive.
            // Therefore map it to an AuthError.unauthorized here.
            throw AuthError.unauthorized
        }
    }
}
