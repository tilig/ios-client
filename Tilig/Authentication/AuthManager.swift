//
//  AuthManager.swift
//  Tilig
//
//  Created by Gertjan Jansen on 11/05/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import AuthenticationServices
import Combine
import Firebase
import FirebaseAuth
import FirebaseAuthUI
import FirebaseGoogleAuthUI
import FirebaseOAuthUI
import FirebaseAnalytics
import SwiftUI
import Sentry

@MainActor class AuthManager: NSObject, ObservableObject, FUIAuthDelegate {
    private var cacheCancellable: AnyCancellable?
    private var authenticateCancellable: AnyCancellable?

    @Published var authState: AuthState? = AuthState.cached
    @Published var isAuthenticating = false

    override init() {
        super.init()
        setupFirebaseAuth()
        authenticateFromCache()
    }

    func authenticateFromCache() {
        isAuthenticating = true
        cacheCancellable = AuthState
            .authenticateFromCache()
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] completion in
                guard let self = self else { return }
                self.isAuthenticating = false
                switch completion {
                case .finished:
                    return
                case .failure(let error):
                    Log("🔑🤯 Cached pipeline: \(error.localizedDescription)", category: .authentication, type: .error)
                    // First clear the published authState
                    self.authState = nil
                    // Then clear the cached auth state.
                    // This order is important to make sure the route change was triggered before
                    // we clear the cache. If we don't do this, some views might still try to reference
                    // the cachced AuthState.
                    try? AuthState.clearCache()
                    // Alert the user
                    if self.shouldAlert(error: error) {
                        AlertItem(
                            title: Text("Sign in failed"),
                            message: Text("We couldn't sign you in. \(error.localizedDescription)"),
                            dismissButton: .default(Text("Ok"))
                        ).post()
                    }
                }
            }, receiveValue: { [weak self] authState in
                Log("🔑 Cached pipeline ✅ success: \(authState)", category: .authentication)
                self?.authState = authState
                SentrySDK.setUser(userProfile: authState.userProfile)
            })
    }

    func authenticate(firebaseUser: FirebaseUser, firebaseUserProfile: FirebaseUserProfile) {
        withAnimation(.spring()) {
            isAuthenticating = true
        }
        authenticateCancellable = AuthState
            .authenticate(firebaseUser: firebaseUser, firebaseUserProfile: firebaseUserProfile)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                guard let self = self else { return }
                withAnimation(.spring()) {
                    self.isAuthenticating = false
                }
                switch completion {
                case .finished:
                    return
                case .failure(let error):
                    self.authState = nil
                    self.handle(error: error)
                }
            } receiveValue: { [weak self] authState in
                Log("🗝 Auth Pipeline success, auth state: \(authState)", category: .authentication)
                self?.authState = authState
                self?.setUserContexts(authState: authState)
            }
    }

    func handle(error: Error) {
        Log("🗝 Auth Pipeline Failed: \(error.localizedDescription)", type: .error)
        guard shouldAlert(error: error) else { return }
        SentrySDK.capture(error: error)
        AlertItem(
            title: Text("Sign in failed"),
            message: Text("We couldn't sign you in. \(error.localizedDescription)"),
            dismissButton: .default(Text("Ok"))
        ).post()
    }

    /// Should the user be alerted of this error?
    /// If false, then silently ignore it
    func shouldAlert(error: Error) -> Bool {
        if let authError = error as? AuthError, case .signedOutFromFirebase = authError {
            return false
        }
        return true
    }

    // MARK: Firebase Auth sign in listener

    var authUI: FUIAuth!

    func setupFirebaseAuth() {
        authUI = FUIAuth.defaultAuthUI()
        authUI.delegate = self

        // Custom init to set custom params as FirebaseAuthUI
        // does not allow you to pass custom params through its
        // default FUIOAuth.microsoftAuthProvider() method
        let microsoftProvider = FUIOAuth.init(
            authUI: authUI,
            providerID: "microsoft.com",
            buttonLabelText: "Sign in with Microsoft",
            shortName: "Microsoft",
            buttonColor: UIColor(red: 18, green: 18, blue: 18, alpha: 1),
            iconImage: UIImage(named: "microsoft")!,
            scopes: ["user.readwrite"],
            customParameters: ["prompt": "select_account"],
            loginHintKey: "login_hint"
        )

        authUI.providers = [
            FUIGoogleAuth(authUI: authUI, scopes: [kGoogleUserInfoProfileScope]),
            FUIOAuth.appleAuthProvider(),
            microsoftProvider
        ]
    }

    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        if let error = error {
            if let err = error as? ASAuthorizationError, err.code == .canceled {
                Log("User did cancel Apple authorization.", category: .authentication)
                return
            }

            let errCode = AuthErrorCode(_nsError: error as NSError)

            if errCode.code == .accountExistsWithDifferentCredential {
                AuthError.accountExistsWithDifferentProvider.raiseGlobalAlert()

            } else if errCode.code != .webContextCancelled {
                AuthError.unexpectedFirebaseResult.raiseGlobalAlert()
            }
            return
        }
        guard let firebaseUser = authDataResult?.user,
              let additionalUserInfo = authDataResult?.additionalUserInfo else {
                let error = AuthError.unexpectedFirebaseResult
                error.raiseGlobalAlert()
                return
        }

        // Send verification mail if user isnt verified
        if !firebaseUser.isEmailVerified {
            firebaseUser.sendEmailVerification()

            let error = AuthError.userEmailNotVerified
            // Set a different title than the title we would get with .raiseGlobalAlert()
            let alertItem = AlertItem(
                title: Text("Verify your email address"),
                message: Text(error.localizedDescription),
                dismissButton: .default(Text("Ok"))
            )
            NotificationCenter.default.post(name: .globalError, object: alertItem)
            return
        }

        let firebaseUserProfile = FirebaseUserProfile(additionalUserInfo: additionalUserInfo)
        firebaseUserProfile.debugLog()
        authenticate(firebaseUser: firebaseUser, firebaseUserProfile: firebaseUserProfile)
    }

    /// Set user contexts for 3rd party tools or libraries we use.
    /// Should be called after successful authentication.
    func setUserContexts(authState: AuthState) {
        // Sentry user context
        SentrySDK.setUser(userProfile: authState.userProfile)
        // Firebase analytics for conversion tracking
        Analytics.setUserID(authState.userProfile.email)
    }

    func signOut() throws {
        // Clear local cache
        try AuthState.clearCache()
        // Signout from firebase
        try Auth.auth().signOut()
        // Publish nil authState
        authState = nil
        // Clear Sentry User
        SentrySDK.setUser(nil)
        // Reset all user defaults
        UserDefaults.standard.reset()
        // Clear Firebase Analytics user state
        Analytics.setUserID(nil)
    }

    /// Deletes the user, both from backend and firebase, then signs out
    func deleteUser() async throws {
        guard let authState else { return }

        try await authState.legacyKeyPair.removePublicAndPrivateKeys(
            firebaseUserID: authState.firebaseUser.uid,
            tokenProvider: authState
        )

        try signOut()
    }
}
