//
//  SettingsViewModel.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 12/03/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import Combine
import Toast
import UIKit
import Sentry

@MainActor class SettingsViewModel: ObservableObject {
    init(authManager: AuthManager, userProfile: UserProfile) {
        self.authManager = authManager
        self.email = userProfile.email
        self.name = userProfile.displayName
    }

    var authManager: AuthManager
    var email: String
    var name: String

    // swiftlint:disable line_length
    var edgeURL = "https://microsoftedge.microsoft.com/addons/detail/tilig-password-manager/gidnnnbibkapnoapddmkepgbjcleekbc"
    var chromeURL = "https://chrome.google.com/webstore/detail/tilig-password-manager/fnnhineegblcmjlhmmogickjhhobgpjo"
    var operaURL = "https://chrome.google.com/webstore/detail/tilig-password-manager/fnnhineegblcmjlhmmogickjhhobgpjo"
    var appStoreURL = "https://apps.apple.com/app/tilig-password-manager/id1504118993"
    var playStoreURL = "https://play.google.com/store/apps/details?id=com.tilig.android"

    @Published var isPresentingWebView = false
    @Published var presentedWebViewTitle: String?
    @Published var presentedWebViewURL: URL?

    @Published private(set) var isDeleting: Bool = false

    private var cancellable: AnyCancellable?

    func openURL(_ title: String? = nil, urlString: String) {
        presentedWebViewURL = URL(string: urlString)
        presentedWebViewTitle = title
        isPresentingWebView = true
    }

    func signOut() {
        // Send event before actually signing out, to prevent a race condition with Mixpanel state.
        TrackingEvent.signout.send()
        displaySignOutToast()
        do {
            try self.authManager.signOut()
        } catch {
            error.raiseGlobalAlert()
            SentrySDK.capture(error: error)
        }
    }

    func deleteProfile() {
        isDeleting = true

        Task {
            do {
                try await self.authManager.deleteUser()
            } catch {
                error.raiseGlobalAlert()
                SentrySDK.capture(error: error)
                isDeleting = false
            }

            displayDeletedItemToast()
        }
    }

    func displayDeletedItemToast() {
        let toast = Toast.default(
            image: UIImage(systemName: "exclamationmark.triangle.fill")!,
            title: "Your account was deleted successfully"
        )
        toast.show()
    }

    func displaySignOutToast() {
        Toast.text("You signed out").show()
    }
}
