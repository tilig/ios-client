//
//  EditItemViewModel.swift
//  Tilig
//
//  Created by Gertjan Jansen on 25/10/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//
import Foundation
import Combine
import Sentry
import UIKit
import Toast

enum UpdateTrigger {
    case saveButton
    case restorePassword
    case twoFactorChange
}

@MainActor class EditItemViewModel: ObservableObject {
    init(id: UUID, itemWrapper: ItemWrapper, crypto: Crypto, tokenProvider: TokenProvider) {
        self.id = id
        self.itemWrapper = itemWrapper
        self.initialItem = itemWrapper

        self.crypto = crypto
        self.client = Client(tokenProvider: tokenProvider)

        self.publishProperties()
    }
    private var client: Client
    private var crypto: Crypto

    private(set) var initialItem: ItemWrapper
    private(set) var itemWrapper: ItemWrapper {
        didSet {
            // Prevent a loop
            if oldValue != itemWrapper {
                publishProperties()
            }
        }
    }
    private var debounceHelper = DebounceHelper()

    private func setInitialItem(_ initialItem: ItemWrapper) {
        self.initialItem = initialItem
        self.itemWrapper = initialItem
    }

    private func publishProperties() {
        name = itemWrapper.name
        notes = itemWrapper.notes
        url = itemWrapper.url
        username = itemWrapper.username
        password = itemWrapper.password
        totp = itemWrapper.totp
        brand = itemWrapper.brand
        ssid = itemWrapper.ssid
        ccnumber = itemWrapper.ccnumber?.separate(every: 4)
        ccholder = itemWrapper.ccholder
        ccexp = itemWrapper.ccexp
        cvv = itemWrapper.cvv
        pin = itemWrapper.pin
        zipcode = itemWrapper.zipcode
    }

    var itemUpdated: AnyPublisher<(ItemWrapper, UpdateTrigger), Never> {
        itemUpdatedSubject.eraseToAnyPublisher()
    }
    private var itemUpdatedSubject = PassthroughSubject<(ItemWrapper, UpdateTrigger), Never>()

    @Published var isSubmitting = false

    @Published var id: UUID
    @Published var name: String? {
        didSet { itemWrapper.name = name }
    }
    @Published var url: URL? {
        didSet {
            itemWrapper.url = url
            if url != oldValue {
                updateBrand()
            }
        }
    }
    @Published var username: String? {
        didSet { itemWrapper.username = username }
    }
    @Published var password: String? {
        didSet { itemWrapper.password = password }
    }
    @Published var totp: String? {
        didSet { itemWrapper.totp = totp }
    }
    @Published var ssid: String? {
        didSet { itemWrapper.ssid = ssid }
    }
    @Published var notes: String? {
        didSet { itemWrapper.notes = notes }
    }
    @Published var brand: Brand? {
        didSet { itemWrapper.brand = brand }
    }
    @Published var ccnumber: String? {
        didSet {
            itemWrapper.ccnumber = ccnumber?.replacingOccurrences(of: " ", with: "")
        }
    }
    @Published var ccholder: String? {
        didSet { itemWrapper.ccholder = ccholder }
    }
    @Published var ccexp: String? {
        didSet { itemWrapper.ccexp = ccexp }
    }
    @Published var cvv: String? {
        didSet { itemWrapper.cvv = cvv }
    }
    @Published var pin: String? {
        didSet { itemWrapper.pin = pin }
    }
    @Published var zipcode: String? {
        didSet { itemWrapper.zipcode = zipcode }
    }

    var hasChanges: Bool { itemWrapper != initialItem }

    func undoChanges() {
        itemWrapper = initialItem
    }

    func updateOTP(_ newTOTP: String) async {
        guard totp != newTOTP else { return }
        totp = newTOTP
        await update(trigger: .twoFactorChange)
    }

    func restorePassword(_ version: HistoricVersion) async {
        password = version.value
        await update(trigger: .restorePassword)
        Toast.text("Password restored").show()
    }

    func update(trigger: UpdateTrigger) async {
        if let ccnumber = itemWrapper.ccnumber {
            itemWrapper.info = String(ccnumber.suffix(4))
        }

        if isSubmitting { return }

        isSubmitting = true

        do {
            let encryptedItem = try crypto.encrypt(itemWrapper: itemWrapper)

            let updatedEncryptedItem = try await client.updateItem(encryptedItem)

            let updatedItemWrapper = try crypto.decrypt(fullItem: updatedEncryptedItem)

            // Then publish that
            self.itemUpdatedSubject.send((updatedItemWrapper, trigger))

            setInitialItem(updatedItemWrapper)
        } catch {
            error.raiseGlobalAlert()
        }

        isSubmitting = false
    }

    /// Run an async task that updates the brand that belongs to the current URL.
    ///
    /// Checks for cancellation after doing the request.
    func updateBrand() {
        debounceHelper.debounce(duration: 1.0) {
            var brand: Brand?
            if let url = self.url {
                // Silently fail, but set the brand to `nil`. Don't alert the user.
                brand = try? await self.client.getBrand(for: url)
            }
            // If the task was cancelled during the query, don't do anything with the response.
            if Task.isCancelled { return }
            self.brand = brand
        }
    }
}
