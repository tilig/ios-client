//
//  BiometricViewModel.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 23/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI
import Valet
import Sentry
import LocalAuthentication

@MainActor class BiometricViewModel: ObservableObject {
    private(set) var biometricHelper = BiometricHelper()

    /// Is biometrics supported and configured on device level and allowed for this app?
    var canEvaluate: Bool { biometricHelper.canEvaluate }

    /// Does it support Face ID, Touch ID or none?
    var supportedBiometricType: LABiometryType { biometricHelper.biometryType }

    /// Is the biometric lock enabled by the user?
    @Published private(set) var isEnabled: Bool

    /// Is the biometric lock active?
    @Published private(set) var isLocked: Bool = false

    /// This is used to show a warning notice as soon as the user denies biometrics for the app
    @Published private(set) var biometryPermissionDenied: Bool

    init(biometricHelper: BiometricHelper = BiometricHelper()) {
        self.biometricHelper = biometricHelper
        self.biometryPermissionDenied = biometricHelper.biometryPermissionDenied

        do {
            let data = try Valet.auth.object(forKey: ValetKeys.biometricLockEnabled.rawValue)
            isEnabled = try JSONDecoder().decode(Bool.self, from: data)
        } catch {
            isEnabled = false
        }
    }

    func toggleEnabled() {
        Task {
            do {
                if isEnabled {
                    try disable()
                } else {
                    try await enable()
                }
            } catch {
                if case LAError.biometryNotAvailable = error {
                    // Trigger a rerender of the view
                    biometryPermissionDenied = true
                    return
                }
                handle(error: error)
            }
        }
    }

    /// If the biometric lock is enabled, lock it. Otherwise, do nothing.
    func lockIfEnabled() {
        if isEnabled { isLocked = true }
    }

    private func unlock() {
        Task {
            // Wait until the native animation finishes
            try? await Task.sleep(nanoseconds: 500_000_000)
            // Then trigger our own unlock animation
            isLocked = false
        }
    }

    private func enable() async throws {
        if try await biometricHelper.authenticateBiometric() {
            try setEnabled(true)
            TrackingEvent.biometricEnabled.send()
        }
    }

    private func disable() throws {
        try setEnabled(false)
        TrackingEvent.biometricDisabled.send()
    }

    /// Try to authenticate with biometrics, and unlock if it succeeds
    func tryToUnlock() {
        if !isLocked { return }

        Task {
            do {
                if try await biometricHelper.authenticateBiometric() {
                    unlock()
                }
            } catch {
                if case LAError.biometryNotAvailable = error {
                    return
                }
                if let bioError = error.asBiometricError(), bioError == .authenticationCancelled {
                    TrackingEvent.biometricUnlockCanceled.send()
                    return
                }
                handle(error: error)
            }
        }
    }

    private func setEnabled(_ bool: Bool) throws {
        let boolData = try JSONEncoder().encode(bool)
        try Valet.auth.setObject(boolData, forKey: ValetKeys.biometricLockEnabled.rawValue)
        isEnabled = bool
    }

    private func handle(error: Error) {
        Log("🔑 Biometric error: \(error.localizedDescription)", type: .error)
        SentrySDK.capture(error: error)
        error.raiseGlobalAlert()
    }
}
