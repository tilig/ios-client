//
//  TwoFactorViewModel.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import Combine
import UIKit
import Toast
import AVFoundation

class TwoFactorViewModel: QRCameraDelegate, ObservableObject, Activatable {

    private var twoFAGenerator = TwoFactorGenerator()
    private var lastValidQRCode: String?

    @Published var otpPassword: String?
    @Published var remainingSeconds: Int?
    @Published var showScanError = false
    @Published var showScanCompletedView = false
    @Published var showManualView = false
    @Published var showScannerView = false
    @Published var cameraEnabled = false
    @Published var showLearnMoreView = false
    @Published var showCameraAlert = false

    var itemName: String?
    var website: URL?
    var label: String { itemName ?? website?.absoluteString ?? "this website" }

    @Published var plainOTPToken: String?
    var hasOneTimePassword: Bool { !plainOTPToken.isEmptyOrNil }

    var timer = Timer.publish(every: 1, on: .main, in: .common)
    var cancellables = Set<AnyCancellable>()

    init(name: String?, website: URL?, plainOTPToken: String?) {
        self.itemName = name
        self.website = website
        self.plainOTPToken = plainOTPToken
    }

    func updateItem(name: String?, website: URL?) {
        self.itemName = name
        self.website = website
    }

    /// formViewModel isnt activated at time of initialisation
    /// this is to get get the updated code by the time it is activated
    func activate() {
        if let plainOTPToken = self.plainOTPToken, !plainOTPToken.isEmpty {
            self.updateRemainingSeconds()
            getUpdatedCode(plainOTPToken)
            startTimer()
        }
    }

    func deactivate() {
        cancellables = []
        plainOTPToken = nil
        lastValidQRCode = nil
        timer.connect().cancel()
    }

    func startTimer() {
        timer
            .autoconnect()
            .sink { _ in
                if self.hasOneTimePassword {
                    self.updateRemainingSeconds()
                    guard let lastValidQRCode = self.lastValidQRCode else {
                        return Log("Can't update", type: .error)
                    }
                    if self.twoFAGenerator.refreshCodeIfNecessary() {
                        self.getUpdatedCode(lastValidQRCode)
                    }
                }
            }
            .store(in: &cancellables)
    }

    func onQRCodeFound(_ code: String) {
        TrackingEvent.twoFactorQRCodeScanned.send()
        if showScanCompletedView || code == lastValidQRCode { return }
        getUpdatedCode(code)
        startTimer()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.showScanCompletedView = true
        })
    }

    func updateItemOTP() {
        plainOTPToken = self.lastValidQRCode
    }

    func copyOneTimePassword() {
        if let otpPassword = otpPassword {
            UIPasteboard.general.string = otpPassword
            TrackingEvent.otpCopied.send()
            Toast.text("Two-Factor Code Copied").show()
        }
    }

    func updateRemainingSeconds() {
        remainingSeconds = twoFAGenerator.getRemainingSeconds()
    }

    func getUpdatedCode(_ code: String) {
        guard let otp = twoFAGenerator.getOneTimePassword(code: code) else {
            showScanError = true
            TrackingEvent.twoFactorIncorrectCodeEntered.send()
            assertionFailure("invalid code")
            return
        }
        self.otpPassword = otp
        self.lastValidQRCode = code
        updateItemOTP()
    }

    func onManualSecretCodeEntered(secret: String) {
        TrackingEvent.twoFactorManualCodeEntered.send()
        let label = itemName?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        let code = "otpauth://totp/\(label)?secret=\(secret)".replacingOccurrences(of: " ", with: "")
        getUpdatedCode(code)
        startTimer()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.showScanCompletedView = true
        })
    }

    func requestCameraPermission() {
        AVCaptureDevice.requestAccess(for: .video) { granted in
            DispatchQueue.main.async {
                self.cameraEnabled = granted
                self.showCameraAlert = !granted
                if self.cameraEnabled {
                    TrackingEvent.twoFactorCameraEnabled.send()
                }
            }
        }
    }

    func presentScannerView() {
        self.showScannerView = true
    }

    func presentLearnMoreView() {
        self.showLearnMoreView = true
    }

    func setCameraEnabled(_ isEnabled: Bool) {
        self.cameraEnabled = isEnabled
    }

    func presentManualView() {
        self.showManualView = true
    }

    func presentScanCompletedView() {
        self.showScanCompletedView = true
    }

    func presentCameraAlert() {
        self.showCameraAlert = true
    }

    func presentScanErrorView() {
        self.showScanError = true
    }

    // dismiss views

    func dismissScanErrorView() {
        self.showScanError = false
    }

    func dismissScanCompletedView() {
        showScanCompletedView = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.dismissScannerView()
        })
    }

    func dismissManualView() {
        self.showManualView = false
    }

    func dismissLearnMoreView() {
        self.showLearnMoreView = false
    }

    func dismissCameraAlert() {
        self.showCameraAlert = false
    }

    func dismissScannerView() {
        self.showScannerView = false
    }
}
