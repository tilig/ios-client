//
//  ItemViewModel.swift
//  Tilig
//
//  Created by Gertjan Jansen on 04/06/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import UIKit
import Toast
import Sodium

@MainActor class ItemViewModel: ObservableObject {
    init(itemWrapper: ItemWrapper) {
        self.itemWrapper = itemWrapper
        // TODO: fix force-unwrap
        self.itemType = itemWrapper.itemType!
    }

    @Published private(set) var itemWrapper: ItemWrapper

    @Published var itemType: ItemType

    func update(with itemWrapper: ItemWrapper) {
        self.itemWrapper = itemWrapper
    }

    func activate() {
        TrackingEvent.loginViewed.send()
    }

    func copy(field: ItemField) {
        let value = itemWrapper[keyPath: field.keyPath]
        guard let value = value else { return }
        UIPasteboard.general.string = value
        field.copyEvent.send()
        displayToast(text: "\(field.label.capitalized) copied")
    }

    private func displayToast(text: String) {
        Toast.text(text).show()
    }

    var passwordVersions: [HistoricVersion] {
        itemWrapper.details.history.reversed()
    }

    func copyPasswordVersion(_ version: HistoricVersion) {
        UIPasteboard.general.string = version.value
        TrackingEvent.passwordCopied.send()
        displayToast(text: "Password copied")
    }
}
