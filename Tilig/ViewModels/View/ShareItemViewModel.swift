//
//  ItemViewModel.swift
//  Tilig
//
//  Created by Gertjan Jansen on 04/06/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import UIKit
import Toast
import Sodium
import Combine

@MainActor class ShareItemViewModel: ObservableObject {
    init(itemWrapper: ItemWrapper,
         crypto: Crypto,
         tokenProvider: TokenProvider,
         userProfile: UserProfile
    ) {
        self.itemWrapper = itemWrapper
        self.userEmail = userProfile.email

        self.itemType = itemWrapper.itemType!
        self.client = Client(tokenProvider: tokenProvider)
        self.crypto = crypto

        self.folderMemberships = []
        self.folderMemberships.append(contentsOf: itemWrapper.folder?.folderMemberships ?? [])
        self.folderMemberships.append(contentsOf: itemWrapper.folder?.folderInvitations ?? [])

        getContacts()
    }

    @Published private(set) var itemWrapper: ItemWrapper
    @Published private var client: Client
    @Published private var crypto: Crypto

    @Published var itemType: ItemType

    @Published var isSharing = false
    @Published var showWarning = false
    @Published var warningText: String?
    @Published var userEmail: String?
    @Published var recipientEmail: String = "" {
        didSet { filterContacts() }
    }

    @Published var folderMemberships: [FolderMembership] = []
    @Published private var contacts = [FolderMembershipUser]() {
        didSet {
            filterContacts()
        }
    }

    var itemUpdated: AnyPublisher<ItemWrapper, Never> {
        itemUpdatedSubject.eraseToAnyPublisher()
    }
    private var itemUpdatedSubject = PassthroughSubject<ItemWrapper, Never>()

    @Published private(set) var suggestions = [FolderMembershipUser]()

    @Published var filteredContacts: [FolderMembershipUser] = []

    var hasOtherMemberships: Bool {
        folderMemberships.filter { $0.isMe != true }.count > 0
    }

    var hasEnteredValidEmail: Bool { recipientEmail.isValidEmail }

    func update(with itemWrapper: ItemWrapper) {
        self.itemWrapper = itemWrapper
    }

    private func displayToast(text: String) {
        Toast.text(text).show()
    }

    func filterContacts() {
        filteredContacts = contacts.filter { !folderMemberships.map({ $0.user }).contains($0) }
            .filter {
                recipientEmail.isEmpty ||
                ($0.email).lowercased().contains(recipientEmail.lowercased()) ||
                ($0.displayName ?? "").lowercased().contains(recipientEmail.lowercased())
            }
    }

    var sharingCrypto: SharingCrypto { SharingCrypto(crypto: crypto) }

    func submitShareEmail() {
        // Lock
        if isSharing { return }
        isSharing = true
        showWarning = false

        Task {
            do {
                guard !recipientEmail.isEmpty, recipientEmail.isValidEmail else {
                    throw SharingError.invalidEmail
                }

                guard recipientEmail != userEmail else {
                    throw SharingError.cannotShareWithYourself
                }

                guard !folderMemberships.map({ $0.user.email }).contains(recipientEmail) else {
                    throw SharingError.alreadySharedWithUser
                }

                guard let currentUserId = AuthState.cached?.userProfile.id else {
                    throw SharingError.invalidUser
                }

                let sharingService = SharingService(
                    client: client,
                    crypto: crypto,
                    currentUserId: currentUserId
                )

                let folder = try await sharingService.share(
                    item: itemWrapper.encryptedItem,
                    name: itemWrapper.overview.name,
                    withEmail: recipientEmail
                )

                self.itemWrapper.folder = folder
                self.folderMemberships = []
                self.folderMemberships.append(contentsOf: folder.folderMemberships)
                self.folderMemberships.append(contentsOf: folder.folderInvitations)
                self.isSharing = false
                self.recipientEmail = ""

                self.itemUpdatedSubject.send(itemWrapper)
            } catch {
                error.raiseGlobalAlert()
                // Disabled the error UI again because it has layout issues
//                if let sharingError = error as? SharingError,
//                    .publicKeyNotFound == sharingError ||
//                    .cannotShareWithYourself == sharingError {
//                     showWarning = true
//                     warningText = sharingError.errorDescription
//                } else {
//                    Log("Some other error!", type: .error)
//                    error.raiseGlobalAlert()
//                }

                self.isSharing = false
            }
        }
    }

    func getContacts() {
        Task {
            do {
                self.contacts = try await client.getContacts()
            } catch {
                // fail silently
            }
        }
    }

    private var currentRevokingMembershipIds: [UUID] = []
    func isRevoking(_ membershipId: UUID) -> Bool {
        return currentRevokingMembershipIds.contains(membershipId)
    }

    func revokeAccess(forMembership membership: FolderMembership) {
        guard let folder = itemWrapper.folder else {
            SharingError.cannotRevoke.raiseGlobalAlert()
            return
        }

        // Lock
        if isRevoking(membership.id) { return }
        currentRevokingMembershipIds.append(membership.id)

        Task {
            do {
                // Call the revoke endpoint
                _ = try await client.revokeFolderMembership(
                    forFolderId: folder.id.uuidString,
                    memberId: membership.id.uuidString
                )

                // On success -> remove membership from self.folderMemberships
                folderMemberships = folderMemberships.filter { $0 != membership }
                itemWrapper.folder?.folderInvitations = folderMemberships.filter { $0 != membership }
                itemWrapper.folder?.folderMemberships = folderMemberships.filter { $0 != membership }
                self.itemUpdatedSubject.send(itemWrapper)

                displayToast(text: "Access revoked for \(membership.user.email)")
            } catch {

                // On error -> show alert, don't remove membership
                error.raiseGlobalAlert()
            }

            // Remove lock
            currentRevokingMembershipIds = currentRevokingMembershipIds.filter { $0 != membership.id }
        }
    }
}
