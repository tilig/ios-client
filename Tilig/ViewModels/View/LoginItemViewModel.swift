//
//  ItemViewModel.swift
//  Tilig
//
//  Created by Gertjan Jansen on 04/06/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import UIKit
import Sentry
import Toast

class LoginItemViewModel: ObservableObject {
    init(decryptedLoginItem: ItemWrapper) {
        self.loginItem = decryptedLoginItem
        self.publishProperties()
    }

    func displayToast(text: String) {
        Toast.text(text).show()
    }

    func update(with loginItem: ItemWrapper) {
        self.loginItem = loginItem
    }

    private(set) var loginItem: ItemWrapper {
        didSet { publishProperties() }
    }

    private func publishProperties() {
        name = loginItem.name
        url = loginItem.url
        brand = loginItem.brand
        username = loginItem.username
        password = loginItem.password
        totp = loginItem.totp
        notes = loginItem.notes
        passwordVersions = loginItem.details.history.reversed()
    }

    @Published private(set) var name: String?
    @Published private(set) var url: URL?
    @Published private(set) var brand: Brand?
    @Published private(set) var passwordVersions: [HistoricVersion] = []

    @Published private(set) var username: String?
    @Published private(set) var password: String?
    @Published private(set) var notes: String?
    @Published private(set) var totp: String?

    func activate() {
        TrackingEvent.loginViewed.send()
    }

    func copyUsername() {
        guard let username = username else { return }
        UIPasteboard.general.string = username
        TrackingEvent.usernameCopied.send()
        displayToast(text: username.isValidEmail ? "Email copied" : "Username copied")
    }

    func copyPassword() {
        guard let password = password else { return }
        UIPasteboard.general.string = password
        TrackingEvent.passwordCopied.send()
        displayToast(text: "Password copied")
    }

    func copyWebsite() {
        guard let url = url else { return }
        UIPasteboard.general.string = url.withProtocol?.absoluteString
        TrackingEvent.websiteCopied.send()
        displayToast(text: "Website copied")
    }

    func copyNotes() {
        guard let notes = notes else { return }
        UIPasteboard.general.string = notes
        TrackingEvent.notesCopied.send()
        displayToast(text: "Notes copied")
    }

}
