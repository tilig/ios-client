//
//  DeleteItemViewModel.swift
//  Tilig
//
//  Created by Gertjan Jansen on 03/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import Combine

@MainActor class DeleteItemViewModel: ObservableObject {
    init(item: Item, tokenProvider: TokenProvider) {
        self.item = item
        self.client = Client(tokenProvider: tokenProvider)
    }

    private(set) var item: Item
    private var client: Client

    @Published private(set) var isDeleting: Bool = false

    var itemDeleted: AnyPublisher<Item, Never> {
        itemDeletedSubject.eraseToAnyPublisher()
    }
    private var itemDeletedSubject = PassthroughSubject<Item, Never>()

    func delete() async {
        if isDeleting { return }
        isDeleting = true

        do {
            _ = try await client.deleteItem(item)
            self.itemDeletedSubject.send(item)
        } catch {
            error.raiseGlobalAlert()
        }

        self.isDeleting = false
    }

}

enum DeleteItemError: Error {
    case deletionFailed
    case cannotDeleteNonPersistedItem
}

extension DeleteItemError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .deletionFailed:
            return "Could not delete the item."
        case .cannotDeleteNonPersistedItem:
            return "Could not delete the item (not saved yet)."
        }
    }
}

struct DeleteResponse: Decodable {
    var status: String
}
