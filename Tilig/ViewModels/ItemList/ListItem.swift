//
//  ListItem.swift
//  Tilig
//
//  Created by Gertjan Jansen on 29/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation

struct ListItem {
    var encryptedItem: Item
    var decryptedOverview: DecryptedOverview?
    var decryptionError: Error?

    /// The label of this item
    var name: String? { decryptedOverview?.name ?? encryptedItem.name }

    var itemType: ItemType {
        guard let itemType = encryptedItem.itemType else {
            assertionFailure("No item type set!")
            return .login
        }
        return itemType
    }

    var isShared: Bool {
        if let memberships = encryptedItem.folder?.folderMemberships {
            return !memberships.filter({ $0.isMe != true }).isEmpty
        }
        return false
    }

    /// The iconURL is never decrypted. Therefore it lives in the encryptedItem.
    // var iconURL: URL? { encryptedItem.iconURL }
}

extension ListItem: Identifiable {
    /// Use the encryptedItem's ID as an identifier. This should be fine.
    var id: UUID? { encryptedItem.id }
}

/// Make list items sortable
extension ListItem: Comparable {
    static func < (lhs: ListItem, rhs: ListItem) -> Bool {
        (lhs.name ?? "").lowercased() < (rhs.name ?? "").lowercased()
    }

    static func == (lhs: ListItem, rhs: ListItem) -> Bool {
        return (
            lhs.encryptedItem == rhs.encryptedItem &&
            lhs.decryptedOverview == rhs.decryptedOverview
        )
    }
}
