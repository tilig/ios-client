//
//  ItemListViewModel.swift
//  Tilig
//
//  Created by Gertjan Jansen on 03/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//
import Foundation
import Combine
import Sentry

enum LoadingState {
    /// The first query is being executed
    case fetchingInitial
    /// Results have been loaded from the cache
    case cacheLoaded
    /// A refresh query is being executed (full or partial)
    case refreshing
    /// No query is being executed
    case idle
}

@MainActor class ItemListViewModel: ObservableObject {
    init(crypto: Crypto,
         tokenProvider: TokenProvider,
         itemsCache: ItemsCache = ItemsCache()) {
        self.crypto = crypto
        self.itemsCache = itemsCache
        self.client = Client(tokenProvider: tokenProvider)
    }

    private var crypto: Crypto
    private let itemsCache: ItemsCache
    private var client: Client

    // Non-optional unfortunately, because our TextField's cant bind to optionals yet
    @Published var searchText: String = "" {
        didSet { filterItems() }
    }
    @Published private(set) var loadingState = LoadingState.fetchingInitial
    @Published private(set) var activeItemType: ItemType?
    // TODO: rename to listItems
    @Published private(set) var items = [ListItem]() {
        didSet {
            filterItems()
            publishItemsForActiveItemType()
        }
    }
    @Published private(set) var itemsForActiveItemType = [ListItem]()
    @Published private(set) var filteredItems: [ListItem] = []

    @Published private(set) var lastError: Error?

    /// An item that just has been added. We're keeping this state in order to render
    /// the item details view without waiting for the itemList to be rerendered.
    @Published private(set) var newlyAddedItem: Item?

    /// Has the initial query been done?
    private(set) var initialQueryDone = false
    var lastRefresh: Date {
        Date(timeIntervalSince1970: UserDefaults.standard.double(forKey: UserDefaults.LastRefreshed))
    }

    private var activateTask: Task<Void, Never>?

    func activate() {
        guard activateTask == nil else { return }

        activateTask = Task.detached(priority: .userInitiated) {
            await self.fetchItems()
            TrackingEvent.itemListViewed.send()
            await self.migrateItemsToV2()
            await self.migrateToLibsodiumKeys()
            await MainActor.run {
                self.activateTask = nil
            }
        }
    }

    func setActiveItemType(itemType: ItemType?) {
        Log("Set active item type: \(String(describing: itemType))", category: .itemList)
        activeItemType = itemType
        publishItemsForActiveItemType()
        filterItems()
    }

    func publishItemsForActiveItemType() {
        if let activeItemType {
            itemsForActiveItemType = items.filter { $0.encryptedItem.itemType == activeItemType }
        } else {
            itemsForActiveItemType = items
        }
        Log("Published \(itemsForActiveItemType.count) items for active type", category: .itemList)
    }

    func fetchItems() async {
        // Note: not doing any throttling yet.
        loadingState = initialQueryDone ? .refreshing : .fetchingInitial

        // Load from cache
        // Fail silently because it can fail if the data model has changed.
        if let cachedItems = try? itemsCache.getCachedItems() {
            Log("Loaded \(cachedItems.count) items from cache", category: .itemList)
            await handle(items: cachedItems)
            if cachedItems.count > 0 {
                loadingState = .cacheLoaded
            }
        }

        do {
            // Query
            let queriedItems = try await query()
            Log("Queried \(queriedItems.count) items from server", category: .itemList)
            setLastRefreshed(Date.now)
            initialQueryDone = true
            await handle(items: queriedItems)

            // Update the cache
            try itemsCache.cacheAll(items: queriedItems)
        } catch {
            handle(error: error)
        }

        loadingState = .idle
    }

    // Map, sort and publish the given items
    func handle(items: [Item]) async {
        Log("Mapping \(items.count) items", category: .itemList)
        var listItems: [ListItem] = []
        for item in items {
            listItems.append(await makeListItem(item))
        }
        if Task.isCancelled {
            Log("Task cancelled! 💥", category: .itemList)
            return
        }
        Log("Sorting and setting \(items.count) items", category: .itemList)
        self.items = listItems.sorted { $0 < $1 }
    }

    var migratingKeys = false

    func migrateToLibsodiumKeys() async {
        if migratingKeys { return }

        migratingKeys = true

        let legacyItems = items.filter { $0.encryptedItem.encryptedDek == nil }

        for item in legacyItems {
            do {
                let itemWrapper = try crypto.decrypt(fullItem: item.encryptedItem)
                let encryptedItem = try crypto.encrypt(itemWrapper: itemWrapper)
                _ = try await client.updateItem(encryptedItem)
            } catch {
                // Don't raise an error here, fail silently
                SentrySDK.capture(error: error)
            }
        }

        if !legacyItems.isEmpty {
            await fetchItems()
        }

        migratingKeys = false
    }

    var migrating = false

    func migrateItemsToV2() async {
        if migrating { return }

        migrating = true

        let legacyItems = items.filter({
            $0.encryptedItem.encryptionVersion == 1 || $0.encryptedItem.template == nil
        })
        for item in legacyItems {
            do {
                var itemWrapper = try crypto.decrypt(fullItem: item.encryptedItem)

                guard let id = itemWrapper.id else { return }

                // Update history
                let history = try await client.getLegacyPasswordHistory(id: id)
                try itemWrapper.reencrypt(history: history, crypto: crypto)

                let encryptedItem = try crypto.encrypt(itemWrapper: itemWrapper)

                _ = try await client.updateItem(encryptedItem)
            } catch {
                // Don't raise an error here, fail silently
                SentrySDK.capture(error: error)
            }
        }

        if !legacyItems.isEmpty {
            await fetchItems()
        }

        migrating = false

    }

    private func makeListItem(_ encryptedItem: Item) async -> ListItem {
        do {
            return ListItem(
                encryptedItem: encryptedItem,
                decryptedOverview: try await crypto.decryptOverviewInBackground(
                    encryptedItem: encryptedItem
                )
            )
        } catch {
            // Don't raise an error, fail silently
            SentrySDK.capture(error: error)
            return ListItem(
                encryptedItem: encryptedItem,
                decryptionError: error
            )
        }
    }

    func query() async throws -> [Item] {
        try await client.getItems()
    }

    func filterItems() {
        assert(Thread.isMainThread)
        filteredItems = items
            .filter { item in
                if searchText.isEmpty { return true }
                var matchatable: [String?] = item.decryptedOverview?.urls.map { $0.url?.absoluteString } ?? []
                matchatable += [item.decryptedOverview?.info?.lowercased(), item.name?.lowercased()]
                return matchatable.first { $0?.contains(searchText.lowercased()) == true } != nil
            }
            .sorted { $0 < $1 }

        if let activeItemType {
            filteredItems = filteredItems.filter { $0.encryptedItem.itemType == activeItemType }
        }
    }

    private func sort() {
        items = items.sorted { $0 < $1 }
    }

    func clearItems() {
        activateTask?.cancel()
        items = []
        do {
            try itemsCache.clear()
        } catch {
            error.raiseGlobalAlert()
            SentrySDK.capture(error: error)
        }
    }

    /// Add a new item to the list
    func add(encryptedItem: Item) async {
        activateTask?.cancel()
        let listItem = await makeListItem(encryptedItem)
        newlyAddedItem = encryptedItem
        items.append(listItem)
        sort()
        updateCache()
    }

    /// Replace an existing item in the list
    func update(itemWrapper: ItemWrapper) {
        activateTask?.cancel()

        let listItem = ListItem(
            encryptedItem: itemWrapper.encryptedItem,
            decryptedOverview: itemWrapper.overview
        )
        items = items.map { existingItem in
            existingItem.id == itemWrapper.id ? listItem : existingItem
        }
        updateCache()
    }

    /// Remove an existing item from the list
    func remove(item deletedItem: Item) {
        activateTask?.cancel()
        items = items.filter { $0.id != deletedItem.id }
        updateCache()
    }

    func refreshData() {
        activateTask?.cancel()
        Task.detached(priority: .userInitiated) {
            await self.fetchChanges()
        }
    }

    func setLastRefreshed(_ date: Date) {
        UserDefaults.standard.set(date.timeIntervalSince1970, forKey: UserDefaults.LastRefreshed)
    }

    func fetchChanges() async {
        // Get all items if items havent been refreshed in over 24 hours (60 secs * 60 mins * 24 hrs)
        let timeToLive: TimeInterval = 60 * 60 * 24
        if Date().timeIntervalSince(lastRefresh) >= timeToLive {
            await fetchItems()
            return
        }

        loadingState = .refreshing

        do {
            // Query changes
            let pollingPayload = try await client.getChanges(changedSince: lastRefresh)
            let changedItems = pollingPayload.updates
            let deletions = pollingPayload.deletions
            self.setLastRefreshed(Date.now)

            // Map to list items
            for item in changedItems {
                let listItem = await makeListItem(item)
                if let row = items.firstIndex(where: { $0.id == listItem.id }) {
                    // It's an update
                    items[row] = listItem
                } else {
                    // It's a new item
                    items.append(listItem)
                }
            }

            // Remove deletions
            items = items.filter { !deletions.contains($0.id) }

            sort()

            // Update the cache
            updateCache()
        } catch {
            handle(error: error)
        }

        loadingState = .idle
    }

    /// Updates all items in the cache.
    /// Note this is not super performant for smaller updates, but it's a
    /// simple solution that doesn't cause race conditions easily.
    /// When it fails, an alert will be thrown.
    private func updateCache() {
        do {
            try itemsCache.cacheAll(items: items.map { $0.encryptedItem })
        } catch {
            handle(error: error)
        }
    }

    func handle(error: Error) {
        SentrySDK.capture(error: error)
        error.raiseGlobalAlert()
        lastError = error
    }
}

#if DEBUG
extension ItemListViewModel {
    /// For testing and debug views
    func set(items: [ListItem]) {
        self.items = items
    }
}

#endif
