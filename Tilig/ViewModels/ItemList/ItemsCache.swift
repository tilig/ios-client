//
//  ItemsCache.swift
//  Tilig
//
//  Created by Gertjan Jansen on 20/11/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import Valet
import Sentry

class ItemsCache {

    private var valet: Valet { Valet.itemsCache }

    /// Replaces the full cache with the given items
    func cacheAll(items: [Item]) throws {
        try clear()
        for (index, item) in items.enumerated() {
            try cache(item: item, index: index)
        }
        try cache(numberOfItems: items.count)
    }

    func getCachedItems() throws -> [Item] {
        guard let numberOfItems = getCachedNumberOfItems() else { return [] }
        if numberOfItems < 1 { return [] }

        var items: [Item] = []
        for index in 0...(numberOfItems-1) {
            items.append(try getCachedItem(index: index))
        }
        return items
    }

    func clear() throws {
        try valet.removeAllObjects()
    }

    private func getKey(forIndex index: Int) -> String {
        "item\(index)"
    }

    private func cache(item: Item, index: Int) throws {
        let data = try JSONEncoder.snakeCaseConverting.encode(item)
        try valet.setObject(data, forKey: getKey(forIndex: index))
    }

    private func cache(numberOfItems: Int) throws {
        // Storing as Int or NSNumber didn't work
        try valet.setString("\(numberOfItems)", forKey: ValetKeys.cachedNumberOfItems.rawValue)
    }

    private func getCachedItem(index: Int) throws -> Item {
        let key = getKey(forIndex: index)
        let data = try valet.object(forKey: key)
        return try JSONDecoder.snakeCaseConverting.decode(Item.self, from: data)
    }

    private func getCachedNumberOfItems() -> Int? {
        guard let string = try? valet.string(forKey: ValetKeys.cachedNumberOfItems.rawValue) else {
            return nil
        }
        return Int(string)
    }
}
