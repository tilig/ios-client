//
//  PrefilledWebsiteSelectViewModel.swift
//  Tilig
//
//  Created by Gertjan Jansen on 13/01/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import Combine

class PrefillWebsiteViewModel: ObservableObject {

    init() {
        filteredWebsites = websites
    }

    @Published var searchText: String = "" {
        didSet {
            self.filteredWebsites = websites
                .filter({ website -> Bool in
                    searchText.isEmpty || website.name.lowercased().contains(searchText.lowercased())
                })
        }
    }

    @Published var filteredWebsites: [PrefillableWebsite] = []

    var websites: [PrefillableWebsite] = [
        PrefillableWebsite(
            name: "Airbnb",
            url: URL(string: "https://airbnb.com")!,
            localAssetName: "airbnb"
        ),
        PrefillableWebsite(
            name: "Amazon",
            url: URL(string: "https://amazon.com")!,
            localAssetName: "amazon"
        ),
        PrefillableWebsite(
            name: "Booking",
            url: URL(string: "https://booking.com")!,
            localAssetName: "booking"
        ),
        PrefillableWebsite(
            name: "Ebay",
            url: URL(string: "https://ebay.com")!,
            localAssetName: "ebay"
        ),
        PrefillableWebsite(
            name: "Facebook",
            url: URL(string: "https://facebook.com")!,
            localAssetName: "facebook"
        ),
        PrefillableWebsite(
            name: "Instagram",
            url: URL(string: "https://instagram.com")!,
            localAssetName: "instagram"
        ),
        PrefillableWebsite(
            name: "LinkedIn",
            url: URL(string: "https://linkedin.com")!,
            localAssetName: "linkedin"
        ),
        PrefillableWebsite(
            name: "Paypal",
            url: URL(string: "https://paypal.com")!,
            localAssetName: "paypal"
        ),
        PrefillableWebsite(
            name: "Pinterest",
            url: URL(string: "https://pinterest.com")!,
            localAssetName: "pinterest"
        ),
        PrefillableWebsite(
            name: "Reddit",
            url: URL(string: "https://reddit.com")!,
            localAssetName: "reddit"
        ),
        PrefillableWebsite(
            name: "Roblox",
            url: URL(string: "https://roblox.com")!,
            localAssetName: "roblox"
        ),
        PrefillableWebsite(
            name: "Spotify",
            url: URL(string: "https://spotify.com")!,
            localAssetName: "spotify"
        ),
        PrefillableWebsite(
            name: "Twitter",
            url: URL(string: "https://twitter.com")!,
            localAssetName: "twitter"
        ),
        PrefillableWebsite(
            name: "Yelp",
            url: URL(string: "https://yelp.com")!,
            localAssetName: "yelp"
        )
    ]
}
