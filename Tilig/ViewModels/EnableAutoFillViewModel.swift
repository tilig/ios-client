//
//  AutoFillViewModel.swift
//  Tilig
//
//  Created by Gertjan Jansen on 07/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import SwiftUI
import AuthenticationServices

enum AutoFillExplanationFlavor {
    case onboarding
    case userInitiated
}

@MainActor class EnableAutoFillViewModel: ObservableObject {

    /// Note: this class acts a bit weird due to the async nature of checking if autofil is enabled.
    init(credentialIdentityStateProvider: CredentialIdentityStateProvider = CredentialIdentityStateProvider()) {
        self.credentialIdentityStateProvider = credentialIdentityStateProvider

        // Init props based on UserDefaults state
        self.autoFillEnabled = UserDefaults.standard.bool(forKey: UserDefaults.LastKnownAutoFillEnabledState)
        self.onboardingCompleted = UserDefaults.standard.bool(forKey: UserDefaults.AutoFillOnboardingCompleted)

        // Note: async in bg
        updateAutoFillEnabledFlag()
    }

    private var credentialIdentityStateProvider: CredentialIdentityStateProvider

    /// Should we present the autofill explanation sheet after the user signed in?
    @Published private(set) var onboardingCompleted: Bool

    /// Has the user autofill enabled?
    @Published private(set) var autoFillEnabled: Bool {
        didSet {
            trackStateChange(isEnabled: autoFillEnabled)
        }
    }

    /// We have 2 slightly different versions, or "flavors" of the explanation view
    @Published private(set) var explanationFlavor = AutoFillExplanationFlavor.onboarding

    /// Should the explanation be presented?
    @Published private(set) var presentAsSheet = false

    func activate() {
        // Reinit props based on UserDefaults state
        self.autoFillEnabled = UserDefaults.standard.bool(forKey: UserDefaults.LastKnownAutoFillEnabledState)
        self.onboardingCompleted = UserDefaults.standard.bool(forKey: UserDefaults.AutoFillOnboardingCompleted)

        // Note: async in bg
        updateAutoFillEnabledFlag()
    }

    func completeFinalStep() {
        setOnboardingCompleted()
    }

    func setOnboardingCompleted() {
        UserDefaults.standard.setValue(true, forKey: UserDefaults.AutoFillOnboardingCompleted)
        onboardingCompleted = true
        presentAsSheet = false
    }

    /// Called when the user explicitly skips the autofill explanation
    func skipAutoFill() {
        setOnboardingCompleted()
        presentAsSheet = false
        TrackingEvent.autoFillExplanationSkipped.send()
    }

    /// Update self.autoFillEnabled with the latest value
    /// If the user is not in the onboarding/explanation flow, it also sets the onboardingCompleted flag
    /// to true to prevent it from appearing, while autofill is already enabled.
    func updateAutoFillEnabledFlag() {
        // This is fetched async
        credentialIdentityStateProvider.getState { [weak self] isEnabled in
            guard let self = self else { return }
            Task { @MainActor in
                withAnimation {
                    self.autoFillEnabled = isEnabled
                }
                // If the user is in the explanation wizard, don't complete the
                // onboarding yet (that will happen after the next screen).
                // Otherwise, mark it as completed.
                if isEnabled {
                    self.setOnboardingCompleted()
                }
            }
        }
    }

    /// Present the autofill explanation view.
    /// Each `flavor` will present a slighly different view.
    func presentAutoFillExplanation(flavor: AutoFillExplanationFlavor = .userInitiated) {
        explanationFlavor = flavor
        presentAsSheet = true
    }

    // MARK: Mixpanel

    /// The last AutoFillEnabledState we sent to Mixpanel. `True` means we told Mixpanel
    /// that autofill was enabled, `false` means we told Mixpanel it was disabled.
    var lastTrackedAutoFillEnabledState: Bool {
        UserDefaults.standard.bool(forKey: UserDefaults.LastKnownAutoFillEnabledState)
    }

    /// Store the last enabled state we sent to Mixpanel
    func storeLastAutoFillEnabledState(_ isEnabled: Bool) {
        UserDefaults.standard.set(isEnabled, forKey: UserDefaults.LastKnownAutoFillEnabledState)
    }

    /// Send event to Mixpanel if state changes
    private func trackStateChange(isEnabled: Bool) {
        if isEnabled != lastTrackedAutoFillEnabledState {
            if isEnabled {
                FirebaseTrackingEvent.autofillEnabled.send()
                TrackingEvent.autofillEnabled.send()
            } else {
                TrackingEvent.autofillDisabled.send()
            }

            self.storeLastAutoFillEnabledState(isEnabled)
        }
    }
}
