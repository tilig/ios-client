//
//  AddItemViewModelProtocol.swift
//  Tilig
//
//  Created by Gertjan Jansen on 21/11/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import Combine

protocol FormFields: Encodable {
    var valid: Bool { get }
}

@MainActor protocol AddItemViewModelProtocol: ObservableObject {
    init(crypto: Crypto, tokenProvider: TokenProvider)

    var itemCreated: AnyPublisher<Item, Never> { get }

    // Maybe these two are not strictly necessary, but a first step to making
    // a more generic solution:

    func getEncryptedItem() throws -> Item

    @MainActor func save() async
}
