//
//  LoginFormViewModel.swift
//  Tilig
//
//  Created by Gertjan Jansen on 25/10/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//
import Foundation
import Combine
import Sentry
import UIKit
import Toast

enum AddLoginSteps {
    case addWebsite // step 1
    case addOtherDetails // step 2
}

struct AddLoginFormFields: FormFields {
    var name: String?
    var website: URL?
    var username: String?
    var password: String?

    // Currently always valid
    var valid: Bool { true }

    /// Did the user enter an email address as the username?
    var usernameIsEmail: Bool { username?.isValidEmail ?? false }
}

// This ViewModel is currently used for both step 1 and 2 of the add login flow
// A different approach would be to create two separate viewmodels.
// There's also a PrefillWebsiteViewModel that complicates things even more, and
// probably needs to be merged in here.
@MainActor class AddLoginViewModel: ObservableObject, AddItemViewModelProtocol {
    required init(crypto: Crypto, tokenProvider: TokenProvider) {
        self.crypto = crypto
        self.client = Client(tokenProvider: tokenProvider)
    }
    private let client: Client
    private let crypto: Crypto

    @Published var formFields = AddLoginFormFields()
    @Published var currentStep = AddLoginSteps.addWebsite
    @Published var isSubmitting = false
    @Published var brand: Brand?
    private var debounceHelper = DebounceHelper()

    var saveButtonDisabled: Bool { !formFields.valid || isSubmitting }

    // Kept this very basic for now
    func goToNextStep() {
        if currentStep == .addWebsite {
            currentStep = .addOtherDetails
        }
    }

    // Kept this very basic for now
    func goToPreviousStep() {
        if currentStep == .addOtherDetails {
            currentStep = .addWebsite

            // clear fields
            localIconAssetName = nil
            formFields = AddLoginFormFields()
        }
    }

    // MARK: Step 1
    // This is non-nil to make it work with an input
    @Published var searchText: String = "" {
        didSet {
            if !searchText.isEmpty {
                isSearchingForBrand = true
                Task { await brandSearch(prefix: searchText) }
            } else {
                isSearchingForBrand = false
                brands = []
            }
        }
    }
    @Published var isSearchingForBrand = false

    // Used as a fallback when brand data (that populates the iconURL) is not present yet
    @Published var localIconAssetName: String?
    @Published var brands: [Brand] = []

    var itemCreated: AnyPublisher<Item, Never> {
        itemCreatedSubject.eraseToAnyPublisher()
    }
    private var itemCreatedSubject = PassthroughSubject<Item, Never>()

    /// Go to the next step and populate that based on the search input
    func continueWithSearchText() {
        localIconAssetName = nil
        formFields.name = searchText

        // TODO: I think we should check if this is a valid URL, otherwise
        // force the user to enter this. Discuss with team because it should
        // be implemented on all platforms.
        // Gertjan: I believe we allow for non-valid URLs
        formFields.website = URL(string: searchText)
        Task {
            guard let url = formFields.website else {
                return self.brand = nil
            }
            self.brand = await fetchBrand(for: url)
        }

        goToNextStep()
    }

    /// Go to the next step and populate that based on the given brand
    func continueWithBrand(_ brand: Brand) {
        self.brand = brand
        localIconAssetName = nil
        formFields.name = brand.name
        formFields.website = URL(string: brand.domain)

        goToNextStep()
    }

    /// Go to the next step and populate that based on the selected website
    func continueWithPrefilledWebsite(_ prefillableWebsite: PrefillableWebsite) {
        localIconAssetName = prefillableWebsite.localAssetName
        formFields.name = prefillableWebsite.name
        formFields.website = prefillableWebsite.url

        goToNextStep()
    }

    /// Returns an Item based on the form fields, fully encrypted
    func getEncryptedItem() throws -> Item {
        var itemWrapper = ItemWrapper(template: .loginV1)
        itemWrapper.name = formFields.name
        itemWrapper.url = formFields.website
        itemWrapper.username = formFields.username
        itemWrapper.password = formFields.password
        return try crypto.encrypt(itemWrapper: itemWrapper)
    }

    @MainActor func save() async {
        if isSubmitting { return }
        isSubmitting = true

        do {
            let encryptedItem = try getEncryptedItem()
            let savedLogin = try await client.saveItem(encryptedItem)
            self.itemCreatedSubject.send(savedLogin)
        } catch {
            error.raiseGlobalAlert()
        }

        isSubmitting = false
    }

    @MainActor func brandSearch(prefix: String) async {
        debounceHelper.debounce(duration: 1.0) {
            do {
                self.brands = try await self.client.brandSearch(prefix: prefix)

                self.retrieveUpdatedBrandsDetails()
            } catch {
                error.raiseGlobalAlert()
            }
        }
    }

    // `brandSearch` results might not include the brands' logos.
    // The backend tries to update these brands' logos immediately after this query.
    // so we then fetch individual details for each brand whose isFetched value is false
    func retrieveUpdatedBrandsDetails() {
        Task {
            try? await Task.sleep(nanoseconds: UInt64(0.5 * .nanosecondsPerSecond))

            for brand in self.brands where brand.isFetched != true {
                if let domain = URL(string: brand.domain),
                   let updatedBrand = await self.fetchBrand(for: domain) {
                    self.brands = (self.brands.map { $0.domain == brand.domain ? updatedBrand : $0 })
                }
            }
        }
    }

    @MainActor func fetchBrand(for domain: URL) async -> Brand? {
        do {
            return try await client.getBrand(for: domain)
        } catch URLError.resourceUnavailable {
            return nil
        } catch {
            return nil
        }
    }
}
