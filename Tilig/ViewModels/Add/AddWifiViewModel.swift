//
//  AddWifiViewModel.swift
//  Tilig
//
//  Created by Gertjan Jansen on 25/10/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//
import Foundation
import Combine

struct AddWifiFormFields: FormFields {
    var name: String?
    var ssid: String?
    var password: String?
    var notes: String?

    var valid: Bool { name != nil }
}

@MainActor class AddWifiViewModel: ObservableObject, AddItemViewModelProtocol {
    required init(crypto: Crypto, tokenProvider: TokenProvider) {
        self.crypto = crypto
        self.client = Client(tokenProvider: tokenProvider)
    }
    private let client: Client
    private let crypto: Crypto

    @Published var formFields = AddWifiFormFields()
    @Published var isSubmitting = false

    var saveButtonDisabled: Bool { !formFields.valid || isSubmitting }

    var itemCreated: AnyPublisher<Item, Never> {
        itemCreatedSubject.eraseToAnyPublisher()
    }
    private var itemCreatedSubject = PassthroughSubject<Item, Never>()

    /// Returns an Item based on the form fields, fully encrypted
    func getEncryptedItem() throws -> Item {
        var itemWrapper = ItemWrapper(template: .wifiV1)
        itemWrapper.name = formFields.name
        itemWrapper.ssid = formFields.ssid
        itemWrapper.password = formFields.password
        itemWrapper.name = formFields.name
        itemWrapper.notes = formFields.notes
        return try crypto.encrypt(itemWrapper: itemWrapper)
    }

    @MainActor func save() async {
        if isSubmitting { return }
        isSubmitting = true

        do {
            let encryptedItem = try getEncryptedItem()
            let savedWifi = try await client.saveItem(encryptedItem)
            self.itemCreatedSubject.send(savedWifi)
        } catch {
            error.raiseGlobalAlert()
        }

        isSubmitting = false
    }
}
