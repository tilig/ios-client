//
//  AddNoteViewModel.swift
//  Tilig
//
//  Created by Gertjan Jansen on 25/10/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//
import Foundation
import Combine
import Sentry
import DomainParser
import UIKit
import Toast

struct AddNoteFormFields: FormFields {
    var name: String?
    var notes: String?

    var valid: Bool { name != nil }
}

@MainActor class AddNoteViewModel: ObservableObject, AddItemViewModelProtocol {
    required init(crypto: Crypto, tokenProvider: TokenProvider) {
        self.crypto = crypto
        self.client = Client(tokenProvider: tokenProvider)
    }
    private let client: Client
    private let crypto: Crypto

    @Published var formFields = AddNoteFormFields()
    @Published var isSubmitting = false

    var saveButtonDisabled: Bool { !formFields.valid || isSubmitting }

    var itemCreated: AnyPublisher<Item, Never> {
        itemCreatedSubject.eraseToAnyPublisher()
    }
    private var itemCreatedSubject = PassthroughSubject<Item, Never>()

    /// Returns an Item based on the form fields, fully encrypted
    func getEncryptedItem() throws -> Item {
        var noteItem = ItemWrapper(template: .noteV1)
        noteItem.name = formFields.name
        noteItem.notes = formFields.notes
        return try crypto.encrypt(itemWrapper: noteItem)
    }

    @MainActor func save() async {
        if isSubmitting { return }
        isSubmitting = true

        do {
            let encryptedItem = try getEncryptedItem()
            let savedNote = try await client.saveItem(encryptedItem)
            self.itemCreatedSubject.send(savedNote)
        } catch {
            error.raiseGlobalAlert()
        }

        isSubmitting = false
    }
}
