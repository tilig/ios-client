//
//  Sentry.swift
//  Tilig
//
//  Created by Gertjan Jansen on 12/08/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import Sentry
import Combine

extension SentrySDK {
    /// Setup Sentry for the current environment
    static func setup() {
        #if DEBUG
        if !UserDefaults.standard.bool(forKey: "enableSentry") {
            Log("Sentry is disabled.")
            return
        }
        let environment = "development"
        #else
        let environment = "production"
        #endif
        SentrySDK.start { options in
            options.dsn = "https://4368466bf003497eb04491b09b4813c5@o390798.ingest.sentry.io/5237945"
            options.enableAutoSessionTracking = true
            options.attachStacktrace = true
            options.environment = environment
        }
    }

    /// Set current user on Sentry
    static func setUser(userProfile: UserProfile) {
        let user = User()
        user.email = userProfile.email
        user.userId = userProfile.id.uuidString
        SentrySDK.setUser(user)
    }

    /// A util to add breadcrumbs easily
    static func addSimpleBreadcrumb(_ message: String, level: SentryLevel = .warning) {
        let crumb = Breadcrumb(level: .info, category: "simple")
        crumb.message = message
        SentrySDK.addBreadcrumb(crumb)
    }
}
