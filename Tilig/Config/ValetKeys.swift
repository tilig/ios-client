//
//  ValetKeys.swift
//  Tilig
//
//  Created by Gertjan Jansen on 20/10/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//
import Valet

enum ValetKeys: String {
    /// The auth container key
    case auth = "auth"
    /// Auth state
    case authState = "auth-state"
    // Biometric lock
    case biometricLockEnabled = "biometric-lock-enabled"

    /// Items cache container
    case itemsCache = "items-cache"
    /// The number of items cached
    case cachedNumberOfItems = "cached-number-of-items"
}

extension Valet {
    /// The Valet hat contains all cached authentication credentials
    static var auth: Valet {
        Valet.valet(
            with: Identifier(nonEmpty: ValetKeys.auth.rawValue)!,
            accessibility: .whenUnlocked
        )
    }

    /// The Valet that contains all cached items
    static var itemsCache: Valet {
        Valet.valet(
            with: Identifier(nonEmpty: ValetKeys.itemsCache.rawValue)!,
            accessibility: .whenUnlocked
        )
    }
}
