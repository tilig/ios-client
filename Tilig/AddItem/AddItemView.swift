//
//  AddItemView.swift
//  Tilig
//
//  Created by Gertjan Jansen on 22/11/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

struct AddItemView: View {
    let itemType: ItemType

    var body: some View {
        Group {
            switch itemType {
            case .login:
                AddLoginView()
            case .note:
                AddNoteView()
            case .wifi:
                AddWifiView()
            case .card:
                AddCardView()
            case .custom:
                EmptyView()
            }
        }
        .consumingViewModel(.createItem(itemType))
    }
}
