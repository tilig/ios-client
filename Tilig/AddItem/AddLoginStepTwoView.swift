//
//  AddLoginStepTwoView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/06/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct AddLoginStepTwoView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var viewModel: AddLoginViewModel

    func onItemCreated() {
        Task {
            // Dismiss the keyboard to fix a weird bug on iOS 16.0 where the autofill enabled
            // notice was filling half the screen after going through this add flow.
            // Hooray for SwiftUI!
            UIApplication.shared.endEditing(true)
            // Then add a bit of delay to let the newly added item transition
            // complete before dismissing the sheet.
            try? await Task.sleep(nanoseconds: UInt64(0.5 * .nanosecondsPerSecond))
            // And then dismiss the sheet
            self.presentationMode.wrappedValue.dismiss()
        }
    }

    func save() {
        Task { await viewModel.save() }
    }

    var body: some View {
        Group {
            PopupHeaderView(
                title: "Add Login",
                closeButtonAction: { Task { viewModel.goToPreviousStep() } },
                hasBackground: true,
                isBackEnabled: true
            )
            ScrollView {
                VStack(spacing: 0) {
                    HStack {
                        if let localAssetName = viewModel.localIconAssetName {
                            Image(localAssetName)
                                .fixedHeight(32)
                        } else {
                            // If iconURL is nil, this wil render a placeholder
                            ListIcon(
                                iconURL: viewModel.brand?.iconURL,
                                background: false,
                                backgroundColor: .white,
                                hasPadding: false,
                                cornerRadius: 8
                            )
                            .frame(width: 32, height: 32)
                        }

                        AddItemNameField(value: $viewModel.formFields.name)
                    }
                    .padding(.bottom)

                    Group {
                        Text("Website")
                            .defaultButtonFont(size: 16)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.bottom, 8)

                        WebsiteField("Add website", value: $viewModel.formFields.website)
                            .padding(.bottom)
                    }

                    Group {
                        Text(viewModel.formFields.usernameIsEmail ? "Email" : "Username")
                            .defaultButtonFont(size: 16)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.bottom, 8)

                        FormTextField(
                            "Add your email or username",
                            value: $viewModel.formFields.username,
                            textContentType: .emailAddress
                        )
                        .padding(.bottom)
                    }

                    Group {
                        // A hack to prevent iOS from autofilling the username field
                        InvisibleInputField()

                        Text("Password")
                            .defaultButtonFont(size: 16)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.bottom, 8)

                        GeneratePasswordField(password: $viewModel.formFields.password)
                            .padding(.bottom)
                    }

                    DefaultButton(title: "Save Login", action: save)
                        .disabled(viewModel.saveButtonDisabled)
                        .frame(height: 50)

                    Spacer()
                }
                .padding([.horizontal, .top])
            }
        }
        .onReceive(viewModel.itemCreated) { _ in
            onItemCreated()
        }
    }
}

struct AddLoginStepTwoView_Previews: PreviewProvider {
    static var previews: some View {
        AddLoginStepTwoView()
    }
}
