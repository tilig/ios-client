//
//  AddLoginView.swift
//  Tilig
//
//  Created by Gertjan Jansen on 31/05/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import SwiftUI
import Combine

// This wrapper view makes sure a new view model is created on view appear, instead of on view init
struct AddLoginView: View {
    @EnvironmentObject var viewModel: AddLoginViewModel

    var body: some View {
        Group {
            switch viewModel.currentStep {
            case .addWebsite:
                AddLoginStepOneView()
            case .addOtherDetails:
                AddLoginStepTwoView()
            }
        }
        .globalErrorHandler()
    }
}
