//
//  AddLoginStepOneView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/06/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct AddLoginStepOneView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var viewModel: AddLoginViewModel

    var body: some View {
        VStack(spacing: 0) {
            PopupHeaderView(
                title: "Add Login",
                closeButtonAction: { self.presentationMode.wrappedValue.dismiss() },
                hasBackground: true,
                isBackEnabled: false
            )

            Text("Website or app")
                .defaultButtonFont(size: 16)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding()

            SearchInput(label: "Name of website or app", searchText: $viewModel.searchText)
                .padding(.horizontal)

            if viewModel.searchText.isEmpty {
                Text("Or pick a suggestion")
                    .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding([.horizontal, .top])
                    .background(Color.backgroundGray)

                PrefillWebsiteView()
            } else {
                AutocompleteResultsView()
            }
        }
        .background(Color.white)
    }
}

struct AddLoginStepOneView_Previews: PreviewProvider {
    static var previews: some View {
        AddLoginStepOneView()
    }
}

struct AutocompleteResultsView: View {
    @EnvironmentObject var viewModel: AddLoginViewModel

    var body: some View {
        ScrollView(.vertical) {
            VStack(alignment: .leading) {
                Button(action: viewModel.continueWithSearchText) {
                    AutocompleteItemView(
                        title: viewModel.searchText
                    ) {
                        Image("custom_suggestion")
                            .resizable()
                    }
                }

                ForEach(viewModel.brands, id: \.self) { brand in
                    Button(action: {
                        viewModel.continueWithBrand(brand)
                    }) {
                        AutocompleteItemView(title: brand.domain) {
                            ListIcon(iconURL: brand.iconURL,
                                     background: false,
                                     backgroundColor: .clear,
                                     hasPadding: false,
                                     cornerRadius: 0
                            )
                        }
                    }
                }
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
        }
    }
}

private struct AutocompleteItemView<Icon: View>: View {
    var title: String
    var icon: () -> Icon

    var body: some View {
        HStack {
            icon()
                .frame(width: 32, height: 32, alignment: .center)
                .padding(.trailing, 8)

            Text(title)
                .defaultButtonFont(color: .blue, size: 15)

            Spacer()

            Image(systemName: "chevron.right")
                .frame(width: 16, height: 16, alignment: .center)
                .foregroundColor(.borderGray)
        }
        .padding(.horizontal)
    }
}
