//
//  PrefillWebsiteView.swift
//  Tilig
//
//  Created by Gertjan Jansen on 13/01/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import SwiftUI
import Combine
import Mixpanel

struct PrefillWebsiteView: View {

    @EnvironmentObject var addLoginViewModel: AddLoginViewModel
    @ObservedObject var viewModel = PrefillWebsiteViewModel()

    func onSelect(website: PrefillableWebsite) {
        addLoginViewModel.continueWithPrefilledWebsite(website)
    }

    let columns = [GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible())]

    var body: some View {
        ScrollView {
            LazyVGrid(columns: columns) {
                ForEach(viewModel.websites, id: \.self) { website in
                    tile(forWebsite: website)
                }
            }
            .responsivePadding(compact: 16, regular: 48)
        }
        .background(Color.backgroundGray.edgesIgnoringSafeArea(.vertical))
    }

    func tile(forWebsite website: PrefillableWebsite) -> some View {
        Button(action: {
            onSelect(website: website)
        }) {
            VStack {
                Image(website.localAssetName)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(height: 32, alignment: .center)

                Text(website.name)
                    .defaultButtonFont(color: .blue, size: 12)
            }
            .padding()
            .frame(maxWidth: .infinity, maxHeight: 80)
            .background(Color.white)
            .overlay(
                RoundedRectangle(cornerRadius: 8)
                    .stroke(Color.borderGray, lineWidth: 0.4)
            )
        }
        .frame(maxWidth: .infinity)

    }
}

#if DEBUG
struct PrefilledWebsiteSelect_Previews: PreviewProvider {
    static var previews: some View {
        PrefillWebsiteView()
    }
}
#endif
