//
//  AddNoteView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/10/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

 import SwiftUI

 struct AddNoteView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var viewModel: AddNoteViewModel

    func onNoteCreated() {
        Task {
            // Dismiss the keyboard to fix a weird bug on iOS 16.0 where the autofill enabled
            // notice was filling half the screen after going through this add flow.
            // Hooray for SwiftUI!
            UIApplication.shared.endEditing(true)
            // Then add a bit of delay to let the newly added item transition
            // complete before dismissing the sheet.
            try? await Task.sleep(nanoseconds: UInt64(0.5 * .nanosecondsPerSecond))
            // And then dismiss the sheet
            self.presentationMode.wrappedValue.dismiss()
        }
    }

    func save() {
        Task { await viewModel.save() }
    }

    var body: some View {
        VStack {
            PopupHeaderView(
                title: "Add Note",
                closeButtonAction: { self.presentationMode.wrappedValue.dismiss() },
                hasBackground: true,
                isBackEnabled: false
            )

            ScrollView {
                VStack(spacing: 0) {
                    HStack {
                        Image(systemName: "doc.text")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .foregroundColor(.mediumPurple)
                            .frame(height: 32, alignment: .center)
                            .padding(.trailing, 8)

                        AddItemNameField(placeholder: "Enter note name", value: $viewModel.formFields.name)
                    }
                    .padding(.bottom)

                    Text("Note content")
                        .defaultButtonFont(size: 16)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.vertical, 8)

                    NotesField(
                        label: "Enter your text here. Notes are always encrypted",
                        value: $viewModel.formFields.notes
                    )
                    .padding(.bottom)

                    DefaultButton(title: "Save note", action: save)
                        .disabled(viewModel.saveButtonDisabled)
                        .frame(height: 50)

                    Spacer()
                }
                .padding([.horizontal, .top])
            }
        }
        .onReceive(viewModel.itemCreated) { _ in
            onNoteCreated()
        }
    }
}
