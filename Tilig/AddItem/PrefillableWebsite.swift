//
//  PreffilledWebsite.swift
//  Tilig
//
//  Created by Gertjan Jansen on 13/01/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation

struct PrefillableWebsite: Hashable {
    var name: String
    var url: URL?
    // used to select local icon name from Assets
    // TODO: would be nice if we can use cached brandfetch data instead
    var localAssetName: String
}
