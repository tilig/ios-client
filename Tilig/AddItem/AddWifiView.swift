//
//  AddWifiView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/10/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

 import SwiftUI

 struct AddWifiView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var viewModel: AddWifiViewModel

    func onWifiCreated() {
        Task {
            // Dismiss the keyboard to fix a weird bug on iOS 16.0 where the autofill enabled
            // notice was filling half the screen after going through this add flow.
            // Hooray for SwiftUI!
            UIApplication.shared.endEditing(true)
            // Then add a bit of delay to let the newly added item transition
            // complete before dismissing the sheet.
            try? await Task.sleep(nanoseconds: UInt64(0.5 * .nanosecondsPerSecond))
            // And then dismiss the sheet
            self.presentationMode.wrappedValue.dismiss()
        }
    }

    func save() {
        Task { await viewModel.save() }
    }

    var body: some View {
        VStack {
            PopupHeaderView(
                title: "Add a WiFi network",
                closeButtonAction: { self.presentationMode.wrappedValue.dismiss() },
                hasBackground: true,
                isBackEnabled: false
            )

            ScrollView {
                VStack(spacing: 0) {
                    HStack {
                        Image(systemName: "wifi")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .foregroundColor(.wifiBlue)
                            .frame(width: 32, height: 32, alignment: .center)
                            .padding(.trailing, 8)

                        AddItemNameField(
                            placeholder: "Enter a title",
                            value: $viewModel.formFields.name,
                            focused: true
                        )
                    }
                    .padding(.bottom)

                    InputGroup {
                        Text("Network name")
                            .defaultButtonFont(color: .blue.opacity(0.75), size: 12)
                            .padding(.bottom, 4)

                        FormTextField(
                            "E.g. \"Router3868\"",
                            value: $viewModel.formFields.ssid,
                            editMode: true
                        )
                        .padding(.bottom)
                    }

                    InputGroup {
                        Text("Network password")
                            .defaultButtonFont(color: .blue.opacity(0.75), size: 12)
                            .padding(.bottom, 4)

                        PasswordField(
                            placeholder: "Enter the password",
                            password: $viewModel.formFields.password,
                            isHidden: false,
                            showGenerateButton: false
                        )
                        .padding(.bottom)
                    }

                    InputGroup {
                        Text("Notes")
                            .defaultButtonFont(color: .blue.opacity(0.75), size: 12)
                            .padding(.bottom, 4)

                        NotesField(
                            label: "Anything else you'd like to add.",
                            value: $viewModel.formFields.notes
                        )
                        .padding(.bottom)
                    }

                    DefaultButton(title: "Save", action: save)
                        .disabled(viewModel.saveButtonDisabled)
                        .frame(height: 50)

                    Spacer()
                }
                .padding([.horizontal, .top])
            }
        }
        .onReceive(viewModel.itemCreated) { _ in
            onWifiCreated()
        }
    }
}
