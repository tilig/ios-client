//
//  AddCardView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/10/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

 import SwiftUI

 struct AddCardView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var viewModel: AddCardViewModel

    func onCardCreated() {
        Task {
            // Dismiss the keyboard to fix a weird bug on iOS 16.0 where the autofill enabled
            // notice was filling half the screen after going through this add flow.
            // Hooray for SwiftUI!
            UIApplication.shared.endEditing(true)
            // Then add a bit of delay to let the newly added item transition
            // complete before dismissing the sheet.
            try? await Task.sleep(nanoseconds: UInt64(0.5 * .nanosecondsPerSecond))
            // And then dismiss the sheet
            self.presentationMode.wrappedValue.dismiss()
        }
    }

    func save() {
        Task { await viewModel.save() }
    }

    var body: some View {
        VStack {
            PopupHeaderView(
                title: "Add a new card",
                closeButtonAction: { self.presentationMode.wrappedValue.dismiss() },
                hasBackground: true,
                isBackEnabled: false
            )

            ScrollView {
                ScrollViewReader { proxy in
                    VStack(spacing: 0) {
                        HStack {
                            Image(systemName: "creditcard")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .foregroundColor(.creditCardGreen)
                                .frame(width: 32, height: 32, alignment: .center)
                                .padding(.trailing, 8)

                            AddItemNameField(
                                placeholder: "E.g. Work Credit Card",
                                value: $viewModel.formFields.name,
                                focused: true
                            )
                        }
                        .padding(.bottom)

                        InputGroup {
                            Text("Card number")
                                .defaultButtonFont(color: .blue.opacity(0.75), size: 12)
                                .padding(.bottom, 4)

                            FormTextField(
                                "0000 0000 0000 0000",
                                value: $viewModel.formFields.ccnumber,
                                editMode: true,
                                keyboardType: .numberPad
                            )
                            .padding(.bottom)
                        }

                        InputGroup {
                            Text("Cardholder name")
                                .defaultButtonFont(color: .blue.opacity(0.75), size: 12)
                                .padding(.bottom, 4)

                            FormTextField(
                                "E.g. Gertjan Jansen",
                                value: $viewModel.formFields.ccholder,
                                editMode: true
                            )
                            .padding(.bottom)
                        }

                        InputGroup {
                            Text("Expiration date")
                                .defaultButtonFont(color: .blue.opacity(0.75), size: 12)
                                .padding(.bottom, 4)

                            ExpirationDateField(
                                "MM/YY",
                                value: $viewModel.formFields.ccexp
                            )
                            .padding(.bottom)
                        }

                        InputGroup {
                            Text("Secure code")
                                .defaultButtonFont(color: .blue.opacity(0.75), size: 12)
                                .padding(.bottom, 4)

                            PasswordField(
                                placeholder: "035",
                                password: $viewModel.formFields.cvv,
                                isHidden: false,
                                showGenerateButton: false,
                                keyboardType: .numberPad,
                                characterLimit: 4
                            )
                            .padding(.bottom)
                        }

                        InputGroup {
                            Text("Pin code")
                                .defaultButtonFont(color: .blue.opacity(0.75), size: 12)
                                .padding(.bottom, 4)

                            PasswordField(
                                placeholder: "1242",
                                password: $viewModel.formFields.pin,
                                isHidden: false,
                                showGenerateButton: false,
                                keyboardType: .numberPad,
                                characterLimit: 4
                            )
                            .padding(.bottom)

                        }

                        InputGroup {
                            Text("ZIP")
                                .defaultButtonFont(color: .blue.opacity(0.75), size: 12)
                                .padding(.bottom, 4)

                            FormTextField(
                                "Enter a ZIP or postal code",
                                value: $viewModel.formFields.zipcode,
                                editMode: true
                            )
                            .padding(.bottom)
                        }

                        InputGroup {
                            Text("Notes")
                                .defaultButtonFont(color: .blue.opacity(0.75), size: 12)
                                .padding(.bottom, 4)

                            NotesField(
                                label: "Anything else you'd like to add.",
                                value: $viewModel.formFields.notes,
                                onFocusChange: { hasFocus in
                                    if hasFocus {
                                        withAnimation {
                                            proxy.scrollTo(4)
                                        }
                                    }
                                }
                            )
                            .id(4)
                            .padding(.bottom)
                        }

                        DefaultButton(title: "Save", action: save)
                            .disabled(viewModel.saveButtonDisabled)
                            .frame(height: 50)

                        Spacer()
                    }
                    .padding([.horizontal, .top])
                }
            }
        }
        .onReceive(viewModel.itemCreated) { _ in
            onCardCreated()
        }
    }
}
