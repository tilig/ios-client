//
//  CircularViews.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 13/03/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct CircularProgressBar: View {
    var value: Int
    var total: Int
    var radius = 12
    var lineWidth: Double = 2
    var strokeColor = Color.green

    var body: some View {
        Circle()
            .fill(Color.clear)
            .frame(width: CGFloat(radius) * 2, height: CGFloat(radius) * 2)
            .overlay(
                ZStack {
                    Circle()
                        .stroke(
                            style: StrokeStyle(
                                lineWidth: CGFloat(lineWidth),
                                lineCap: .round,
                                lineJoin: .round
                            )
                        )
                        .foregroundColor(.gray.opacity(0.1))

                    Circle().trim(from: 0, to: progress())
                        .stroke(
                            style: StrokeStyle(
                                lineWidth: CGFloat(lineWidth),
                                lineCap: .round,
                                lineJoin: .round
                            )
                        )
                        .rotationEffect(.degrees(-90))
                        .foregroundColor(strokeColor)
                        .animation(
                            .easeInOut(duration: 0.2)
                        )
                }
            )
    }

    func progress() -> CGFloat {
        return (CGFloat(value) / CGFloat(total))
    }
}

struct CircularProgressButton: View {
    var action: () -> Void
    var progress: Int = 1

    var body: some View {
        Button(action: action) {
            ZStack {
                VStack {
                    Image(systemName: "chevron.right")
                        .foregroundColor(.white)
                        .padding()
                }
                .frame(width: 54, height: 54)
                .background(
                    Circle().foregroundColor(.mediumPurple)
                )
                .shadow(color: .borderGray, radius: 1, x: 0, y: 1)
                .overlay(
                    VStack {
                        CircularProgressBar(
                            value: progress,
                            total: 4,
                            radius: 36,
                            lineWidth: 3.5,
                            strokeColor: .mediumPurple
                        )
                    }
                )
            }
        }
    }
}
