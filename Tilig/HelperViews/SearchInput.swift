//
//  SearchInput.swift
//  Tilig
//
//  Created by Gertjan Jansen on 13/01/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import SwiftUI

struct SearchInput: View {
    var label: String = "Search for an item"
    @Binding var searchText: String
    @FocusState private var focused: Bool
    var onEditingChanged: (Bool) -> Void = { _ in }
    var onFocusChanged: (Bool) -> Void = { _ in }

    var body: some View {
        HStack {
            Image(systemName: "magnifyingglass")
                .padding(.leading, 8)

            TextField(label,
                text: $searchText,
                onEditingChanged: { edit in
                    focused = edit
                    onFocusChanged(edit)
                    onEditingChanged(edit)
                }
            )
            .focused(self.$focused)
            .foregroundColor(.blue.opacity(0.5))
            .font(.stolzlBook(size: 16))
            .frame(height: 35)
            // TODO: Find another fix for Autocorrection triggering navigation back to item list
            .disableAutocorrection(true)
            .padding(.leading, 2)

            if focused {
                Button(action: {
                    self.searchText = ""
                    focused = false
                    onFocusChanged(false)
                }) {
                    Image(systemName: "xmark.circle.fill")
                        .padding(.trailing, 8)
                }
            }
        }
        .padding(EdgeInsets(top: 8, leading: 8, bottom: 8, trailing: 6))
        .foregroundColor(.blue.opacity(0.5))
        .background(focused ? Color.white : Color.backgroundGray)
        .cornerRadius(10.0)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(focused ? Color.mediumPurple : .brightGray, lineWidth: 1)
        )
        .padding(.bottom, 8)
        .onTapGesture {
            focused = true
            onFocusChanged(true)
        }
    }
}

#if DEBUG
struct SearchInput_Previews: PreviewProvider {
    @State static var searchText = "Some text"

    static var previews: some View {
        VStack {
            SearchInput(label: "Search for text", searchText: $searchText)
        }.padding()
    }
}
#endif
