//
//  LegacyTextField.swift
//  Tilig
//
//  Created by Gertjan Jansen on 08/10/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

// Focussable input for iOS 13 & 14
// Source: https://stackoverflow.com/a/59059359
struct FocussableTextField: UIViewRepresentable {
    @Binding public var isFirstResponder: Bool
    @Binding public var text: String
    public var placeholder: String?

    public var configuration = { (_: UITextField) in }

    public init(
        placeholder: String? = nil,
        text: Binding<String>,
        isFirstResponder: Binding<Bool>,
        configuration: @escaping (UITextField) -> Void = { _ in }
    ) {
        self.configuration = configuration
        self._text = text
        self.placeholder = placeholder
        self._isFirstResponder = isFirstResponder
    }

    public func makeUIView(context: Context) -> UITextField {
        let view = UITextField()
        view.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        view.addTarget(context.coordinator, action: #selector(Coordinator.textViewDidChange), for: .editingChanged)
        view.delegate = context.coordinator
        return view
    }

    public func updateUIView(_ uiView: UITextField, context: Context) {
        uiView.text = text
        uiView.placeholder = placeholder
        configuration(uiView)
        DispatchQueue.main.async {
            switch isFirstResponder {
            case true: uiView.becomeFirstResponder()
            case false: uiView.resignFirstResponder()
            }
        }
    }

    public func makeCoordinator() -> Coordinator {
        Coordinator($text, isFirstResponder: $isFirstResponder)
    }

    public class Coordinator: NSObject, UITextFieldDelegate {
        var text: Binding<String>
        var isFirstResponder: Binding<Bool>

        init(_ text: Binding<String>, isFirstResponder: Binding<Bool>) {
            self.text = text
            self.isFirstResponder = isFirstResponder
        }

        @objc public func textViewDidChange(_ textField: UITextField) {
            self.text.wrappedValue = textField.text ?? ""
        }

        public func textFieldDidBeginEditing(_ textField: UITextField) {
            self.isFirstResponder.wrappedValue = true
        }

        public func textFieldDidEndEditing(_ textField: UITextField) {
            DispatchQueue.main.async {
                self.isFirstResponder.wrappedValue = false
            }
        }

        // Added this myself. In most cases we want to resign input on return
        public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            self.isFirstResponder.wrappedValue = false
            return false
        }
    }
}
