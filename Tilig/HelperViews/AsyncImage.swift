//
//  AsyncImageView.swift
//  Tilig
//
//  Created by Gertjan Jansen on 22/09/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import SwiftUI
import Combine
import SDWebImageSVGCoder

// Simple solution for `RLRequest.CachePolicy.returnCacheDataElseLoad` (below)
// not working as I would expect.
class ImageCacher {
    static var shared = ImageCacher()

    var cache: [URL: UIImage] = [:]

    func get(url: URL) -> UIImage? {
        cache[url]
    }

    func set(url: URL, image: UIImage) {
        cache[url] = image
    }
}

class ImageLoader: ObservableObject {
    @Published var image: UIImage?
    @Published var error: Error?

    private var currentURL: URL?
    private var cancellable: AnyCancellable?

    deinit {
        cancellable?.cancel()
    }

    func load(url: URL) {
        Task.detached(priority: .background) {
            if let cached = ImageCacher.shared.get(url: url) {
                await self.updateImage(cached)
                return
            }

            // Cache the request
            let request = URLRequest(
                url: url,
                cachePolicy: URLRequest.CachePolicy.returnCacheDataElseLoad
            )

            guard self.currentURL != url else { return }
            self.currentURL = url

            let (data, response) = try await URLSession.current.data(for: request)
            // Throw URLError for non-200 responses
            guard let httpResponse = response as? HTTPURLResponse,
                  httpResponse.statusCode == 200 else {
                throw URLError(.badServerResponse)
            }

            // Parse data into image
            if let image = UIImage(data: data) {
                await self.updateImage(image)
                return
            }
            let svgCoderOptions = [SDImageCoderOption.decodeThumbnailPixelSize: CGSize(width: 100, height: 100)]
            if let image = SDImageSVGCoder().decodedImage(with: data, options: svgCoderOptions) {
                await self.updateImage(image)
            }

            throw URLError(.badServerResponse)
        }
    }

    @MainActor
    func updateImage(_ image: UIImage) async {
        self.image = image
    }
}

@available(iOS 14, *) struct AsyncImage<Placeholder: View, ActivityIndicator: View>: View {
    @StateObject private var loader = ImageLoader()

    var url: URL
    /// Placeholder for 'notFound'
    var placeholder: Placeholder?
    var activityIndicator: ActivityIndicator?

    var body: some View {
        image
            .onAppear {
                loader.load(url: url)
            }
            .onChange(of: url, perform: { newValue in
                loader.load(url: newValue)
            })
    }

    private var image: some View {
        Group {
            if loader.image != nil {
                Image(uiImage: loader.image!)
                    .renderingMode(.original)
                    .resizable()
            } else if loader.error != nil {
                placeholder
            } else {
                activityIndicator
            }
        }
    }
}

#if DEBUG
struct AsyncImageView_Previews: PreviewProvider {
    static var placeholder = ActivityIndicator()
    static var activityIndicator = ActivityIndicator()

    static var previews: some View {
        if #available(iOS 14, *) {
            AsyncImage(
                url: URL(string: "https://tilig.com/theme/images/headerlogo.png")!,
                placeholder: self.placeholder,
                activityIndicator: self.activityIndicator
            )
            .aspectRatio(contentMode: .fit)
            .frame(width: 60, height: 60, alignment: .center)
        } else {
            EmptyView()
        }
    }
}
#endif
