//
//  RoundedCornerStyle.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 28/01/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

/// ViewModifier for rounding specific `corners` of a view with particular `radius`

// Example: If you want to round the 2 bottom corners of a view instead all corners,
// you can pass `[.bottomLeft, .bottomRight]` to the `corners` property.
// Used in PasswordField to round just the bottom corners of the Generate Password button

struct RoundedCornerStyle: ViewModifier {
    var radius: CGFloat
    var corners: UIRectCorner

    struct RoundedCornerShape: Shape {

        var radius = CGFloat.infinity
        var corners = UIRectCorner.allCorners

        func path(in rect: CGRect) -> Path {
            let path = UIBezierPath(
                roundedRect: rect,
                byRoundingCorners: corners,
                cornerRadii: CGSize(width: radius, height: radius)
            )
            return Path(path.cgPath)
        }
    }

    func body(content: Content) -> some View {
        content
            .clipShape(RoundedCornerShape(radius: radius, corners: corners))
    }
}

extension View {
    /// view extension to easily use RoundCornerStyle ViewModifier, accepts `radius` and `corners`
    func cornerRadius(radius: CGFloat, corners: UIRectCorner) -> some View {
        ModifiedContent(content: self, modifier: RoundedCornerStyle(radius: radius, corners: corners))
    }
}
