//
//  GlobalErrorHandler.swift
//  Tilig
//
//  Created by Gertjan Jansen on 03/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import SwiftUI

struct GlobalErrorHandler: ViewModifier {
    @State var globalAlertItem: AlertItem?

    func body(content: Content) -> some View {
        content
            // Error handling: same as in ContentView for the main app
            .alert(item: $globalAlertItem) { alertItem in
                alertItem.toAlert()
            }
            /// Use this to easily present alert's based on any error
            .onReceive(NotificationCenter.default.publisher(for: .globalError)) { notification in
                /// If a predefined alert item is passed, present that
                if let alertItem = notification.object as? AlertItem {
                    globalAlertItem = alertItem
                }
                /// If an error is passed, create a generic alert item
                else if let error = notification.object as? Error {
                    // isRefreshFailedError also triggers a route change, which might interfere with
                    // the alert. Therefore wait 1 second in that case.
                    let delay = error.isRefreshFailedError ? 1.0 : 0.0
                    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                        self.globalAlertItem = AlertItem(
                            title: Text("Something went wrong"),
                            message: Text(error.localizedDescription),
                            dismissButton: .default(Text("Ok"))
                        )
                    }
                }
            }
    }
}

extension View {
    func globalErrorHandler() -> some View {
        return self.modifier(GlobalErrorHandler())
    }
}

extension Error {
    /// Post this error to the global error handler, which raises an alert item
    func raiseGlobalAlert() {
        Task { @MainActor in
            NotificationCenter.default.post(name: .globalError, object: self)
        }
    }
}
