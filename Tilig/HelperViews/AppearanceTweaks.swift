//
//  AppearanceTweaks.swift
//  Tilig
//
//  Created by Gertjan Jansen on 03/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

struct AppearanceTweaks {
    var navigationBar = UINavigationBar.appearance()
    var tableView = UITableView.appearance()
    var tabView = UITabBar.appearance()
    var pageControl = UIPageControl.appearance()

    func install() {
        // Recent changes for iOS 15: https://developer.apple.com/forums/thread/682420
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor(red: 58/255, green: 47/255, blue: 88/255, alpha: 1.0)
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        navigationBar.standardAppearance = appearance
        navigationBar.tintColor = .white
        navigationBar.scrollEdgeAppearance = navigationBar.standardAppearance

        // Remove gray background for NavigationViews on iOS 14
        tableView.backgroundColor = .clear

        // Remove transparent tab background
        if #available(iOS 15.0, *) {
            tabView.scrollEdgeAppearance = UITabBarAppearance()
        }

        // Set color for active and non-active page control items
        pageControl.currentPageIndicatorTintColor = .black
        pageControl.pageIndicatorTintColor = .lightGray
    }

    func installForExtension() {
        let textColor: UIColor = .white
        let coloredAppearance = UINavigationBarAppearance()
        coloredAppearance.configureWithTransparentBackground()
        coloredAppearance.backgroundColor = .clear
        coloredAppearance.shadowColor = .clear
        coloredAppearance.titleTextAttributes = [.foregroundColor: textColor]
        coloredAppearance.largeTitleTextAttributes = [.foregroundColor: textColor]

        UIBarButtonItem.appearance().tintColor = textColor

        navigationBar.standardAppearance = coloredAppearance
        navigationBar.compactAppearance = coloredAppearance
        navigationBar.scrollEdgeAppearance = coloredAppearance
        navigationBar.tintColor = textColor
    }
}
