//
//  KeyboardReadable.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 04/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import UIKit
import Combine
import SwiftUI

/// This protocol publishes the when the keyboard is shown and when it closes
protocol KeyboardReadable {
    var keyboardPublisher: AnyPublisher<Bool, Never> { get }
}

extension KeyboardReadable {
    var keyboardPublisher: AnyPublisher<Bool, Never> {
        Publishers.Merge(
            NotificationCenter.default
                .publisher(for: UIResponder.keyboardWillShowNotification)
                .map { _ in true },

            NotificationCenter.default
                .publisher(for: UIResponder.keyboardWillHideNotification)
                .map { _ in false }
        )
        .eraseToAnyPublisher()
    }
}
