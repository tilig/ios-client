//
//  ActivityIndicator.swift
//  Tilig
//
//  Created by Gertjan Jansen on 22/10/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import SwiftUI

struct ConfigurableActivityIndicator: UIViewRepresentable {
    typealias UIView = UIActivityIndicatorView

    var color: UIColor

    func makeUIView(context: UIViewRepresentableContext<Self>) -> UIActivityIndicatorView {
        UIActivityIndicatorView()
    }

    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<Self>) {
        uiView.color = color
        uiView.startAnimating()
    }
}

struct ActivityIndicator: View {
    @Environment(\.colorScheme) var colorScheme
    var color: Color?

    private var getColor: UIColor {
        if let color { return UIColor(color) }
        return colorScheme == .dark ? .white : .black
    }

    var body: some View {
        ConfigurableActivityIndicator(color: getColor)
    }
}
