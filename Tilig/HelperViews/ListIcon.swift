//
//  SwiftUIView.swift
//  Bolted
//
//  Created by Jacob Schatz on 1/23/20.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import SwiftUI
import Combine
import os.log

struct ListIcon: View {
    var iconURL: URL?
    var background = true
    var backgroundColor = Color(.secondarySystemBackground)
    var hasPadding = false
    var cornerRadius: CGFloat = 5.0
    var itemType: ItemType? = .login

    func createImage(systemName: String, color: Color) -> some View {
        Image(systemName: systemName)
            .renderingMode(.template)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .cornerRadius(cornerRadius)
            .foregroundColor(color)
            .padding(hasPadding ? 12 : 0)
    }

    var body: some View {
        ZStack {
            if background {
                RoundedRectangle(cornerRadius: cornerRadius)
                    .fill(backgroundColor)
            }

            switch itemType {
            case .login:
                icon
                    .aspectRatio(contentMode: .fit)
                    .cornerRadius(cornerRadius)
                    .padding(hasPadding ? 12 : 0)
            case .note:
                createImage(systemName: "doc.text", color: .mediumPurple)
            case .wifi:
                createImage(systemName: "wifi", color: .wifiBlue)
            case .card:
                createImage(systemName: "creditcard", color: .creditCardGreen)
            case .none, .custom:
                EmptyView()
            }
        }
        .compositingGroup()
    }

    var icon: some View {
        Group {
            if let iconURL = iconURL {
                AsyncImage(
                    url: iconURL,
                    placeholder: noImagePlaceholder,
                    activityIndicator: activityIndicator
                )
            } else {
                noImagePlaceholder
            }
        }
    }

    var activityIndicator = ActivityIndicator()

    var noImagePlaceholder: some View {
        Image("placeholder_icon")
            .renderingMode(.original)
            .resizable()
    }
}

#if DEBUG
 struct ListIcon_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            HStack {
                ListIcon(
                    iconURL: URL(string: "https://asset.brandfetch.io/ido5G85nya/idnsPRMIrl.jpeg")!,
                    background: true,
                    backgroundColor: .white,
                    hasPadding: true,
                    cornerRadius: 8,
                    itemType: .login
                )
                .clipped()
                .frame(width: 80, height: 80, alignment: .center)
                .opacity(0.5)
                .padding()
            }
            .background(Color.blue)

            ListIcon(iconURL: ItemPreviewData().data[0].iconURL)
                .frame(width: 80, height: 80, alignment: .center)
        }

    }
 }
#endif
