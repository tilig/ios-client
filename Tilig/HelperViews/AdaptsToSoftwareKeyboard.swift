//
//  AdaptsToSoftwareKeyboard.swift
//  Tilig
//
//  Created by Jacob Schatz on 2/11/20.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import Foundation
import UIKit
import Combine
import SwiftUI

struct AdaptsToSoftwareKeyboard: ViewModifier {
    @State var currentHeight: CGFloat = 0 {
        didSet {
            guard currentHeight > 0 else { return }
            if lowestHeight == nil || currentHeight < lowestHeight! {
                self.lowestHeight = currentHeight
            }
        }
    }

    // I found out that the QuickType bar makes the keyboard height too high.
    // If we use that height as the offset, the padding is too high, resulting in a white bar
    // above the keyboard. Therefore we're tracking what the lowest height is that we encountered,
    // which should be the height of the keyboard without the QuickType bar.
    // (This is not waterproof, but we haven't found a better way to do it yet)
    @State var lowestHeight: CGFloat?
    @State var cancellable: AnyCancellable?

    // This is a fix for a small white bar that otherwise appears
    // on top of the keyboard. Somehow the value `rect.height` is a bit too high.
    // No clue why.
    var heightCorrection: CGFloat = 5

    var bottomPadding: CGFloat {
        currentHeight == 0 ? 0 : (lowestHeight ?? 0) - heightCorrection
    }

    func body(content: Content) -> some View {
        Group {
            content
                .padding(.bottom, currentHeight)
                .edgesIgnoringSafeArea(currentHeight == 0 ? Edge.Set() : .bottom)
        }
        .environment(\.keyboardHeight, currentHeight)
        .onAppear(perform: subscribeToKeyboardEvents)
        .onDisappear {
            cancellable?.cancel()
        }
    }

    private let keyboardWillOpen = NotificationCenter.default
        .publisher(for: UIResponder.keyboardWillShowNotification)
        .compactMap { $0.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? CGRect }
        .map { $0.height }
        .eraseToAnyPublisher()

    private let keyboardWillChangeFrame = NotificationCenter.default
        .publisher(for: UIResponder.keyboardWillChangeFrameNotification)
        .compactMap { $0.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? CGRect }
        .map { $0.height }

        .eraseToAnyPublisher()

    private let keyboardWillHide = NotificationCenter.default
        .publisher(for: UIResponder.keyboardWillHideNotification)
        .map { _ in CGFloat.zero }
        .eraseToAnyPublisher()

    private func subscribeToKeyboardEvents() {
        cancellable = Publishers.MergeMany(keyboardWillOpen, keyboardWillChangeFrame, keyboardWillHide)
            .subscribe(on: RunLoop.main)
            .removeDuplicates()
            .assign(to: \.currentHeight, on: self)
    }
}

private struct KeyboardHeightKey: EnvironmentKey {
    static let defaultValue = CGFloat(0)
}

/// An environment variable to read the current keyboard height.
/// Useful for debugging.
extension EnvironmentValues {
    var keyboardHeight: CGFloat {
        get { self[KeyboardHeightKey.self] }
        set { self[KeyboardHeightKey.self] = newValue }
    }
}
