//
//  RouteModel.swift
//  Tilig
//
//  Created by Gertjan Jansen on 31/05/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

enum Route: Equatable {
    case itemList
}

@MainActor class RouteModel: ObservableObject {
    @Published private(set) var currentRoute: Route
    @Published private(set) var initialRoute: Route

    init(initialRoute: Route = .itemList) {
        self.currentRoute = initialRoute
        self.initialRoute = initialRoute
    }

    /// Push a new route, with an animation
    /// - route: The new route to push
    /// - forceOverwrite: Force a route publish, even it it is the same as the current one
    func pushRoute(_ route: Route, forceOverwrite: Bool = false) {
        // Don't do anything if it's not the same as the current route
        // to prevent ObservedObject triggering an update when route has not changed
        // because that breaks certain SwiftUI views.
        if currentRoute == route && !forceOverwrite {
            Log("Skipping route change (is current): \(currentRoute)", category: .routing)
            return
        }

        Log("Animating to new route: \(currentRoute)", category: .routing)
        withAnimation(.easeIn(duration: 0.5)) {
            currentRoute = route
        }
    }
}
