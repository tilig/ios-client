//
//  GlobalFactory.swift
//  Tilig
//
//  Created by Gertjan Jansen on 10/11/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import Combine
import SwiftUI

/// This factory takes care of models that should never be replaced or re-initialized. This factory itself should be
/// initialized on app/scene launch and not reinitialized after that.
@MainActor class GlobalFactory: ObservableObject {

    init() {
        authManager = AuthManager()

        autoFillViewModel = EnableAutoFillViewModel()

        routeModel = RouteModel()

        biometricViewModel = BiometricViewModel()

        // Maybe this should be owned by AuthManager to make it simpler
        mixpanelManager = MixpanelManager(
            authState: authManager.authState,
            authStatePublisher: authManager.$authState.eraseToAnyPublisher()
        )

        createAuthenticatedModelFactoryOnAuthChange()
    }

    let authManager: AuthManager
    let routeModel: RouteModel
    let mixpanelManager: MixpanelManager
    let autoFillViewModel: EnableAutoFillViewModel
    let biometricViewModel: BiometricViewModel

    // MARK: ViewModelFactory

    @Published var authenticatedModelFactory: AuthenticatedModelFactory?
    var cancellable: AnyCancellable?

    func createAuthenticatedModelFactoryOnAuthChange() {
        cancellable = authManager.$authState.removeDuplicates().sink { [weak self] authState in
            guard let self else { return }
            // It's a little bit weird that withAnimation is used here, but these makes sure the slide
            // animation in ContentView works. Other alternative would be to use an onReceive wrapper
            // in ContentView that applies the `withAnimation`. Both approaches have pros and cons.
            withAnimation {
                if let authState {
                    self.authenticatedModelFactory = AuthenticatedModelFactory(
                        authManager: self.authManager,
                        tokenProvider: authState,
                        userProfile: authState.userProfile,
                        crypto: Crypto(keyPair: authState.keyPair, legacyKeyPair: authState.legacyKeyPair)
                    )
                } else {
                    self.authenticatedModelFactory?.deactivate()
                    self.authenticatedModelFactory = nil
                }
            }
        }
    }

}
