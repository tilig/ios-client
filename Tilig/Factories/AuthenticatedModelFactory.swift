//
//  AuthenticatedModelFactory.swift
//  Tilig
//
//  Created by Gertjan Jansen on 28/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import Combine
import Sentry

@MainActor class AuthenticatedModelFactory: ObservableObject {
    /// Initialized with:
    /// - authManager, to pass signout and deletion events up.
    /// - tokenProvider, for making requests
    /// - userProfile, to show the username and email
    /// - crypto, for encryption and decryption
    init(authManager: AuthManager, tokenProvider: TokenProvider, userProfile: UserProfile, crypto: Crypto) {
        self.authManager = authManager
        self.tokenProvider = tokenProvider
        self.userProfile = userProfile
        self.crypto = crypto
    }

    /// Clears all state.
    /// This should be called by the GlobalFactory, before deinitialization. (SwiftUI might keep this object
    /// alive longer, so we want to do some cleanup instead of waiting for deinit.)
    func deactivate() {
        // Clear all items, just in case the itemViewModel is not being removed
        // from memory by clearing the cache. This should also clear the cred loader.
        itemListViewModel.clearItems()
        // Clear the item instance cache
        instanceFactory = nil
    }

    private var authManager: AuthManager
    private var tokenProvider: TokenProvider
    private var userProfile: UserProfile
    private var crypto: Crypto

    private(set) var credLoader: CredentialIdentitiesLoader?

    private(set) lazy var itemListViewModel: ItemListViewModel = {
        let viewModel = ItemListViewModel(
            crypto: crypto,
            tokenProvider: tokenProvider
        )

        // Credential identities loader is dependent on the ItemListViewModel
        // Would be nice if we can create a better solution for this
        credLoader = CredentialIdentitiesLoader(
            crypto: crypto,
            items: viewModel.items.map { $0.encryptedItem },
            itemsPublisher: viewModel.$items.map { $0.map { $0.encryptedItem } }.eraseToAnyPublisher()
        )

        return viewModel
    }()

    private var signOutCancellable: AnyCancellable?
    private var deleteUserCancellable: AnyCancellable?

    private(set) lazy var settingsViewModel: SettingsViewModel = {
        SettingsViewModel(
            authManager: authManager,
            userProfile: userProfile
        )
    }()

    private(set) lazy var getStartedViewModel: GetStartedViewModel = {
        return GetStartedViewModel(
            userProfile: userProfile,
            autoFillEnabled: EnableAutoFillViewModel().autoFillEnabled,
            biometricViewModel: BiometricViewModel(),
            tokenProvider: tokenProvider
        )
    }()

    private var itemCreatedCancellable: AnyCancellable?

    // Don't cache this one. We want to re-create this for every new call.
    func addItemViewModel(forType itemType: ItemType) -> any AddItemViewModelProtocol {
        var viewModel: any AddItemViewModelProtocol
        switch itemType {
        case .login:
            viewModel = AddLoginViewModel(crypto: crypto, tokenProvider: tokenProvider)
        case .note:
            viewModel = AddNoteViewModel(crypto: crypto, tokenProvider: tokenProvider)
        case .wifi:
            viewModel = AddWifiViewModel(crypto: crypto, tokenProvider: tokenProvider)
        case .card:
            viewModel = AddCardViewModel(crypto: crypto, tokenProvider: tokenProvider)
        case .custom:
            // TODO: Implement Custom types
            fatalError()
        }
        itemCreatedCancellable = viewModel
            .itemCreated
            .sink { [weak self] encryptedItem in
                guard let self else { return }
                Task {
                    await self.itemListViewModel.add(encryptedItem: encryptedItem)
                }
            }
        return viewModel
    }

    // MARK: Item instance

    var instanceFactory: ItemInstanceFactory?

    private var currentItemId: UUID?

    private var itemUpdatedCancellable: AnyCancellable?
    private var itemDeletedCancellable: AnyCancellable?

    /// Set the currently active instance. This should reset all cached viewmodels, as soon as each of them is called.
    /// For optimization, we could add pass in the details and overview, but I decided to keep it simple.
    func setInstance(forItem encryptedItem: Item) throws {
        // Don't clear cache when we're instantiating the same item
        if currentItemId == encryptedItem.id { return }
        currentItemId = encryptedItem.id

        instanceFactory = try ItemInstanceFactory(
            encryptedItem: encryptedItem,
            crypto: crypto,
            tokenProvider: tokenProvider,
            userProfile: userProfile
        )

        // Propagate instance updates and deletions to the list viewmodel
        itemUpdatedCancellable = instanceFactory?.itemUpdated.sink { [weak self] itemWrapper in
            self?.itemListViewModel.update(itemWrapper: itemWrapper)
            self?.shareItemViewModel?.update(with: itemWrapper)
        }
        itemDeletedCancellable = instanceFactory?.itemDeleted.sink { [weak self] item in
            self?.itemListViewModel.remove(item: item.encryptedItem)
        }
    }

    /// Remove the currently active instance. This is to prevent cached items from not getting updated
    func clearInstance() {
        currentItemId = nil
        instanceFactory = nil
    }

    // MARK: Temp instance factory shortcuts

    var itemViewModel: ItemViewModel? { instanceFactory?.itemViewModel }
    var editItemViewModel: EditItemViewModel? { instanceFactory?.editItemViewModel }
    var twoFactorViewModel: TwoFactorViewModel? { instanceFactory?.twoFactorViewModel }
    var deleteItemViewModel: DeleteItemViewModel? { instanceFactory?.deleteItemViewModel }
    var shareItemViewModel: ShareItemViewModel? { instanceFactory?.shareItemViewModel }

}
