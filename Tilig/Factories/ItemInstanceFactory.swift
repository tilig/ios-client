//
//  ItemInstanceFactory.swift
//  Tilig
//
//  Created by Gertjan Jansen on 12/11/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import Combine

/// The `ItemInstanceFactory` manages all viewmodels related to a specific item instance.
/// It serves as a cache for a particular item instance, and manages relationships between the different viewmodels.
/// It exposes passthroughsubjects for when an item is updated or deleted, to let the outside world know when
/// a change was made.
@MainActor class ItemInstanceFactory: ObservableObject {
    struct InvalidInstanceError: Error { }

    /// Set the currently active instance. This should reset all cached viewmodels, as soon as each of them is called.
    /// For optimization, we could add pass in the details and overview, but I decided to keep it simple.
    init(encryptedItem: Item, crypto: Crypto, tokenProvider: TokenProvider, userProfile: UserProfile) throws {
        let currentItemWrapper = try crypto.decrypt(fullItem: encryptedItem)

        guard let id = encryptedItem.id else {
            throw InvalidInstanceError()
        }

        self.id = id
        self.currentItemWrapper = currentItemWrapper
        self.crypto = crypto
        self.tokenProvider = tokenProvider
        self.userProfile = userProfile
    }

    var itemUpdated: AnyPublisher<ItemWrapper, Never> { itemUpdatedSubject.eraseToAnyPublisher() }
    var itemUpdatedSubject = PassthroughSubject<ItemWrapper, Never>()

    var itemDeleted: AnyPublisher<ItemWrapper, Never> { itemDeletedSubject.eraseToAnyPublisher() }
    var itemDeletedSubject = PassthroughSubject<ItemWrapper, Never>()

    private var currentItemWrapper: ItemWrapper
    private var id: UUID
    private var crypto: Crypto
    private var tokenProvider: TokenProvider
    private var userProfile: UserProfile

    private(set) lazy var itemViewModel: ItemViewModel = {
        ItemViewModel(itemWrapper: currentItemWrapper)
    }()

    private var sharedItemUpdatedCancellable: AnyCancellable?

    private(set) lazy var shareItemViewModel: ShareItemViewModel = {
        let viewModel = ShareItemViewModel(
            itemWrapper: currentItemWrapper,
            crypto: crypto,
            tokenProvider: tokenProvider,
            userProfile: userProfile
        )

        sharedItemUpdatedCancellable = viewModel.itemUpdated.sink { itemWrapper in
            self.itemUpdatedSubject.send(itemWrapper)
            self.currentItemWrapper = itemWrapper
            self.itemViewModel.update(with: itemWrapper)
        }

        return viewModel
    }()

    private var updatedCancellable: AnyCancellable?

    private(set) lazy var editItemViewModel: EditItemViewModel = {
        let viewModel = EditItemViewModel(
            id: id,
            itemWrapper: currentItemWrapper,
            crypto: crypto,
            tokenProvider: tokenProvider
        )

        updatedCancellable = viewModel.itemUpdated.sink { (itemWrapper, _) in
            self.itemUpdatedSubject.send(itemWrapper)
            self.currentItemWrapper = itemWrapper
            self.itemViewModel.update(with: itemWrapper)
            self.shareItemViewModel.update(with: itemWrapper)
            if itemWrapper.itemType == .login {
                self.twoFactorViewModel.updateItem(name: itemWrapper.name, website: itemWrapper.url)
            }
        }

        return viewModel
    }()

    private var deletedCancellable: AnyCancellable?

    private(set) lazy var deleteItemViewModel: DeleteItemViewModel = {
        let viewModel = DeleteItemViewModel(
            item: currentItemWrapper.encryptedItem,
            tokenProvider: tokenProvider
        )

        deletedCancellable = viewModel
            .itemDeleted
            .sink { [weak self] _ in
                guard let self else { return }
                self.itemDeletedSubject.send(self.currentItemWrapper)
            }

        return viewModel
    }()

    private var tokenUpdatedCancellable: AnyCancellable?

    private(set) lazy var twoFactorViewModel: TwoFactorViewModel = {
        let viewModel = TwoFactorViewModel(
            name: editItemViewModel.name,
            website: editItemViewModel.url,
            plainOTPToken: editItemViewModel.totp
        )

        // Connect the 2FA model to the edit account model
        tokenUpdatedCancellable = viewModel.$plainOTPToken.sink(receiveValue: { [weak self] plainOTPToken in
            if let plainOTPToken = plainOTPToken {
                Task { await self?.editItemViewModel.updateOTP(plainOTPToken) }
            }
        })

        return viewModel
    }()
}
