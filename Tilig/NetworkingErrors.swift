//
//  Errors.swift
//  Tilig
//
//  Created by Gertjan Jansen on 12/08/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation

// Convenience method to filter our networking errors easier
extension Error {
    /// Is this a .badServerResponse URLError?
    var isBadServerResponseError: Bool {
        if let urlError = self as? URLError, case .badServerResponse = urlError.code {
            return true
        }
        return false
    }

    /// Is this a .badURL URLError?
    var isBadURLError: Bool {
        if let urlError = self as? URLError, case .badURL = urlError.code {
            return true
        }
        return false
    }

    /// Is this a 404?
    var isNotFoundError: Bool {
        if let urlError = self as? URLError, case .resourceUnavailable = urlError.code {
            return true
        }
        return false
    }

    // Is it an AuthError.unauthorized error?
    var isUnauthorizedError: Bool {
        if let authError = self as? AuthError, case .unauthorized = authError {
            return true
        }
        return false
    }

    // Is it an AuthError.refreshFailed?
    var isRefreshFailedError: Bool {
        if let authError = self as? AuthError, case .refreshFailed = authError {
            return true
        }
        return false
    }
}
