//
//  ContentView.swift
//  Tilig
//
//  Created by Jacob Schatz on 1/22/20.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import SwiftUI
import Combine
import os.log

struct ContentView: View {
    @EnvironmentObject var authManager: AuthManager
    @EnvironmentObject var globalFactory: GlobalFactory

    var body: some View {
        VStack {
            ShutdownView()
            ZStack {
                if let authState = authManager.authState,
                   let authenticatedModelFactory = globalFactory.authenticatedModelFactory {
                    SignedInView()
                    // Enforce SwiftUI to rerender. Using the accessToken here because that's
                    // unique, even when singin in with the same account again.
                    // When you change this, make sure you test the transition animations well.
                        .id(authState.tiligAuthTokens.accessToken)
                        .transition(.asymmetric(
                            // Sign in
                            insertion: .move(edge: .trailing),
                            // Sign out
                            removal: .opacity
                        ))
                        .environmentObject(
                            AuthStateWrapper(
                                authState: authState,
                                authStatePublisher: authManager.$authState.eraseToAnyPublisher()
                            )
                        )
                        .environmentObject(authenticatedModelFactory)
                } else {
                    IntroView()
                        .transition(.asymmetric(insertion: .slide, removal: .move(edge: .leading)))
                }
            }
        }
        .globalErrorHandler()
        .onAppear {
            AppearanceTweaks().install()
        }
        .preferredColorScheme(.light)
    }
}
