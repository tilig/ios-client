//
//  MigrationNotice.swift
//  Tilig
//
//  Created by Gertjan Jansen on 27/05/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Combine
import Foundation
import os.log

struct MigrationMessage: Decodable {
    var message: String?
}

class MigrationNoticeChecker {
    @Published var migrationNotice: String?

    var cancellable: AnyCancellable?

    func activate() {
        let baseURL = URL(string: ConfigLoader.loadConfig().backendURL)!
        let url = URL(string: "\(baseURL)/v3/migration_notice")!
        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        // Do a request every 2 minutes
//        cancellable = Timer.publish(every: 20, on: .main, in: .default)
//            .autoconnect()
//            .setFailureType(to: URLError.self) // this is required for iOS 13
//            .flatMap({ _ in
//                return URLSession.shared.dataTaskPublisher(for: request)
//            })

        cancellable = URLSession.shared.dataTaskPublisher(for: request)
            .tryMap({ data, response -> Data in
                print("CHECK")
                guard let httpResponse = response as? HTTPURLResponse,
                      httpResponse.statusCode == 200 else {
                    throw URLError(.badServerResponse)
                }
                OSLog.debug(String(data: data, encoding: .utf8) ?? "--")
                return data
            })
            .decode(type: MigrationMessage.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .map {
                $0.message
            }
            // when this endpoint returns an error, don't show the notice
            .catch({ (error) -> Just<String?> in
                OSLog.debug("Catched migration notice error: \(error)")
                return Just(nil)
            })
            .assign(to: \.migrationNotice, on: self)
    }
}
