//
//  KeyChainClearer.swift
//  Tilig
//
//  Created by Gertjan Jansen on 15/01/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import Foundation
import Valet

class KeyChainClearer {
    /// Sets a flag to be able to track if the app was reinstalled
    static func setPersistenceFlag() {
        UserDefaults.standard.set(true, forKey: "persistCache")
    }

    /// The app is reinstalled, or is a clean install.
    static var isReinstalled: Bool {
        !UserDefaults.standard.bool(forKey: "persistCache")
    }

    /// If the user uninstalls Tilig, we want the chache to be cleared.
    /// Unfortunately, the only way to do that is with a UserDefault flag.
    /// An even better way to do this would be to entangle it with a key that
    /// is stored in the UserDefaults, as described here: https://developer.apple.com/forums/thread/36442.
    static func clearIfReinstalled() throws {
        if isReinstalled {
            Log("Fresh install => clearing keychain.")
            // Clear the auth cache. This should also clear the biometrics flag.
            try AuthState.clearCache()
            // Clear any items that were cached in the keychain.
            try ItemsCache().clear()
        }
    }
}
