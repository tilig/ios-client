//
//  BiometricLockView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 29/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct BehindBiometricLockViewModifier: ViewModifier {
    @EnvironmentObject var viewModel: BiometricViewModel

    func body(content: Content) -> some View {
        ZStack {
            content

            if viewModel.isEnabled {
                BiometricLockView()
            }
        }
    }
}

extension View {
    func behindBiometricLock() -> some View {
        modifier(BehindBiometricLockViewModifier())
    }
}

struct BiometricLockView: View {
    @EnvironmentObject var viewModel: BiometricViewModel

    var displayLogo: Bool = true

    @State var slideOpen = false
    @State var isLocked = false

    var body: some View {
        BiometricLockRenderingView(
            isLocked: isLocked,
            slideOpen: slideOpen,
            biometryPermissionDenied: viewModel.biometryPermissionDenied,
            displayLogo: displayLogo,
            action: { viewModel.tryToUnlock() }
        )
        .onAppear { viewModel.tryToUnlock() }
        .onReceive(viewModel.$isLocked) { isLocked in
            let wasLocked = self.isLocked
            self.isLocked = isLocked

            // slideOpen should animate only on unlock, not on lock
            if isLocked {
                slideOpen = false
            } else {
                // don't slide when is was already unlocked (happens after signin)
                withAnimation(.easeIn(duration: wasLocked ? 0.5 : 0)) {
                    slideOpen = true
                }
            }
        }
    }
}

private struct BiometricLockRenderingView: View {
    @Environment(\.horizontalSizeClass) var sizeClass

    // This is not animated
    var isLocked: Bool
    // This is animated
    var slideOpen: Bool

    /// If this is true, then the user disabled biometrics while the lock was active
    var biometryPermissionDenied: Bool

    var displayLogo: Bool
    var action: () -> Void

    var body: some View {
        GeometryReader { proxy in
            VStack {
                VStack {
                    Spacer()

                    if displayLogo {
                        Image("logo_dark_transparent")
                            .resizable()
                            .scaledToFit()
                            .padding(sizeClass == .compact ? 20 : 40)
                            .frame(height: 120)
                            .padding(.top, 100)
                    }

                    Spacer()
                }
                .frame(maxWidth: .infinity)
                .background(Color.white)
                .border(isLocked ? .white : .purple)
                .offset(
                    CGSize(
                        width: 0,
                        height: -proxy.size.height * (slideOpen ? 1.0 : 0.0)
                    )
                )

                VStack {
                    if biometryPermissionDenied {
                        BiometryDeniedLockScreenError()
                    } else {
                        Image(systemName: "lock.fill")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 120, height: 120, alignment: .center)
                            .padding(.vertical, 20)
                            .foregroundColor(.gray)

                        DefaultButton(
                            title: "Unlock Tilig",
                            image: Image(systemName: "lock.open.fill"),
                            action: action
                        )
                        .padding(.horizontal, 30)
                    }

                    Spacer()
                }
                .frame(maxWidth: .infinity)
                .background(Color.white)
                .border(isLocked ? .white : .purple)
                .offset(
                    CGSize(
                        width: 0,
                        height: proxy.size.height * (slideOpen ? 1.0 : 0.0)
                    )
                )
            }
        }
    }
}

struct BiometryDeniedLockScreenError: View {
    var body: some View {
        VStack {
            // swiftlint:disable line_length
            Text("You can't access Tilig because you denied the use of biometrics for this app, while the biometric lock was active. Please re-enable Tilig's permission for face ID (or touch ID) in the settings.")
                .padding(.bottom, 20)

            Link("Open settings", destination: URL(string: UIApplication.openSettingsURLString)!)
        }
        .padding(.horizontal, 30)
        .padding(.bottom, 50)
    }
}

#if DEBUG
struct BiometricLock_Previews: PreviewProvider {
    @State static var slideOpen = false

    static var previews: some View {
        ZStack {
            Color.red

            BiometricLockRenderingView(
                isLocked: false,
                slideOpen: slideOpen,
                biometryPermissionDenied: false,
                displayLogo: true,
                action: {
                    // Don't know why this doesn't work in previews
                    withAnimation {
                        slideOpen = true
                    }
                }
            )
        }
        .ignoresSafeArea()
    }
}
#endif
