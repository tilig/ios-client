//
//  AutoFillTutorialView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 17/03/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct AutoFillTutorialView: View {
    var videoPath: String? {
        return Bundle.main.path(
            forResource: "tutorial",
            ofType: "mov"
        )
    }

    var body: some View {
        VStack(alignment: .leading, spacing: 16) {
            Text("How to enable AutoFill")
                .largeBoldFont(color: .blue, size: 16)
                .multilineTextAlignment(.center)

            if let path = videoPath,
               let url = URL(fileURLWithPath: path) {
                VideoPlayerView(url: url)
                    .padding(.horizontal)
                    .roundedRectangleBackground(
                        fillColor: .mediumPurple.opacity(0.1),
                        shadowColor: .clear,
                        shadowRadius: 0
                    )
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color.brightGray, lineWidth: 1)
                    )
                    .frame(minHeight: 220)
            }

            AutoFillSetupTutorialList()
        }
        .padding()
        .roundedRectangleBackground(fillColor: .lightPurple, shadowColor: .clear, shadowRadius: 0)
    }
}

struct AutoFillSetupTutorialList: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 16) {
            NumberedItemView(
                number: 1,
                text: "Open device settings",
                iconName: "preferences"
            )

            NumberedItemView(
                number: 2,
                customText: {
                    Text("Tap")
                        .defaultButtonFont(color: .blue, size: 14) +
                    Text(" Passwords")
                        .defaultBoldFont(color: .blue, size: 15)
                }(),
                iconName: "key"
            )

            NumberedItemView(
                number: 3,
                customText: Text("Password Options")
                    .defaultBoldFont(color: .blue, size: 15),
                iconName: "password_options"
            )

            NumberedItemView(
                number: 4,
                customText: {
                    Text("Turn on")
                        .defaultButtonFont(color: .blue, size: 14) +
                    Text(" AutoFill Passwords")
                        .defaultBoldFont(color: .blue, size: 15)
                }(),
                iconName: "toggle_button"
            )

            NumberedItemView(
                number: 5,
                customText: {
                    Text("Select")
                        .defaultButtonFont(color: .blue, size: 14) +
                    Text(" Tilig")
                        .defaultBoldFont(color: .blue, size: 15)
                }(),
                iconName: "small_logo"
            )
        }
    }
}

struct AutoFillTutorialView_Previews: PreviewProvider {
    static var previews: some View {
        AutoFillTutorialView()
    }
}
