//
//  SettingsButtonsTesting.swift
//  Tilig
//
//  Created by Gertjan Jansen on 11/01/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

#if DEBUG
// IMPORTANT: Don't include this in release builds to make sure it doesn't get shipped to Apple.
//
// Use this view to test which URL takes you to the settings screen
// Relevant sources:
// - https://medium.com/@contact.jmeyers/complete-list-of-ios-url-schemes-for-apple-settings-always-updated-20871139d72f
// - https://www.reddit.com/r/shortcuts/comments/i9rjbh/an_updated_list_of_settings_urls/
// - https://gist.github.com/deanlyoung/368e274945a6929e0ea77c4eca345560
//
// Current results:
// - On iOS 15 App-prefs:PASSWORDS seems to work best
// - On iOS 14 App-prefs:PASSWORDS seems to work as well
// - On iOS 13 App-prefs:ACCOUNTS_AND_PASSWORDS seems to work
//
struct SettingsButtonsTesting: View {
    let prefixes = [
        "App-prefs:",
        "prefs:root=",
        "prefs:"
    ]

    let routes = [
        "PASSWORDS",
        // Tried these, but no luck:
//        "PASSWORDS&path=AutoFill%20Passwords",
//        "PASSWORDS&path=AUTOFILL_PASSWORDS",
//        "PASSWORDS&path=AUTOFILL",
//        "PASSWORDS&path=ADD_ACCOUNT",
        "Bluetooth",
        "Passwords",
        "ACCOUNTS_AND_PASSWORDS"
    ]

    var urls: [String] {
        return prefixes.map { pre in
            routes.map { route in
                pre.appending(route)
            }
        }.flatMap { $0 }
    }

    var body: some View {
        VStack {
            ForEach(urls, id: \.self) { url in
                Button(action: {
                    let url = URL(string: url)!
                    UIApplication.shared.open(url)
                }) {
                    Text(url)
                }
            }
        }
    }
}
#endif
