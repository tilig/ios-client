//
//  VideoPlayer.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 11/02/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI
import AVKit

struct VideoPlayerView: UIViewRepresentable {
    let url: URL

    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<VideoPlayerView>) {
    }

    func makeUIView(context: Context) -> UIView {
        return PlayerUIView(url: url)
    }

}

class PlayerUIView: UIView {
    private let playerLayer = AVPlayerLayer()

    init(url: URL) {
        super.init(frame: .zero)
        if UIDevice.current.userInterfaceIdiom == .phone {
            playerLayer.videoGravity = .resizeAspectFill
        }
        playerLayer.player = AVPlayer(url: url)
        layer.addSublayer(playerLayer)
        playerLayer.player?.play()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        playerLayer.frame = bounds
    }
}
