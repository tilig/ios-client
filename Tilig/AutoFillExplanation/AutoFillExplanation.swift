//
//  AutoFillExplanation.swift
//  Tilig
//
//  Created by Gertjan Jansen on 07/12/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import SwiftUI
import os.log

struct AutoFillExplanation: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var viewModel: EnableAutoFillViewModel
    @Environment(\.horizontalSizeClass) var sizeClass

    // AutoFillCompletedView implements its own skip button
    var showSkipButton: Bool { viewModel.explanationFlavor == .onboarding }

    var body: some View {
        VStack(alignment: .leading) {
            if !showSkipButton {
                PopupHeaderView(title: "", closeButtonAction: {
                    presentationMode.wrappedValue.dismiss()
                })
            }

            VStack(alignment: .leading, spacing: showSkipButton ? 16 : 8) {
                if showSkipButton {
                    HStack {
                        Spacer()
                        Button(action: { Task { viewModel.skipAutoFill() } }) {
                            Text("Skip")
                                .defaultFont(color: .mediumPurple, size: 15)
                        }
                        .padding(.trailing, 10)
                    }
                }

                Text("""
                Move freely
                while browsing
                """)
                .largeBoldFont(color: .blue, size: 24)
                .multilineTextAlignment(.leading)

                VStack(alignment: .leading, spacing: 10) {
                    Text("Enjoy AutoFill  🎉")
                        .largeBoldFont(color: .blue, size: 16)
                        .multilineTextAlignment(.leading)

                    Text("With AutoFill you can automatically fill in forms when navigating the web")
                        .defaultButtonFont(color: .subTextLightPurple, size: 14)
                        .multilineTextAlignment(.leading)
                        .lineSpacing(4)
                        .frame(maxWidth: .infinity, alignment: .leading)
                }
                .padding()
                .roundedRectangleBackground(fillColor: .lightPurple, shadowColor: .clear, shadowRadius: 0)

                AutoFillTutorialView()

                DefaultButton(title: "Open phone settings", textSize: 14, action: {
                    Task { @MainActor in
                        UIApplication.shared.open(URLOpenTracker.shared.getAndTrackUrl())
                        viewModel.setOnboardingCompleted()
                    }
                })
                .responsivePadding(compact: 0, regular: 16)
            }
            .responsivePadding(.horizontal, compact: 16, regular: 48)
        }
        .preferredColorScheme(.light)
    }
}

#if DEBUG
struct AutoFillExplanation_Previews: PreviewProvider {
    static var previews: some View {
        AutoFillExplanation()
    }
}
#endif
