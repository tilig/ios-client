//
//  AutoFillSheet.swift
//  Tilig
//
//  Created by Gertjan Jansen on 07/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import SwiftUI

struct AutoFillSheetPresenter: ViewModifier {
    var viewModel: EnableAutoFillViewModel
    @State var present = false

    func body(content: Content) -> some View {
        content
            // For some reason, this onAppear line is necessary to make the onReceive calls below work
            // on iOS14 (on real device)
            .onAppear { }
            .onReceive(viewModel.$presentAsSheet) { presentAsSheet in
                self.present = presentAsSheet
            }
            .sheet(isPresented: $present) {
                AutoFillExplanation()
                    .environmentObject(viewModel)
            }
    }
}

extension View {
    func autoFillSheetPresenter(enableAutoFillViewModel: EnableAutoFillViewModel) -> some View {
        self.modifier(AutoFillSheetPresenter(viewModel: enableAutoFillViewModel))
    }
}
