//
//  Endpoints.swift
//  Tilig
//
//  Created by Gertjan Jansen on 12/05/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import Alamofire
import Combine
import os.log

// TODO: this should be a struct!
// read the full article: https://www.swiftbysundell.com/clips/4/
enum Endpoint {
    case authenticate
    case brandSearch(String, Int = 20)
    case getBrand(URL)
    case getPublicKey(String)
    case folderMemberships(UUID)
    case folders
    case getContacts
    case revokeFolderMembership(folderId: String, memberId: String)
    case items
    case item(UUID)
    case polling(Date)
    case keypairs
    case passwordHistory(String)
    case profile
    case userSettings
    case refresh
}

extension Date {
    /// Format this date for use in URL queries
    var urlQueryFormat: String? {
        ISO8601DateFormatter()
            .string(from: self)
            .addingPercentEncoding(withAllowedCharacters: .afURLQueryAllowed)
    }
}

extension Endpoint {
    var url: URL {
        switch self {
        case .authenticate:
            return .makeForEndpoint("authenticate")
        case .folderMemberships(let folderId):
            return .makeForEndpoint("folders/\(folderId)/folder_memberships")
        case .folders:
            return .makeForEndpoint("folders")
        case .getContacts:
            return .makeForEndpoint("contacts")
        case .profile:
            return .makeForEndpoint("profile")
        case .userSettings:
            return .makeForEndpoint("profile/user_settings")
        case .refresh:
            return .makeForEndpoint("refresh")
        case .polling(let changedSince):
            var endpointString = "polling/items"
            if let timestamp = ISO8601DateFormatter()
                    .string(from: changedSince)
                    .addingPercentEncoding(withAllowedCharacters: .afURLQueryAllowed) {
                endpointString.append("?after=\(timestamp)")
            }
            return .makeForEndpoint(endpointString)
        case .items:
            return .makeForEndpoint("items")
        case .item(let itemId):
            return .makeForEndpoint("items/\(itemId)")
        case .brandSearch(let prefix, let pageSize):
            if let prefix = prefix.addingPercentEncoding(withAllowedCharacters: .afURLQueryAllowed) {
                return .makeForEndpoint("brands/search?prefix=\(prefix)&page_size=\(pageSize)")
            }
            return .makeForEndpoint("brands/search?prefix=\(prefix)&page_size=\(pageSize)")
        case .getBrand(let url):
            return .makeForEndpoint("brands?url=\(url.absoluteString)")
        case .getPublicKey(let email):
            return .makeForEndpoint("users/public_key?email=\(email)")
        case .revokeFolderMembership(let folderId, let memberId):
            return .makeForEndpoint("folders/\(folderId)/folder_memberships/\(memberId)")
        case .passwordHistory(let secretId):
            return .makeForEndpoint("items/\(secretId)/password_versions")
        case .keypairs:
            return .makeForEndpoint("keypairs")
        }
    }
}

private extension URL {
    static func makeForEndpoint(_ endpoint: String) -> URL {
        #if DEBUG
        // TODO: this doesn't work for the AutoFill extension. Setup app groups.
        // https://stackoverflow.com/questions/45607903/sharing-userdefaults-between-extensions
        let apiHost = UserDefaults.standard.string(forKey: "backendURL") ?? "https://api-staging.tilig.com/api"
        // let apiHost = "https://app.tilig.com/api"
        return URL(string: "\(apiHost)/v3/\(endpoint)")!
        #else
        return URL(string: "https://api.tilig.com/api/v3/\(endpoint)")!
        #endif
    }
}

typealias DataTaskPublisherOutput = AnyPublisher<URLSession.DataTaskPublisher.Output, Error>

extension URLSession {
    var enableLogging: Bool { UserDefaults.standard.bool(forKey: "logNetworking") == true }

    /// URLSession.shared by default, but can be overwritten for mocks
    static var current = URLSession.shared

    /// The globaly defined bearer token used for all requests by default
    /// by mapping from the AuthState publisher, we
    var accessTokenPublisher: AnyPublisher<Token, Error> {
        AuthState
            .authenticateFromCache()
            .map { $0.tiligAuthTokens.accessToken }
            .eraseToAnyPublisher()
    }

    /// A dataTaskPublisher for signed-in requests. It tries to refresh the access token first, if it is expired.
    func dataTaskPublisher(for endpoint: Endpoint,
                           method: HTTPMethod = .get,
                           body: Data? = nil,
                           authorizationHeader: String? = nil) -> DataTaskPublisherOutput {
        if let authorizationHeader = authorizationHeader {
            return _dataTaskPublisher(for: endpoint,
                                      method: method,
                                      body: body,
                                      authorizationHeader: authorizationHeader
            ).eraseToAnyPublisher()
        }
        return accessTokenPublisher
            .flatMap { [self] accessToken -> DataTaskPublisherOutput in
                self._dataTaskPublisher(
                    for: endpoint,
                    method: method,
                    body: body,
                    authorizationHeader: "Bearer \(accessToken.rawValue)"
                )
            }
            .eraseToAnyPublisher()
    }

    /// A dataTaskPublisher for predefined endpoints, using default headers that don't include an authentication token
    private func _dataTaskPublisher(for endpoint: Endpoint,
                                    method: HTTPMethod = .get,
                                    body: Data? = nil,
                                    authorizationHeader: String) -> DataTaskPublisherOutput {
        var request: URLRequest
        do {
            request = try URLRequest(
                url: endpoint.url,
                method: method,
                headers: headers(withAuthorizationHeader: authorizationHeader)
            )
        } catch {
            return Fail(error: error).eraseToAnyPublisher()
        }
        request.httpBody = body

        assert(request.headers["Content-Type"] != nil)
        assert(request.headers["x-tilig-version"] != nil)
        assert(request.headers["x-tilig-platform"] != nil)

        if enableLogging {
            Log("🌍 \(method) for \(endpoint.url)")
            if let body = body {
                Log("🌍 POST Body: \(String(data: body, encoding: .utf8)!)", category: .requests)
            }
            // Log("🌍 Authorization: \(request.headers["Authorization"] ?? "")")
        }
        return dataTaskPublisher(for: request)
            .mapError { $0 as Error }
            .eraseToAnyPublisher()
    }

    /// Publisher that takes an encodable type as body
    func dataTaskPublisher<E: Encodable>(for endpoint: Endpoint,
                                         method: HTTPMethod = .get,
                                         encodable body: E? = nil,
                                         authorizationHeader: String? = nil) -> DataTaskPublisherOutput {
        let encoder = JSONEncoder.snakeCaseConverting
        let data: Data
        do {
            data = try encoder.encode(body)
        } catch {
            return Fail(error: error).eraseToAnyPublisher()
        }
        return dataTaskPublisher(for: endpoint,
                                 method: method,
                                 body: data,
                                 authorizationHeader: authorizationHeader)
    }

    // MARK: Headers

    var defaultHeaders: HTTPHeaders {
        [
            "x-tilig-version": VersionIdentifier.tiligVersion,
            "x-tilig-platform": VersionIdentifier.tiligPlatform,
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
    }

    /// Override the default headers with the given headers
    func headers(withAuthorizationHeader authorizationHeader: String) -> HTTPHeaders {
        var final = defaultHeaders
        final.update(name: "Authorization", value: authorizationHeader)
        return final
    }
}

// Async-await support
extension URLSession {

    /// Do a request to the given endpoint, and return the response as the decoded type.
    /// Throws an error when the request or decoding fails.
    func request<D: Decodable>(for endpoint: Endpoint,
                               method: HTTPMethod = .get,
                               data: Data? = nil,
                               using accessToken: Token) async throws -> D {
        // Execute the request in the background
        let task = Task.detached(priority: .background) {

            assert(!Thread.isMainThread)
            var request = try URLRequest(
                url: endpoint.url,
                method: method,
                headers: self.headers(withAuthorizationHeader: "Bearer \(accessToken.rawValue)")
            )
            request.httpBody = data

            assert(request.headers["Content-Type"] != nil)
            assert(request.headers["x-tilig-version"] != nil)
            assert(request.headers["x-tilig-platform"] != nil)

            let (data, response) = try await URLSession.current.data(for: request)
            self.log(request: request, httpBody: request.httpBody, response: data)
            let decoder = JSONDecoder.snakeCaseConverting

            let httpResponse = (response as? HTTPURLResponse)
            switch httpResponse?.statusCode {
            case 200, 201, 204:
                return try decoder.decode(D.self, from: data)
            case 400:
                throw URLError(.badURL)
            case 404:
                throw URLError(.resourceUnavailable)
            case 401:
                throw AuthError.unauthorized
            case 500:
                throw URLError(.badServerResponse)
            default:
                throw URLError(.badServerResponse)
            }
        }
        // Return the item on the main thread
        let response = try await task.value
        return await MainActor.run { response }
    }

    /// Convience method to pass encodables as a body for requests
    func request<D: Decodable, E: Encodable>(for endpoint: Endpoint,
                                             method: HTTPMethod = .get,
                                             body: E,
                                             using accessToken: Token) async throws -> D {
        let encoder = JSONEncoder.snakeCaseConverting
        let data = try encoder.encode(body)
        return try await request(for: endpoint, method: method, data: data, using: accessToken)
    }
}

extension URLSession {
    func log(request: URLRequest, httpBody: Data?, response: Data) {
        let responseString = String(describing: String(data: response, encoding: .utf8))
        var bodyString: String = ""
        if let httpBody {
            bodyString = String(describing: String(data: httpBody, encoding: .utf8))
        }
        Log("""
🌎 Request: \(String(describing: request.method))
   Method: \(String(describing: request.url))
   HTTPBody: \(bodyString))
   Response: \(responseString)
""", category: .requests)
    }
}

extension JSONEncoder {
    static let snakeCaseConverting: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.keyEncodingStrategy = .convertToSnakeCase
        return encoder
    }()
}

extension JSONDecoder {
    static let snakeCaseConverting: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
}

extension Publisher where Output == Data {
    func decode<T: Decodable>(
        as type: T.Type = T.self,
        using decoder: JSONDecoder = .snakeCaseConverting
    ) -> Publishers.Decode<Self, T, JSONDecoder> {
        decode(type: type, decoder: decoder)
    }
}

extension Publisher where Output == URLSession.DataTaskPublisher.Output {
    func decodeResponse<T: Decodable>(as type: T.Type = T.self) -> AnyPublisher<T, Error> {
        self.receive(on: DispatchQueue.main)
            .tryMap { data, response in
                let httpResponse = (response as? HTTPURLResponse)
                switch httpResponse?.statusCode {
                case 200:
                    logSuccess(data: data, httpResponse: httpResponse!)
                    return data
                case 201:
                    logSuccess(data: data, httpResponse: httpResponse!)
                    return data
                case 204:
                    logSuccess(data: data, httpResponse: httpResponse!)
                    return data
                case 400:
                    logError(message: "Not Found (400)", httpResponse: httpResponse)
                    throw URLError(.badURL)
                case 401:
                    logError(message: "Unauthorized (401)", httpResponse: httpResponse)
                    throw AuthError.unauthorized
                case 500:
                    logError(message: "Server error (500)", httpResponse: httpResponse)
                    throw URLError(.badServerResponse)
                default:
                    logError(message: "Unknown error (\(httpResponse?.statusCode ?? 0)", httpResponse: httpResponse)
                    throw URLError(.badServerResponse)
                }
            }
            .decode(as: type)
            .eraseToAnyPublisher()
    }

    /// Convenience method for logging network errors
    private func logError(message errorMessage: String, httpResponse: HTTPURLResponse?) {
        guard let httpResponse = httpResponse else {
            Log("🌍⚠️ Could not unwrap to HTTPURLResponse", type: .error)
            return
        }
        let url = httpResponse.url?.absoluteString ?? ""
        #if DEBUG
        // Only log tokens in DEBUG, never in production.
        let token = httpResponse.headers["Authorization"] ?? ""
        Log("🌍⚠️ Request \(url) => \(errorMessage) [token: \(token)]", type: .error)
        #else
        Log("🌍⚠️ Request \(url) => \(errorMessage)", type: .error)
        #endif
    }

    /// Convenience method for logging successful network responses
    private func logSuccess(data: Data, httpResponse: HTTPURLResponse) {
        #if DEBUG
        if UserDefaults.standard.bool(forKey: "logNetworking") == true {
            let url = httpResponse.url?.absoluteString ?? ""
            Log("🌍 Response for \(url) => \(String(data: data, encoding: .utf8)!)", category: .requests)
        }
        #endif
    }
}
