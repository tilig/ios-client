//
//  GetStartedAddLoginView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 15/03/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct GetStartedAddLoginView: View {
    @EnvironmentObject var viewModel: GetStartedViewModel

    var body: some View {
        VStack(alignment: .center, spacing: 16) {
            Text("Start by adding your first login to take advantage of Tilig's features.")
                .defaultButtonFont(color: .blue.opacity(0.4), size: 14)
                .padding([.horizontal, .top], 12)

            Image("add_login")
                .responsiveHeight(compact: 64, regular: 64)

            DefaultButton(title: "Add a login", textSize: 14, action: {
                withAnimation {
                    viewModel.presentingAddLoginView = true
                }
            })
            .padding([.horizontal, .bottom], 12)
        }
        .roundedRectangleBackground(fillColor: .white, shadowColor: .clear, shadowRadius: 0)
        .sheet(isPresented: $viewModel.presentingAddLoginView) {
            AddLoginView()
                .consumingViewModel(.createItem(.login))
        }
        .onAppear {
            TrackingEvent.getStartedAddLoginExplanationAppeared.send()
        }
    }
}

struct GetStartedAddLoginView_Previews: PreviewProvider {
    static var previews: some View {
        GetStartedAddLoginView()
    }
}
