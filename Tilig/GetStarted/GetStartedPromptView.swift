//
//  GetStartedPromptView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/03/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct GetStartedPromptView: View {
    @EnvironmentObject var viewModel: GetStartedViewModel

    var body: some View {
        VStack {
            if viewModel.getStartedCompleted == false && viewModel.isNewUser {
                HStack {
                    Image(systemName: "airplane.departure")
                        .resizable()
                        .renderingMode(.template)
                        .aspectRatio(contentMode: .fit)
                        .foregroundColor(.white)
                        .frame(width: 20, height: 20)

                    Text("Get Started")
                        .defaultButtonFont(color: .white, size: 14)

                    Spacer()

                    CircularProgressBar(
                        value: viewModel.stepsCompleted,
                        total: viewModel.totalSteps,
                        radius: 12,
                        lineWidth: 4,
                        strokeColor: .white
                    )
                }
                .padding()
                .roundedRectangleBackground(
                    fillColor: .darkPurple.opacity(0.7),
                    shadowColor: .clear,
                    shadowRadius: 0
                )
                .padding([.horizontal, .top], 8)
                .onTapGesture {
                    viewModel.presentGetStartedView = true
                    TrackingEvent.getStartedPromptClicked.send()
                }
                .onAppear {
                    TrackingEvent.getStartedPromptAppeared.send()
                }
            }

            NavigationLink(
                destination: GetStartedView().consumingViewModel(.getStarted),
                isActive: $viewModel.presentGetStartedView
            ) {
                EmptyView()
            }
        }
        .onAppear {
            Task { await viewModel.getUpdatedUserSettings() }
        }
    }
}

struct GetStartedPromptView_Previews: PreviewProvider {
    static var previews: some View {
        GetStartedPromptView()
    }
}
