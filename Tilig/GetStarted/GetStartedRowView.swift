//
//  GetStartedItemView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 15/03/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct GetStartedRowView: View {
    @EnvironmentObject var viewModel: GetStartedViewModel
    var step: GetStartedStep
    var number: Int

    var body: some View {
        VStack(spacing: 12) {
            HStack {
                NumberedItemView(
                    number: number,
                    customText: Text(step.type.title).subTextMediumFont(color: .blue, size: 16)
                )

                if step.isChecked {
                    Image("checkmark_green")
                        .resizable()
                        .frame(width: 24, height: 24)
                }
            }
            .contentShape(Rectangle())
            .onTapGesture {
                withAnimation(.linear(duration: 0.5)) {
                    viewModel.toggleExpand(for: step)
                }
            }

            if step.isExpanded {
                switch step.type {
                case .enableBiometric:
                    GetStartedBiometricView()
                case .addLogin:
                    GetStartedAddLoginView()
                case .enableAutoFill:
                    GetStartedAutoFillView()
                case .learnAutoFill:
                    GetStartedAutoFillView(showLearnHowVideo: true)
                case .connectDesktop:
                    GetStartedConnectDesktopView()
                }
            }
        }
        .padding(12)
        .roundedRectangleBackground(
            fillColor: step.isChecked ? .lightGreen : .lightPurple,
            shadowColor: .clear,
            shadowRadius: 0
        )
    }
}

struct GetStartedRowView_Previews: PreviewProvider {
    static var previews: some View {
        GetStartedRowView(step: GetStartedStep(type: .addLogin), number: 1)
    }
}
