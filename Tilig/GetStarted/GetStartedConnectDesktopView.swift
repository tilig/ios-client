//
//  GetStartedAddLoginView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 15/03/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct GetStartedConnectDesktopView: View {
    @EnvironmentObject var viewModel: GetStartedViewModel

    var body: some View {
        VStack {
            VStack(alignment: .center, spacing: 20) {
                Text("Your data is saved across devices. Connect your desktop to see it work!")
                    .defaultButtonFont(color: .blue.opacity(0.4), size: 14)
                    .multilineTextAlignment(.leading)
                    .fixedSize(horizontal: false, vertical: true)

                Image("desktop")
                    .resizable()
                    .renderingMode(.template)
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(.blue)
                    .frame(height: 48)

                Text("app.tilig.com")
                    .defaultBoldFont(color: .white, size: 14)
                    .padding(.vertical, 8)
                    .padding(.horizontal, 24)
                    .roundedRectangleBackground(
                        fillColor: .blue,
                        shadowColor: .clear,
                        shadowRadius: 0,
                        cornerRadius: 32
                    )

                VStack(alignment: .leading, spacing: 20) {
                    NumberedItemView(
                        number: 1,
                        text: "On your computer, visit the link above",
                        textSize: 12
                    )

                    NumberedItemView(
                        number: 2,
                        customText: {
                            Text("Login with ")
                                .defaultButtonFont(color: .blue, size: 12) +
                            Text(viewModel.userEmail)
                                .defaultBoldFont(color: .blue, size: 13)
                        }()
                    )

                    NumberedItemView(
                        number: 3,
                        text: "Your data is now saved across devices!",
                        textSize: 12
                    )
                }
            }
        }
        .padding(12)
        .roundedRectangleBackground(fillColor: .white, shadowColor: .clear, shadowRadius: 0)
        .onAppear {
            TrackingEvent.getStartedConnectDesktopExplanationAppeared.send()
        }
    }
}

struct GetStartedConnectDesktopView_Previews: PreviewProvider {
    static var previews: some View {
        GetStartedConnectDesktopView()
    }
}
