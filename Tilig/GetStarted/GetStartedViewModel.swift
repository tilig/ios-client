//
//  GetStartedViewModel.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 15/03/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI
import Sentry

enum GetStartedStepType: String {
    case enableBiometric
    case addLogin
    case enableAutoFill
    case learnAutoFill
    case connectDesktop

    var title: String {
        switch self {
        case .enableBiometric: return "Turn on Biometric Lock"
        case .addLogin: return "Add a Login"
        case .enableAutoFill: return "Enable AutoFill"
        case .learnAutoFill: return "Learn how to AutoFill"
        case .connectDesktop: return "Connect your Desktop"
        }
    }
}

class GetStartedStep: Identifiable, Equatable {
    static func == (lhs: GetStartedStep, rhs: GetStartedStep) -> Bool {
        return lhs.type == rhs.type
    }

    var id: UUID { UUID() }
    var type: GetStartedStepType
    var isChecked: Bool = false
    var isExpanded: Bool = false

    init(type: GetStartedStepType, isChecked: Bool = false, isExpanded: Bool = false) {
        self.type = type
        self.isChecked = isChecked
        self.isExpanded = isExpanded
    }
}

@MainActor class GetStartedViewModel: ObservableObject {
    @Published private(set) var steps = [GetStartedStep]()
    @Published var stepsCompleted = 0
    var totalSteps: Int { steps.count }

    var biometricCompleted: Bool { isCompleted(.enableBiometric) }
    var firstLoginAdded: Bool { isCompleted(.addLogin) }
    var learntAutoFill: Bool { isCompleted(.learnAutoFill) }
    var enableAutoFillCompleted: Bool { isCompleted(.enableAutoFill) }
    var getStartedCompleted: Bool {
        iOSOnboardingSettings.completed || iOSOnboardingSettings.dismissed
    }
    var hasConnectedDesktop: Bool {
        userProfile.applicationSettings?.clientsSignedIn
            .map({ $0.lowercased() }).contains("web") ?? false
    }

    @Published var presentingAddLoginView = false
    @Published var presentGetStartedView = false
    var isNewUser: Bool { userProfile.userSettings?.onboarding == nil }
    var userEmail: String { userProfile.email }

    private var client: Client
    private var userProfile: UserProfile
    private var iOSOnboardingSettings: OnboardingSetup
    private var timer: Timer?

    init(
        userProfile: UserProfile,
        autoFillEnabled: Bool,
        biometricViewModel: BiometricViewModel,
        tokenProvider: TokenProvider
    ) {
        self.userProfile = userProfile
        self.client = Client(tokenProvider: tokenProvider)
        self.iOSOnboardingSettings = userProfile.userSettings?.ios ??
            OnboardingSetup(completed: false, dismissed: false, stepsCompleted: [])

        if biometricViewModel.canEvaluate && !biometricViewModel.biometryPermissionDenied {
            let step = GetStartedStep(
                type: .enableBiometric,
                isChecked: biometricViewModel.isEnabled || biometricCompleted
            )
            steps.append(step)
        }

        steps.append(GetStartedStep(type: .addLogin, isChecked: firstLoginAdded))

        steps.append(
            GetStartedStep(
                type: autoFillEnabled ? .learnAutoFill : .enableAutoFill,
                isChecked: autoFillEnabled ? learntAutoFill : enableAutoFillCompleted
            )
        )

        let connectDesktop = GetStartedStep(type: .connectDesktop, isChecked: hasConnectedDesktop)
        steps.insert(connectDesktop, at: hasConnectedDesktop ? 0 : steps.count)
        stepsCompleted = steps.filter({ $0.isChecked }).count
    }

    func activate() {
        // expand first unchecked item
        guard let currentIndex = steps.firstIndex(where: { !$0.isChecked }) else { return }
        toggleExpand(for: steps[currentIndex])
        steps[currentIndex].isExpanded = true

        if !hasConnectedDesktop {
            // poll every 5s to check if desktop has been connected
            timer =  Timer.scheduledTimer(withTimeInterval: 5.0, repeats: true) { (_) in
                Task { await self.getUpdatedUserSettings() }
            }
        }
    }

    func deActivateTimer() {
        timer?.invalidate()
        timer = nil
    }

    func toggleExpand(for step: GetStartedStep) {
        steps = steps.map { existingStep in
            let newStep = existingStep
            newStep.isExpanded = existingStep == step ? !step.isExpanded : false
            if step.isChecked { newStep.isExpanded = false }
            return newStep
        }
    }

    func updateAutoFillSteps(autoFillEnabled: Bool) async {
        guard let currentIndex = steps.firstIndex(where: { $0.type == .enableAutoFill })
            else { return }

        steps = steps.map { existingStep in
            if steps[currentIndex] == existingStep {
                let newStep = GetStartedStep(type: .learnAutoFill)
                if learntAutoFill { newStep.isChecked = true }
                return newStep
            }
            return existingStep
        }
    }

    func markStepAsChecked(type: GetStartedStepType) {
        steps.forEach { step in
            step.isChecked = (step.type == type) ? true : step.isChecked
        }
        stepsCompleted = steps.filter({ $0.isChecked }).count
    }

    func completeStep(type: GetStartedStepType) async {
        try? await Task.sleep(nanoseconds: UInt64(1.0 * .nanosecondsPerSecond))
        addToCompletedSteps(type: type)
        markStepAsChecked(type: type)
        moveToNextStep()
    }

    func addToCompletedSteps(type: GetStartedStepType) {
        guard !iOSOnboardingSettings.stepsCompleted.contains(type.rawValue) else { return }
        iOSOnboardingSettings.stepsCompleted.append(type.rawValue)

        updateUserSettings()
    }

    func isCompleted(_ type: GetStartedStepType) -> Bool {
        return iOSOnboardingSettings.stepsCompleted.contains(type.rawValue)
    }

    func moveToNextStep() {
        guard let currentIndex = steps.firstIndex(where: { $0.isExpanded }) else { return }
        toggleExpand(for: steps[currentIndex])
        let nextIndex = currentIndex + 1
        if nextIndex < steps.count && !steps[nextIndex].isExpanded {
            toggleExpand(for: steps[currentIndex + 1])
        }
    }

    func dismissForever() {
        iOSOnboardingSettings.dismissed = true
        presentGetStartedView = false
        TrackingEvent.getStartedDismissed.send()
        updateUserSettings()
    }

    func completeOnboarding() {
        iOSOnboardingSettings.completed = true
        presentGetStartedView = false
        updateUserSettings()
    }

    func resetSteps() {
        iOSOnboardingSettings.dismissed = false
        iOSOnboardingSettings.completed = false
        iOSOnboardingSettings.stepsCompleted = []
        updateUserSettings()
        steps.forEach { step in
            step.isChecked = false
        }
        stepsCompleted = 0
    }

    func getUpdatedUserSettings() async {
        do {
            self.userProfile = try await client.getUserProfile()

            // update local profile
            guard var authState = AuthManager().authState else { return }
            authState.userProfile = userProfile
            try AuthState.cache(authState: authState)

            if hasConnectedDesktop {
                self.addToCompletedSteps(type: .connectDesktop)
                self.markStepAsChecked(type: .connectDesktop)
                self.moveToNextStep()
                self.deActivateTimer()
            }
        } catch {
            // Don't raise an error here, fail silently
            SentrySDK.capture(error: error)
        }
    }

    func updateUserSettings() {
        Task {
            do {
                let userSettings = UserSettings(
                    onboarding: userProfile.userSettings?.onboarding,
                    ios: iOSOnboardingSettings
                )
                let payload = UserSettingsPayload(userSettings: userSettings)
                _ = try await client.updateUserSettings(payload: payload)

                // update local settings
                guard var authState = AuthManager().authState else { return }
                authState.userProfile.userSettings = userSettings
                try AuthState.cache(authState: authState)
            } catch {
                // Don't raise an error here, fail silently
                SentrySDK.capture(error: error)
            }
        }
    }
}
