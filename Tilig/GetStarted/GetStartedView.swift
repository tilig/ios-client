//
//  GetStartedView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 01/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import SwiftUI
import Firebase
import Sentry

struct GetStartedView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var biometricViewModel: BiometricViewModel
    @EnvironmentObject var enableAutoFillViewModel: EnableAutoFillViewModel
    @EnvironmentObject var viewModel: GetStartedViewModel

    func backAction() {
        presentationMode.wrappedValue.dismiss()
    }

    func dismissForever() {
        let alertItem = AlertItem(
            id: UUID(),
            title: Text("You're doing great!"),
            message: Text("Completing these steps will help get the most out of Tilig!"),
            primaryButton: .destructive(Text("Dismiss forever"), action: { viewModel.dismissForever() }),
            secondaryButton: .cancel(Text("Take me back"))
        )
        alertItem.post()
    }

    var body: some View {
        VStack(spacing: 0) {
            PopupHeaderView(
                title: "",
                closeButtonAction: backAction,
                hasBackground: false,
                isBackEnabled: true
            )

            GeometryReader { geometry in
                ScrollView(showsIndicators: false) {
                    VStack(alignment: .leading, spacing: 12) {
                        Text("Get Started")
                            .defaultBoldFont(color: .blue, size: 24)

                        Text("Take the most out of Tilig by completing the following steps.")
                            .defaultButtonFont(color: .blue.opacity(0.4), size: 16)
                            .multilineTextAlignment(.leading)

                        VStack {
                            ForEach(Array(viewModel.steps.enumerated()), id: \.element.id) { (index, step) in
                                GetStartedRowView(step: step, number: (index+1))
                            }
                        }

                        Spacer()

                        if viewModel.stepsCompleted < viewModel.totalSteps {
                            VStack(alignment: .center) {
                                Button(action: { dismissForever() }) {
                                    Text("Dismiss forever")
                                        .defaultButtonFont(color: .mediumPurple, size: 16)
                                }

                                #if DEBUG
                                Button(action: { viewModel.resetSteps() }) {
                                    Text("Reset steps")
                                        .defaultButtonFont(color: .mediumPurple, size: 16)
                                }
                                #endif
                            }
                            .frame(maxWidth: .infinity)

                        } else {
                            DefaultButton(title: "Explore Tilig", action: { viewModel.completeOnboarding() })
                                .onAppear {
                                    TrackingEvent.getStartedCompletedAppeared.send()
                                }

                        }
                    }
                    .responsivePadding(compact: 16, regular: 48)
                    .frame(width: geometry.size.width, height: geometry.size.height)
                }
            }
        }
        .background(Color.backgroundGray.edgesIgnoringSafeArea(.vertical))
        .swipeRightToDismiss()
        .navigationBarHidden(true)
        .onChange(of: enableAutoFillViewModel.autoFillEnabled, perform: { isEnabled in
            if isEnabled {
                Task { await viewModel.completeStep(type: .enableAutoFill) }
            }
        })
        .onChange(of: biometricViewModel.isEnabled, perform: { isEnabled in
            if isEnabled {
                _ = withAnimation {
                    Task { await viewModel.completeStep(type: .enableBiometric) }
                }
            }
        })
        .onAppear {
            withAnimation {
                if enableAutoFillViewModel.autoFillEnabled {
                    Task {
                        await viewModel.updateAutoFillSteps(
                            autoFillEnabled: enableAutoFillViewModel.autoFillEnabled
                        )
                    }
                }
                viewModel.activate()
            }
            TrackingEvent.getStartedAppeared.send()
        }
        .onDisappear {
            viewModel.deActivateTimer()
        }
    }
}
