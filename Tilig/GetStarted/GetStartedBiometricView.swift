//
//  GetStartedBiometricView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 15/03/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct GetStartedBiometricView: View {
    @EnvironmentObject var biometricViewModel: BiometricViewModel
    @EnvironmentObject var viewModel: GetStartedViewModel

    var hasFaceID: Bool { biometricViewModel.supportedBiometricType == .faceID }
    var body: some View {
        VStack(alignment: .center, spacing: 24) {
            Text("Unlock your app using biometrics")
                .defaultButtonFont(color: .blue.opacity(0.4), size: 14)

            Image(hasFaceID ? "face_id" : "touch_id")
                .responsiveHeight(compact: 64, regular: 64)

            DefaultButton(
                title: hasFaceID ? "Use Face ID" : "Use Touch ID",
                textSize: 14,
                action: { biometricViewModel.toggleEnabled() }
            )
            .frame(height: 40)

            Button(action: { viewModel.moveToNextStep() }) {
                Text("Do it later")
                    .defaultButtonFont(color: .mediumPurple, size: 14)
                    .padding(12)
            }
        }
        .padding(12)
        .roundedRectangleBackground(fillColor: .white, shadowColor: .clear, shadowRadius: 0)
        .onAppear {
            TrackingEvent.getStartedBiometricExplanationAppeared.send()
        }
    }
}

struct GetStartedBiometricView_Previews: PreviewProvider {
    static var previews: some View {
        GetStartedBiometricView()
    }
}
