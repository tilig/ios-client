//
//  GetStartedBiometricView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 15/03/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct GetStartedAutoFillView: View {
    @EnvironmentObject var enableAutoFillViewModel: EnableAutoFillViewModel
    @EnvironmentObject var viewModel: GetStartedViewModel
    @State var showLearnHowVideo = false

    var videoPath: String? {
        return Bundle.main.path(
            forResource: showLearnHowVideo ? "howToAutoFill" : "tutorial",
            ofType: "mov"
        )
    }

    var body: some View {
        VStack(alignment: .center, spacing: 16) {
            Text("Have your forms filled in with Tilig AutoFill and browse seamlessly")
                .defaultButtonFont(color: .blue.opacity(0.4), size: 14)
                .multilineTextAlignment(.leading)
                .fixedSize(horizontal: false, vertical: true)

            if let path = videoPath,
               let url = URL(fileURLWithPath: path) {
                VideoPlayerView(url: url)
                    .padding(.horizontal)
                    .roundedRectangleBackground(
                        fillColor: .mediumPurple.opacity(0.1),
                        shadowColor: .clear,
                        shadowRadius: 0
                    )
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color.brightGray, lineWidth: 1)
                    )
                    .frame(minHeight: 220)
            }

            if showLearnHowVideo {
                DefaultButton(
                    title: "Got it!",
                    textSize: 14,
                    action: {
                        _ = withAnimation {
                            Task { await viewModel.completeStep(type: .learnAutoFill) }
                        }
                    }
                )
                .frame(height: 40)

            } else {

                AutoFillSetupTutorialList()

                DefaultButton(
                    title: "Open settings",
                    textSize: 14,
                    action: {
                        Task { @MainActor in
                            UIApplication.shared.open(URLOpenTracker.shared.getAndTrackUrl())
                        }
                    }
                )
                .frame(height: 40)

                Button(action: { viewModel.moveToNextStep() }) {
                    Text("Do it later")
                        .defaultButtonFont(color: .mediumPurple, size: 14)
                        .padding(12)
                }
            }
        }
        .padding(12)
        .roundedRectangleBackground(fillColor: .white, shadowColor: .clear, shadowRadius: 0)
        .onAppear {
            TrackingEvent.getStartedAutoFillExplanationAppeared.send()
        }
    }
}

struct GetStartedAutoFillView_Previews: PreviewProvider {
    static var previews: some View {
        GetStartedAutoFillView()
    }
}
