//
//  FirstAutoFillState.swift
//  Tilig
//
//  Created by Gertjan Jansen on 11/01/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import Foundation

/// State for tracking if user has completed their first autoFill and whether it has been tracked with Firebase.
/// This is necessary because Firebase Analytics doesn't work in extension contexts.
struct AutoFillTrackingState {

    enum FirstAutoFill: Int {
        /// The user hasn't done an autofill yet
        case notCompleted = 0
        /// The user has done an autofill, but it hasn't been tracked/logged yet
        case completedWithoutTracking = 1
        /// The user has done their first autofill, and it has been tracked/logged.
        case completedAndTracked = 2
    }

    enum ThirdAutoFill: Int {
        /// The user hasn't done their third autofill yet
        case notCompleted = 0
        /// The user has done their third autofill, but it hasn't been tracked/logged yet
        case completedWithoutTracking = 1
        /// The user has done their third autofill, and it has been tracked/logged.
        case completedAndTracked = 2
    }

    /// Get/set a global FirstAutoFillState state, synced with UserDefaults.
    static var firstAutoFillState: FirstAutoFill {
        get {
            FirstAutoFill(
                rawValue: UserDefaults.standard.integer(forKey: UserDefaults.FirstAutoFill)
            ) ?? .notCompleted
        }
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: UserDefaults.FirstAutoFill)
        }
    }

    static var autoFillCount: Int {
        get {
            UserDefaults.standard.integer(forKey: UserDefaults.AutoFillCount)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: UserDefaults.AutoFillCount)
        }
    }

    /// Get/set a global ThirdAutoFillState state, synced with UserDefaults.
    static var thirdAutoFillState: ThirdAutoFill {
        get {
            ThirdAutoFill(
                rawValue: UserDefaults.standard.integer(forKey: UserDefaults.ThirdAutoFill)
            ) ?? .notCompleted
        }
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: UserDefaults.ThirdAutoFill)
        }
    }

    /// Check if there's an autofill completed, that's not yet tracked with Firebase.
    static func handleUntrackedEvents() {
        if Self.firstAutoFillState == FirstAutoFill.completedWithoutTracking {
            FirebaseTrackingEvent.firstAutofill.send()
            Self.firstAutoFillState = .completedAndTracked
        }
        handleUntrackedThirdAutofillEvent()
    }

    /// Check if third autofill is completed and track it on Firebase.
    private static func handleUntrackedThirdAutofillEvent() {
        if Self.thirdAutoFillState == .completedWithoutTracking {
            FirebaseTrackingEvent.thirdAutofill.send()
            Self.thirdAutoFillState = .completedAndTracked
        }
    }

    static func incrementAutoFillCount() {
        Self.autoFillCount += 1
        if Self.autoFillCount == 3 {
            Self.thirdAutoFillState = .completedWithoutTracking
        }
    }
}
