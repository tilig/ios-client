//
//  TrackingEvent.swift
//  Tilig
//
//  Created by Gertjan Jansen on 25/08/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Foundation
import Mixpanel
import Sentry

enum TrackingEvent: String {

    // MARK: All tracking events
    case showWelcomeInstructions = "Welcome Instructions Appeared"
    case showWelcomeInstructionsCompleted = "Welcome Instructions Completed"
    case showAutofillExplanationStep1 = "AutoFill Explanation (step 1) Appeared"
    case showAutofillExplanationStep2 = "AutoFill Explanation (step 2) Appeared"
    case systemSettingsOpened = "System Settings Opened"
    case autoFillExplanationSkipped = "AutoFill Explanation Skipped"
    case autofillEnabled = "AutoFill Enabled"
    case autofillDisabled = "AutoFill Disabled"

    case firstPasswordMessageAppeared = "First Password Message Appeared"
    // case firstPasswordMessageSkipped = "First Password Message Skipped"
    case firstPasswordWebsiteSelected = "First Password Website Selected"

    case itemListViewed = "Account List Viewed"
    case loginViewed = "Login Viewed"
    case itemSearched = "Item Searched"
    case usernameCopied = "Username Copied"
    case passwordCopied = "Password Copied"
    case passwordRevealed = "Password Revealed"
    case fieldCopied = "Field Copied"
    case notesCopied = "Notes Copied"
    case websiteCopied = "Website Copied"
    case websiteOpened = "Website Opened"
    case passwordGenerated = "Password Generated"
    case passwordHistoryViewed = "Password history viewed"
    case passwordRestored = "Password restored"

    case creditCardViewed = "Credit Card Viewed"
    case creditCardNumberCopied = "Credit Card Number Copied"
    case creditCardHolderCopied = "Credit Cardholder Copied"
    case creditCardExpirationDateCopied = "Credit Card Expiration Date Copied"
    case creditCardSecurityCodeCopied = "Credit Card Security Code Copied"
    case creditCardPinCodeCopied = "Credit Card Pin Code Copied"
    case creditCardZipCodeCopied = "Credit Card Zip Code Copied"

    case twoFactorReadMoreTapped = "2FA Read More Tapped"
    case twoFactorScanQRTapped = "2FA Scan QR Code Tapped"
    case twoFactorCameraEnabled = "2FA Camera Enabled"
    case twoFactorManualButtonTapped = "2FA Add QR Manually Tapped"
    case twoFactorManualCodeEntered = "2FA Secret Manually Entered"
    case twoFactorQRCodeScanned = "2FA QR Code Scanned"
    case twoFactorSetupCancelled = "2FA Setup Cancelled"
    case twoFactorIncorrectCodeEntered = "2FA Incorrect Code Entered"
    case otpCopied = "OTP Copied"
    case signout = "Sign Out"

    case wifiViewed = "Wifi Viewed"
    case wifiNetworkNameCopied = "Wifi Network Name Copied"

    // MARK: AutoFill extension
    case autoFillExtensionOpened = "AutoFill Extension Opened"
    case autoFillSuggested = "AutoFill Suggested"
    case autoFillNoSuggestions = "AutoFill No Suggestions"
    case autoFillSelected = "AutoFill Selected"
    /// Fires when an autofill is cancelled, either through user interaction or because of an error
    case autoFillExtensionClosed = "AutoFill Extension Closed"
    /// Fires when the user taps the + icon in the autofill extension.
    case autoFillAddItemTapped = "AutoFill Add Account Tapped"
    /// Fires when the user selects the "login" option in the "Login or signup"
    case autoFillAddExistingSelected = "AutoFill Add Existing Selected"
    /// Fires when the user selects the "signup" option in the "Login or signup"
    case autoFillAddSuggestedSelected = "AutoFill Add Suggested Selected"
    /// Fires when the user opens the search view in the autofill extension
    case autoFillSearchAppeared = "AutoFill Search Appeared"
    /// Fires when user opens the confirmation view in the autofill extension
    case phishingWarningAppeared = "Phishing Warning Appeared"
    /// Fires when user selects the 'Use password from ' button on the confirmation sceen in the autofill extension
    case phishingWarningAccepted = "Phishing Warning Accepted"
    /// Fires when user cancels the confirmation view in the autofill extension
    case phishingWarningCanceled = "Phishing Warning Canceled"

    // MARK: Settings Events
    case settingsViewed = "App Settings Viewed"
    case faqsViewed = "FAQ viewed"
    case getInTouchViewed = "Get in touch viewed"
    case privacyPolicyViewed = "PrivacyPolicy viewed"
    case termsViewed = "Terms viewed"

    case biometricEnabled = "Preference Biometrics Enabled"
    case biometricDisabled = "Preference Biometrics Disabled"
    case biometricInfoTapped = "Preference Biometrics Information Tapped"
    case biometricNotSetUp = "Biometrics Not Set Up"
    case biometricUnlockCanceled = "Biometrics Unlock Canceled"

    case deleteTiligAccountWarningViewed = "Delete Tilig Account Warning Viewed"
    case deleteTiligAccountWarningConfirmed = "Delete Tilig Account Warning Confirmed"
    case deleteTiligAccountTextInputConfirmed = "Delete Tilig Account Text Input Confirmed"

    // MARK: Getting Started Events
    case getStartedPromptAppeared = "Getting Started Prompt Appeared"
    case getStartedPromptClicked = "Getting Started Prompt Clicked"
    case getStartedAppeared = "Getting Started Appeared"
    case getStartedDismissed = "Getting Started Dismissed"
    case getStartedCompletedAppeared = "Getting Started Completed Appeared"
    case getStartedBiometricExplanationAppeared = "Biometric Explanation Appeared"
    case getStartedAddLoginExplanationAppeared = "Add Login Explanation Appeared"
    case getStartedConnectDesktopExplanationAppeared = "Connect Desktop Explanation Appeared"
    case getStartedAutoFillExplanationAppeared = "Autofill Explanation Appeared"

    #if DEBUG
    // MARK: Development and testing events
    case test = "Test Event"
    #endif

    // MARK: Send event method

    func send(withProperties properties: Properties = [:]) {
        Log("[📈 Tracking Event] [\(Mixpanel.mainInstance().distinctId)] \(rawValue)", category: .mixpanel)

        // Make sure we always catch missing x-tilig-platform headers
        let platformTag = Mixpanel.mainInstance().currentSuperProperties()["x-tilig-platform"]
        if platformTag == nil {
            assertionFailure("Platform tag not set")
            SentrySDK.capture(message: "No x-tilig-platform tag set")
        }

        Mixpanel.mainInstance().track(
            event: self.rawValue,
            properties: properties
        )
    }
}
