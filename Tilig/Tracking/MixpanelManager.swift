//
//  MixpanelManager.swift
//  Tilig
//
//  Created by Gertjan Jansen on 01/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import Mixpanel
import Combine
import UIKit

class MixpanelManager {

    static let properties: Properties = [
        "x-tilig-version": VersionIdentifier.tiligVersion,
        "x-tilig-platform": VersionIdentifier.tiligPlatform
    ]

    static func setup() {
        Mixpanel.initialize(token: mixpanelToken, superProperties: properties)
    }

    /// Set super properties that are tagged on each event
    static func setSuperProperties() {
        // Note: We suspect this is being done async in the background, so might
        // take a short while before it is actually applied.
        Mixpanel.mainInstance().registerSuperProperties(properties)
    }

    static func getSuperProperties() -> [String: Any] {
        return Mixpanel.mainInstance().currentSuperProperties()
    }

    static var mixpanelToken: String {
        #if DEBUG
        if UserDefaults.standard.bool(forKey: "useMixpanelStagingEnv") {
            // Staging
            Log("📈 Mixpanel environment: staging", category: .mixpanel)
            return "78569554a084e6994a22ec9c14729ee2"
        }
        // Development
        Log("📈 Mixpanel environment: development", category: .mixpanel)
        return "f701c3675c6a4512df1f4231b886788f"
        #else
        // Production
        Log("📈 Mixpanel environment: production", category: .mixpanel)
        return "2889b8a9e6d50b7c5997d8f8a1a62929"
        #endif
    }

    /// Initialize with the initial authState and (optionally) a publisher that publishes authState changes
    /// Note that `MixpanelManager.setup()` should have been called before this.
    init(authState: AuthState?, authStatePublisher: AnyPublisher<AuthState?, Never>? = nil) {
        self.authState = authState
        cancellable = authStatePublisher?
            .dropFirst()
            .sink { [weak self] authState in
                self?.authState = authState
            }
    }

    private var cancellable: AnyCancellable?

    private var authState: AuthState? {
        didSet {
            if let authState = authState {
                // Signed in
                Self.setup()
                Self.setMixpanelIdentity(authState: authState)
            } else {
                // Signed out
                Self.endMixpanelSession()
            }
        }
    }

    static func setMixpanelIdentity(authState: AuthState) {
        let prevId = Mixpanel.mainInstance().distinctId
        Log("📈 Set Mixpanel identity: \(authState.mixpanelDistinctId) (previous: \(prevId))", category: .mixpanel)
        // Start new Mixpanel session identified by the user id
        Mixpanel.mainInstance().identify(distinctId: authState.mixpanelDistinctId)
    }

    static func endMixpanelSession() {
        Log("📈 Reset mixpanel", category: .mixpanel)
        Mixpanel.mainInstance().reset()
        Self.setSuperProperties()
    }
}

extension AuthState {
    var mixpanelDistinctId: String {
        userProfile.id.uuidString.lowercased()
    }
}
