//
//  FirebaseTrackingEvent.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 10/10/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import FirebaseCore
import FirebaseAnalytics

/// Tracking events for Firebase Analytics
enum FirebaseTrackingEvent: String {
    case autofillEnabled = "autofill_enabled"
    case firstAutofill = "first_autofill"
    case thirdAutofill = "third_autofill"

    func send() {
        Log("[📈 Firebase Analytics Event]: \(rawValue)", category: .general)

        Analytics.logEvent(self.rawValue, parameters: nil)
    }
}
