//
//  AsyncPublisher.swift
//  Tilig
//
//  Created by Gertjan Jansen on 29/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//
import Combine

/// An async-await compatible-version of a Combine publisher
/// Source: https://www.swiftbysundell.com/articles/connecting-async-await-with-other-swift-code/
extension Publisher {
    func singleOutput() async throws -> Output {
        for try await output in values {
            // Since we're immediately returning upon receiving
            // the first output value, that'll cancel our
            // subscription to the current publisher:
            return output
        }

        throw MissingOutputError()
    }
}

struct MissingOutputError: Error {}

/// A Combine-compatible version of an async API
/// Source: https://www.swiftbysundell.com/articles/creating-combine-compatible-versions-of-async-await-apis/
extension Future where Failure == Error {
    convenience init(operation: @escaping () async throws -> Output) {
        self.init { promise in
            Task {
                do {
                    let output = try await operation()
                    promise(.success(output))
                } catch {
                    promise(.failure(error))
                }
            }
        }
    }
}
