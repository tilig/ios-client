//
//  Activatable.swift
//  Tilig
//
//  Created by Gertjan Jansen on 29/09/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import SwiftUI

protocol Activatable: AnyObject {
     func activate()
}

extension View {
    func activating(_ object: Activatable) -> some View {
        onAppear(perform: object.activate)
    }
}

protocol MainActorActivatable: AnyObject {
    @MainActor
    func activate() async
}

extension View {
    func activating(_ object: MainActorActivatable) -> some View {
        onAppear {
            Task {
                await object.activate()
            }
        }
        // for iOS 15:
//        task {
//            await object.activate()
//        }
    }
}
