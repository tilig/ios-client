//
//  PushToTrailingLeft.swift
//  Tilig
//
//  Created by Gertjan Jansen on 08/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

/// Push a view to the bottom right corner.
/// This can be modified to make it push to other corners as well, of course.
struct PushViewToBottomRight: ViewModifier {
    func body(content: Content) -> some View {
        VStack {
            Spacer()
            HStack {
                Spacer()
                content
            }
        }
    }
}

extension View {
    func pushToBottomRight() -> some View {
        self.modifier(PushViewToBottomRight())
    }
}
