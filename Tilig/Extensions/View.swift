//
//  View.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 10/10/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

extension View {
    /// Applies the given transform (modifier) if the given condition evaluates to `true`.
    /// https://www.avanderlee.com/swiftui/conditional-view-modifier/
    /// - Parameters:
    ///   - condition: The condition to evaluate.
    ///   - transform: The transform to apply to the source `View`.
    /// - Returns: Either the original `View` or the modified `View` if the condition is `true`.
    @ViewBuilder func `if`<Content: View>(
        _ condition: @autoclosure () -> Bool,
        transform: (Self) -> Content
    ) -> some View {
        if condition() {
            transform(self)
        } else {
            self
        }
    }
}
