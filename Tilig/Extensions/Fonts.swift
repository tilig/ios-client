//
//  Fonts.swift
//  Tilig
//
//  Created by Gertjan Jansen on 30/11/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import SwiftUI

// Support for scaled fonts on iOS 13
// https://www.hackingwithswift.com/quick-start/swiftui/how-to-use-dynamic-type-with-a-custom-font

struct ScaledFont: ViewModifier {
    @Environment(\.sizeCategory) var sizeCategory
    var name: String
    var size: CGFloat

    func body(content: Content) -> some View {
        let scaledSize = UIFontMetrics.default.scaledValue(for: size)
        return content.font(.custom(name, size: scaledSize))
    }
}

extension View {
    func scaledFont(name: String, size: CGFloat) -> some View {
        return self.modifier(ScaledFont(name: name, size: size))
    }
}

extension View {
    func titleFont() -> some View {
        // 26 is comparable to Apple's Helvetica .title(), but Goldplay looks a bit smaller.
        return self.modifier(ScaledFont(name: "Goldplay-Semibold", size: 28))
    }

    func buttonFont() -> some View {
        // 26 is comparable to Apple's Helvetica .title(), but Goldplay looks a bit smaller.
        return self.modifier(ScaledFont(name: "Goldplay-SemiBold", size: 20))
    }

    func legacyLargeTextFont() -> some View {
        return self
            .modifier(ScaledFont(name: "Now-Regular", size: 20))
            .lineSpacing(8.0)
    }

    func defaultFont(size: Double = 20) -> some View {
        return self.modifier(ScaledFont(name: "Spoof-Regular", size: CGFloat(size)))
    }

    func defaultMediumFont(size: Double = 20) -> some View {
        return self.modifier(ScaledFont(name: "Spoof-Medium", size: CGFloat(size)))
    }

    func defaultButtonFont(color: Color = .blue, size: Double = 18) -> some View {
        return self.modifier(ScaledFont(name: "Stolzl-Regular", size: CGFloat(size)))
    }
}

// New fonts

extension Font {
    static func spoofRegular(size: Double = 18) -> Font {
        return self.baseFontAction(name: "Spoof-Regular", size: size)
    }

    static func spoofBold(size: Double = 18) -> Font {
        return self.baseFontAction(name: "Spoof-Bold", size: size)
    }

    static func spoofMedium(size: Double = 18) -> Font {
        return self.baseFontAction(name: "Spoof-Medium", size: size)
    }

    static func stolzlRegular(size: Double = 18) -> Font {
        return self.baseFontAction(name: "Stolzl-Regular", size: size)
    }

    static func stolzlBook(size: Double = 18) -> Font {
        return self.baseFontAction(name: "Stolzl-Book", size: size)
    }

    static func stolzlMedium(size: Double = 18) -> Font {
        return self.baseFontAction(name: "Stolzl-Medium", size: size)
    }

    static func spaceMono(size: Double = 18) -> Font {
        return self.baseFontAction(name: "SpaceMono-Regular", size: size)
    }

    static func baseFontAction(name: String, size: Double) -> Font {
        return Font.custom(name, size: CGFloat(size))
    }
}

extension Text {
    func largeFont(color: Color = .blue) -> Text {
        self.font(.spoofRegular(size: 23))
            .foregroundColor(color)
    }

    func largeBoldFont(color: Color = .green, size: Double = 23) -> Text {
        self.font(.spoofBold(size: size))
            .foregroundColor(color)
    }

    func defaultFont(color: Color = .blue, size: Double = 20) -> Text {
        self.font(.spoofRegular(size: size))
            .foregroundColor(color)
    }

    func defaultButtonFont(color: Color = .blue, size: Double = 18) -> Text {
        self.font(.stolzlRegular(size: size))
            .foregroundColor(color)
    }

    func subTextFont(color: Color = .blue, size: Double = 18) -> Text {
        self.font(.stolzlBook(size: size))
            .foregroundColor(color)
    }

    func subTextMediumFont(color: Color = .blue, size: Double = 18) -> Text {
        self.font(.stolzlMedium(size: size))
            .foregroundColor(color)
    }

    func defaultBoldFont(color: Color = .blue, size: Double = 20) -> Text {
        self.font(.spoofBold(size: size))
            .foregroundColor(color)
    }

    func defaultMediumFont(color: Color = .blue, size: Double = 18) -> Text {
        self.font(.spoofMedium(size: size))
            .foregroundColor(color)
    }

    func defaultCodeFont(color: Color = .blue, size: Double = 18) -> Text {
        self.font(.spaceMono(size: size))
            .foregroundColor(color)
    }
}

extension UIFont {

    static func defaultButtonFont(size: Double = 18) -> UIFont {
        return self.baseFontAction(name: "Stolzl-Regular", size: size)
    }

    static func defaultMediumFont(size: Double = 18) -> UIFont {
        return self.baseFontAction(name: "Spoof-Medium", size: size)
    }

    static func defaultBoldFont(size: Double = 18) -> UIFont {
        self.baseFontAction(name: "Spoof-Bold", size: size)
    }

    static func subTextFont(size: Double = 18) -> UIFont {
        self.baseFontAction(name: "Stolzl-Book", size: size)
    }

    static func baseFontAction(name: String, size: Double) -> UIFont {
        guard let customFont = UIFont(name: name, size: CGFloat(size)) else {
            return UIFont.systemFont(ofSize: CGFloat(size))
        }
        return UIFontMetrics.default.scaledFont(for: customFont)
    }
}
