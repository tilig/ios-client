//
//  URL+WithProtocol.swift
//  Tilig
//
//  Created by Gertjan Jansen on 05/10/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation

extension URL {
    // TODO: https://gitlab.com/subshq/clients/tilig-ios/-/issues/278
    /// The website URL, prepended with `http://` protocol if it wasn't present
    var withProtocol: URL? {
        var stringURL = absoluteString
        if !stringURL.starts(with: "http://") && !stringURL.starts(with: "https://") {
            stringURL = "https://\(stringURL)"
        }
        return URL(string: stringURL)
    }
}
