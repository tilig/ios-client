//
//  ColorExtension.swift
//  Tilig
//
//  Created by Jacob Schatz on 3/31/20.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

extension Color {
    public static let backgroundPurple = Color(red: 245/255, green: 245/255, blue: 255/255)
    public static let darkPurple = Color(red: 5/255, green: 0/255, blue: 63/255)
    public static let mediumPurple = Color(red: 105/255, green: 91/255, blue: 255/255)
    public static let lightPurple = Color(red: 241/255, green: 238/255, blue: 255/255)
    public static let subTextLightPurple = Color(red: 130/255, green: 127/255, blue: 160/255)
    public static let purple = Color(red: 211/255, green: 28/255, blue: 227/255)

    public static let lightGray = Color(red: 251/255, green: 251/255, blue: 251/255)
    public static let brightGray = Color(red: 232/255, green: 232/255, blue: 237/255)
    public static let borderGray = Color(red: 208/255, green: 208/255, blue: 220/255)
    public static let backgroundGray = Color(red: 247/255, green: 247/255, blue: 249/255)
    public static let lightRed = Color(red: 254/255, green: 239/255, blue: 239/255)
    public static let lightGreen = Color(red: 201/255, green: 228/255, blue: 217/255)

    public static let blue = Color(red: 5/255, green: 0/255, blue: 63/255)
    public static let green = Color(red: 82/255, green: 163/255, blue: 130/255)
    public static let yellow = Color(red: 252/255, green: 194/255, blue: 0/255)
    public static let wifiBlue = Color(red: 18/255, green: 194/255, blue: 228/255)
    public static let creditCardGreen = Color(red: 21/255, green: 117/255, blue: 4/255)

    // Colors used only in the AutoFill extension
    public static let extensionLightPurple = Color(red: 236/255, green: 235/255, blue: 255/255)
    public static let extensionBorderPurple = Color(red: 199/255, green: 194/255, blue: 255/255)
    public static let extensionLightGreen = Color(red: 241/255, green: 248/255, blue: 245/255)
    public static let extensionBorderGreen = Color(red: 145/255, green: 199/255, blue: 177/255)
    public static let extensionLightYellow = Color(red: 255/255, green: 250/255, blue: 235/255)

    init(hex string: String) {
        var string: String = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if string.hasPrefix("#") {
            _ = string.removeFirst()
        }

        // Double the last value if incomplete hex
        if !string.count.isMultiple(of: 2), let last = string.last {
            string.append(last)
        }

        // Fix invalid values
        if string.count > 8 {
            string = String(string.prefix(8))
        }

        // Scanner creation
        let scanner = Scanner(string: string)

        var color: UInt64 = 0
        scanner.scanHexInt64(&color)

        if string.count == 2 {
            let mask = 0xFF

            let g = Int(color) & mask

            let gray = Double(g) / 255.0

            self.init(.sRGB, red: gray, green: gray, blue: gray, opacity: 1)

        } else if string.count == 4 {
            let mask = 0x00FF

            let g = Int(color >> 8) & mask
            let a = Int(color) & mask

            let gray = Double(g) / 255.0
            let alpha = Double(a) / 255.0

            self.init(.sRGB, red: gray, green: gray, blue: gray, opacity: alpha)

        } else if string.count == 6 {
            let mask = 0x0000FF
            let r = Int(color >> 16) & mask
            let g = Int(color >> 8) & mask
            let b = Int(color) & mask

            let red = Double(r) / 255.0
            let green = Double(g) / 255.0
            let blue = Double(b) / 255.0

            self.init(.sRGB, red: red, green: green, blue: blue, opacity: 1)

        } else if string.count == 8 {
            let mask = 0x000000FF
            let r = Int(color >> 24) & mask
            let g = Int(color >> 16) & mask
            let b = Int(color >> 8) & mask
            let a = Int(color) & mask

            let red = Double(r) / 255.0
            let green = Double(g) / 255.0
            let blue = Double(b) / 255.0
            let alpha = Double(a) / 255.0

            self.init(.sRGB, red: red, green: green, blue: blue, opacity: alpha)

        } else {
            self.init(.sRGB, red: 1, green: 1, blue: 1, opacity: 1)
        }
    }
}
