//
//  ResponsivePadding.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 18/11/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

struct ResponsivePaddingModifier: ViewModifier {
    @Environment(\.horizontalSizeClass) var sizeClass
    var edges: Edge.Set
    var compact: Double
    var regular: Double

    func body(content: Content) -> some View {
        content
            .padding(edges, sizeClass == .compact ? compact : regular)
    }
}

extension View {
    /// sets a padding on the content based on it's horizontal size class
    func responsivePadding(_ edges: Edge.Set = .all, compact: Double, regular: Double) -> some View {
        self.modifier(ResponsivePaddingModifier(edges: edges, compact: compact, regular: regular))
    }
}
