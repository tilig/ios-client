//
//  TimeInterval+NanoSeconds.swift
//  Tilig
//
//  Created by Gertjan Jansen on 24/10/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation

extension TimeInterval {
    static let nanosecondsPerSecond = TimeInterval(NSEC_PER_SEC)
}
