//
//  SwipeToNavigateBack.swift
//  Tilig
//
//  Created by Gertjan Jansen on 17/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct SwipeRightToDismiss: ViewModifier {
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>

    func navigateBack() {
        self.mode.wrappedValue.dismiss()
    }

    func body(content: Content) -> some View {
        content.swipeRight(action: navigateBack)
    }
}

extension View {
    func swipeRightToDismiss() -> some View {
        self.modifier(SwipeRightToDismiss())
    }
}
