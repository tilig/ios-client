//
//  AlertItem.swift
//  Tilig
//
//  Created by Gertjan Jansen on 22/10/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import SwiftUI

struct AlertItem: Identifiable {
    var id = UUID()
    var title: Text
    var message: Text?
    var dismissButton: Alert.Button?
    var primaryButton: Alert.Button?
    var secondaryButton: Alert.Button?

    /// Should be by SwiftUI to render the actual Alert
    func toAlert() -> Alert {
        if let dismissButton = dismissButton {
            return Alert(
                title: title,
                message: message,
                dismissButton: dismissButton
            )
        } else if let primaryButton = primaryButton,
                  let secondaryButton = secondaryButton {
            return Alert(
                title: title,
                message: message,
                primaryButton: primaryButton,
                secondaryButton: secondaryButton
            )
        } else {
            return Alert(title: Text("⚠️ An unkown error occured"))
        }
    }

    /// Posts this AlertItem through NotificationCenter, so any subscribed view
    /// can render the alert.
    func post() {
        NotificationCenter.default.post(name: .globalError, object: self)
    }
}
