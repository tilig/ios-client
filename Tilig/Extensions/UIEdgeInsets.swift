//
//  UIEdgeInsets.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 08/02/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

// UIEdgeInsets as an Environment Value,
// Used to add appropriate padding to edgesIgnoringSafeArea views
// Source: https://stackoverflow.com/a/66880368/8694980

private extension UIEdgeInsets {

    var insets: EdgeInsets {
        EdgeInsets(top: top, leading: left, bottom: bottom, trailing: right)
    }
}

private struct SafeAreaInsetsKey: EnvironmentKey {

    static var defaultValue: EdgeInsets {
        (UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.safeAreaInsets ?? .zero).insets
    }
}

extension EnvironmentValues {

    var safeAreaInsets: EdgeInsets {
        self[SafeAreaInsetsKey.self]
    }
}
