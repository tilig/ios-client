//
//  Labels.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 07/02/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct RoundedLabel: View {
    var text: String
    var textColor = Color.mediumPurple
    var backgroundColor = Color.lightPurple

    var body: some View {
        Text(text)
            .defaultButtonFont(color: textColor, size: 10)
            .padding(4)
            .background {
                RoundedRectangle(cornerRadius: 10, style: .continuous)
                    .fill(backgroundColor)
            }
    }
}

struct WarningLabel: View {
    var title = "Error!"
    var text: String
    var backgroundColor = Color.lightPurple
    var displayButton = true
    var buttonAction: () -> Void

    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Image(systemName: "exclamationmark.triangle.fill")
                    .foregroundColor(.red)
                    .frame(width: 14, height: 14)

                Text(title)
                    .defaultButtonFont(color: .red, size: 12)
            }

            HStack {
                Text(text)
                    .defaultButtonFont(size: 12)

                if displayButton {
                    Button(action: buttonAction) {
                        Text("Ok")
                            .defaultButtonFont(color: .mediumPurple, size: 12)
                    }
                }
            }

            // Stretch the VStack fullwidth
            HStack { Spacer() }
        }
        .padding()
        .roundedRectangleBackground(
            fillColor: backgroundColor,
            shadowColor: .clear,
            shadowRadius: 0
        )
    }
}
