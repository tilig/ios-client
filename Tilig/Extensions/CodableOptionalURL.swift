//
//  CodableOptionalURL.swift
//  Tilig
//
//  Created by Gertjan Jansen on 26/10/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import SwiftUI

@propertyWrapper
struct CodableOptionalURL {
    var wrappedValue: URL?
}

extension CodableOptionalURL {
    init(string: String) {
        wrappedValue = URL(string: string)
    }
}

extension CodableOptionalURL: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let stringValue: String? = try? container.decode(String.self)
        wrappedValue = URL(string: stringValue ?? "")
    }
}
extension CodableOptionalURL: Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        // Encode to empty string
        try container.encode(wrappedValue?.absoluteString ?? "")
    }
}

extension CodableOptionalURL: Equatable { }
