//
//  Image.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 09/10/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

extension Image {

    /// Fix height for resizable Image
    func fixedHeight(_ height: Double) -> some View {
        self
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(maxHeight: height)
    }

    /// Set responsive height for Image based on size class
    func responsiveHeight(compact: Double, regular: Double) -> some View {
        self
            .resizable()
            .aspectRatio(contentMode: .fit)
            .responsiveHeight(compact: compact, regular: regular)
    }
}

struct ResponsiveHeightModifier: ViewModifier {
    @Environment(\.horizontalSizeClass) var sizeClass
    var compact: Double
    var regular: Double
    var alignment: Alignment

    func body(content: Content) -> some View {
        content
            .frame(maxHeight: sizeClass == .compact ? compact : regular, alignment: alignment)
    }
}

extension View {
    /// sets height on the content based on its horizontal size class
    func responsiveHeight(compact: Double, regular: Double, alignment: Alignment = .center) -> some View {
        self.modifier(ResponsiveHeightModifier(compact: compact, regular: regular, alignment: alignment))
    }
}
