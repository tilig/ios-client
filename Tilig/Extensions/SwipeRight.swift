//
//  OnSwipeRight.swift
//  Tilig
//
//  Created by Gertjan Jansen on 17/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct SwipeRight: ViewModifier {
    var action: () -> Void

    func body(content: Content) -> some View {
        content.highPriorityGesture(DragGesture(minimumDistance: 25, coordinateSpace: .local).onEnded { value in
            let width = value.translation.width
            let height = value.translation.height
            let swipedHorizontally = abs(height) < abs(width)
            if swipedHorizontally && width > 50.0 {
                action()
            }
        })
    }
}

extension View {
    func swipeRight(action: @escaping () -> Void) -> some View {
        self.modifier(SwipeRight(action: action))
    }
}
