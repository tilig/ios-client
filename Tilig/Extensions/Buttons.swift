//
//  Buttons.swift
//  Tilig
//
//  Created by Gertjan Jansen on 12/01/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import SwiftUI

// Support for scaled fonts on iOS 13
// https://www.hackingwithswift.com/quick-start/swiftui/how-to-use-dynamic-type-with-a-custom-font

struct BackButton: View {
    var foregroundColor: Color = .white
    var usesXMarkIcon: Bool = false
    var hasPadding: Bool = false
    var action: () -> Void

    var body: some View {
        Button(action: action) {
            Image(systemName: usesXMarkIcon ? "xmark" : "chevron.left")
                .resizable()
                .font(.title.weight(.medium))
                .foregroundColor(foregroundColor)
                .frame(width: usesXMarkIcon ? 20 : 12, height: 20)
                .padding(hasPadding ? 16 : 0)
        }
    }
}

struct DefaultButton: View {
    @Environment(\.isEnabled) private var isEnabled: Bool

    let title: String
    var textSize: Double = 16
    var foregroundColor: Color = .white
    var backgroundColor: Color = .mediumPurple
    var image: Image?
    var padding: Double = 16
    var fullWidth: Bool = true
    var action: () -> Void

    var body: some View {
        Button(action: action) {
            HStack {
                if let image {
                    image
                        .resizable()
                        .foregroundColor(.white)
                        .frame(width: 15, height: 15)
                }

                Text(title)
                    .font(.stolzlRegular(size: textSize))
                    .foregroundColor(foregroundColor)
            }
            .padding(padding)
            .frame(alignment: .center)
            .if(fullWidth) { view in
                view.frame(maxWidth: .infinity)
            }
        }
        .roundedRectangleBackground(fillColor: backgroundColor, shadowRadius: 1.0)
        .opacity(isEnabled ? 1.0 : 0.5)
    }
}

struct WhiteButton: View {
    @Environment(\.isEnabled) private var isEnabled: Bool

    let title: String
    var textSize: Double = 14
    var cornerRadius: Double = 10
    var foregroundColor: Color = .mediumPurple
    var fullWidth: Bool = false
    var maxHeight: Double = 50
    var action: () -> Void

    var body: some View {
        Button(action: action) {
            Text(title)
                .font(.stolzlRegular(size: textSize))
                .foregroundColor(foregroundColor)
                .padding()
                .frame(alignment: .center)
                .if(fullWidth) { view in
                    view.frame(maxWidth: .infinity)
                }
                .frame(height: maxHeight)
        }
        .background(
            ZStack {
                RoundedRectangle(cornerRadius: cornerRadius)
                    .fill(Color.white)

                RoundedRectangle(cornerRadius: cornerRadius)
                    .stroke(Color.borderGray, lineWidth: 0.5)
            }
        )
        .shadow(color: .borderGray, radius: 1, x: 0, y: 1)
        .opacity(isEnabled ? 1.0 : 0.5)
    }
}

struct RoundedRectangleModifier: ViewModifier {
    @State var fillColor: Color
    @State var shadowColor: Color
    @State var radius: CGFloat
    @State var cornerRadius: CGFloat

    func body(content: Content) -> some View {
        content
            .background(
                RoundedRectangle(cornerRadius: cornerRadius, style: .continuous)
                    .fill(fillColor)
                    .shadow(
                        color: shadowColor,
                        radius: radius,
                        x: 0,
                        y: 1
                    )
            )
    }
}

extension View {
    func roundedRectangleBackground(
        fillColor: Color,
        shadowColor: Color = .black.opacity(0.4),
        shadowRadius: CGFloat = 2.0,
        cornerRadius: CGFloat = 10
    ) -> some View {
        self.modifier(
            RoundedRectangleModifier(
                fillColor: fillColor,
                shadowColor: shadowColor,
                radius: shadowRadius,
                cornerRadius: cornerRadius
            )
        )
    }
}

struct Buttons_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            Button(action: { }) {
                Text("Fill in on Bla")
                    .defaultButtonFont(color: Color.white, size: 14)
            }

            Button(action: { }) {
                Text("Delete this item")
                    .defaultButtonFont(color: Color.red, size: 14)
            }
        }

    }
}
