//
//  DefaultBackground.swift
//  Tilig
//
//  Created by Gertjan Jansen on 05/10/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import SwiftUI

struct WhiteRoundedRectangleBackground: ViewModifier {
    func body(content: Content) -> some View {
        content
            .padding()
            .frame(maxWidth: .infinity, alignment: .center)
            .background(
                RoundedRectangle(cornerRadius: 10)
                    .fill(.white)
            )
            .padding(.bottom)
    }
}

extension View {
    func whiteRectangleBackground() -> some View {
        modifier(WhiteRoundedRectangleBackground())
    }
}
