//
//  String.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 21/04/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func separate(every stride: Int = 3, with separator: Character = " ") -> String {
        return String(enumerated().map { $0 > 0 && $0 % stride == 0 ? [separator, $1] : [$1]}.joined())
    }

    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }

    var isValidEmail: Bool {
        let emailPred = NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}")
        return emailPred.evaluate(with: self)
    }
}
