//
//  OptionalExtension.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 28/01/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation

extension Optional where Wrapped == URL {
    // extension to check whether a URL is empty or nil
    var isEmptyOrNil: Bool {
        return self?.absoluteString.isEmpty ?? true
    }
}

extension Optional where Wrapped == String {
    // extension to check whether a String is empty or nil
    var isEmptyOrNil: Bool {
        return self?.isEmpty ?? true
    }
}
