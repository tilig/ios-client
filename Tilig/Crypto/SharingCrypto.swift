//
//  SharingService.swift
//  Tilig
//
//  Created by Gertjan Jansen on 06/02/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import Sodium
import Foundation

struct UpgradePayload {
    var itemID: UUID
    var publicKek: Box.KeyPair.PublicKey
    var encryptedFolderDek: String
    var ownEncryptedPrivateKek: String
    var otherEncryptedPrivateKek: String?
}

struct SharingCrypto {
    init(crypto: Crypto) {
        self.crypto = crypto
    }

    private let crypto: Crypto

    func upgradeToFolder(item: Item, shareePublicKey: Box.KeyPair.PublicKey?) throws -> UpgradePayload {
        guard let itemId = item.id else {
            throw SharingError.invalidItem
        }
        // Make sure this item is using the new encryption
        guard let encryptedDek = item.encryptedDek else {
            throw SharingError.encryptedDekMissing
        }
        if item.encryptedFolderDek != nil {
            throw SharingError.alreadyUpgradedToFolder
        }

        // First decrypt the DEK using the user's private key (user Kek)
        let decryptedDek = try crypto.decrypt(encryptedDek: encryptedDek)

        // Generate a new keypair
        let folderKek = try Box.KeyPair.generate()

        // Then re-encrypt using the new folder Kek for the current user
        let encryptedFolderDek = try crypto.encrypt(
            key: decryptedDek,
            withKeyPair: folderKek
        )

        // Encrypt the folderKek's private key for each user
        let privateKek = folderKek.secretKey
        // This uses the user's public key by default
        let ownEncryptedPrivateKek = try crypto.encrypt(
            privateKek: privateKek,
            withPublicKey: crypto.keyPair.publicKey
        )
        // This uses the other user's public key
        if let shareePublicKey {
            let otherEncryptedPrivateKek = try crypto.encrypt(
                privateKek: privateKek,
                withPublicKey: shareePublicKey
            )

            return UpgradePayload(
                itemID: itemId,
                publicKek: folderKek.publicKey,
                encryptedFolderDek: encryptedFolderDek,
                ownEncryptedPrivateKek: ownEncryptedPrivateKek,
                otherEncryptedPrivateKek: otherEncryptedPrivateKek
            )
        }

        return UpgradePayload(
            itemID: itemId,
            publicKek: folderKek.publicKey,
            encryptedFolderDek: encryptedFolderDek,
            ownEncryptedPrivateKek: ownEncryptedPrivateKek
        )
    }

    /// Returns the private KEK, encrypted with sharee's public key
    func encryptPrivateKekForNewUser(inFolder folder: Folder,
                                     shareePublicKey: Box.KeyPair.PublicKey) throws -> String {
        // Get the encrypted private KEK for the current user
        // First decrypt the private Kek, using the user's keypair
        let decryptedPrivateKek = try crypto.decrypt(
            encryptedPrivateKek: folder.myEncryptedPrivateKey,
            withKeyPair: crypto.keyPair
        )

        // Then re-encrypt the private Kek, using the other user's public keypair
        let shareeEncryptedPrivateKek = try crypto.encrypt(
            privateKek: decryptedPrivateKek,
            withPublicKey: shareePublicKey
        )

        return shareeEncryptedPrivateKek
    }
}
