//
//  Crypto.swift
//  Tilig
//
//  Created by Gertjan Jansen on 29/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Sodium
import Foundation

enum CryptoError: Error {
    case keyLength
    case nonceGenerationFailed
    case decryptionFailed
    case encryptionFailed
    case base64ConversionFailed
    case stringConversionFailed

    // DEK encryption
    case dekEncryptionFailed
    case dekDecryptionFailed

    // Item encryption
    case detailsEncryptionFailed
    case overviewEncryptionFailed
    case detailsDecryptionFailed
    case overviewDecryptionFailed

    // KEK encryption
    case keyEncryptionFailed
    case keyDecryptionFailed
}

// extension CryptoError: LocalizedError {
//    public var errorDescription: String? {
//        switch self {
//        case .keyLength:
//            return "Key has wrong length"
//        case .nonceGenerationFailed:
//            return "Failed to generate nonce"
//        case .decryptionFailed:
//            return "Decryption failed"
//        case .encryptionFailed:
//            return "Encryption failed"
//        case .stringConversionFailed:
//            return "String conversion failed"
//        }
//    }
// }

struct Crypto {
    private let sodium = Sodium()

    // TODO: private
    let keyPair: Box.KeyPair
    // This can be private after fully migrating to v2
    // (it's being used in the migration script)
    let legacyKeyPair: LegacyKeyPair

    init(keyPair: Box.KeyPair, legacyKeyPair: LegacyKeyPair) {
        self.keyPair = keyPair
        self.legacyKeyPair = legacyKeyPair
    }

    /// Generates a DEK that can be used to encrypt payloads.
    func generateDek() throws -> Bytes {
        sodium.secretBox.key()
    }

    /// Encrypts the given payload using the given DEK
    func encrypt(payload: String, dek: Bytes) throws -> String {
        if dek.count != sodium.secretBox.KeyBytes {
            throw CryptoError.keyLength
        }

        guard let encrypted: Bytes = sodium.secretBox.seal(
            message: payload.bytes,
            secretKey: dek
        ) else {
            throw CryptoError.encryptionFailed
        }

        return try bin2base64(encrypted)
    }

    /// Decrypt the given payload using the DEK.
    /// Payload should be a base64 encoded string
    func decrypt(payload: String, dek: Bytes) throws -> String {
        if dek.count != sodium.secretBox.KeyBytes {
            throw CryptoError.keyLength
        }

        let bytes = try base642bin(payload)

        guard let decryptedBytes: Bytes = sodium.secretBox.open(
            nonceAndAuthenticatedCipherText: bytes,
            secretKey: dek
        ) else {
            throw CryptoError.decryptionFailed
        }

        guard let decryptedString = String(data: Data(decryptedBytes), encoding: .utf8) else {
            throw CryptoError.stringConversionFailed
        }

        return decryptedString
    }

    /// Encrypt a DEK using asymmetric encryption, with the user's private KEK
    func encrypt(dek: Bytes) throws -> String {
        guard let cipherText: Bytes = sodium.box.seal(
            message: dek,
            recipientPublicKey: keyPair.publicKey,
            senderSecretKey: keyPair.secretKey
        ) else {
            throw CryptoError.dekEncryptionFailed
        }

        return try bin2base64(cipherText)
    }

    /// Encrypt a DEK using asymmetric encryption, with the given public key
    func encrypt(key: Bytes, withKeyPair keyPair: Box.KeyPair) throws -> String {
        guard let cipherText: Bytes = sodium.box.seal(
            message: key,
            recipientPublicKey: keyPair.publicKey,
            senderSecretKey: keyPair.secretKey
        ) else {
            throw CryptoError.dekEncryptionFailed
        }

        return try bin2base64(cipherText)
    }

    /// Decrypt a DEK using asymmetric encryption
    func decrypt(encryptedDek: String) throws -> Bytes {
        try decrypt(encryptedDek: encryptedDek, withKeyPair: keyPair)
    }

    func decrypt(encryptedDek: String, withKeyPair keyPair: Box.KeyPair) throws -> Bytes {
        let dekBytes = try base642bin(encryptedDek)

        guard let dek: Bytes = sodium.box.open(
            nonceAndAuthenticatedCipherText: dekBytes,
            senderPublicKey: keyPair.publicKey,
            recipientSecretKey: keyPair.secretKey
        ) else {
            throw CryptoError.dekDecryptionFailed
        }

        return dek
    }

    /// Decrypt a DEK using asymmetric encryption, with the given RSA key pair's private key
    /// Returns a base64 encoded string.
    func legacyEncrypt(dek: Bytes) throws -> String {
        return try legacyKeyPair.encrypt(buffer: dek)
    }

    /// Decrypt a base64 string using a given RSA keypair
    /// Returns a DEK
    func legacyDecrypt(rsaEncryptedDek: String) throws -> Bytes {
        let result = try legacyKeyPair.decryptBuffer(cipherText: rsaEncryptedDek)
        let data = Data(bytes: result.buffer, count: result.bufferSize)
        return Bytes(data)
    }

    /// Helper function to convert to base64, URL safe without padding
    /// Throws an error on failure.
    private func bin2base64(_ encoded: Bytes) throws -> String {
        let encoded = sodium.utils.bin2base64(encoded, variant: .URLSAFE_NO_PADDING)
        guard let encoded else {
            throw CryptoError.base64ConversionFailed
        }
        return encoded
    }

    /// Helper function to convert base64 to bytes
    /// Throws an error on failure.
    private func base642bin(_ payload: String) throws -> Bytes {
        // Remove any newlines that other clients might have sneaked in
        let payload = payload.replacingOccurrences(of: "\n", with: "")
        guard let bytes = sodium.utils.base642bin(
            payload,
            variant: .URLSAFE_NO_PADDING
        ) else {
            throw CryptoError.base64ConversionFailed
        }
        return bytes
    }

    func encrypt(privateKek: Bytes, withPublicKey publicKey: Box.PublicKey) throws -> String {
        guard let cipherText: Bytes = sodium.box.seal(
            message: privateKek,
            recipientPublicKey: publicKey
        ) else {
            throw CryptoError.keyEncryptionFailed
        }

        return try bin2base64(cipherText)
    }

    func decrypt(encryptedPrivateKek key: String, withKeyPair keyPair: Box.KeyPair) throws -> Bytes {
        let bytes = try base642bin(key)

        guard let privateKey: Bytes = sodium.box.open(
            anonymousCipherText: bytes,
            recipientPublicKey: keyPair.publicKey,
            recipientSecretKey: keyPair.secretKey
        ) else {
            throw CryptoError.keyDecryptionFailed
        }

        return privateKey
    }

    /// Encode a public key with base64 encoding
    func encode(_ publicKey: Box.PublicKey) throws -> String {
        return try bin2base64(publicKey)
    }

    /// Turn a base64 encoded public key into a usable Box.PublicKey.
    /// Note that this does not decrypt anyting, just some base64 decoding.
    func decode(_ publicKey: String) throws -> Box.PublicKey {
        let bytes = try base642bin(publicKey)
        return Box.KeyPair.PublicKey(bytes)
    }
}

/// Item encryption and decryption
extension Crypto {
    func encrypt(item: Item,
                 overview: DecryptedOverview,
                 details: DecryptedDetails) throws -> Item {
        var updatedItem = item

        // Get or create a DEK
        let dek: Bytes

        if let folderDek = try decrypt(folderDekForItem: item) {
            dek = folderDek
        } else if let encryptedDek = updatedItem.encryptedDek {
            dek = try decrypt(encryptedDek: encryptedDek)
        } else if let rsaEncryptedDek = updatedItem.rsaEncryptedDek {
            // In this case, the DEK has not been re-encrypted with the new keypair yet
            dek = try legacyDecrypt(rsaEncryptedDek: rsaEncryptedDek)
            updatedItem.encryptedDek = try encrypt(dek: dek)
        } else {
            dek = try generateDek()
            updatedItem.encryptedDek = try encrypt(dek: dek)
            updatedItem.rsaEncryptedDek = try legacyEncrypt(dek: dek)
        }

        // The values based on the new encryption
        let originalItem = try decrypt(fullItem: item)

        if item.legacyEncryption {
            let legacyFields = try decryptLegacyFields(item: item)

            // Legacy stuff
            // `newLegacyFields` are the decrypted values based on the legacy encryption
            let newLegacyFields = LegacyFields(overview: overview, details: details)

            try updatedItem.encryptLegacyFields(
                newFields: newLegacyFields,
                originalFields: legacyFields,
                legacyKeyPair: legacyKeyPair
            )
        }

        // Update the encrypted overview, if necessary. The `overview` var is the SOT.
        if item.encryptedOverview == nil || originalItem.overview != overview {
            updatedItem.encryptedOverview = try encrypt(overview: overview, dek: dek)
        }

        // Update the encrypted details, if necessary
        if item.encryptedDetails == nil || originalItem.details != details {
            var details = details
            details.trackPasswordChange(previousDetails: originalItem.details)
            updatedItem.encryptedDetails = try encrypt(details: details, dek: dek)
        }

        updatedItem.encryptionVersion = 2

        // The website field on the encrypted instance should stay in sync with the overview's first URL
        updatedItem.website = overview.urls.first?.url

        // More legacy stuff
        if updatedItem.legacyEncryption {
            updatedItem.name = overview.name
            updatedItem.androidAppIds = overview.androidAppIds
        }

        return updatedItem
    }

    func getItemDek(item: Item) throws -> Bytes? {
        if let folderDek = try decrypt(folderDekForItem: item) {
            return folderDek
        } else if let encryptedDek = item.encryptedDek {
            return try decrypt(encryptedDek: encryptedDek)
        } else if let rsaEncryptedDek = item.rsaEncryptedDek {
            return try legacyDecrypt(rsaEncryptedDek: rsaEncryptedDek)
        } else {
            return nil
        }
    }

    /// Returns nil of the item does not have a folder
    func decrypt(folderDekForItem item: Item) throws -> Bytes? {
        if let folderDek = item.encryptedFolderDek,
           let folder = item.folder,
           let encodedPublicKey = folder.publicKey {
            let privateKek = try decrypt(
                encryptedPrivateKek: folder.myEncryptedPrivateKey,
                withKeyPair: keyPair
            )
            let publicKek = try decode(encodedPublicKey)
            let folderKekPair = Box.KeyPair(
                publicKey: publicKek,
                secretKey: privateKek
            )
            return try decrypt(
                encryptedDek: folderDek,
                withKeyPair: folderKekPair
            )
        }
        return nil
    }

    // Decrypt the item with legacy encryption, but encrypt with new encryption on the go
    func decrypt(fullItem item: Item) throws -> ItemWrapper {
        var overview = DecryptedOverview()
        var details = DecryptedDetails()

        if let dek = try getItemDek(item: item) {
            if let encryptedOverview = item.encryptedOverview {
                overview = try decrypt(encryptedOverview: encryptedOverview, dek: dek)
            }

            if let encryptedDetails = item.encryptedDetails {
                details = try decrypt(encryptedDetails: encryptedDetails, dek: dek)
            }
        }

        if item.legacyEncryption {
            let legacyFields = try decryptLegacyFields(item: item)
            overview.update(fromLegacyFields: legacyFields, andItem: item)
            details.update(fromLegacyFields: legacyFields)
        }

        return ItemWrapper(
            item: item,
            overview: overview,
            details: details
        )
    }

    func decryptOverview(encryptedItem: Item) throws -> DecryptedOverview {
        try decrypt(fullItem: encryptedItem).overview
    }

    func decryptOverviewInBackground(encryptedItem: Item) async throws -> DecryptedOverview {
        let task = Task.detached(priority: .background) {
            return try decrypt(fullItem: encryptedItem).overview
        }
        return try await task.value
    }

    func encrypt(overview: DecryptedOverview, dek: Bytes) throws -> String {
        let encoder = JSONEncoder.snakeCaseConverting
        let data = try encoder.encode(overview)
        guard let jsonString = String(data: data, encoding: .utf8) else {
            throw CryptoError.overviewEncryptionFailed
        }
        return try encrypt(payload: jsonString, dek: dek)
    }

    func decrypt(encryptedOverview: String, dek: Bytes) throws -> DecryptedOverview {
        let decryptedJSON = try decrypt(payload: encryptedOverview, dek: dek)
        guard let data = decryptedJSON.data(using: .utf8) else {
            throw CryptoError.overviewDecryptionFailed
        }
        let decoder = JSONDecoder.snakeCaseConverting
        return try decoder.decode(DecryptedOverview.self, from: data)
    }

    func encrypt(details: DecryptedDetails, dek: Bytes) throws -> String {
        let encoder = JSONEncoder.snakeCaseConverting
        let data = try encoder.encode(details)
        guard let jsonString = String(data: data, encoding: .utf8) else {
            throw CryptoError.detailsDecryptionFailed
        }
        return try encrypt(payload: jsonString, dek: dek)
    }

    func decrypt(encryptedDetails: String, dek: Bytes) throws -> DecryptedDetails {
        let decryptedJSON = try decrypt(payload: encryptedDetails, dek: dek)
        guard let data = decryptedJSON.data(using: .utf8) else {
            throw CryptoError.detailsDecryptionFailed
        }
        let decoder = JSONDecoder.snakeCaseConverting
        return try decoder.decode(DecryptedDetails.self, from: data)
    }

    /// Decrypt a legacy item, and return the decrypted fields as a LegacyFields struct.
    /// The `item` should be a legacy encrypted item here.
    func decryptLegacyFields(item: Item) throws -> LegacyFields {
        var legacyFields = LegacyFields()
        if let username = item.username {
            legacyFields.username = try legacyKeyPair.decrypt(cipherText: username)
        }
        if let password = item.password {
            legacyFields.password = try legacyKeyPair.decrypt(cipherText: password)
        }
        if let notes = item.notes {
            legacyFields.notes = try legacyKeyPair.decrypt(cipherText: notes)
        }
        if let otp = item.otp {
            legacyFields.otp = try legacyKeyPair.decrypt(cipherText: otp)
        }
        return legacyFields
    }

}

extension Item {
    /// Update the item's legacy fields with encrypted values of the given newFields.
    /// Returns an updated instance of the given item.
    ///
    /// - Parameters:
    ///    - newFields: the new values to encrypt
    ///    - onItem: the item to update with the encrypted values
    ///    - orignalFields: the initial decrypted values of the item (to determine if re-encryption is necessary)
    mutating func encryptLegacyFields(newFields: LegacyFields,
                                      originalFields: LegacyFields,
                                      legacyKeyPair: LegacyKeyPair) throws {
        if originalFields.username != newFields.username {
            if let username = newFields.username {
                self.username = try legacyKeyPair.encrypt(plainText: username)
            } else {
                self.username = nil
            }
        }

        if originalFields.password != newFields.password {
            if let password = newFields.password {
                self.password = try legacyKeyPair.encrypt(plainText: password)
            } else {
                self.password = nil
            }
        }

        if originalFields.notes != newFields.notes {
            if let notes = newFields.notes {
                self.notes = try legacyKeyPair.encrypt(plainText: notes)
            } else {
                self.notes = nil
            }
        }

        if originalFields.otp != newFields.otp {
            if let otp = newFields.otp {
                self.otp = try legacyKeyPair.encrypt(plainText: otp)
            } else {
                self.otp = nil
            }
        }
    }
}

extension DecryptedDetails {
    func findInMain(kind: FieldKind) -> Field? {
        main.first(where: { $0.kind == kind })
    }

    /// For forwards compatibility, any string is allowed for `kind`.
    mutating func setInMain(kind: FieldKind, value: String?) {
        guard findInMain(kind: kind)?.value != value else { return }

        let field = Field(kind: kind, value: value)
        if let index = main.firstIndex(where: { $0.kind == kind }) {
            main[index] = field
        } else {
            main.append(field)
        }
    }

    mutating func update(fromLegacyFields legacyFields: LegacyFields) {
        notes = legacyFields.notes

        setInMain(kind: .username, value: legacyFields.username)
        setInMain(kind: .password, value: legacyFields.password)
        setInMain(kind: .totp, value: legacyFields.otp)
    }

    /// Populate the v2 history based on the v1 history
    mutating func update(fromLegacyPasswordHistory legacyPasswordVersions: [LegacyPasswordVersion],
                         legacyKeyPair: LegacyKeyPair) throws {
        history = try legacyPasswordVersions.map { version in
            HistoricVersion(
                kind: .password,
                value: try legacyKeyPair.decrypt(cipherText: version.password),
                replacedAt: try version.getDate()
            )
        }
    }

    mutating func trackPasswordChange(previousDetails: DecryptedDetails) {
        let oldPassword = previousDetails.findInMain(kind: .password)?.value
        let newPassword = findInMain(kind: .password)?.value

        if let password = oldPassword, password != newPassword {
            history.append(.init(kind: .password, value: password, replacedAt: Date()))
        }
    }
}

extension DecryptedOverview {
    /// Updates the overview with data from the legacy item field, for which the decrypted values are
    /// passed in as `LegacyFields`.
    /// Updating is not the best decription, because it overwrites everything.
    mutating func update(fromLegacyFields legacyFields: LegacyFields, andItem item: Item) {
        name = item.name
        info = legacyFields.username

        if let website = item.website {
            // OverviewURL does not get a name here
            urls = [OverviewURL(url: website)]
        } else {
            urls = []
        }

        if let appIds = item.androidAppIds {
            androidAppIds = appIds
        } else {
            androidAppIds = []
        }
    }
}
