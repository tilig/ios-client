//
//  EmptyItemsList.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 13/01/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct EmptyItemsList: View {
    @EnvironmentObject var viewModel: ItemListViewModel
    var action: () -> Void

    var title: String {
        switch viewModel.activeItemType {
        case .note:
            return "Add your first secure note"
        case .login:
            return "Add your first login"
        case .wifi:
            return "Add your first WiFi password"
        case .card:
            return "Add your first credit card"
        default:
            return "Welcome to Tilig"
        }
    }

    var subTitle: String {
        switch viewModel.activeItemType {
        case .note:
            return "Add pincodes, bank account details or other sensitive information"
        case .login:
            return "The more logins you add, the easier it becomes to log in on websites"
        case .wifi:
            return "Add passwords for WiFi and other networks"
        case .card:
            return "Add information on your credit cards to make purchases and payments easy"
        default:
            return "Start by adding your first item below"
        }
    }

    var body: some View {
        ZStack {
            RefreshableScrollView(onRefresh: { refreshComplete in
                Task {
                    await viewModel.fetchChanges()
                    refreshComplete()
                }
            }) {

                VStack(alignment: .center) {
                    Image("squirrel_transparent")
                        .resizable()
                        .scaledToFit()
                        .opacity(0.6)
                        .frame(height: 120)
                        .padding(.top, 64)

                    Text(title)
                        .defaultButtonFont(color: .blue, size: 24)
                        .padding(.top)

                    Text(subTitle)
                        .subTextFont(color: .blue.opacity(0.5), size: 16)
                        .multilineTextAlignment(.center)
                        .fixedSize(horizontal: false, vertical: true)
                        .lineSpacing(4)
                        .padding(.top, 4)

                    Spacer()
                }
                .padding()
            }

            AddItemButton()
        }
    }
}
