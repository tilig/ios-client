//
//  ItemCell.swift
//  Tilig
//
//  Created by Jacob Schatz on 1/24/20.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import SwiftUI
import Sentry
import os.log

struct ItemListCell: View {
    var item: ListItem
    var subTitle: String? {
        if let info = item.decryptedOverview?.info {
            if item.itemType == .card && !info.isEmpty {
                return (String(repeating: "•", count: 12) + info).separate(every: 4)
            }
            return info
        }
        return item.itemType == .login ? "-" : nil
    }

    var body: some View {
        HStack(spacing: 8) {
            ItemListIcon(item: item)
                .frame(width: 32, height: 32, alignment: .center)

            VStack(alignment: .leading, spacing: 0) {
                Text(item.name ?? "-")
                    .defaultButtonFont(color: .blue, size: 15)
                    .lineLimit(1)

                if let subTitle {
                    Spacer()
                        .frame(height: 2)

                    Text(subTitle)
                        .subTextFont(color: .blue.opacity(0.5), size: 13)
                        .lineLimit(1)
                }
            }

            Spacer()

            if item.isShared {
                Image("shared_item")
                    .frame(width: 16, height: 16, alignment: .center)
                    .foregroundColor(.borderGray)
            }

            Image(systemName: "chevron.right")
                .frame(width: 16, height: 16, alignment: .center)
                .foregroundColor(.borderGray)

        }
        .padding(.vertical, 10)
        .padding(.horizontal, 16)
    }
}
