//
//  NewLoginButton.swift
//  Tilig
//
//  Created by Gertjan Jansen on 21/09/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import SwiftUI

struct AddItemButton: View {
    @EnvironmentObject var viewModel: ItemListViewModel

    var body: some View {
        AddItemButtonRenderingView(activeItemType: viewModel.activeItemType)
    }
}

private struct AddItemButtonRenderingView: View {
    var activeItemType: ItemType?
    @State var selectedType: ItemType?
    @State var presentTypeSelectionMenu = false

    func reset() {
        selectedType = nil
        presentTypeSelectionMenu = false
    }

    var body: some View {
        VStack(alignment: .trailing, spacing: 10) {
            if let activeItemType = activeItemType {
                DefaultButton(
                    title: activeItemType.label,
                    image: Image(systemName: "plus.circle.fill"),
                    fullWidth: false
                ) {
                    selectedType = activeItemType
                }
            } else {
                if presentTypeSelectionMenu {
                    ForEach(ItemType.allCases) { itemType in
                        if itemType != ItemType.custom {
                            DefaultButton(
                                title: itemType.shortLabel,
                                image: Image(systemName: "plus.circle.fill"),
                                fullWidth: false
                            ) {
                                selectedType = itemType
                            }
                        }
                    }
                }

                Button(action: {
                    presentTypeSelectionMenu = !presentTypeSelectionMenu
                }) {
                    Image(systemName: "plus.circle.fill")
                        .resizable()
                        .foregroundColor(.white)
                        .frame(width: 25, height: 25)
                        .rotationEffect(.init(degrees: self.presentTypeSelectionMenu ? 45 : 0), anchor: .center)
                        .padding()
                }
                .background(
                    RoundedRectangle(cornerRadius: 10)
                        .fill(presentTypeSelectionMenu ? .gray : .mediumPurple)
                        .shadow(color: .borderGray, radius: 1, x: 0, y: 1)
                )
            }
        }
        .padding(.trailing)
        .padding(.bottom)
        .pushToBottomRight()
        .sheet(item: $selectedType, content: { selectedType in
            AddItemView(itemType: selectedType)
        })
        .onChange(of: activeItemType) { _ in
            reset()
        }
    }
}

#if DEBUG
struct NewItemButton_Previews: PreviewProvider {
    static var previews: some View {
        AddItemButtonRenderingView(
            activeItemType: nil
        )
    }
}
#endif
