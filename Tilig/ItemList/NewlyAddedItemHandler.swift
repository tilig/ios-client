//
//  NewlyAddedItemHandler.swift
//  Tilig
//
//  Created by Gertjan Jansen on 07/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

/// This view is a workaround to make sure an item that was just added can be presented immediately,
/// without waiting for the item list to re-render. It's kind of complicted, but I didn't find a better way to do it.
struct NewlyAddedItemHandler: View {
    @EnvironmentObject var routeModel: RouteModel
    @EnvironmentObject var viewModel: ItemListViewModel
    @EnvironmentObject var getStartedViewModel: GetStartedViewModel

    @State var showNewlyAddedItem = false

    var body: some View {
        Group {
            // For quick navigation to newly added item details
            if let newlyAddedItem = viewModel.newlyAddedItem {
                NavigationLink(
                    destination: newItemDestination(newlyAddedItem: newlyAddedItem),
                    isActive: $showNewlyAddedItem
                ) {
                    EmptyView()
                }
            }
        }
        .onReceive(viewModel.$newlyAddedItem, perform: { item in
            // trying to prevent that back button does not work (seemed to happen sometimes)
            // Don't know why exactly, probably some other SwiftUI nav bug
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                // Only show new item when flow was not initiated by getStarted
                showNewlyAddedItem = item != nil && (getStartedViewModel.presentingAddLoginView == false)
                if item != nil {
                    _ = withAnimation {
                        Task { await getStartedViewModel.completeStep(type: .addLogin) }
                    }
                }
            }
        })
        // If a route change to .itemList is triggered, stop presenting any subviews.
        .onReceive(routeModel.$currentRoute, perform: { currentRoute in
            if case .itemList = currentRoute {
                showNewlyAddedItem = false
            }
        })
    }

    func newItemDestination(newlyAddedItem: Item) -> some View {
        return AnyView(ItemView().setInstanceModels(forItem: newlyAddedItem))
    }
}

// struct NewlyAddedItemHandler_Previews: PreviewProvider {
//    static var previews: some View {
//        NewlyAddedItemHandler()
//    }
// }
