//
//  EmptySearchResultsView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 04/11/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI

struct EmptySearchResultsView: View {

    var body: some View {
        VStack(alignment: .center) {
            Image("empty_search_results")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(height: 32, alignment: .center)

            Text("Sorry, no results")
                .defaultButtonFont(color: .blue.opacity(0.5), size: 16)

            Spacer()
        }
        .padding(.top, 32)
    }
}
