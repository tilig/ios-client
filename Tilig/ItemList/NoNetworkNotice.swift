//
//  NoNetworkNotice.swift
//  Tilig
//
//  Created by Gertjan Jansen on 15/01/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct NoNetworkNotice: View {
    @EnvironmentObject var networkMonitor: NetworkMonitor

    var body: some View {
        if !networkMonitor.hasConnection {
            HStack {
                Spacer()

                Text("No internet connection.")
                    .padding(.vertical, 5)

                Spacer()
            }
            .foregroundColor(Color.white)
            .background(Color.mediumPurple)
        }
    }
}

#if DEBUG
struct NoNetworkNotice_Previews: PreviewProvider {
    static var previews: some View {
        NoNetworkNotice()
    }
}
#endif
