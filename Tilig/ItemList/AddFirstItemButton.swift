//
//  AddFirstItemButton.swift
//  Tilig
//
//  Created by Gertjan Jansen on 08/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct AddFirstItemButton: View {
    @EnvironmentObject var viewModel: ItemListViewModel

    @State var presentAddItemSheet = false

    var body: some View {
        DefaultButton(
            title: "Add your first \(viewModel.activeItemType?.label ?? "item")",
            textSize: 18,
            image: Image(systemName: "plus.circle.fill")
        ) {
            presentAddItemSheet = true
        }
        .sheet(isPresented: $presentAddItemSheet) {
            AddItemView(itemType: viewModel.activeItemType ?? .login)
        }
    }
}

struct AddFirstLoginButton_Previews: PreviewProvider {
    static var previews: some View {
        AddFirstItemButton()
    }
}
