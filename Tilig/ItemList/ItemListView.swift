//
//  ItemList.swift
//  Tilig
//
//  Created by Gertjan Jansen on 01/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import SwiftUI

struct ItemListView: View {
    @EnvironmentObject var networkMonitor: NetworkMonitor
    @EnvironmentObject var authManager: AuthManager
    @EnvironmentObject var viewModel: ItemListViewModel
    @EnvironmentObject var routeModel: RouteModel
    @State var selectedItemId: String?

    var body: some View {
        NavigationView {
            VStack(spacing: 0) {
                ItemListHeader()

                NoNetworkNotice()

                NewlyAddedItemHandler()

                GetStartedPromptView()

                ItemsTabView()

                if viewModel.loadingState == .fetchingInitial {
                    Spacer()
                    ActivityIndicator()
                    Spacer()
                } else if viewModel.itemsForActiveItemType.isEmpty {
                    EmptyItemsList(action: { viewModel.refreshData() })
                } else {
                    ZStack {
                        if viewModel.filteredItems.isEmpty {
                            EmptySearchResultsView()
                        } else {
                            RefreshableScrollView(
                                onRefresh: { refreshComplete in
                                    Task {
                                        await viewModel.fetchChanges()
                                        refreshComplete()
                                    }
                                }
                            ) {
                                LazyVStack(alignment: .leading) {
                                    ForEach(viewModel.filteredItems) { item in
                                        NavigationLink(
                                            destination: ItemView().setInstanceModels(forItem: item.encryptedItem),
                                            tag: item.id?.uuidString ?? "",
                                            selection: $selectedItemId
                                        ) {
                                            ItemListCell(item: item)
                                        }
                                        .onChange(of: selectedItemId) { newValue in
                                            if newValue != nil {
                                                UIApplication.shared.endEditing(true)
                                            }
                                        }
                                    }
                                }
                                .frame(maxWidth: .infinity, maxHeight: .infinity)
                            }
                        }

                        AddItemButton()
                    }
                }

                EnableAutoFillWarning()

#if DEBUG
                DebugToolsView()
#endif
            }
            .navigationBarHidden(true)
            .edgesIgnoringSafeArea(.top)
            .onAppear {
                // This onAppear block can get called when signing out,
                // in which case we don't want to active (= do a query)
                // therefore check if authState is not nil.
                if authManager.authState != nil && networkMonitor.hasConnection {
                    viewModel.activate()
                }

            }
            // If a route change to .itemList is triggered, stop presenting the item details view
            // Note that the `NewlyAddedAccountHandler` does something similar.
            .onReceive(routeModel.$currentRoute, perform: { currentRoute in
                if case .itemList = currentRoute {
                    selectedItemId = nil
                }
            })
            .resignKeyboardOnDragGesture()
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

#if DEBUG
struct ItemList_Previews: PreviewProvider {
    static var viewModel = ItemListViewModel(
        crypto: .random(),
        tokenProvider: .mocked()
    ) {
        didSet {
            viewModel.set(items: [ ListItem(encryptedItem: .random()) ])
        }
    }

    static var previews: some View {
        ItemListView()
            .environmentObject(viewModel)
    }
}
#endif
