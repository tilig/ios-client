//
//  ItemsTabView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 24/10/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct Tab {
    var title: String
    var itemType: ItemType?
}

struct ItemsTabView: View {
    @EnvironmentObject var viewModel: ItemListViewModel

    @State var selectedTabIndex: Int = 0

    var tabs: [Tab] {
        [
            Tab(title: "All items", itemType: nil),
            Tab(title: "Logins", itemType: ItemType.login),
            Tab(title: "Notes", itemType: ItemType.note),
            Tab(title: "Cards", itemType: ItemType.card),
            Tab(title: "WiFi", itemType: ItemType.wifi)
        ]
    }

    func selectTab(tabIndex: Int) {
        withAnimation {
            selectedTabIndex = tabIndex
        }
        viewModel.setActiveItemType(itemType: tabs[tabIndex].itemType)
    }

    func isActive(tabIndex: Int) -> Bool {
        tabIndex == selectedTabIndex
    }

    var body: some View {
        GeometryReader { geo in
            ScrollView(.horizontal, showsIndicators: false) {
                ScrollViewReader { proxy in
                    VStack(spacing: 0) {
                        HStack(spacing: 0) {
                            ForEach(0 ..< tabs.count, id: \.self) { index in
                                Button(action: { selectTab(tabIndex: index) }, label: {
                                    VStack(spacing: 0) {
                                        HStack {
                                            Text(tabs[index].title)
                                                .defaultMediumFont(
                                                    color: isActive(tabIndex: index) ? Color.mediumPurple : .gray,
                                                    size: 14
                                                )
                                                .padding(EdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 0))
                                        }
                                        .frame(
                                            width: geo.size.width / CGFloat(tabs.count),
                                            height: 52
                                        )
                                        // Bar Indicator
                                        Rectangle().fill(isActive(tabIndex: index) ? Color.mediumPurple : Color.clear)
                                            .frame(height: 3)
                                    }.fixedSize()
                                })
                                .contentShape(Rectangle())
                                .accentColor(Color.white)
                                .buttonStyle(PlainButtonStyle())
                            }
                        }
                        .onChange(of: selectedTabIndex) { index in
                            withAnimation {
                                proxy.scrollTo(index)
                            }
                        }

                        Rectangle().fill(Color.mediumPurple.opacity(0.5))
                            .frame(height: 0.5)
                            .offset(y: -0.5)
                    }
                }
            }
        }
        .frame(height: 55)
    }
}
