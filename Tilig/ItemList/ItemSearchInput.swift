//
//  ItemSearchInput.swift
//  Tilig
//
//  Created by Gertjan Jansen on 03/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import SwiftUI

struct ItemSearchInput: View {
    var label: String = "Search for an item"
    @Binding var searchText: String
    var onEditingChanged: (Bool) -> Void = { _ in }
    var onFocusChanged: (Bool) -> Void = { _ in }

    var body: some View {
        HStack(alignment: .center) {
            SearchInput(
                label: label,
                searchText: $searchText,
                onEditingChanged: onEditingChanged,
                onFocusChanged: onFocusChanged
            )
        }
        // I set a max height to prevent weird resizing when selecting input
        // which works in simulator, but doesn't in real app.
        // might not be important when implementing to new design, so let's see that first.
        .frame(maxHeight: 50)
        // .padding(Edge.Set(arrayLiteral: [.top, .leading, .trailing]))
    }
}

#if DEBUG
struct ItemSearchInput_Previews: PreviewProvider {
    @State static var searchText = "Test"
    @State static var searchText2 = ""

    static var previews: some View {
        VStack {
            ItemSearchInput(label: "Search for an item", searchText: $searchText)
                .background(Color.orange)

            ItemSearchInput(label: "Search for an item", searchText: $searchText2)
                .background(Color.yellow)
        }
    }
}
#endif
