//
//  ItemListHeader.swift
//  Tilig
//
//  Created by Gertjan Jansen on 03/06/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct ItemListHeader: View {
    @EnvironmentObject var viewModel: ItemListViewModel
    @State var isNavigationLinkActive: Bool = false

    var body: some View {
        ZStack {
            Image("items_header")
                .resizable()
                .scaledToFill()
                .frame(height: 140)
                .clipped()

            HStack {
                ItemSearchInput(searchText: $viewModel.searchText) { isFocused in
                    if isFocused {
                        TrackingEvent.itemSearched.send(withProperties: ["Source": "Inside App"])
                    }
                }

                Spacer()

                NavigationLink(
                    destination: SettingsView().consumingViewModel(.settings),
                    isActive: $isNavigationLinkActive
                ) {
                    Image(systemName: "gearshape.fill")
                        .resizable()
                        .renderingMode(.template)
                        .aspectRatio(contentMode: .fit)
                        .foregroundColor(.white)
                        .frame(height: 30)
                }
                .onChange(of: isNavigationLinkActive) { newValue in
                    if newValue {
                        UIApplication.shared.endEditing(true)
                    }
                }
            }
            .padding(.top, 40)
            .padding(.horizontal, 16)
            .padding(.bottom, -6)
        }
        .frame(height: 140)
    }
}

struct ItemListHeader_Previews: PreviewProvider {
    static var previews: some View {
        ItemListHeader()
    }
}
