//
//  ItemListIcon.swift
//  Tilig
//
//  Created by Gertjan Jansen on 13/01/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import SwiftUI

struct ItemListIcon: View {
    var item: ListItem

    var body: some View {
        ListIcon(
            iconURL: item.encryptedItem.iconURL,
            backgroundColor: .clear,
            itemType: item.itemType
        )
    }
}

#if DEBUG
struct ItemListIcon_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            ItemListIcon(item: ListItem(encryptedItem: ItemPreviewData().data[0]))
                .frame(width: 80, height: 80, alignment: .center)

            ItemListIcon(item: ListItem(encryptedItem: ItemPreviewData().data[1]))
                .frame(width: 80, height: 80, alignment: .center)
        }
    }
}
#endif
