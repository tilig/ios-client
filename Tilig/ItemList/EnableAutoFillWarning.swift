//
//  EnableAutoFillWarning.swift
//  Tilig
//
//  Created by Gertjan Jansen on 03/06/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct EnableAutoFillWarning: View {
    @EnvironmentObject var enableAutoFillViewModel: EnableAutoFillViewModel

    var body: some View {
        if enableAutoFillViewModel.autoFillEnabled == false {
            HStack {
                Text("Enable autofill to let Tilig type your passwords for you!")
                    .defaultButtonFont(size: 16)
                    .minimumScaleFactor(0.2)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.trailing, 10)

                WhiteButton(
                    title: "Enable",
                    textSize: 16,
                    foregroundColor: .blue,
                    maxHeight: 40,
                    action: {
                        enableAutoFillViewModel.presentAutoFillExplanation()
                    }
                )
            }
            .padding([.horizontal, .top])
            .padding(.bottom, 24)
            .frame(maxHeight: 80)
            .background(Color.yellow.edgesIgnoringSafeArea(.bottom))
        } else {
            EmptyView()
        }
    }
}
