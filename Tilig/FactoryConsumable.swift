//
//  FactoryConsumable.swift
//  Tilig
//
//  Created by Gertjan Jansen on 05/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Combine
import SwiftUI

// Not: not available in the autofill extension (yet)
enum ViewModelType {
    // Global models
    case itemList
    case settings
    case createItem(ItemType)
    case getStarted

    // Instance models
    case itemDetails
    case editItem
    case twoFactor
    case deleteItem
    case shareItem

}

struct ConsumingViewModelModifier: ViewModifier {
    @EnvironmentObject var authenticatedModelFactory: AuthenticatedModelFactory

    var viewModelType: ViewModelType

    func body(content: Content) -> some View {
        // This is repetitive, but it's very hard to make this generic because of
        // ObservableObject being a protocol - and not a return type.
        // I also didn't want to introduce a parent 'ViewModel' class because that
        // creates other problems, like haveing to override initializers.
        switch viewModelType {
        case .itemList:
            content
                .environmentObject(authenticatedModelFactory.itemListViewModel)
        case .settings:
            content
                .environmentObject(authenticatedModelFactory.settingsViewModel)
        case .getStarted:
            content
                .environmentObject(authenticatedModelFactory.getStartedViewModel)
        case .createItem(let itemType):
            let model = authenticatedModelFactory.addItemViewModel(forType: itemType)
            if let model = model as? AddLoginViewModel {
                content
                    .environmentObject(model)
            }
            if let model = model as? AddNoteViewModel {
                content
                    .environmentObject(model)
            }
            if let model = model as? AddWifiViewModel {
                content
                    .environmentObject(model)
            }
            if let model = model as? AddCardViewModel {
                content
                    .environmentObject(model)
            }
        case .itemDetails:
            if let viewModel = authenticatedModelFactory.itemViewModel {
                content
                    .environmentObject(viewModel)
            } else {
                InstanceErrorView()
            }
        case .twoFactor:
            if let viewModel = authenticatedModelFactory.twoFactorViewModel {
                content
                    .environmentObject(viewModel)
            } else {
                InstanceErrorView()
            }
        case .editItem:
            if let viewModel = authenticatedModelFactory.editItemViewModel {
                content
                    .environmentObject(viewModel)
            } else {
                InstanceErrorView()
            }
        case .deleteItem:
            if let viewModel = authenticatedModelFactory.deleteItemViewModel {
                content
                    .environmentObject(viewModel)
            } else {
                InstanceErrorView()
            }
        case .shareItem:
            if let viewModel = authenticatedModelFactory.shareItemViewModel {
                content
                    .environmentObject(viewModel)
            } else {
                InstanceErrorView()
            }
        }
    }
}

/// Quick solution for showing an error when there's no active account instance set in the factory
struct InstanceErrorView: View {
    var body: some View {
        // TODO: fix 
        Text("Error: no account instance")
    }
}

extension View {
    func consumingViewModel(_ viewModelType: ViewModelType) -> some View {
        self.modifier(ConsumingViewModelModifier(viewModelType: viewModelType))
    }
}
