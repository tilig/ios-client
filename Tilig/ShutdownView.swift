//
//  ShutdownView.swift
//  Tilig
//
//  Created by Gertjan Jansen on 12/04/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct ShutdownView: View {
    var body: some View {
        VStack(alignment: .center) {
            Text("⚠️ URGENT")
                .bold()
                .foregroundColor(.red)
            Text("Tilig shuts down on April 30th, 2023.")
                .bold()
                .foregroundColor(.red)

            Text("Move your passwords to a different password manager as soon as possible.")
                .padding(.top, 10)
                .multilineTextAlignment(.center)

            DefaultButton(title: "Read more") {
                if let url = URL(string: "https://www.tilig.com/") {
                    UIApplication.shared.open(url)
                }
            }
        }
        .padding()
        .background(Color.lightGray)
    }

}
