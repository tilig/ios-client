//
//  UserDefaults.swift
//  Tilig
//
//  Created by Gertjan Jansen on 08/12/2020.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import Foundation

extension UserDefaults {
    public static let standard = UserDefaults(suiteName: "group.com.tilig.Tilig")!

    // Onboarding flow
    public static let AutoFillOnboardingCompleted = "AutoFillOnboardingCompleted"

    // Mixpanel
    // This is used to keep track of
    public static let LastKnownAutoFillEnabledState = "MixpanelAutoFillEnabled"

    // Used to check if user has completed first autofill
    public static let FirstAutoFill = "FirstAutoFill"

    // Used to check if user has completed third autofill
    public static let ThirdAutoFill = "ThirdAutoFill"

    // Used to keep track of the autofill count
    public static let AutoFillCount = "AutoFillCount"

    // Used to keep track of last polled date
    public static let LastRefreshed = "LastRefreshed"
}

extension UserDefaults {
    func reset() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
}
