//
//  SettingsDeleteView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 01/11/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct SettingsDeleteView: View {
    @State var presentDeleteProfileSheet = false
    @State var presentWarning: Bool = false
    var isDeleting: Bool
    var deleteAction: () -> Void = { }

    var body: some View {
        Group {
            if isDeleting {
                ActivityIndicator()

            } else {
                VStack {
                    if presentWarning {
                        VStack {
                            Text("Are you sure you want to delete all your data and passwords?")
                                .defaultButtonFont(color: .blue, size: 14)
                                .lineSpacing(4)
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding(.bottom, 4)

                            HStack {
                                Image(systemName: "exclamationmark.triangle.fill").font(.system(size: 12))
                                    .foregroundColor(.blue.opacity(0.5))

                                Text("This action cannot be undone")
                                    .subTextFont(color: .blue.opacity(0.5), size: 12)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                            }
                            .padding(.bottom)

                            HStack(spacing: 0) {
                                WhiteButton(
                                    title: "Cancel",
                                    textSize: 14,
                                    foregroundColor: .blue.opacity(0.75),
                                    fullWidth: true
                                ) {
                                    presentWarning.toggle()
                                }

                                Spacer()
                                    .frame(width: 20)

                                DefaultButton(title: "Yes, continue", backgroundColor: .red, fullWidth: true) {
                                    presentDeleteProfileSheet.toggle()
                                    TrackingEvent.deleteTiligAccountWarningConfirmed.send()
                                }
                            }
                            .frame(height: 60)
                        }
                    } else {
                        Button(action: {
                            presentWarning.toggle()
                            TrackingEvent.deleteTiligAccountWarningViewed.send()
                        }) {
                            HStack {
                                Image(systemName: "xmark.circle.fill").font(.system(size: 18))
                                    .foregroundColor(.red)

                                Text("Delete Tilig account")
                                    .defaultButtonFont(color: .red, size: 14)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                            }
                        }
                    }
                }
            }
        }
        .padding()
        .frame(maxWidth: .infinity, alignment: .center)
        .background(presentWarning ? Color.lightRed : Color.white)
        .cornerRadius(10.0)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(presentWarning ? Color.red : .borderGray, lineWidth: 1.0)
        )
        .padding(.top, 8)
        .padding(.horizontal, 4)
        .padding(.bottom)
        .sheet(isPresented: $presentDeleteProfileSheet) {
            DeleteProfileView(deleteAction: deleteAction)
                .consumingViewModel(.itemList)
        }
    }
}
