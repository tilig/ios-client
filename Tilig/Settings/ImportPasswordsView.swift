//
//  ImportPasswordsView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 14/02/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct ImportPasswordsView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    func backAction() {
        presentationMode.wrappedValue.dismiss()
    }

    var appLinkText: Text {
        Text("Go to ")
            .defaultButtonFont(color: .blue, size: 14) +
        Text("app.tilig.com/import")
            .defaultBoldFont(color: .blue, size: 15)
    }

    var body: some View {
        VStack(spacing: 0) {
            PopupHeaderView(
                title: "Import passwords",
                closeButtonAction: backAction,
                hasBackground: false,
                isBackEnabled: true
            )

            ScrollView(showsIndicators: false) {
                VStack(spacing: 16) {
                    HStack {
                        Text("Use your desktop computer")
                            .defaultButtonFont(color: .blue, size: 14)

                        Spacer()

                        Image("desktop")
                    }
                    .padding(.horizontal)
                    .padding(.vertical, 8)
                    .roundedRectangleBackground(
                        fillColor: .mediumPurple.opacity(0.1),
                        shadowRadius: 0
                    )

                    VStack(alignment: .leading, spacing: 20) {
                        NumberedItemView(
                            number: 1, customText: appLinkText)

                        NumberedItemView(
                            number: 2, text: "Select your current password manager")

                        NumberedItemView(
                            number: 3, text: "Follow the steps to create a .CSV file with your passwords")

                        NumberedItemView(
                            number: 4, text: "Upload .CSV file to Tilig, in your desktop")
                    }
                    .padding(.vertical)
                    .whiteRectangleBackground()

                    Spacer()

                    Image("devices_illustration")
                        .frame(height: 120, alignment: .center)

                    Text("Your passwords will be available across your devices")
                        .defaultButtonFont(color: .blue.opacity(0.5), size: 12)
                        .multilineTextAlignment(.center)
                        .responsivePadding(.horizontal, compact: 32, regular: 64)

                    Spacer()
                }
            }
            .responsivePadding(compact: 16, regular: 48)
        }
        .background(Color.backgroundGray.edgesIgnoringSafeArea(.vertical))
        .swipeRightToDismiss()
        .navigationBarHidden(true)
    }
}

struct NumberedItemView: View {
    var number: Int
    var text: String?
    var textSize: Double = 14
    var customText: Text?
    var iconName: String?

    var body: some View {
        HStack(spacing: 16) {
            Text("\(number)")
                .defaultButtonFont(color: .blue.opacity(0.5), size: textSize)
                .frame(width: 24, height: 24)
                .roundedRectangleBackground(fillColor: .mediumPurple.opacity(0.1), cornerRadius: 12)
                .overlay(
                    Circle()
                        .stroke(.white, lineWidth: 1)
                )

            if let text {
                Text(text)
                    .defaultButtonFont(color: .blue, size: textSize)
            }

            if let customText {
                customText
            }

            if let iconName {
                Spacer()

                Image(iconName)
                    .frame(width: 24, height: 24)
            }
        }
        .frame(maxWidth: .infinity, alignment: .leading)
    }
}

struct ImportPasswordsView_Previews: PreviewProvider {
    static var previews: some View {
        ImportPasswordsView()
    }
}
