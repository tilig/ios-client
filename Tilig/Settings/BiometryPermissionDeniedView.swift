//
//  BiometryNotEnrolledView.swift
//  Tilig
//
//  Created by Gertjan Jansen on 10/01/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct BiometryPermissionDeniedView: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text("Biometric Lock")
                .defaultButtonFont(color: .blue, size: 14)

            // swiftlint:disable line_length
            Text("You've denied the use of biometry for this app. Please give Tilig permission for biometrics in the settings.")
                .subTextFont(color: .blue.opacity(0.5), size: 12)

            Link("Open settings", destination: URL(string: UIApplication.openSettingsURLString)!)
        }
    }
}

struct BiometryPermissionDenied_Previews: PreviewProvider {
    static var previews: some View {
        BiometryPermissionDeniedView()
    }
}
