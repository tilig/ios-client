//
//  ModalWebView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 02/08/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import SwiftUI
import WebKit

struct ModalWebView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    var title: String?
    var url: URL

    func backAction() {
        presentationMode.wrappedValue.dismiss()
    }

    var body: some View {
        VStack(spacing: 0) {
            PopupHeaderView(
                title: title ?? "",
                closeButtonAction: backAction,
                hasBackground: false,
                isBackEnabled: false
            )

            WebView(url: url)
                .edgesIgnoringSafeArea(.bottom)
        }
    }
}

private struct WebView: UIViewRepresentable {

    var url: URL

    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }

    func updateUIView(_ webView: WKWebView, context: Context) {
        let request = URLRequest(url: url)
        webView.load(request)
    }
}
