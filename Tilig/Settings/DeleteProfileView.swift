//
//  DeleteProfileView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 04/10/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct DeleteProfileView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var itemListViewModel: ItemListViewModel
    var deleteAction: () -> Void = { }
    @State var confirmationText: String?
    @State var error: DeleteProfileError?

    func confirmDeleteTextMatch() {
        guard confirmationText?.uppercased() == "DELETE" else {
            error = .confirmationTextDoesNotMatch
            return
        }
        TrackingEvent.deleteTiligAccountTextInputConfirmed.send()
        dismiss()

        Task {
            // wait 1 second to allow the dismiss UI operation
            try await Task.sleep(nanoseconds: 1_000_000_000)
            deleteAction()
        }
    }

    func dismiss() {
        self.presentationMode.wrappedValue.dismiss()
    }

    var confirmDeleteText: Text {
        Text("Confirm deleting your Tilig account by typing the word ")
            .defaultButtonFont(size: 14) +
        Text("DELETE")
            .defaultBoldFont(color: .red, size: 16) +
        Text(" below.")
            .defaultButtonFont(size: 14)
    }

    var countText: String {
        let loginCount = itemListViewModel.items.filter({ $0.itemType == .login }).count
        let cardCount = itemListViewModel.items.filter({ $0.itemType == .card }).count
        let noteCount = itemListViewModel.items.filter({ $0.itemType == .note }).count
        let wifiCount = itemListViewModel.items.filter({ $0.itemType == .wifi }).count

        var text = ""
        text += ((loginCount > 0) ? ", \(loginCount) " + (loginCount == 1 ? "login" : "logins") : "")
        text += ((noteCount > 0) ? ", \(noteCount) " + (noteCount == 1 ? "secure note" : "secure notes") : "")
        text += ((wifiCount > 0) ? ", \(wifiCount) " + (wifiCount == 1 ? "wifi item" : "wifi items") : "")
        text += ((cardCount > 0) ? ", \(cardCount) " + (cardCount == 1 ? "credit card" : "credit cards") : "")

        return text
    }

    var body: some View {
        VStack {
            PopupHeaderView(
                title: "",
                closeButtonAction: dismiss
            )

            ScrollView {
                VStack(spacing: 16) {
                    Image(systemName: "xmark.circle.fill").font(.system(size: 24))
                        .foregroundColor(.red)

                    Text("Delete account?")
                        .subTextMediumFont(size: 20)
                        .multilineTextAlignment(.center)

                    Text("You'll lose your secret encryption key\(countText) on all devices permanently")
                        .defaultButtonFont(size: 14)
                        .multilineTextAlignment(.leading)
                        .fixedSize(horizontal: false, vertical: true)
                        .lineSpacing(4)

                    VStack(spacing: 16) {
                        confirmDeleteText
                            .multilineTextAlignment(.center)
                            .fixedSize(horizontal: false, vertical: true)

                        VStack(alignment: .leading, spacing: 4) {
                            FormTextField(
                                "DELETE",
                                value: $confirmationText,
                                editMode: true,
                                focusedBorderColor: .red
                            )

                            if error != nil {
                                HStack {
                                    Image(systemName: "info.circle.fill")
                                        .foregroundColor(.red)

                                    Text("Sorry, please enter the exact text")
                                        .defaultButtonFont(color: .red, size: 12)
                                }
                            }
                        }

                        HStack(spacing: 0) {
                            WhiteButton(
                                title: "Cancel",
                                textSize: 14,
                                foregroundColor: .blue.opacity(0.75),
                                fullWidth: true,
                                action: dismiss
                            )

                            Spacer()
                                .frame(width: 20)

                            DefaultButton(
                                title: "Delete forever",
                                backgroundColor: .red,
                                fullWidth: true,
                                action: confirmDeleteTextMatch
                            )
                        }
                        .frame(height: 60)
                    }
                    .padding()
                    .roundedRectangleBackground(
                        fillColor: .lightRed,
                        shadowColor: .clear,
                        shadowRadius: 0,
                        cornerRadius: 5
                    )

                    Spacer()
                }
                .padding()
            }
        }
        .onChange(of: confirmationText) { _ in
            error = nil
        }
    }
}

struct DeleteProfileView_Previews: PreviewProvider {
    static var previews: some View {
        DeleteProfileView()
    }
}

enum DeleteProfileError: Error {
    case confirmationTextDoesNotMatch
}

extension DeleteProfileError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .confirmationTextDoesNotMatch:
            return "Sorry, please enter the exact text"
        }
    }
}
