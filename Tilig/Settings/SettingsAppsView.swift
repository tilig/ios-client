//
//  SettingsAppsView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 11/02/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct SettingsAppsView: View {
    @EnvironmentObject var viewModel: SettingsViewModel
    @State var isPresentingWebView = false
    @State var url: URL?

    var body: some View {
        VStack(spacing: 16) {
            SettingsRowView(
                title: "Open webapp",
                buttonImage: Image("external_link"),
                action: { UIApplication.shared.open(URL(string: "https://app.tilig.com")!) }
            )

            Divider()

            SettingsExpandableView(title: "Browser extensions") {
                VStack {
                    SettingsSubRowView(
                        imageName: "chrome-logo",
                        title: "Chrome",
                        action: { viewModel.openURL(urlString: viewModel.chromeURL)}
                    )
                    .responsivePadding(.top, compact: 10, regular: 20)

                    SettingsSubRowView(
                        imageName: "edge-logo",
                        title: "Edge",
                        action: { viewModel.openURL(urlString: viewModel.edgeURL)}
                    )

                    SettingsSubRowView(
                        imageName: "opera-logo",
                        title: "Opera",
                        action: { viewModel.openURL(urlString: viewModel.operaURL)}
                    )
                }
            }
        }
    }
}

private struct SettingsExpandableView<Content: View>: View {
    @EnvironmentObject var viewModel: SettingsViewModel
    @State var title: String
    @State var isExpanded: Bool = false
    @State var isSubViewVisible = false

    var content: () -> Content

    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text(title)
                    .defaultButtonFont(color: .blue, size: 14)

                Spacer()

                Image(systemName: isExpanded ? "chevron.up" : "chevron.down")
                    .frame(width: 16, height: 16, alignment: .center)
                    .foregroundColor(.borderGray)
            }

            if isExpanded {
                Group {
                    content()
                }
                .opacity(isSubViewVisible ? 1.0 : 0.0)
            }
        }
        .contentShape(Rectangle())
        .onTapGesture {
            withAnimation {
                isExpanded.toggle()
                if !isExpanded {
                    isSubViewVisible = false
                }
            }
        }
        .onChange(of: isExpanded) { isExpanded in
            if isExpanded {
                withAnimation(.default.delay(0.2)) {
                    isSubViewVisible = isExpanded
                }
            }
        }

    }
}

struct SettingsSubRowView: View {
    var imageName: String
    var title: String
    var action: () -> Void = { }

    var body: some View {
        Button(action: action) {
            HStack {
                Image(imageName)
                    .resizable()
                    .frame(width: 16, height: 16, alignment: .center)
                    .foregroundColor(.borderGray)

                Text(title)
                    .subTextFont(color: .blue, size: 14)

                Spacer()
            }
            .padding(.vertical, 8)
        }
        .contentShape(Rectangle())
    }
}

struct SettingsAppsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsAppsView()
    }
}
