//
//  BiometryNotEnrolledView.swift
//  Tilig
//
//  Created by Gertjan Jansen on 10/01/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct BiometryNotEnrolledView: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text("Biometric Lock")
                .defaultButtonFont(color: .blue, size: 14)

            // swiftlint:disable line_length
            Text("The biometric lock cannot be activated because you haven't configured face ID or touch ID on your device.")
                .subTextFont(color: .blue.opacity(0.5), size: 12)
        }
    }
}

struct BiometryNotEnrolledView_Previews: PreviewProvider {
    static var previews: some View {
        BiometryNotEnrolledView()
    }
}
