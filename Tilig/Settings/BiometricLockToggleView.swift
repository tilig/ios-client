//
//  BiometricLockToggleView.swift
//  Tilig
//
//  Created by Fitzgerald Afful on 22/09/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct BiometricLockToggleView: View {
    @EnvironmentObject var viewModel: BiometricViewModel

    func displayAlert() {
        let supportedType = viewModel.supportedBiometricType == .faceID ? "face recognition" : "your fingerprint"
        let detail = """
        The biometric lock adds an additional layer of security.

        When enabled, you'll be able to unlock Tilig using \(supportedType).
        """
        let alertItem = AlertItem(
            id: UUID(),
            title: Text("About the Biometric Lock").defaultBoldFont(color: .mediumPurple),
            message: Text(detail),
            dismissButton: .default(Text("Got it!"))
        )
        alertItem.post()
    }

    func infoButtonTapped() {
       displayAlert()
       TrackingEvent.biometricInfoTapped.send()
    }

    var body: some View {
        if viewModel.canEvaluate && !viewModel.biometryPermissionDenied {
            Toggle(isOn: Binding<Bool>(
                get: { viewModel.isEnabled },
                set: { _ in viewModel.toggleEnabled() }
            )) {
                HStack {
                    Text("Enable Biometric Lock")
                        .defaultButtonFont(color: .blue, size: 14)
                        .padding(.trailing)

                    Spacer()
                }
            }
        } else if viewModel.biometryPermissionDenied {
            BiometryPermissionDeniedView()
        } else {
            BiometryNotEnrolledView()
        }
    }
}

struct BiometricLockToggleView_Previews: PreviewProvider {
    static var previews: some View {
        BiometricLockToggleView()
    }
}
