//
//  SettingsView.swift
//  Tilig
//
//  Created by Gertjan Jansen on 01/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import SwiftUI
import Firebase
import Sentry

struct SettingsView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var viewModel: SettingsViewModel
    @EnvironmentObject var biometricViewModel: BiometricViewModel
    @State var presentImportPasswordsView = false

    func signOut() {
        let alertItem = AlertItem(
            id: UUID(),
            title: Text("Are you sure you want to sign out?"),
            message: Text("You can easily sign in again."),
            primaryButton: .destructive(Text("Sign Out"), action: { viewModel.signOut() }),
            secondaryButton: .cancel()
        )
        alertItem.post()
    }

    func backAction() {
        presentationMode.wrappedValue.dismiss()
    }

    func openMail() {
        // does not work in simulator
        let email = "support@tilig.com"
        if let url = URL(string: "mailto:\(email)") {
            UIApplication.shared.open(url)
        }
        TrackingEvent.getInTouchViewed.send()
    }

    var body: some View {
        VStack(spacing: 0) {
            PopupHeaderView(
                title: "Settings",
                closeButtonAction: backAction,
                hasBackground: false,
                isBackEnabled: true
            )

            ScrollView(showsIndicators: false) {
                VStack(alignment: .leading, spacing: 0) {
                    SettingsSectionHeaderView(title: "Account")

                    SettingsLogOutRow(
                        title: viewModel.name,
                        subTitle: viewModel.email,
                        buttonAction: signOut
                    )
                    .padding(.top, 8)

                    SettingsSectionHeaderView(title: "Preferences")

                    BiometricLockToggleView()
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .whiteRectangleBackground()
                        .padding(.top, 8)

                    Group {
                        SettingsSectionHeaderView(title: "Your data")

                        NavigationLink(
                            destination: ImportPasswordsView(),
                            isActive: $presentImportPasswordsView) {
                            EmptyView()
                        }

                        VStack {
                            SettingsRowView(title: "Import passwords", action: {
                                presentImportPasswordsView.toggle()
                            })

                            Divider()
                                .padding(.vertical, 4)

                            SettingsRowView(title: "Export passwords", action: {
                                if let url = URL(string: "https://app.tilig.com/export") {
                                    UIApplication.shared.open(url)
                                }
                            })
                        }
                        .whiteRectangleBackground()
                        .padding(.top, 8)
                    }

                    Group {
                        SettingsSectionHeaderView(title: "Apps")

                        SettingsAppsView()
                            .whiteRectangleBackground()
                            .padding(.top, 8)
                    }

                    Group {
                        SettingsSectionHeaderView(title: "Support")

                        VStack {
                            SettingsRowView(
                                title: "FAQ's",
                                subTitle: "Read our frequently asked questions",
                                action: { viewModel.openURL("FAQ's", urlString: "https://www.tilig.com/faq") }
                            )

                            Divider()
                                .padding(.vertical, 4)

                            SettingsRowView(
                                title: "Get in touch",
                                subTitle: "Send us an email",
                                action: openMail
                            )
                        }
                        .whiteRectangleBackground()
                        .padding(.top, 8)
                    }

                    Group {
                        SettingsSectionHeaderView(title: "Legal")

                        VStack(spacing: 16) {
                            SettingsRowView(
                                title: "Privacy Policy",
                                action: {
                                    viewModel.openURL("Privacy Policy",
                                            urlString: "https://www.tilig.com/legal/privacy-policy"
                                    )
                                }
                            )

                            Divider()

                            SettingsRowView(
                                title: "Terms and conditions",
                                action: {
                                    viewModel.openURL("Terms and conditions",
                                            urlString: "https://www.tilig.com/legal/terms"
                                    )
                                }
                            )
                        }
                        .whiteRectangleBackground()
                        .padding(.top, 8)
                    }

                    Group {
                        SettingsSectionHeaderView(title: "Danger zone")

                        SettingsDeleteView(
                            isDeleting: viewModel.isDeleting,
                            deleteAction: { viewModel.deleteProfile() }
                        )

                        Spacer()

                        Text("version " + VersionIdentifier.appVersion)
                            .subTextFont(color: .blue.opacity(0.5), size: 12)
                            .frame(maxWidth: .infinity, alignment: .center)
                            .padding()
                    }
                }
            }
            .responsivePadding(.horizontal, compact: 16, regular: 48)
        }
        .sheet(isPresented: $viewModel.isPresentingWebView) {
            if let url = viewModel.presentedWebViewURL {
                ModalWebView(title: viewModel.presentedWebViewTitle, url: url)
            }
        }
        .background(Color.backgroundGray.edgesIgnoringSafeArea(.vertical))
        .swipeRightToDismiss()
        .navigationBarHidden(true)
        .onAppear {
            TrackingEvent.settingsViewed.send()
        }
    }
}

struct SettingsSectionHeaderView: View {
    var title: String

    var body: some View {
        Text(title)
            .defaultMediumFont(size: 18)
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.top, 24)
    }
}

struct SettingsRowView: View {
    var title: String
    var subTitle: String?
    var buttonImage: Image?
    var action: () -> Void = { }

    var body: some View {
        Button(action: action) {
            HStack {
                VStack(alignment: .leading) {
                    Text(title)
                        .defaultButtonFont(color: .blue, size: 14)

                    if let subTitle = subTitle {
                        Text(subTitle)
                            .subTextFont(color: .blue.opacity(0.5), size: 12)
                    }
                }

                Spacer()

                if let buttonImage {
                    buttonImage
                        .renderingMode(.template)
                        .resizable()
                        .frame(width: 16, height: 16, alignment: .center)
                        .foregroundColor(.borderGray)

                } else {
                    Image(systemName: "chevron.right")
                        .frame(width: 16, height: 16, alignment: .center)
                        .foregroundColor(.borderGray)
                }
            }
        }
    }
}

struct SettingsLogOutRow: View {
    var title: String
    var subTitle: String
    var buttonAction: () -> Void = { }

    var body: some View {
        HStack {
            VStack {
                Text(title)
                    .defaultButtonFont(color: .blue, size: 14)
                    .frame(maxWidth: .infinity, alignment: .leading)

                Text(subTitle)
                    .subTextFont(color: .blue.opacity(0.5), size: 12)
                    .frame(maxWidth: .infinity, alignment: .leading)
            }

            Spacer()

            WhiteButton(
                title: "Log out",
                maxHeight: 35,
                action: buttonAction
            )
        }
        .whiteRectangleBackground()
    }
}
