//
//  AppDelegate.swift
//  Tilig
//
//  Created by Jacob Schatz on 1/22/20.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import UIKit
import Sentry
import Mixpanel
import Firebase
import FirebaseFirestore
import FirebaseCore
import FirebaseAnalytics
import FirebaseAuthUI
import Sodium

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        setupFirebase()
        #if DEBUG
        handleDevelopmentFlags()
        #endif
        FeatureFlagManager.setup()
        SentrySDK.setup()
        clearKeyChainIfReinstalled()
        MixpanelManager.setup()
        return true
    }

    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey: Any] = [:]
    ) -> Bool {
        // Firebase
        // swiftlint:disable force_cast
        let sourceApplication = options[UIApplication.OpenURLOptionsKey.sourceApplication] as! String?
        if FUIAuth.defaultAuthUI()?.handleOpen(url, sourceApplication: sourceApplication) ?? false {
            return true
        }
        return false
    }

    // MARK: UISceneSession Lifecycle

    func application(
        _ application: UIApplication,
        configurationForConnecting connectingSceneSession: UISceneSession,
        options: UIScene.ConnectionOptions
    ) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running,
        // this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: Configure services

    private func setupFirebase() {
        FirebaseApp.configure()
        Analytics.setAnalyticsCollectionEnabled(true)

        #if DEBUG
        if UserDefaults.standard.bool(forKey: "useEmulators") {
            configureFirebaseEmulator()
        }
        #endif
    }

    // MARK: Cache clearing

    private func clearKeyChainIfReinstalled() {
        do {
            try KeyChainClearer.clearIfReinstalled()
        } catch {
            SentrySDK.capture(error: error)
            fatalError("Cache clearing failed, won't continue.")
        }
    }

}

#if DEBUG
extension AppDelegate {
    func handleDevelopmentFlags() {
        if UserDefaults.standard.bool(forKey: "clearAuthCache") {
            clearAuthCache()
        }
        if UserDefaults.standard.bool(forKey: "resetAllUserDefaults") {
            resetUserDefaults()
        }
        if UserDefaults.standard.bool(forKey: "simulateInvalidAuthState") {
            simulateInvalidAuthState()
        }
    }

    func simulateInvalidAuthState() {
        let invalidAuthState = AuthState(
            firebaseUser: CodeableFirebaseUser.random(),
            tiligAuthTokens: TiligAuthTokens.random(),
            keyPair: Box.KeyPair.random(),
            // swiftlint:disable force_try
            legacyKeyPair: try! LegacyKeyPair.generateRSAKeyPair(),
            userProfile: UserProfile.random()
        )
        // swiftlint:disable force_try
        try! AuthState.cache(authState: invalidAuthState)
    }

    func clearAuthCache() {
        Log("Clearing auth cache")
        // swiftlint:disable force_try
        try! AuthState.clearCache()
        try? Auth.auth().signOut()
    }

    func resetUserDefaults() {
        Log("Resetting user defaults")
        UserDefaults.standard.reset()
    }

    func configureFirebaseEmulator() {
        let settings = Firestore.firestore().settings
        let hostname = UserDefaults.standard.string(forKey: "localIP") ?? "localhost"
        settings.host = "\(hostname):8081"
        settings.isPersistenceEnabled = false
        settings.isSSLEnabled = false
        Firestore.firestore().settings = settings

        Log("✅ Configured Firestore emulator (port \(settings.host)")
    }
}
#endif
