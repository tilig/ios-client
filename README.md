# Tilig iOS Client

[![Build Status](https://app.bitrise.io/app/04d72d8bca498dca/status.svg?token=RtVPM-riaqq8UqRY5cIQ5w&branch=master)](https://app.bitrise.io/app/04d72d8bca498dca)

## Release deployment

To be able to build in release mode, you need to do the following:

### GoogleSerice-Info.plist

Make sure you download the `GoogleService-Info.plist` from Firebase production environment and put it at `Tilig/Config/Production/GoogleService-Info.plist`. It can be found in the Firebase project settings.

After that, make sure you do a clean build.

### Other configuration files

There is a `ConfigLoader` in `Tilig/Config` that loads different configuration files depending on the Build Configiration (Release or Build).
