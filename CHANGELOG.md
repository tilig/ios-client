#  Tilig iOS App Changelog

## Release 4.16

- [Getting Started Implementation](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/382)


## Release 4.15

- [Onboarding redesign](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/375)
- [Fix created item not appearing under matching items in autofill extension](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/385)

## Release 4.14

- [Fix search filtering](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/378)
- [Invite non-tilig user implementation](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/379)
- [Sharing Item updates](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/366)
- [Use is_fetched in Brandfetch](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/367)


## Release 4.13.1

- [Fix custom types error](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/383)


## Release 4.13

- [Fix autofill extension scroll issue](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/377)

## Release 4.12

- [Implement Deletions as part of polling](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/363)
- [Import passwords / Apps sections](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/361)
- [Delete Profile layout update](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/362)
- [Keyboard type fixes](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/365)
- [Fix autoFill error: Unknown host](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/368)
- [Fix empty note distortions](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/369)
- [Remove dash for wifi and note items](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/370)
- [Export section on Settings page](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/371)

## Release 4.11

- [Folder sharing feature](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/350)
- [Add event for 3rd Autofill on iOS to Firebase](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/339)


## Release 4.10

- [Implement Credit Card feature](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/321)
- [Implement PNG/SVG options for brand images](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/319)
- [RSA to Sodium keys migration](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/342)
- [Fix last view item not being updated after refresh](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/336)
- [Fix select Microsoft account](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/329)
- [Replace loader structs with API Client](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/341)
- [Add Welcome Completed Mixpanel Event](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/337)
- [Implement missing Mixpanel events](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/340)
- [Fix (Can't autofill this: unknown type) bug](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/380)


## Release 4.9

- [Implement new design for AutoFill Extension](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/308)
- [Fix signup error](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/332)
- [Simplify Item model - Changes to Legacy encryption flag](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/334)

## Release 4.8

- [Implement Wifi feature](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/310)
- [Offline access by caching the item store](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/294)

## Release 4.7

- [Implement Biometric lock feature](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/236) + [Biometric error fixes](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/324)
- [Implement Pull to refresh](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/320)
- [Implement Confirmation alert for Sign out](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/307)
- [Firebase Conversion](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/245)
- [Display Enable 2FA button when totp is false](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/295)

## Release 4.6

- [Implement Sign in with Microsoft](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/292)
- [Implement Feature flags](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/311)
- [Libsodium keypairs](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/309)

## Release 4.5

- [Implement Secure Notes](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/290)
- [Fix empty list after switching accounts](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/289)
- [Fix keyboard type for website field](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/288)
- [Remove internal error dialog for canceling Apple authorization](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/281)

## Release 4.3

- [Make brand name optional](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/280)
- [Enable autofill flow fixes and improvements](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/279)
- [2FA Bug Fixes](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/233)
- [Refactor the factory](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/276)
- [Fix z-index of item title when collapsed](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/274)
- [Remove AlamoFireImage](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/272)
- [Rename Account to Item (and Login)](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/273)
- [Make password history work with V2 encryption](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/261)

## Release 4.2

- [Fix error thrown when entering name instead of url in Add Account flow](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/269)
- [Fix PageView Swipe bug](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/268)
- [Delete Tilig account redesign](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/243)
- [Make sign in buttons tappable (UI effect)](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/241)
- [Refactor Button components](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/247)
- [Implement tests for Account list view model](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/267)
- [Make Password History work with V2 Encryption](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/261)


## Release 4.1

- [Fix encryption payloads with a blocksize of 255](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/264)
- [Migrate legacy items](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/256)
- [Fix page indicator color in dark mode](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/263)
- [Fix white area at bottom of screen](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/259)
- [Account list design improvements](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/235)
- [Fix tests for the RouteModel](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/262)
- [Fix a bug related to discarding changes](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/257)
- [Related domains AutoFill in QuickType bar](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/228)
- [Notes field improvements](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/225)
- [Implement new Mixpanel events](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/249)

## Release 4.0

This is a big release: we implemented hybrid encryption. This means we encrypt and decrypt items using the new encryption in the background.

- [Hybrid encryption](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/218)
- [Improved polling](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/252)
- [Fixed a bug in dark mode in the extension](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/242)

Other changes:
- We started using async/await 
- The Bitrise CI pipeline works again

## Release 3.9

### Notes

- [Fix extension domain matching when there are no related domains](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/239)
- [Autocomplete brandfetch update](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/227)
- [New password generator](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/223)
- [Fix for AutoFill Extension UI Bugs on iPad](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/224)
- [Implement SVG support for images](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/226)
- [2FA views refactoring](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/234)

## Hotfix Release 3.8.1

### Notes

- [Added fix for password not updated after updating an account](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/229)


## Release 3.8

### Notes

- [Added swipe to navigate back behaviour to account details, and edit screens](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/205)
- [Added viewing password history and restoring password functionality](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/202)
- [Added new navigation flow (Removed tabs)](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/186)
- [Added autofilling related domains fields with accounts on tilig](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/212)
- [Fixed bugs with Editing accounts](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/209)
- [Added new screen for when offline](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/213)
- [Made Notes field multiline in Editing Account](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/209)
- [Fixed iPad bug where Account name could not be edited](https://gitlab.com/subshq/clients/tilig-ios/-/merge_requests/215)
