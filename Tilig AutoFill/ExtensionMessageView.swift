//
//  ExtensionLoadingView.swift
//  Tilig Autofill
//
//  Created by Fitzgerald Afful on 01/03/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI
import AuthenticationServices

struct ExtensionMessageView: View {
    var text: Text?
    var message: String?
    var userCancelled: () -> Void

    var body: some View {
        VStack(alignment: .center, spacing: 0) {
            Image("extension_empty")
                .resizable()
                .scaledToFit()
                .frame(maxHeight: 120)
                .padding(.vertical, 20)

            if let text = text {
                text
                    .multilineTextAlignment(.center)
                    .lineLimit(4)
                    .minimumScaleFactor(0.2)
            }

            if let message = message {
                Text(message)
                    .subTextFont(color: .blue, size: 16)
                    .multilineTextAlignment(.center)
            }

            Spacer()
        }
        .toolbar {
            ToolbarItem(placement: .navigationBarLeading) {
                CloseToolbarButton(onClose: userCancelled)
            }
        }
    }
}
