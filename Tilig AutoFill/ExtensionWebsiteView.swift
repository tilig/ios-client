//
//  ExtensionWebsiteView.swift
//  Tilig Autofill
//
//  Created by Fitzgerald Afful on 24/01/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct ExtensionWebsiteView: View {
    var label: String
    var text: String
    var iconURL: URL?
    var backgroundColor: Color = .extensionLightPurple
    var borderColor: Color = .extensionBorderPurple
    private var fieldHeight: Double { 56.0 }

    var body: some View {
        VStack {
            Text(label)
                .defaultButtonFont(size: 12)
                .frame(maxWidth: .infinity, alignment: .leading)

            HStack {
                Text(text)
                    .defaultButtonFont(size: 16)
                    .frame(maxWidth: .infinity, alignment: .leading)

                ListIcon(
                    iconURL: iconURL,
                    background: false,
                    backgroundColor: .clear,
                    hasPadding: false,
                    cornerRadius: 0
                )
                .frame(width: 24, height: 24)
            }
            .padding(.horizontal, 16)
            .frame(height: fieldHeight)
            .background(RoundedRectangle(cornerRadius: 8).fill(backgroundColor))
            .overlay(
                RoundedRectangle(cornerRadius: 8)
                    .stroke(borderColor, lineWidth: 1)
            )
        }
    }
}
