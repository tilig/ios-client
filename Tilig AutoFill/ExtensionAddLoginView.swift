//
//  ExtensionAddLoginView.swift
//  Tilig Autofill
//
//  Created by Fitzgerald Afful on 14/12/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct ExtensionAddLoginView: View {
    @EnvironmentObject var viewModel: QuickCreateViewModel
    @EnvironmentObject var extensionViewModel: ExtensionViewModel

    func save() {
        Task { await viewModel.save() }
    }

    var body: some View {
        ScrollView {
            VStack(spacing: 0) {
                Text("Login name")
                    .defaultButtonFont(color: .blue.opacity(0.75), size: 12)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.bottom, 8)

                HStack {
                    ListIcon(
                        iconURL: viewModel.brand?.iconURL,
                        background: false,
                        backgroundColor: .white,
                        hasPadding: false,
                        cornerRadius: 8
                    )
                    .frame(width: 32, height: 32)

                    AddItemNameField(placeholder: "Enter login name", value: $viewModel.name)
                }
                .padding(.bottom)

                Group {
                    Text("Website")
                        .defaultButtonFont(color: .blue.opacity(0.75), size: 12)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.bottom, 8)

                    WebsiteField("Add website", value: $viewModel.url)
                        .padding(.bottom)
                }

                Group {
                    Text("Email or Username")
                        .defaultButtonFont(color: .blue.opacity(0.75), size: 12)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.bottom, 8)

                    FormTextField(
                        "Enter an email or username",
                        value: $viewModel.plainTextUsername,
                        keyboardType: .emailAddress,
                        textContentType: .emailAddress
                    )
                    .padding(.bottom)
                }

                Group {
                    Text("Password")
                        .defaultButtonFont(color: .blue.opacity(0.75), size: 12)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.bottom, 8)

                    GeneratePasswordField(password: $viewModel.plainTextPassword)
                        .padding(.bottom)
                }

                DefaultButton(title: "Store and Autofill", action: save)
                    .disabled(viewModel.saveButtonDisabled)
                    .frame(height: 50)

                Spacer()
            }
            .padding([.horizontal, .top])
        }
        .onReceive(viewModel.itemCreated) { item in
            Task { await extensionViewModel.updateCache() }
            extensionViewModel.completeRequest(withItem: item)
        }
        .activating(viewModel)
        .navigationBarBranded(withTitle: "Add a new Login")
    }
}
