//
//  ExtensionItemCell.swift
//  Tilig Autofill
//
//  Created by Fitzgerald Afful on 02/03/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct ExtensionItemCell: View {
    @EnvironmentObject var authState: AuthStateWrapper

    var item: Item

    var overview: DecryptedOverview? {
        do {
            let crypto = Crypto(
                keyPair: authState.keyPair,
                legacyKeyPair: authState.legacyKeyPair
            )
            return try crypto.decryptOverview(encryptedItem: item)
        } catch {
            Log("Error decrypting: \(error)")
            error.raiseGlobalAlert()
        }
        return nil
    }

    var body: some View {
        HStack {
            ListIcon(
                iconURL: item.iconURL,
                background: false,
                backgroundColor: .clear,
                hasPadding: false,
                cornerRadius: 0
            )
            .frame(width: 32, height: 32, alignment: .center)
            .padding(.trailing)

            VStack(alignment: .leading, spacing: 0) {
                Text(overview?.name ?? "-")
                    .defaultButtonFont(color: .blue, size: 16)
                    .lineLimit(1)
                    .padding(.bottom, 4)

                Text(overview?.info ?? "-")
                    .subTextFont(color: .blue.opacity(0.75), size: 14)
                    .lineLimit(1)
            }

            Spacer()

        }
        .padding()
        .background(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color.brightGray, lineWidth: 1.0)
                .shadow(color: .black.opacity(0.1), radius: 5, x: 0, y: 1)
        )
        .padding(.horizontal)
    }
}
