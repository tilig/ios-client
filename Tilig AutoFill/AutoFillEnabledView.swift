//
//  AutoFillEnabledView.swift
//  Tilig Autofill
//
//  Created by Gertjan Jansen on 12/01/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import SwiftUI
import os.log
import AuthenticationServices

struct AutoFillEnabledView: View {
    var extensionContext: ASCredentialProviderExtensionContext

    func buttonTapped() {
        // TODO
        // let url = URL(string: "tilig://autofill-enabled")!

        // Sources:
        // https://stackoverflow.com/questions/46240431/accessing-shared-variable-of-uiapplication-from-inside-share-extension-in?answertab=active#tab-top
        // https://stackoverflow.com/questions/32609776/uiapplication-sharedapplication-is-unavailable
        //
        // This was hard to figure out. Apple has an openUrl method on extensionContext, but
        // that does not work in an AutoFill extension. Also, UIApplication.shared is not available.
        // However, by changing the 'Require Only App-Extension-Safe API' build setting to NO, this is possible:

        // let application = UIApplication.value(forKeyPath: #keyPath(UIApplication.shared)) as! UIApplication
        // application.open(url)

        // Finally, complete the extension request
        self.extensionContext.completeExtensionConfigurationRequest()
    }

    var body: some View {
        VStack(alignment: .center, spacing: 20) {

            Spacer()

            Image(systemName: "checkmark.circle.fill")
                .responsiveHeight(compact: 60, regular: 60)
                .foregroundColor(.green)

            Text("Autofill is enabled")
                .largeBoldFont(color: .blue, size: 24)

            Text("Tilig will now help you fill forms while browsing")
                .defaultButtonFont(color: .subTextLightPurple, size: 16)
                .multilineTextAlignment(.center)

            Spacer()

            Text(" Go back to the Tilig app to continue.")
                .defaultButtonFont(color: .subTextLightPurple, size: 16)

            DefaultButton(title: "Done", textSize: 16, action: buttonTapped)
        }
        .padding(.horizontal, 20)
        .padding(.vertical, 40)
    }
}
