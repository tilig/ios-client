//
//  MockedExtensionModels.swift
//  Tilig Autofill
//
//  Created by Gertjan Jansen on 22/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import AuthenticationServices

class CredentialHandlerMock: CredentialHandlerProtocol {
    @Published var requestCompleted: Bool = false
    @Published var requestCancelled: Bool = false
    @Published var completedWithItem: Item?
    @Published var cancelledWithError: ASExtensionError.Code?

    func completeRequest(withItem item: Item) {
        requestCompleted = true
        completedWithItem = item
    }

    func cancelRequest(withError extensionError: ASExtensionError.Code) {
        requestCancelled = true
        cancelledWithError = extensionError
    }
}
