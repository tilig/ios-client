//
//  ExtensionViewModel.swift
//  Tilig Autofill
//
//  Created by Gertjan Jansen on 20/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import AuthenticationServices
import UIKit
import SwiftUI
import Combine
import DomainParser
import Sentry

@MainActor
class ExtensionViewModel: ObservableObject, MainActorActivatable {
    var domain: String?
    @Published var error: Error?
    @Published var loading = true
    @Published var matchingItems: [Item] = []
    @Published private(set) var domainBrand: Brand?

    private var relatedDomains: RelatedDomains = []
    private let credentialHandler: CredentialHandlerProtocol
    private let itemsCache = ItemsCache()
    private let client: Client

    init(serviceIdentifiers: [ASCredentialServiceIdentifier],
         credentialHandler: CredentialHandlerProtocol,
         tokenProvider: TokenProvider) throws {
        self.credentialHandler = credentialHandler

        self.domain = try DomainParser().getDomain(forServiceIdentifiers: serviceIdentifiers)
        if let domain {
            self.relatedDomains = RelatedDomainsHelper().findRelatedDomains(domain)
        }
        self.client = Client(tokenProvider: tokenProvider)
    }

    func activate() async {
        // We want to trigger autofill extension opened as soon as possible
        TrackingEvent.autoFillExtensionOpened.send()

        await findMatchingItems()
    }

    // Complete the extension request by returning the given item
    func completeRequest(withItem item: Item) {
        credentialHandler.completeRequest(withItem: item)
    }

    func cancelRequest(withError error: ASExtensionError.Code) {
        credentialHandler.cancelRequest(withError: error)
    }

    // We're doing a server request here. That might be a bit slow, but the benefit
    // of doing this is that we don't have to worry about having up-to-date data
    // on device. If we would do that, we'd have to a mechanism to always keep the item
    // list data up to date with either background fetching or push messages.
    func findMatchingItems() async {
        do {
            Log("🚪 Filter items list by domain: \(domain)")
            // First show the matching items based on the cache
            try filterMatchingItemsFromCache()
            // Hide the loading indicator
            withAnimation {
                self.loading = false
            }

            // Then do a query to update the cache
            // try await updateCache()
            // Then update the matching items, because there might be new items
            // try await filterMatchingItemsFromCache()

            if matchingItems.isEmpty {
                TrackingEvent.autoFillNoSuggestions.send()
            } else {
                TrackingEvent.autoFillSuggested.send()
            }
        } catch {
            handleError(error)
        }
    }

    func filterMatchingItemsFromCache() throws {
        SentrySDK.addSimpleBreadcrumb("Filtering items from cache")

        // Fail silently because it can fail if the data model has changed.
        if let cachedItems = try? itemsCache.getCachedItems() {
            matchingItems = filterMatching(items: cachedItems)
        }
    }

    /// This does a full query to make sure we have the latest changes.
    /// This can be optimized more by only fetching changes since timestamp X.
    func updateCache() async {
        do {
            SentrySDK.addSimpleBreadcrumb("Quering items & updating cache")
            let items = try await client.getItems()
            try itemsCache.cacheAll(items: items)
        } catch {
            handleError(error)
        }
    }

    /// Filter the given items on domain
    func filterMatching(items: [Item]) -> [Item] {
        items.filter { isMatching(encryptedItem: $0) }
    }

    /// Does the given Item match the current domain?
    func isMatching(encryptedItem: Item) -> Bool {
        guard let itemDomain = encryptedItem.domain else { return false }
        return isMatching(itemDomain: itemDomain)
    }

    /// Does the given domain match the current domain?
    func isMatching(itemDomain: String) -> Bool {
        itemDomain == domain || relatedDomains.contains(itemDomain)
    }

    // Fetch brand for domain
    func fetchDomainBrand() async {
        if let domain,
           let url = URL(string: domain),
           let brandURL = try? await client.getBrand(for: url) {
            self.domainBrand = brandURL
        } else {
            self.domainBrand = nil
        }
    }

    // Note this one can be called from outside the viewmodel with the current setup
    // we need a dedicated error handler for the autofill extension in a next refactoring round
    func handleError(_ error: Error) {
        self.error = error
        SentrySDK.capture(error: error)
    }
}
