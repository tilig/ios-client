//
//  QuickCreateViewModel.swift
//  Tilig Autofill
//
//  Created by Gertjan Jansen on 20/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Combine
import Foundation
import Sentry
import Sodium

enum ValidationError: Error {
    case nameIsRequired
    case invalidWebsiteFormat
    case usernameIsRequired
    case passwordIsRequired
}

extension ValidationError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .nameIsRequired:
            return "Please fill in a name for this login"
        case .invalidWebsiteFormat:
            return "Please enter a valid website address"
        case .usernameIsRequired:
            return "Please fill in a username for this login"
        case .passwordIsRequired:
            return "Please fill in a password for this login"
        }
    }
}

@MainActor class QuickCreateViewModel: ObservableObject, Activatable {
    @Published var plainTextUsername: String?
    @Published var plainTextPassword: String?
    @Published var name: String?
    @Published var brand: Brand?
    @Published var url: URL? {
        didSet {
            if url != oldValue && self.isActivated {
                updateBrand()
            }
        }
    }
    private var isActivated = false

    var crypto: Crypto
    var domain: String?
    var client: Client
    private var debounceHelper = DebounceHelper()

    init(domain: String?,
         keyPair: Box.KeyPair,
         legacyKeyPair: LegacyKeyPair,
         tokenProvider: TokenProvider) {
        self.domain = domain
        self.crypto = Crypto(keyPair: keyPair, legacyKeyPair: legacyKeyPair)
        self.client = Client(tokenProvider: tokenProvider)
        if let domain {
            self.url = URL(string: domain)
        }
        self.name = domain
    }

    var usernameIsEmail: Bool { plainTextUsername?.isValidEmail ?? false }

    var saveButtonDisabled: Bool { plainTextPassword == nil || plainTextUsername == nil }

    var itemCreated: AnyPublisher<Item, Never> {
        itemCreatedSubject.eraseToAnyPublisher()
    }
    private var itemCreatedSubject = PassthroughSubject<Item, Never>()

    func activate() {
        // TODO: not for Apple private relay
        // Set the user's default email address
        self.plainTextUsername = AuthState.cached?.userProfile.email
        updateBrand()
        isActivated = true
    }

    func save() async {
        guard let plainTextUsername = plainTextUsername, !plainTextUsername.isEmpty else {
            ValidationError.usernameIsRequired.raiseGlobalAlert()
            return
        }
        guard let plainTextPassword = plainTextPassword, !plainTextPassword.isEmpty else {
            ValidationError.passwordIsRequired.raiseGlobalAlert()
            return
        }

        var itemWrapper = ItemWrapper(template: .loginV1)
        itemWrapper.name = name
        itemWrapper.password = plainTextPassword
        itemWrapper.username = plainTextUsername
        itemWrapper.url = url

        do {
            let encryptedItem = try crypto.encrypt(itemWrapper: itemWrapper)

            assert(encryptedItem.encryptedDetails != nil)
            assert(encryptedItem.encryptedOverview != nil)
            assert(encryptedItem.encryptionVersion == 2)

            let savedItem = try await client.saveItem(encryptedItem)

            Log("Saved Item: \(savedItem)")

            assert(savedItem.encryptionVersion == 2)
            assert(savedItem.encryptedDetails != nil)
            assert(savedItem.encryptedOverview != nil)

            itemCreatedSubject.send(savedItem)
        } catch {
            SentrySDK.capture(error: error)
            error.raiseGlobalAlert()
        }
    }

    private func generateRandomPassword() {
        self.plainTextPassword = PasswordGenerator.randomPassword()
        TrackingEvent.passwordGenerated.send()
    }

    func updateBrand() {
        debounceHelper.debounce(duration: 1.0) {
            var brand: Brand?
            if let url = self.url {
                // Silently fail, but set the brand to `nil`. Don't alert the user.
                brand = try? await self.client.getBrand(for: url)
            }
            // If the task was cancelled during the query, don't do anything with the response.
            if Task.isCancelled { return }
            self.brand = brand
        }
    }

    func fetchBrand() async {
        guard let url = url else {
            return self.brand = nil
        }
        do {
            self.brand = try await client.getBrand(for: url)
        } catch URLError.resourceUnavailable {
            self.brand = nil
        } catch {
            error.raiseGlobalAlert()
            self.brand = nil
        }
    }
}
