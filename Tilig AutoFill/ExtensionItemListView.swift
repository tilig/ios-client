//
//  ExtensionItemListView.swift
//  Tilig
//
//  Created by Gertjan Jansen on 01/06/2021.
//  Copyright © 2021 SubsLLC. All rights reserved.
//

import SwiftUI
import AuthenticationServices
import DomainParser

struct ExtensionItemListView: View {
    @EnvironmentObject var extensionViewModel: ExtensionViewModel
    @EnvironmentObject var listViewModel: ItemListViewModel
    @EnvironmentObject var authState: AuthStateWrapper

    @State private var viewDidLoad = false
    @State private var isSearchBarFocused = false
    @State private var hideMatches = false

    var body: some View {
        ZStack {
            ScrollView {
                if !hideMatches && extensionViewModel.domain != nil {
                    MatchingItemsListView()
                }

                LazyVStack(pinnedViews: [.sectionHeaders]) {
                    Section {
                        switch listViewModel.loadingState {
                        case .fetchingInitial, .refreshing:
                            Spacer()
                            ActivityIndicator()
                            Spacer()
                        case .idle, .cacheLoaded:
                            if listViewModel.items.isEmpty {
                                ExtensionEmptyItemsView()

                            } else if listViewModel.filteredItems.isEmpty {
                                EmptySearchResultsView()

                            } else {
                                FilteredItemsListView(
                                    isSearching: isSearchBarFocused
                                )
                                .padding(.top, -8)
                            }
                        }
                    } header: {
                        if !listViewModel.itemsForActiveItemType.isEmpty {
                            ItemSearchInput(
                                label: "Search for a login",
                                searchText: $listViewModel.searchText,
                                onFocusChanged: { isFocused in
                                    if isFocused {
                                        TrackingEvent.itemSearched.send(withProperties: ["Source": "Inside App"])
                                    }

                                    Task {
                                        withAnimation {
                                            hideMatches = isFocused
                                        }
                                        try? await Task.sleep(nanoseconds: UInt64(0.5 * .nanosecondsPerSecond))
                                        withAnimation {
                                            isSearchBarFocused = isFocused
                                        }
                                    }
                                }
                            )
                            .padding()
                            .background(.white)
                            .overlay(Divider(), alignment: .bottom)
                        }
                    }
                }
            }

            NavigationLink(destination: ExtensionAddLoginView()) {
                Image(systemName: "plus.circle.fill")
                    .foregroundColor(.white)
                    .padding()
                    .roundedRectangleBackground(fillColor: .mediumPurple)
                    .frame(width: 32, height: 32)
            }
            .pushToBottomRight()
            .padding(24)
        }
        .ignoresSafeArea(.keyboard)
        .onReceive(listViewModel.$lastError.compactMap { $0 }, perform: { error in
            extensionViewModel.handleError(error)
        })
        .onAppear {
            if !viewDidLoad {
                listViewModel.setActiveItemType(itemType: .login)
                listViewModel.activate()
                viewDidLoad = true
            }
        }
        .navigationBarBranded(withTitle: "Logins")
        .toolbar {
            ToolbarItem(placement: .navigationBarLeading) {
                CloseToolbarButton {
                    extensionViewModel.cancelRequest(withError: .userCanceled)
                }
            }
        }
    }
}
