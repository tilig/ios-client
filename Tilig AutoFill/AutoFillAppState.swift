//
//  ApplicationState.swift
//  Tilig Autofill
//
//  Created by Jacob Schatz on 3/11/20.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import Foundation
import Valet
import AuthenticationServices
import os.log

class AutoFillAppState {
    static let shared = AutoFillAppState()

    var baseService: BaseService { BaseService.shared }

    private init() {
        setBaseServiceToken()
    }

    var authValet: Valet { .auth }

    /// Pull the token from the main app through Valet
    /// and set it on this BaseService's object
    func setBaseServiceToken() {
        // The extension fetches the token that's set in the main app through Valet
        guard let token = try? authValet.string(forKey: ValetKeys.tiligToken) else {
            OSLog.autoFillDebug("AutoFillAppState] No Tilig token found (user signed out)")
            baseService.setToken(nil)
            return
        }

        OSLog.autoFillDebug("[AutoFillAppState] Tilig token found: \(token)")
        baseService.setToken(token)
    }

    var isSignedIn: Bool {
        baseService.accessToken != nil
    }
}
