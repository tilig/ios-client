//
//  RootErrorView.swift
//  Tilig Autofill
//
//  Created by Gertjan Jansen on 02/06/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

/// Use this error view when the ExtensionViewModel coult not be initialized
struct RootErrorView: View {
    var error: Error
    var userCancelled: () -> Void

    var body: some View {
        NavigationView {
            VStack {
                ShutdownView()
                ExtensionMessageView(
                    text: Text("An error occured"),
                    message: error.localizedDescription,
                    userCancelled: userCancelled
                )
                .navigationBarBranded(withTitle: "Oops...")
            }
        }
    }
}

struct RootErrorView_Previews: PreviewProvider {
    static var previews: some View {
        RootErrorView(error: AuthError.unauthorized) {
            Log("Close")
        }
    }
}
