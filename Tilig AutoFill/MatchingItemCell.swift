//
//  MatchingItemCell.swift
//  Tilig Autofill
//
//  Created by Fitzgerald Afful on 30/11/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct MatchingItemCell: View {
    @EnvironmentObject var authState: AuthStateWrapper

    var item: Item
    var action: () -> Void = { }

    var overview: DecryptedOverview? {
        do {
            let crypto = Crypto(keyPair: authState.keyPair, legacyKeyPair: authState.legacyKeyPair)
            return try crypto.decryptOverview(encryptedItem: item)
        } catch {
            Log("Error decrypting: \(error)")
            error.raiseGlobalAlert()
        }
        return nil
    }

    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 0) {

                Text(overview?.name ?? "-")
                    .defaultButtonFont(color: .blue, size: 14)
                    .lineLimit(1)

                Spacer()
                    .frame(height: 2)

                Text(overview?.info ?? "-")
                    .subTextFont(color: .blue.opacity(0.5), size: 12)
                    .lineLimit(1)
            }

            Spacer()

            Button(action: action) {
                HStack {
                    Image("autofill_wand")
                        .resizable()
                        .foregroundColor(.white)
                        .frame(width: 15, height: 15)

                    Text("Autofill")
                        .font(.stolzlRegular(size: 12))
                        .foregroundColor(.mediumPurple)
                }
                .padding(8)
                .frame(alignment: .center)
            }
            .overlay(
                RoundedRectangle(cornerRadius: 6)
                    .stroke(Color.mediumPurple, lineWidth: 1)
            )
            .background(
                RoundedRectangle(cornerRadius: 6)
                    .fill(Color.white)
            )

        }
        .padding(.vertical, 10)
        .padding(.horizontal, 16)
    }
}
