//
//  SignedOutView.swift
//  Tilig Autofill
//
//  Created by Gertjan Jansen on 22/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct ExtensionSignedOutView: View {
    var userCancelled: () -> Void

    var body: some View {
        NavigationView {
            VStack {
                ShutdownView()
                ExtensionMessageView(
                    message: "You are not signed in. \nPlease open the Tilig app and sign in.",
                    userCancelled: userCancelled
                )
                .navigationBarBranded(withTitle: "Signed out")
                .navigationViewStyle(StackNavigationViewStyle())
            }
        }
        .preferredColorScheme(.light)
        .colorScheme(.light)
    }
}

#if DEBUG
struct SignedOutView_Previews: PreviewProvider {
    static var previews: some View {
        ExtensionSignedOutView {
            Log("Cancelled")
        }
    }
}
#endif
