//
//  DomainParser+ServiceIdentifier.swift
//  Tilig Autofill
//
//  Created by Gertjan Jansen on 21/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import DomainParser
import AuthenticationServices
import Sentry

/// An extension of DomainParser to map ASCredentialServiceIdentifier to a domain
extension DomainParser {
    func getDomain(forServiceIdentifiers serviceIdentifiers: [ASCredentialServiceIdentifier]) throws -> String? {
        guard let serviceIdentifier = serviceIdentifiers.first else { return nil }

        // We have 2 types of service identifiers:
        // 1) URL
        // 2) Domain
        // It looks like Safari always passes a URL. Haven't come across the domain type.
        switch serviceIdentifier.type {
        // This is the most likely case
        case .URL:
            guard let host = URL(string: serviceIdentifier.identifier)?.host else {
                return nil
            }
            // Websites that have the same eTLD+1 are considered "same-site". Websites that have a
            // different eTLD+1 are "cross-site". The `domain` field on the Account mode is the eTLD.
            // So what we need to do is figure out the eTLD of the given URL.
            // Parse the domain, taking into account the Public Suffix list
            guard let domain = self.parse(host: host)?.domain else {
                return nil
            }
            return domain
        // We haven't experienced this case yet
        case .domain:
            return serviceIdentifier.identifier
        @unknown default:
            return nil
        }
    }
}
