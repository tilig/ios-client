//
//  FilteredItemsListView.swift
//  Tilig Autofill
//
//  Created by Fitzgerald Afful on 14/12/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct FilteredItemsListView: View {
    @EnvironmentObject var extensionViewModel: ExtensionViewModel
    @EnvironmentObject var listViewModel: ItemListViewModel
    @EnvironmentObject var authStateWrapper: AuthStateWrapper
    var isSearching: Bool = false
    @State var presentAddLogin = false
    @State var selectedItemId: String?

    var items: [ListItem] {
        if isSearching {
            return listViewModel.filteredItems
        }
        return listViewModel.itemsForActiveItemType.filter { item -> Bool in
            guard let domain = item.encryptedItem.domain else { return true }
            return !extensionViewModel.isMatching(itemDomain: domain)
        }
    }

    var body: some View {
        LazyVStack(alignment: .leading) {
            ForEach(items) { listItem in
                let encryptedItem = listItem.encryptedItem

                if extensionViewModel.isMatching(encryptedItem: encryptedItem) {
                    Button(action: {
                        extensionViewModel.completeRequest(withItem: encryptedItem)
                    }) {
                        ItemListCell(item: listItem)
                    }
                } else {
                    NavigationLink(
                        destination: ExtensionConfirmationView(item: encryptedItem)
                    ) {
                        ItemListCell(item: listItem)
                    }
                }
            }

            ExtensionAddLoginButton(isVisible: isSearching)
                .padding(.bottom, 64)
        }
        .background(.white)
    }
}
