//
//  ExtensionEmptyItemsView.swift
//  Tilig Autofill
//
//  Created by Fitzgerald Afful on 14/12/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct ExtensionEmptyItemsView: View {

    var body: some View {
        VStack(alignment: .center) {
            Image("squirrel_transparent")
                .resizable()
                .scaledToFit()
                .frame(height: 120)
                .padding(.top, 32)

            Text("""
                Start by adding your first account below. \
                The more accounts you add the more useful Tilig becomes
                """
            )
            .defaultButtonFont(color: .blue.opacity(0.75), size: 16)
            .multilineTextAlignment(.center)
            .fixedSize(horizontal: false, vertical: true)
            .lineSpacing(4)
            .padding(.top, 32)

            Spacer()
        }
        .padding(.horizontal, 32)

    }
}
