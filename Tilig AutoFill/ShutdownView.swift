//
//  ShutdownView.swift
//  Tilig Autofill
//
//  Created by Gertjan Jansen on 12/04/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

//
//  ShutdownView.swift
//  Tilig
//
//  Created by Gertjan Jansen on 12/04/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

struct ShutdownView: View {
    var body: some View {
        VStack(alignment: .center) {
            Text("⚠️ URGENT")
                .bold()
                .foregroundColor(.red)
            Text("Tilig shuts down on April 30th, 2023.")
                .bold()
                .foregroundColor(.red)

            Text("Move your passwords to a different password manager as soon as possible.")
                .padding(.top, 10)
                .multilineTextAlignment(.center)

            Text("Read more on tilig.com")
                .bold()
                .padding(.top, 10)
        }
        .padding()
        .background(Color.lightGray)
    }

}
