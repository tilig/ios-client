//
//  ExtensionHomeView.swift
//  Tilig Autofill
//
//  Created by Fitzgerald Afful on 18/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI
import AuthenticationServices

struct ExtensionRootView: View {
    @EnvironmentObject var viewModel: ExtensionViewModel
    @EnvironmentObject var biometricViewModel: BiometricViewModel

    var body: some View {
        NavigationView {
            VStack {
                ShutdownView()
                if biometricViewModel.isLocked {
                    BiometricLockView(displayLogo: false)
                        .navigationBarBranded(withTitle: "Unlock Tilig")
                } else if viewModel.error != nil {
                    ExtensionErrorView()
                } else if viewModel.loading {
                    ExtensionLoadingView()
                } else {
                    ExtensionItemListView()
                }
            }
        }
        .activating(viewModel)
        .navigationViewStyle(.stack)
        .globalErrorHandler()
        .preferredColorScheme(.light)
        .colorScheme(.light)
        .onAppear {
            AppearanceTweaks().installForExtension()
        }
    }
}
