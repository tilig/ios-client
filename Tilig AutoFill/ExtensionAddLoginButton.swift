//
//  ExtensionAddLoginButton.swift
//  Tilig Autofill
//
//  Created by Fitzgerald Afful on 20/01/2023.
//  Copyright © 2023 SubsLLC. All rights reserved.
//

import SwiftUI

struct ExtensionAddLoginButton: View {
    @EnvironmentObject var viewModel: ExtensionViewModel

    // This flag was added to make sure that the NavigationLink is kept alive while
    // the view itself is being hidden.
    var isVisible: Bool = true

    var body: some View {
        NavigationLink(destination: ExtensionAddLoginView()) {
            if isVisible {
                HStack {
                    Image(systemName: "plus.circle.fill")
                        .foregroundColor(.white)
                        .padding(8)
                        .roundedRectangleBackground(
                            fillColor: .mediumPurple,
                            shadowColor: .clear,
                            shadowRadius: 0
                        )
                        .frame(width: 24, height: 24)
                        .padding(.horizontal, 8)

                    VStack(alignment: .leading, spacing: 0) {
                        Text("ADD A NEW LOGIN FOR")
                            .tracking(1)
                            .defaultButtonFont(color: .blue.opacity(0.5), size: 10)

                        Spacer()
                            .frame(height: 2)

                        Text("\(viewModel.domain ?? "this website or app")")
                            .defaultButtonFont(color: .blue, size: 14)
                    }

                    Spacer()
                }
                .padding(.horizontal)
                .padding(.top, 8)
            } else {
                EmptyView()
            }
        }
    }
}
