//
//  MatchingItemsView.swift
//  Tilig Autofill
//
//  Created by Gertjan Jansen on 19/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI
import AuthenticationServices

struct MatchingItemsListView: View {
    @EnvironmentObject var viewModel: ExtensionViewModel

    var title: Text {
        var text = "No login for "
        if viewModel.matchingItems.count > 0 {
            text = viewModel.matchingItems.count == 1 ? "Your login for " : "Your logins for "
        }
        return Text(text)
            .subTextMediumFont(color: .blue, size: 14) +
        Text("\(viewModel.domain ?? "this website or app")")
            .subTextMediumFont(color: .mediumPurple, size: 14)
    }

    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            title
                .padding([.horizontal, .top])
                .padding(.bottom, 8)

            if viewModel.matchingItems.isEmpty {
                ExtensionAddLoginButton()
            } else {
                ForEach(viewModel.matchingItems) { item in
                    Button(action: { viewModel.completeRequest(withItem: item) }) {
                        MatchingItemCell(item: item) {
                            viewModel.completeRequest(withItem: item)
                        }
                    }
                }
            }
        }
        .padding(.bottom)
        .background(Color.mediumPurple.opacity(0.1))
        .overlay(Divider().background(Color.borderGray), alignment: .bottom)
    }
}

#if DEBUG
// struct MatchingItemsView_Previews: PreviewProvider {
//    static var previews: some View {
//        MatchingItemsView()
//    }
// }
#endif
