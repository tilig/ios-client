//
//  CredentialContextHandler.swift
//  Tilig Autofill
//
//  Created by Gertjan Jansen on 21/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import Combine
import AuthenticationServices
import Sentry
import Sodium

protocol CredentialHandlerProtocol {
    func completeRequest(withItem item: Item)
    func cancelRequest(withError extensionError: ASExtensionError.Code)
}

/// This handler is being used for both handling request completion and cancellation
class CredentialHandler: CredentialHandlerProtocol {
    private var extensionContext: ASCredentialProviderExtensionContext
    private var crypto: Crypto?

    init(extensionContext: ASCredentialProviderExtensionContext,
         keyPair: Box.KeyPair?,
         legacyKeyPair: LegacyKeyPair?,
         onError: ((Error) -> Void)? = nil) {
        self.extensionContext = extensionContext

        if let keyPair, let legacyKeyPair {
            self.crypto = Crypto(keyPair: keyPair, legacyKeyPair: legacyKeyPair)
        }

        if let onError {
            self.onError = onError
        }
    }

    private(set) var onError: (Error) -> Void = { error in
        SentrySDK.addSimpleBreadcrumb("mapItemToCredentials failed")
        SentrySDK.capture(error: error)
    }

    func completeRequest(withItem encryptedItem: Item) {
        do {
            completeRequest(withCredential: try mapItemToCredentials(encryptedItem))
        } catch {
            onError(error)
            cancelRequest(withError: .failed)
        }
    }

    /// Complete the extension request by returning the given item (credential)
    private func completeRequest(withCredential credential: ASPasswordCredential) {
        flagFirstAutoFill()

        AutoFillTrackingState.incrementAutoFillCount()

        TrackingEvent.autoFillSelected.send()

        self.extensionContext.completeRequest(
            withSelectedCredential: credential
        )
    }

    /// If this is the first autofill, set a flag that the first autofill is done,
    /// so that the main app can trigger a conversion event with FirebaseAnalytics.
    func flagFirstAutoFill() {
        if AutoFillTrackingState.firstAutoFillState == .notCompleted {
            AutoFillTrackingState.firstAutoFillState = .completedWithoutTracking
        }
    }

    /// Cancel the extension request
    func cancelRequest(withError extensionError: ASExtensionError.Code) {
        TrackingEvent.autoFillExtensionClosed.send()

        let error = NSError(
            domain: ASExtensionErrorDomain,
            code: extensionError.rawValue
        )
        extensionContext.cancelRequest(withError: error)
    }

    internal func mapItemToCredentials(_ item: Item) throws -> ASPasswordCredential {
        guard let crypto = crypto else { throw AuthError.unauthorized }

        let itemWrapper = try crypto.decrypt(fullItem: item)

        let username = itemWrapper.username ?? ""
        let password = itemWrapper.password ?? ""

        return ASPasswordCredential(
            user: username,
            password: password
        )
    }
}
