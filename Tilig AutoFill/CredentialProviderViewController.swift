//
//  CredentialProviderViewController.swift
//  Bolted AutoFill
//
//  Created by Jacob Schatz on 1/26/20.
//  Copyright © 2020 SubsLLC. All rights reserved.
//

import Combine
import AuthenticationServices
import SwiftUI
import Sentry
import os.log

struct SearchParams: Encodable {
    var domain: String
}

class CredentialProviderViewController: ASCredentialProviderViewController {
    var subscriptions = Set<AnyCancellable>()

    var authState: AuthState? { AuthState.cached }
    var client: Client? {
        guard let authState else { return nil }
        return Client(tokenProvider: authState)
    }

    var isSignedIn: Bool { authState != nil }
    let urlSession = URLSession.current
    var biometricViewModel = BiometricViewModel()

    required init?(coder: NSCoder) {
        SentrySDK.setup()
        super.init(coder: coder)
    }

    // The object that takes care of completing or cancelling the extension request
    var credentialHandler: CredentialHandlerProtocol {
        return credentialHandlerMock ?? CredentialHandler(
            extensionContext: self.extensionContext,
            keyPair: authState?.keyPair,
            legacyKeyPair: authState?.legacyKeyPair
        )
    }
    // Unfortunately, couldn't find a good way to pass this through the initializer
    var credentialHandlerMock: CredentialHandlerProtocol?

    // MARK: UI

    // Apple tells us to avoid expensive setup in viewDidLoad()
    // Don't do requests here, or the view that appears when enabling autofill will crash.
    override func viewDidLoad() {
        activateMixpanelManager()
        overrideUserInterfaceStyle = .light
        showSignedOutViewIfNecessary()
    }

    private var mixpanelManager: MixpanelManager?

    func activateMixpanelManager() {
        MixpanelManager.setup()

        if let authState = authState {
            MixpanelManager.setMixpanelIdentity(authState: authState)
        }
    }

    /// Pull the auth state from the main app and change state accordingly
    func showSignedOutViewIfNecessary() {
        if !isSignedIn {
            showSignedOutView()
        }
    }

    /// Present the view that shows the user they need to sign in
    func showSignedOutView() {
        SentrySDK.addSimpleBreadcrumb("showSignedOutView")

        showSwiftUIView(
            ExtensionSignedOutView(userCancelled: { [weak self] in
                self?.credentialHandler.cancelRequest(withError: .userCanceled)
            })
        )
    }

    // MARK: Credential identities

    /*
     Prepare your UI to list available credentials for the user to choose from. The items in
     'serviceIdentifiers' describe the service the user is logging in to, so your extension can
     prioritize the most relevant credentials in the list.
    */
    override func prepareCredentialList(for serviceIdentifiers: [ASCredentialServiceIdentifier]) {
        SentrySDK.addSimpleBreadcrumb("prepareCredentialList")

        guard let authState = authState else {
            return showSignedOutView()
        }

        do {
            let viewModel = try ExtensionViewModel(
                serviceIdentifiers: serviceIdentifiers,
                credentialHandler: credentialHandler,
                tokenProvider: authState
            )

            let listViewModel = ItemListViewModel(
                crypto: Crypto(keyPair: authState.keyPair, legacyKeyPair: authState.legacyKeyPair),
                tokenProvider: authState
            )

            let quickCreateViewModel = QuickCreateViewModel(
                domain: viewModel.domain,
                keyPair: authState.keyPair,
                legacyKeyPair: authState.legacyKeyPair,
                tokenProvider: authState
            )

            biometricViewModel.lockIfEnabled()

            showSwiftUIView(
                ExtensionRootView()
                    .environmentObject(viewModel)
                    .environmentObject(listViewModel)
                    .environmentObject(AuthStateWrapper(authState: authState))
                    .environmentObject(quickCreateViewModel)
                    .environmentObject(biometricViewModel)
            )
        } catch {
            SentrySDK.addSimpleBreadcrumb("Failed to init ExtensionViewModel")
            SentrySDK.capture(error: error)
            showErrorView(error: error)
        }
    }

    /*
     Implement this method if your extension supports showing credentials in the QuickType bar.
     When the user selects a credential from your app, this method will be called with the
     ASPasswordCredentialIdentity your app has previously saved to the ASCredentialIdentityStore.
     Provide the password by completing the extension request with the associated ASPasswordCredential.
     If using the credential would require showing custom UI for authenticating the user, cancel
     the request with error code ASExtensionError.userInteractionRequired.
    */
    override func provideCredentialWithoutUserInteraction(for credentialIdentity: ASPasswordCredentialIdentity) {
        guard isSignedIn, let client else {
            return credentialHandler.cancelRequest(withError: .userInteractionRequired)
        }

        guard let recordIdentifier = credentialIdentity.recordIdentifier,
              let itemId = UUID(uuidString: recordIdentifier) else {
            return credentialHandler.cancelRequest(withError: .failed)
        }

        Task {
            do {
                let item = try await client.getItem(id: itemId)
                return credentialHandler.completeRequest(withItem: item)
            } catch {
                // In case the error was caused by a network connection error, we should try to return
                // the item from the cache. If it's not a connection error, then the cache will probably also
                // not return an item, which is why we handle the error after that.
                if let cachedItem = findCachedItem(forItemId: itemId) {
                    return credentialHandler.completeRequest(withItem: cachedItem)
                } else {
                    // Otherwise report error and cancel with failure
                    SentrySDK.addSimpleBreadcrumb("provideCredentialWithoutUserInteraction failed")
                    SentrySDK.capture(error: error)
                    return credentialHandler.cancelRequest(withError: .failed)
                }
            }
        }
    }

    private func findCachedItem(forItemId itemId: UUID) -> Item? {
        if let cachedItems = try? ItemsCache().getCachedItems(),
           let item = cachedItems.first(where: { $0.id == itemId }) {
            return item
        }
        return nil
    }

    /*
     Implement this method if provideCredentialWithoutUserInteraction(for:) can fail with
     ASExtensionError.userInteractionRequired. In this case, the system may present your extension's
     UI and call this method. Show appropriate UI for authenticating the user then provide the password
     by completing the extension request with the associated ASPasswordCredential.
     */
    override func prepareInterfaceToProvideCredential(for credentialIdentity: ASPasswordCredentialIdentity) {
        // It should already be in signed out state at this point, but just to be sure.
        showSignedOutViewIfNecessary()
    }

    /// The system calls this method after the user enables your extension in Settings.
    override func prepareInterfaceForExtensionConfiguration() {
        let enabledView = AutoFillEnabledView(extensionContext: extensionContext)

        let viewController = UIHostingController(rootView: enabledView)
        // Enforce light mode
        viewController.view.backgroundColor = .white
        // Make sure the modal completely overlaps this viewcontroller
        viewController.modalPresentationStyle = .fullScreen

        present(viewController, animated: false)
    }

    func showSwiftUIView<V: View>(_ view: V) {
        let vc = UIHostingController(rootView: view)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false)
    }

    func showErrorView(error: Error) {
        showSwiftUIView(
            RootErrorView(
                error: error,
                userCancelled: { [weak self] in
                    self?.credentialHandler.cancelRequest(withError: .failed)
                }
            )
        )
    }
}
