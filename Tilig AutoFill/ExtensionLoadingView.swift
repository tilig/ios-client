//
//  ExtensionLoadingView.swift
//  Tilig Autofill
//
//  Created by Gertjan Jansen on 21/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct ExtensionLoadingView: View {
    var body: some View {
        VStack {
            ActivityIndicator()
                .navigationBarBranded(withTitle: "Loading...")
        }
    }
}

#if DEBUG
struct ExtensionLoadingView_Previews: PreviewProvider {
    static var previews: some View {
        ExtensionLoadingView()
    }
}
#endif
