//
//  BrandedNavigationBar.swift
//  Tilig Autofill
//
//  Created by Gertjan Jansen on 20/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct BrandedNavigationBarModifier: ViewModifier {

    init() {
        AppearanceTweaks().installForExtension()
    }

    func body(content: Content) -> some View {
        ZStack {
            content
            VStack {
                GeometryReader { geometry in
                    Image("extension_header")
                        .resizable()
                        .frame(height: geometry.safeAreaInsets.top)
                        .edgesIgnoringSafeArea(.top)
                    Spacer()
                }
            }
        }
    }
}

struct NavigationBarTitleModifier: ViewModifier {
    var title: String

    init(title: String) {
        self.title = title
    }

    func body(content: Content) -> some View {
        content
            .navigationTitle(title)
            .navigationBarTitleDisplayMode(.inline)
    }
}

extension View {
    func navigationBarBranded() -> some View {
        self.modifier(BrandedNavigationBarModifier())
    }

    func navigationBarBranded(withTitle title: String) -> some View {
        self.modifier(BrandedNavigationBarModifier())
            .modifier(NavigationBarTitleModifier(title: title))
    }
}
