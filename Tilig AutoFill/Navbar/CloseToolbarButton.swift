//
//  CloseToolbarItem.swift
//  Tilig Autofill
//
//  Created by Gertjan Jansen on 20/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI
import AuthenticationServices

struct CloseToolbarButton: View {
    var onClose: () -> Void

    var body: some View {
        Button(action: onClose) {
            Label("Add login", systemImage: "xmark")
                .foregroundColor(.white)
        }
    }
}
