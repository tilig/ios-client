//
//  ExtensionErrorView.swift
//  Tilig Autofill
//
//  Created by Gertjan Jansen on 21/05/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import SwiftUI

struct ExtensionErrorView: View {
    @EnvironmentObject var viewModel: ExtensionViewModel

    var message: String {
        viewModel.error?.localizedDescription ?? "Unknown error"
    }

    var body: some View {
        ExtensionMessageView(
            text: Text("An error occured"),
            message: message,
            userCancelled: { viewModel.cancelRequest(withError: .userCanceled) }
        )
        .navigationBarBranded(withTitle: "Oops...")
    }
}

// struct ExtensionErrorView_Previews: PreviewProvider {
//    static var previews: some View {
//        ExtensionErrorView()
//    }
// }
