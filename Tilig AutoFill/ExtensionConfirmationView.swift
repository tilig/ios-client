//
//  ExtensionConfirmationView.swift
//  Tilig Autofill
//
//  Created by Fitzgerald Afful on 03/03/2022.
//  Copyright © 2022 SubsLLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI
import AuthenticationServices
import os.log

struct ExtensionConfirmationView: View {
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var viewModel: ExtensionViewModel
    @EnvironmentObject var authState: AuthStateWrapper
    var item: Item

    // This constant is used to calculate screens with height higher/lower to adjust subviews' paddings
    private var minimumScreenHeight: CGFloat { 700 }
    @State private var phishingWarningAccepted = false

    func dismiss() {
        self.presentationMode.wrappedValue.dismiss()
    }

    var overview: DecryptedOverview? {
        do {
            let crypto = Crypto(
                keyPair: authState.keyPair,
                legacyKeyPair: authState.legacyKeyPair
            )
            return try crypto.decryptOverview(encryptedItem: item)
        } catch {
            Log("Error decrypting: \(error)")
            error.raiseGlobalAlert()
        }
        return nil
    }

    var text: Text {
        Text("Are you sure?\n")
            .largeBoldFont(color: .blue, size: 24) +
        Text("\nYou are about to login with details from\n another item.")
            .subTextFont(color: .blue, size: 14)
    }

    var website: String {
        overview?.urls.first?.url?.absoluteString ?? overview?.name ?? ""
    }

    var body: some View {
        GeometryReader { geometry in
            VStack {
                VStack(alignment: .center, spacing: 0) {

                    Image("add_wrong_item")
                        .resizable()
                        .scaledToFit()
                        .frame(maxHeight: 140)

                    text
                        .multilineTextAlignment(.center)
                        .minimumScaleFactor(0.2)
                        .padding(.bottom, geometry.size.height < minimumScreenHeight ? 25 : 50)

                    if let domain = viewModel.domain {
                        ExtensionWebsiteView(
                            label: "You're currently visiting",
                            text: domain,
                            iconURL: viewModel.domainBrand?.iconURL
                        )
                        .padding(.bottom, geometry.size.height < minimumScreenHeight ? 20 : 40)
                    }

                    ExtensionWebsiteView(
                        label: "You're entering a password from",
                        text: website,
                        iconURL: item.iconURL,
                        backgroundColor: .extensionLightGreen,
                        borderColor: .extensionBorderGreen
                    )
                    .padding(.bottom, geometry.size.height < minimumScreenHeight ? 20 : 40)

                    Spacer()
                        .frame(height: geometry.size.height < minimumScreenHeight ? 20 : 40)

                }
                .padding()

                DefaultButton(
                    title: "Use password from \(overview?.name ?? "Untitled")",
                    action: {
                        viewModel.completeRequest(withItem: item)
                        TrackingEvent.phishingWarningAccepted.send()
                        phishingWarningAccepted = true
                    }
                )
                .padding(16)
                .frame(height: 50)
            }
        }
        .onAppear {
            TrackingEvent.phishingWarningAppeared.send()
            Task { await viewModel.fetchDomainBrand() }
        }
        .onDisappear {
            if !phishingWarningAccepted {
                TrackingEvent.phishingWarningCanceled.send()
            }
        }
        .navigationBarBranded(withTitle: "Are you sure?")
        .globalErrorHandler()
    }
}
